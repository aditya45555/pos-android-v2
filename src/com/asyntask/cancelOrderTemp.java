package com.asyntask;

import java.util.HashMap;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.pos.Api;
import com.pos.Catalogue;
import com.pos.Chilkat;
import com.pos.Db;

public class cancelOrderTemp extends AsyncTask<String, Integer, Long> {
    HashMap<String, String> item = new HashMap<String, String>();
    Dialog dialogloading;
	public cancelOrderTemp(HashMap<String, String> map) {
	   this.item = map;    
	 }
	
	@Override
    protected void onPreExecute() { 
		dialogloading = ProgressDialog.show(Catalogue.getContext(), "", "Loading");	
	}

    @Override
    protected Long doInBackground(String... urls) {
    	if(Api.getCheckConnection().toUpperCase().equals("SUCCESS")){
    		Api.deleteOrderTemp(item.get("sales_detail_id"));
		}
		else{
			Db.cancelOrderTemp(item.get("sales_id"), item.get("sales_detail_id"), item.get("id"));
		//	Toast.makeText(Catalogue.getContext(), "Periksa Kembali Koneksi Internet Anda", Toast.LENGTH_LONG).show();
		}
     return null;
    }
    
    @Override
    protected void onPostExecute(Long result) {
    	Catalogue.getInstance().setDataOrderMenu();
    	dialogloading.dismiss();	
    }
    
}
