package com.asyntask;

import java.util.ArrayList;
import java.util.HashMap;

import com.dialog.DialogPayment;
import com.pos.Api;
import com.pos.Catalogue;
import com.pos.Chilkat;
import com.pos.Db;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.widget.TextView;

public class bayarDialog extends AsyncTask<String, Integer, Long> {
	private ArrayList<HashMap<String, String>> itemListPay = new ArrayList<HashMap<String, String>>();
	private String sales_id;
	private String table_id;
	private Dialog dialogloading;
	private TextView txt_total_harga;
	private Double Total;
	private FragmentManager fragmentmanager;
	private DialogPayment newFragment;
	private Bundle bundle;
	
	public bayarDialog(ArrayList<HashMap<String, String>> itemListPay,String sales_id,String table_id,TextView txt_total_harga,Double Total,FragmentManager fragmentmanager) {
	   this.itemListPay = itemListPay;
	   this.sales_id = sales_id;
	   this.table_id = table_id;
	   this.txt_total_harga = txt_total_harga;
	   this.Total = Total;
	   this.fragmentmanager = fragmentmanager;
	}
	
	@Override
    protected void onPreExecute() { 
		dialogloading = ProgressDialog.show(Catalogue.getContext(), "", "Loading");	
	}

    @Override
    protected Long doInBackground(String... urls) {
    	ArrayList<HashMap<String, String>> dataOrderConfirm = new ArrayList<HashMap<String,String>>();
    	if(Api.getCheckConnection().toUpperCase().equals("SUCCESS")){
    		dataOrderConfirm = Api.getOrderConfirm(sales_id);
    	}
    	else{
    		dataOrderConfirm = Db.getOrderConfirm(sales_id);
    	}
		newFragment = new DialogPayment(txt_total_harga, Total, dataOrderConfirm, itemListPay,table_id);
		bundle = new Bundle();
		bundle.putInt("position", 0);	
     return null;
    }
    
    @Override
    protected void onPostExecute(Long result) {
    	newFragment.setArguments(bundle);
		newFragment.show(fragmentmanager, "dialogpay");
    	dialogloading.dismiss();	
    }
	
}
