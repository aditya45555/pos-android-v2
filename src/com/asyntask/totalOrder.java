package com.asyntask;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.lib.Helper;
import com.pos.Api;
import com.pos.Catalogue;
import com.pos.Chilkat;
import com.pos.Db;

public class totalOrder extends AsyncTask<String, Integer, Long> {
    private HashMap<String, String> map = new HashMap<String, String>();
    private ArrayList<HashMap<String, String>> dataArrayOrderTemp = new ArrayList<HashMap<String, String>>();
    private ArrayList<HashMap<String, String>> dataArrayOrderConfirm = new ArrayList<HashMap<String, String>>();
   
    private Dialog dialogloading;
    private String psalesid;
    private Context context;
  
    
	public totalOrder(String sales_id,Context context) {
	   this.psalesid=psalesid;
	   this.context = context;
	 
	 }
	
	@Override
    protected void onPreExecute() { 
		dialogloading = ProgressDialog.show(Catalogue.getContext(), "", "Loading");	
	}

    @Override
    protected Long doInBackground(String... urls) {
    	if(Api.getCheckConnection().toUpperCase().equals("SUCCESS")){
			dataArrayOrderConfirm = Api.getOrderConfirm(psalesid);
			dataArrayOrderTemp = Api.getOrderTemp(psalesid);
		}
		else{
			if(Helper.checkCashierPrimary(context)){
				dataArrayOrderConfirm = Db.getOrderConfirm(psalesid);
				dataArrayOrderTemp = Db.getOrderTemp(psalesid);
			}
		}
    	Catalogue.getInstance().dataArrayOrderConfirm = dataArrayOrderConfirm;
    	Catalogue.getInstance().dataArrayOrderNotConfirm = dataArrayOrderTemp;
	return null;
    }
    
    @Override
    protected void onPostExecute(Long result) {
    	
    	
    	dialogloading.dismiss();		
    }
    
    
   }
