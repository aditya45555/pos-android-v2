package com.asyntask;

import java.util.HashMap;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.pos.Api;
import com.pos.Catalogue;
import com.pos.Chilkat;

public class closeStore extends AsyncTask<String, Integer, Long> {
    android.app.AlertDialog alertd;
    Dialog dialogloading;
    boolean close = false;
    Context context;
	public closeStore(android.app.AlertDialog alertd,Context context) {
	   this.alertd = alertd;
	   this.context = context;
	 }
	
	@Override
    protected void onPreExecute() { 
		dialogloading = ProgressDialog.show(context, "", "Loading");	
	}

    @Override
    protected Long doInBackground(String... urls) {
    	if(Api.getCheckConnection().toUpperCase().equals("SUCCESS")){
    		if(Api.closeStore()){
    			close = true;
    		}
		}
		else{
			close=false;
			
		}
     return null;
    }
    
    @Override
    protected void onPostExecute(Long result) {
    	if(close == true){
    		Api.setTypeApp(null);
		    ((Activity)(context)).finish();	
    		alertd.cancel();
        	dialogloading.dismiss();
    	}
    	else{
    		Toast.makeText(context, "Gagal Tutup Toko,Coba Kembali !", Toast.LENGTH_LONG).show();
    	}
    	
    }
    
}
