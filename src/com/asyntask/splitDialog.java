package com.asyntask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.adapter.ListConfirmAdapterSplit;
import com.adapter.TableConfirmAdapter;
import com.json.TableJson;
import com.pos.Api;
import com.pos.Catalogue;
import com.pos.Chilkat;
import com.pos.Db;
import com.pos.R;

public class splitDialog extends AsyncTask<String, Integer, Long> {
   private AlertDialog alertd;
   private Dialog dialogloading;
   private String sales_id;
   private ListView listviewconfirm;
   private Context context;
   private Adapter adapterconfirm;
   private ArrayList<HashMap<String, String>> dataOrder;
   private ArrayList<HashMap<String, String>> dataTable;
   private List<String>listtable;
   private ArrayAdapter<String>adapterspinner;
   private Spinner spiner;
   public splitDialog(Context context,AlertDialog alertd,String sales_id,ListView listviewconfirm,Spinner spiner) {
	   this.alertd = alertd;
	   this.sales_id = sales_id;
	   this.listviewconfirm = listviewconfirm;
	   this.context = context;
	   this.spiner = spiner;
	 }
	
	@Override
    protected void onPreExecute() { 
		dialogloading = ProgressDialog.show(Catalogue.getContext(), "", "Loading");	
	}

    @Override
    protected Long doInBackground(String... urls) {
    	if(Api.getCheckConnection().toUpperCase().equals("SUCCESS")){
    		dataOrder = Api.getOrderConfirm(sales_id);
			adapterconfirm = new ListConfirmAdapterSplit(context, R.layout.header, R.layout.list_menu_confirm_split,dataOrder);
			dataTable = Api.getAvailableTable();
			final List<String>listtable = new ArrayList<String>();
			
			for (int i = 0; i < dataTable.size(); i++) {
				String table_no = dataTable.get(i).get("no");
				listtable.add(table_no);
			}
			adapterspinner = new ArrayAdapter<String>(context, R.layout.spiner_table,R.id.txtnotable,listtable);
		}
		else{
		//	Toast.makeText(Catalogue.getContext(), "Periksa Kembali Koneksi Internet Anda", Toast.LENGTH_LONG).show();
			dataOrder = Db.getOrderConfirm(sales_id);
			adapterconfirm = new ListConfirmAdapterSplit(context, R.layout.header, R.layout.list_menu_confirm_split,dataOrder);
			dataTable = TableJson.getAvailableTable();
			final List<String>listtable = new ArrayList<String>();
			for (int i = 0; i < dataTable.size(); i++) {
				String table_no = dataTable.get(i).get("no");
				listtable.add(table_no);
			}
			adapterspinner = new ArrayAdapter<String>(context, R.layout.spiner_table,R.id.txtnotable,listtable);
		}
     return null;
    }
    
    @Override
    protected void onPostExecute(Long result) {
    	adapterspinner.notifyDataSetChanged();
    	listviewconfirm.setAdapter((ListAdapter) adapterconfirm);
    	spiner.setAdapter(adapterspinner);
    	Catalogue.getInstance().dataOrderConfirm = dataOrder;
    	Catalogue.getInstance().dataTable = dataTable;
    	alertd.show();
    	dialogloading.dismiss();
    }
    
}
