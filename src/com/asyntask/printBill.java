package com.asyntask;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Adapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.adapter.TableConfirmAdapter;
import com.dialog.DialogTip;
import com.lib.Print;
import com.pos.Api;
import com.pos.Catalogue;
import com.pos.Chilkat;
import com.pos.Main;
import com.pos.R;

public class printBill extends AsyncTask<String, Integer, Long> {
 
   private Dialog dialogloading;
   private String sales_id;
   private String printOrder;
   private ArrayList<HashMap<String, String>> dataConfirm;
   private Context context;
   
   public printBill(String sales_id,Context context) {
	   this.sales_id = sales_id;
	   this.context = context;
   }
	
	@Override
    protected void onPreExecute() { 
		dialogloading = ProgressDialog.show(Catalogue.getContext(), "", "Loading");	
	}

    @Override
    protected Long doInBackground(String... urls) {
    	if(Api.getCheckConnection().toUpperCase().equals("SUCCESS")){
    		dataConfirm =  Api.getOrderConfirm(sales_id);
    		if(dataConfirm.size()>0){
    			Catalogue.getInstance().printBilll(dataConfirm);
    		}
		}
		else{
		//	Toast.makeText(Catalogue.getContext(), "Periksa Kembali Koneksi Internet Anda", Toast.LENGTH_LONG).show();
		}
     return null;
    }
    
    @Override
    protected void onPostExecute(Long result) {
	if(dataConfirm.size()>0){
		Catalogue.getInstance().closePrint();
		dialogloading.dismiss();	
	}
	else{
		Toast.makeText(context, "Tidak Ada Data Yang Bisa Di Print", Toast.LENGTH_LONG).show();
		dialogloading.dismiss();
	}
    }
    
}
