package com.asyntask;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.widget.Adapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.adapter.TableConfirmAdapter;
import com.pos.Api;
import com.pos.Catalogue;
import com.pos.Chilkat;
import com.pos.Db;
import com.pos.R;

public class mergeDialog extends AsyncTask<String, Integer, Long> {
   private AlertDialog alertd;
   private Dialog dialogloading;
   private String sales_id;
   private ListView listviewconfirm;
   private Context context;
   private Adapter adapterconfirm;
   private ArrayList<HashMap<String, String>> dataOrder;
   
   public mergeDialog(Context context,AlertDialog alertd,String sales_id,ListView listviewconfirm,ArrayList<HashMap<String, String>> dataOrder) {
	   this.alertd = alertd;
	   this.sales_id = sales_id;
	   this.listviewconfirm = listviewconfirm;
	   this.context = context;
	   this.dataOrder = dataOrder;
	 }
	
	@Override
    protected void onPreExecute() { 
		dialogloading = ProgressDialog.show(Catalogue.getContext(), "", "Loading");	
	}

    @Override
    protected Long doInBackground(String... urls) {
    	if(Api.getCheckConnection().toUpperCase().equals("SUCCESS")){
    		dataOrder = Api.getListOrder(sales_id);
			adapterconfirm = new TableConfirmAdapter(context, R.layout.header, R.layout.list_table_confirm,dataOrder);
		}
		else{
			dataOrder = Db.getListOrder(sales_id);
			adapterconfirm = new TableConfirmAdapter(context, R.layout.header, R.layout.list_table_confirm,dataOrder);
		//	Toast.makeText(Catalogue.getContext(), "Periksa Kembali Koneksi Internet Anda", Toast.LENGTH_LONG).show();
		}
     return null;
    }
    
    @Override
    protected void onPostExecute(Long result) {
    	listviewconfirm.setAdapter((ListAdapter) adapterconfirm);
    	Catalogue.getInstance().dataOrder = dataOrder;
    	alertd.show();
    	dialogloading.dismiss();	
    }
    
}
