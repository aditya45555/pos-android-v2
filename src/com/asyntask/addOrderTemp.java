package com.asyntask;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.lib.Helper;
import com.pos.Api;
import com.pos.Catalogue;
import com.pos.Chilkat;
import com.pos.Db;

public class addOrderTemp extends AsyncTask<String, Integer, Long> {
    private HashMap<String, String> map = new HashMap<String, String>();
    private ArrayList<HashMap<String, String>> dataArrayOrderMenu = new ArrayList<HashMap<String, String>>();
    private ArrayList<HashMap<String, String>> dataArrayStatus = new ArrayList<HashMap<String, String>>();
    private ArrayList<HashMap<String, String>> dataArrayOrderTemp = new ArrayList<HashMap<String, String>>();
    private ArrayList<HashMap<String, String>> dataArrayOrderConfirm = new ArrayList<HashMap<String, String>>();
    
    private HashMap<String, String>mapchart = new HashMap<String, String>();
    private Dialog dialogloading;
    private String psalesid;
    private String ptableId;
    private Context context;
	public addOrderTemp(Context context,ArrayList<HashMap<String, String>> dataArrayOrderTemp, ArrayList<HashMap<String, String>> dataArrayOrderConfirm, HashMap<String, String> map,String psalesid,String ptableid) {
	   this.map = map;
	   this.psalesid=psalesid;
	   this.dataArrayOrderTemp = dataArrayOrderTemp;
	   this.dataArrayOrderConfirm = dataArrayOrderConfirm;
	   this.ptableId = ptableid;
	   this.context = context;
	 }
	
	@Override
    protected void onPreExecute() { 
		dialogloading = ProgressDialog.show(Catalogue.getContext(), "", "Loading");	
	}

    @Override
    protected Long doInBackground(String... urls) {
    	dataArrayOrderMenu.clear();
    	dataArrayStatus.clear();
    	mapchart.put("sales_id", psalesid);
    	mapchart.put("id", map.get("menu_id"));
    	mapchart.put("name", map.get("name"));
    	mapchart.put("price", map.get("price"));
		mapchart.put("quantity", String.valueOf(1));
		mapchart.put("discount", "0");
		mapchart.put("status", "notconfirm");
		mapchart.put("menu_type", map.get("menu_type"));
		dataArrayOrderMenu.add(mapchart);
		Db.setOrderTemp(psalesid,map.get("menu_id"), dataArrayOrderMenu);
		
	return null;
    }
    
    @Override
    protected void onPostExecute(Long result) {
    	try {
			Thread.sleep(500);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Catalogue.getInstance().setDataOrderMenu();
		dialogloading.dismiss();
		Toast.makeText(context, "Menu Berhasil Di Tambahkan", Toast.LENGTH_SHORT).show();
    }
    
    
   }
