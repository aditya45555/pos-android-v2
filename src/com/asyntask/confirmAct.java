package com.asyntask;

import java.util.HashMap;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.widget.Toast;

import com.pos.Api;
import com.pos.Catalogue;
import com.pos.Chilkat;
import com.pos.Db;

public class confirmAct extends AsyncTask<String, Integer, Long> {
    AlertDialog alertd;
    Dialog dialogloading;
    String sales_id;
	public confirmAct(AlertDialog alertd,String sales_id) {
	   this.alertd = alertd;
	   this.sales_id = sales_id;
	 }
	
	@Override
    protected void onPreExecute() { 
		dialogloading = ProgressDialog.show(Catalogue.getContext(), "", "Loading");	
	}

    @Override
    protected Long doInBackground(String... urls) {
    	Db.confirmOrder(sales_id);
     return null;
    }
    
    @Override
    protected void onPostExecute(Long result) {
    	try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	Catalogue.getInstance().setDataOrderMenu();
    	alertd.cancel();
    	dialogloading.dismiss();
    	Catalogue.getInstance().pager_chart.setCurrentItem(1);
    }
    
}
