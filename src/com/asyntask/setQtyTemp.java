package com.asyntask;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.widget.TextView;

import com.pos.Api;
import com.pos.Catalogue;
import com.pos.Chilkat;

public class setQtyTemp extends AsyncTask<String, Integer, Long> {
    HashMap<String, String> item = new HashMap<String, String>();
    TextView txt_qty;
    AlertDialog alertd;
	Dialog dialogloading;
    public setQtyTemp(HashMap<String, String> map,TextView txt_qty,AlertDialog alertd) {
	   this.item = map;
	   this.txt_qty = txt_qty;
	   this.alertd = alertd;
	 }
	
	@Override
    protected void onPreExecute() { 
		dialogloading = ProgressDialog.show(Catalogue.getContext(), "", "Loading");	
	}

    @Override
    protected Long doInBackground(String... urls) {
    	if(Api.getCheckConnection().toUpperCase().equals("SUCCESS")){
    		int lenghthtxtqty = txt_qty.getText().length();
         	if(lenghthtxtqty>0 && txt_qty.getText().toString()!="0"){
             	String quantity = txt_qty.getText().toString();
             	ArrayList<HashMap<String, String>> dataOrderQty = new ArrayList<HashMap<String,String>>();
             	HashMap<String, String>  map = new HashMap<String, String>();
				map.put("id", item.get("id"));
				map.put("quantity", quantity);
				dataOrderQty.add(map);
             	String resultapi = Api.setOrderTempQuantity(item.get("sales_id"), dataOrderQty);
         	}
		}
		else{
		//	Toast.makeText(Catalogue.getContext(), "Periksa Kembali Koneksi Internet Anda", Toast.LENGTH_LONG).show();
		}
     return null;
    }
    
    @Override
    protected void onPostExecute(Long result) {
    	Catalogue.getInstance().setDataOrderMenu();
    	alertd.cancel();
    	dialogloading.dismiss();	
    }
    
}
   
