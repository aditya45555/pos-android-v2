package com.asyntask;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Adapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.adapter.TableConfirmAdapter;
import com.json.TableJson;
import com.pos.Api;
import com.pos.Catalogue;
import com.pos.CatalogueFragment;
import com.pos.Chilkat;
import com.pos.Db;
import com.pos.R;

public class newOrder extends AsyncTask<String, Integer, Long> {
   private AlertDialog alertd;
   private Dialog dialogloading;
   private String descOrder,table_id,table_no;
   private String apineworder;
   private ArrayList<HashMap<String, String>> dataArrayMember;
   private Context con;
   public newOrder(Context con,String table_no,String descOrder,String table_id,ArrayList<HashMap<String, String>> dataArrayMember) {
	   this.alertd = alertd;
	   this.table_id = table_id;
	   this.descOrder = descOrder;
	   this.table_no = table_no;
	   this.dataArrayMember = dataArrayMember;
	   this.con = con;
	 }
	
	@Override
    protected void onPreExecute() { 
		dialogloading = ProgressDialog.show(con, "", "Loading");	
	}

    @Override
    protected Long doInBackground(String... urls) {
    	//apineworder = Api.setOrder(table_id, descOrder);
    	if(!table_no.toUpperCase().equals("NONE")){
    		apineworder = Db.setOrder("",table_id,table_no,descOrder,dataArrayMember.get(0).get("member_id"),dataArrayMember.get(0).get("member_name"),dataArrayMember.get(0).get("member_email"),dataArrayMember.get(0).get("member_phone"));
    	}
    	else{
    		apineworder = Db.setOrderQueue(table_id, table_no,"", "","", "", "");
    	}
		
     return null;
    }
    
    @Override
    protected void onPostExecute(Long result) {
    	if(!apineworder.toUpperCase().equals("FAILED")){
			try {
				JSONObject jo = new JSONObject(apineworder);
				String sales_id = jo.getString("sales_id");
				String queue = jo.getString("queue");
				String statusTable = "not available";
				TableJson.setUpdateTables(sales_id,table_id, statusTable,dataArrayMember,new ArrayList<HashMap<String,String>>());
				Intent i = new Intent(con, Catalogue.class);
                Bundle b = new Bundle();
                b.putString("sales_id",sales_id);
                b.putString("table_id",table_id);
                b.putString("table_no",table_no);
                b.putSerializable("member", dataArrayMember);
                b.putString("queue",queue);
                i.putExtras(b);
    			((Activity) con).overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
                con.startActivity(i);
                ((Activity) con).finish();
			} catch (JSONException e) {
				// TODO: handle exception
			}
		}
    	dialogloading.dismiss();
    }
    
}
