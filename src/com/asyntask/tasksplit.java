package com.asyntask;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.adapter.ListConfirmAdapterSplit;
import com.adapter.TableConfirmAdapter;
import com.json.TableJson;
import com.pos.Api;
import com.pos.Catalogue;
import com.pos.Chilkat;
import com.pos.Db;
import com.pos.R;

public class tasksplit extends AsyncTask<String, Integer, Long> {
  private String sales_id;
  private String id;
  private ArrayList<HashMap<String, String>>dataArrayMenu;
   public tasksplit(String sales_id,String id,ArrayList<HashMap<String, String>>dataArrayMenu) {
	   this.sales_id = sales_id;
	   this.id = id;
	   this.dataArrayMenu = dataArrayMenu;
	   
   }
	
	@Override
    protected void onPreExecute() { 
		Db.setOrderTemp(sales_id, id, dataArrayMenu);
	}

    @Override
    protected Long doInBackground(String... urls) {
     return null;
    }
    
    @Override
    protected void onPostExecute(Long result) {
    	Db.confirmOrder(sales_id);
    }
    
}
