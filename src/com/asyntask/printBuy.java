package com.asyntask;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONObject;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Adapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.adapter.TableConfirmAdapter;
import com.dialog.DialogTip;
import com.json.TableJson;
import com.pos.Api;
import com.pos.Catalogue;
import com.pos.Chilkat;
import com.pos.Db;
import com.pos.Main;
import com.pos.R;

public class printBuy extends AsyncTask<String, Integer, Long> {
 
   private Dialog dialogloading;
   private JSONObject js;
   private String codetransaction;
   private Activity activity;
   private String sales_id;
   private String table_id;
   private String time_sales;
   
   public printBuy(String sales_id,String table_id,String time_sales,JSONObject js,Activity activity) {
	   this.js = js;
	   this.activity = activity;
	   this.sales_id = sales_id;
	   this.table_id = table_id;
	   this.time_sales = time_sales;
   }
	
	@Override
    protected void onPreExecute() { 
		dialogloading = ProgressDialog.show(Catalogue.getContext(), "", "Loading");	
	}

    @Override
    protected Long doInBackground(String... urls) {
		codetransaction = Db.sendDataJual(sales_id,table_id,time_sales,js);
		DialogTip.getinstance().codetransaction = codetransaction;
		if(Api.getActivePrinter()==true){
			DialogTip.getinstance().olahPrint();
			DialogTip.getinstance().openCd();
		}
     return null;
    }
    
    @Override
    protected void onPostExecute(Long result) {
	if(!codetransaction.toUpperCase().equals("FAILED")){
		if(Api.getActivePrinter() == true){
			DialogTip.getinstance().closePrint();
		}
		dialogloading.dismiss();	
		DialogTip.getinstance().codetransaction = codetransaction;
		Api.phash = "";
		ArrayList<HashMap<String, String>>datamember = new ArrayList<HashMap<String,String>>();
		TableJson.setUpdateTables("", table_id, "available", datamember,new ArrayList<HashMap<String,String>>());
		activity.startActivity(new Intent(activity,Main.class));
		activity.finish();
	}
    }
    
}
