package com.asyntask;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Adapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.adapter.TableConfirmAdapter;
import com.pos.Api;
import com.pos.Catalogue;
import com.pos.CatalogueFragment;
import com.pos.Chilkat;
import com.pos.Db;
import com.pos.R;

public class mergeAct extends AsyncTask<String, Integer, Long> {
   private AlertDialog alertd;
   private Dialog dialogloading;
   private String sales_id;
   private ArrayList<HashMap<String, String>> dataOrder;
   
   public mergeAct(AlertDialog alertd,String sales_id,ArrayList<HashMap<String, String>> dataOrder) {
	   this.alertd = alertd;
	   this.sales_id = sales_id;
	   this.dataOrder = dataOrder;
	 }
	
	@Override
    protected void onPreExecute() { 
		dialogloading = ProgressDialog.show(Catalogue.getContext(), "", "Loading");	
	}

    @Override
    protected Long doInBackground(String... urls) {
    	Db.setMergeTransaction(sales_id, dataOrder);
     return null;
    }
    
    @Override
    protected void onPostExecute(Long result) {
    	Catalogue.getInstance().setDataOrderMenu();
    	alertd.cancel();
    	dialogloading.dismiss();
    	Catalogue.getInstance().pager_chart.setCurrentItem(1);
    }
    
}
