package com.asyntask;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.widget.Adapter;
import android.widget.GridView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.adapter.ListConfirmAdapter;
import com.adapter.MenuAdapterList;
import com.json.MenuJson;
import com.json.PackageJson;
import com.lib.Helper;
import com.pos.Api;
import com.pos.Catalogue;
import com.pos.Chilkat;
import com.pos.R;

public class listMenu extends AsyncTask<String, Integer, Long> {
    private Dialog dialogloading;
    private GridView grid;
    private ArrayList<HashMap<String, String>> DataArrayMenu = new ArrayList<HashMap<String, String>>();
    private ArrayList<HashMap<String, String>> DataArrayOrderTemp = new ArrayList<HashMap<String, String>>();
    private ArrayList<HashMap<String, String>> DataArrayOrderConfirm = new ArrayList<HashMap<String, String>>();
    
    public static MenuAdapterList adapter;
    private String sales_id;
    private String catid;
    private Context context;
    private String menu_type;
    private static String search = "";
    private static listMenu lmenu;
    private String tableId;
	public listMenu(GridView grid,String sales_id,String cat_id,Context context,String menu_type,ArrayList<HashMap<String, String>> dataarrayordertemp,ArrayList<HashMap<String, String>> dataarrayorderconfirm,String ptableid) {
		this.grid = grid;
		this.sales_id = sales_id;
		this.catid = cat_id;
		this.context = context;
		this.menu_type = menu_type;
		this.DataArrayOrderTemp = dataarrayordertemp;
		this.DataArrayOrderConfirm = dataarrayorderconfirm;
		this.tableId = tableId;
		lmenu = this;
	}
	
	
	
	public static listMenu getInstance() {
		// TODO Auto-generated constructor stub
		return lmenu;
	}
	
	
	@Override
    protected void onPreExecute() { 
		dialogloading = ProgressDialog.show(Catalogue.getContext(), "", "Loading");	
	}

    @Override
    protected Long doInBackground(String... urls) {
    	DataArrayMenu = MenuJson.getByCategoryID(catid, menu_type);
     return null;
    }
    
    @Override
    protected void onPostExecute(Long result) {
    	adapter = new MenuAdapterList(
		context,
        R.layout.header, R.layout.list_item_menu, DataArrayMenu ,grid,sales_id,catid,menu_type, DataArrayOrderTemp, DataArrayOrderConfirm,tableId);
    	adapter.notifyDataSetChanged();
    	grid.setAdapter(adapter);
    	dialogloading.dismiss();
    }
    
}

