package com.asyntask;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.ContextWrapper;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;
import android.os.AsyncTask;
import android.util.Log;

import com.pos.Api;
import com.pos.Catalogue;
import com.pos.Chilkat;

public class saveImageFile extends AsyncTask<String, Integer, Long> {
	File ffile;
	String fname;
	String url;
	String ext;
	Context context;
	String subdir;
    Dialog dialogloading;
	public saveImageFile(File ffile,String fname,String url,String ext,Context context,String subdir) {
	  this.ffile = ffile;
	  this.fname = fname;
	  this.url = url;
	  this.ext = ext;
	  this.context = context;
	  this.subdir = subdir;
	 }
	@Override
    protected void onPreExecute() { 
	}

    @Override
    protected Long doInBackground(String... urls) {
    	File mySubDir = new File(ffile, subdir);
		if (!mySubDir.isDirectory())
			mySubDir.mkdir();
		 ContextWrapper cw = new ContextWrapper(context);
		 // Create imageDir
		 File mypath=new File(mySubDir.getAbsolutePath(),fname);
		 
			
		 FileOutputStream fos = null;
		 
        try {
        	
        	URL urlstream = new URL(url);
            HttpURLConnection connection = (HttpURLConnection) urlstream.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
    		Bitmap bitmapImage = BitmapFactory.decodeStream(input);
    		
            fos = new FileOutputStream(mypath);
            CompressFormat formatImage;
            if(ext.toLowerCase().equals("jpg")||ext.toLowerCase().equals("jpeg")){
            	formatImage = Bitmap.CompressFormat.JPEG;
            }
            else{
            	formatImage = Bitmap.CompressFormat.PNG;
            }
            
            bitmapImage.compress(formatImage, 100, fos);
        } catch (Exception e) {
             Log.i("ERROR-SAVE IMAGE", e.toString());
        } 
     return null;
    }
    
    @Override
    protected void onPostExecute(Long result) {
    	
    }
    
}
