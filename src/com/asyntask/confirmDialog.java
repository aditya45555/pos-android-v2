package com.asyntask;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.widget.Adapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.adapter.ListConfirmAdapter;
import com.lib.Helper;
import com.pos.Api;
import com.pos.Catalogue;
import com.pos.Chilkat;
import com.pos.Db;
import com.pos.R;

public class confirmDialog extends AsyncTask<String, Integer, Long> {
    Dialog dialogloading;
    AlertDialog alertd;
    String sales_id;
    ListView listviewconfirm;
    Context context;
    TextView txt_total_menu_confirm;
    TextView txt_total_harga_confirm;
    Adapter adapterconfirm;
    int harga=0,totalharga = 0,totalmenu = 0;
	public confirmDialog(AlertDialog alertd,String sales_id,ListView listviewconfirm,Context context,TextView txt_total_menu_confirm,TextView txt_total_harga_confirm) {
		this.alertd = alertd;
		this.sales_id = sales_id;
		this.listviewconfirm = listviewconfirm;
		this.context=context;
		this.txt_total_harga_confirm = txt_total_harga_confirm;
		this.txt_total_menu_confirm = txt_total_menu_confirm;
		
	}
	
	@Override
    protected void onPreExecute() { 
		dialogloading = ProgressDialog.show(Catalogue.getContext(), "", "Loading");	
	}

    @Override
    protected Long doInBackground(String... urls) {
    	if(Api.getCheckConnection().toUpperCase().equals("SUCCESS")){
			ArrayList<HashMap<String, String>> dataArrayOrderMenuConfirm = Api.getOrderTemp(sales_id);
			adapterconfirm = new ListConfirmAdapter(context, R.layout.header, R.layout.list_menu_confirm,dataArrayOrderMenuConfirm);
			ArrayList<HashMap<String, String>> dataArrayOrderNotConfirm = new ArrayList<HashMap<String, String>>();
			dataArrayOrderNotConfirm = Api.getOrderTemp(sales_id);
			for(int i = 0;i<dataArrayOrderNotConfirm.size();i++){
				harga = Integer.valueOf( dataArrayOrderNotConfirm.get(i).get("price"))*Integer.valueOf( dataArrayOrderNotConfirm.get(i).get("quantity"));
	 			totalmenu += Integer.valueOf( dataArrayOrderNotConfirm.get(i).get("quantity"));
	 			totalharga += harga;
			}
		}
		else{
			ArrayList<HashMap<String, String>> dataArrayOrderMenuConfirm = Db.getOrderTemp(sales_id);
			adapterconfirm = new ListConfirmAdapter(context, R.layout.header, R.layout.list_menu_confirm,dataArrayOrderMenuConfirm);
			ArrayList<HashMap<String, String>> dataArrayOrderNotConfirm = new ArrayList<HashMap<String, String>>();
			dataArrayOrderNotConfirm = Db.getOrderTemp(sales_id);
			for(int i = 0;i<dataArrayOrderNotConfirm.size();i++){
				harga = Integer.valueOf( dataArrayOrderNotConfirm.get(i).get("price"))*Integer.valueOf( dataArrayOrderNotConfirm.get(i).get("quantity"));
	 			totalmenu += Integer.valueOf( dataArrayOrderNotConfirm.get(i).get("quantity"));
	 			totalharga += harga;
			}
		}
     return null;
    }
    
    @Override
    protected void onPostExecute(Long result) {
    	listviewconfirm.setAdapter((ListAdapter) adapterconfirm);
    	dialogloading.dismiss();
    	txt_total_menu_confirm.setText(String.valueOf(totalmenu));
		txt_total_harga_confirm.setText(Helper.toRupiahFormat(String.valueOf(totalharga)));
    	alertd.show();
    	
    }
    
}

