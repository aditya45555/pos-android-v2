/*
 * Copyright (C) 2013 Andreas Stuetz <andreas.stuetz@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pos;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.adapter.ListChartAdapter;
import com.adapter.MemberAdapterList;
import com.adapter.MenuAdapterList;
import com.adapter.QueueAdapter;
import com.adapter.TableAdapter;
import com.asyntask.closeStore;
import com.asyntask.listMenu;
import com.asyntask.newOrder;
import com.json.CategoryJson;
import com.json.ListChartDataJson;
import com.json.MemberJson;
import com.json.MenuJson;
import com.json.PackageJson;
import com.json.TableJson;
import com.lib.FloatingActionButton;
import com.lib.Helper;
import com.pos.Catalogue.PagerAdapterChart;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelSlideListener;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ScrollingView;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

@SuppressLint("NewApi")
public class TableFragment extends Fragment {

	private static final String ARG_POSITION = "position";
	private static final String ARG_TYPE = "type";
	private ArrayList<HashMap<String, String>> dataArrayOrderMenu = new ArrayList<HashMap<String, String>>(); 
	private ArrayList<HashMap<String, String>> dataArray = new ArrayList<HashMap<String, String>>();
	private ArrayList<HashMap<String, String>> dataArrayQuee = new ArrayList<HashMap<String, String>>();
	private ArrayList<HashMap<String, String>> dataArrayTempAll = new ArrayList<HashMap<String, String>>();
	private ArrayList<HashMap<String, String>> dataArrayTemp = new ArrayList<HashMap<String, String>>();
	private ArrayList<HashMap<String, String>> dataArrayTempQuee = new ArrayList<HashMap<String, String>>();
	private ArrayList<HashMap<String, String>> dataArrayList = new ArrayList<HashMap<String, String>>();
	private ArrayList<HashMap<String, String>> dataNotif = new ArrayList<HashMap<String, String>>();
	
	private static Context mContext;
	public static Handler mHandler,mHandlerConnection;
	private  initData task = null;
	private  initDataConnection taskConnection = null;
	
	public static TableAdapter adapter;
	private QueueAdapter adapterquee;
	private int position;
	private String type ;
	private GridView grid;
	private ListView lv;
	private ProgressBar pbar;
	private MemberAdapterList adapterlist;
	private LinearLayout layoutlistnotfound;
	private Button btnregister;
	private TextView txtregistername,txtregisterphone;
	private EditText edttxtregistername,edttxtregisterphone;
	private  AlertDialog alertdgparent;
	private AlertDialog.Builder alertDialogBuilderParent;
	private static TableFragment tablefragment;
	private int countallnotif=0;
	private int countnotif = 0;
	private Button btn_notif;
	private ArrayList<HashMap<String, String>> dataTable = new ArrayList<HashMap<String,String>>();
	private newOrder taskneworder;
	String memberid ="";
	String namemember = "";
	String phonemember = "";
	String emailmember ="";
	
	public static TableFragment newInstance(int position,String id) {
		TableFragment f = new TableFragment();
		Bundle b = new Bundle();
		b.putInt(ARG_POSITION, position);
		b.putString(ARG_TYPE, id);
		f.setArguments(b);
		return f;
	}
	
	public static TableFragment getInstance() {
		if (tablefragment == null)
			tablefragment = new TableFragment();
		return tablefragment;
	}
	
	public  void goToPosition(){
		
		if(dataNotif.size()>0){
			countnotif --;
			if(countnotif<0){
				countnotif = dataNotif.size() - 1;
			}
			
			grid.requestFocusFromTouch();
			grid.setSelection(Integer.valueOf(dataNotif.get(countnotif).get("notif")));
			
		}
		else{
			Toast.makeText(getActivity(), "Tidak Ada Pemberitahuan", Toast.LENGTH_LONG).show();
		}
		
	} 
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		position = getArguments().getInt(ARG_POSITION);
		type = getArguments().getString(ARG_TYPE);
		mHandler = new Handler();
		mHandlerConnection = new Handler();
		task = new initData();
		task.execute();
		
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.layout_grid_table, container, false);
		grid = (GridView) v.findViewById(R.id.grid);
		btn_notif = (Button) v.findViewById(R.id.btn_notif);
		grid.setFocusable(true);
		grid.requestFocus();
		if(position == 0){
			adapter = new TableAdapter(getActivity().getApplicationContext(), R.layout.header, R.layout.grid_table_item, dataArray);
			grid.setAdapter(adapter);
			
		}
		else{
			adapterquee = new QueueAdapter(getActivity().getApplicationContext(), R.layout.header, R.layout.grid_quee_item, dataArrayQuee);
			grid.setAdapter(adapterquee);
			btn_notif.setVisibility(View.GONE);
		}
		
		if(Helper.isTablet(getActivity())){
        	grid.setNumColumns(2);
        }
        else{
        	if(getResources().getConfiguration().ORIENTATION_LANDSCAPE==1){
        		grid.setNumColumns(2);
        	}
        	else{
        		grid.setNumColumns(1);
        	}
        	
        }
        
        grid.setOnItemClickListener(new OnItemClickListener() {
			@Override
			public void onItemClick(AdapterView<?> parent, View view,  int positionin, long id) {
				if(position == 0){
					dialogTable(positionin);
				}
				else{
					dialogQueue(positionin);
				}
				
			}
        });
        
       
    	btn_notif.setText(String.valueOf(countallnotif));
        btn_notif.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				goToPosition();
				
				}
    	   });
		return v;
	}
	
	private void dialogTable(final int position){
		final String tableNo = dataArray.get(position).get("no");
		final String tableId = dataArray.get(position).get("table_id");
		final String total_menu = dataArray.get(position).get("total_menu");
		String statusTable = dataArray.get(position).get("status");
		final ArrayList<HashMap<String, String>> dataArrayMember = new ArrayList<HashMap<String, String>>(); 
		final HashMap<String, String>mapDataMember = new HashMap<String, String>();
		mapDataMember.put("member_id", dataArray.get(position).get("member_id"));
		mapDataMember.put("member_name", dataArray.get(position).get("member_name"));
		mapDataMember.put("member_email", dataArray.get(position).get("member_email"));
		mapDataMember.put("member_phone", dataArray.get(position).get("member_phone"));
		dataArrayMember.add(mapDataMember);
		
		final String descOrder = null;
		int total_price = 0 ;
		
		if(dataArray.get(position).get("total_price").toUpperCase().equals("NULL")){
			total_price =  0;
		}
		else{
			total_price = Integer.valueOf(dataArray.get(position).get("total_price"));
		}
		
		String noQuee = null;
		if(statusTable.toUpperCase().equals("AVAILABLE")){
			final AlertDialog.Builder alertParent = new AlertDialog.Builder(getActivity());
		    alertParent.setTitle("Pemesanan");
		    alertParent.setMessage("Silahkan Pilih");
		    alertParent.setPositiveButton("Order Baru", new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int whichButton) {
		            //New Order Action
	        		taskneworder = new newOrder(getActivity(), tableNo, descOrder, tableId, dataArrayMember);
	        		taskneworder.execute();
		        }
		    });
		    if(!tableNo.toUpperCase().equals("NONE")){
			    alertParent.setNegativeButton("Booking",
			        new DialogInterface.OnClickListener() {
			            public void onClick(DialogInterface dialog, int whichButton) {
			            	//action booking
			            	LayoutInflater li = LayoutInflater.from(getActivity().getApplicationContext());
							View promptsView = li.inflate(R.layout.dialog_name_text, null);
			            	alertDialogBuilderParent = new AlertDialog.Builder(getActivity());
			            	alertDialogBuilderParent.setTitle("Masukan Nama Atau No Telepon");
			            	alertDialogBuilderParent.setView(promptsView);
			            	lv = (ListView)promptsView.findViewById(R.id.listmember);
			            	pbar = (ProgressBar)promptsView.findViewById(R.id.progressBar);
			            	layoutlistnotfound = (LinearLayout)promptsView.findViewById(R.id.layoutlistnotfound);
			            	btnregister = (Button)promptsView.findViewById(R.id.btnregister);
			            	pbar.setVisibility(View.GONE);
			            	layoutlistnotfound.setVisibility(View.GONE);
			            	final EditText txtname = (EditText)promptsView.findViewById(R.id.txtnamemember);
			            	final TextView txtmemberid = (TextView)promptsView.findViewById(R.id.txtmemberid);
			            	
			            	txtname.addTextChangedListener(new TextWatcher() {
								
								@Override
								public void onTextChanged(CharSequence s, int start, int before, int count) {
									// TODO Auto-generated method stub
									if(txtname.getText().length()>2){
										
										pbar.setVisibility(View.VISIBLE);
										layoutlistnotfound.setVisibility(View.GONE);
										dataArrayList.clear();
										dataArrayList = MemberJson.getByNameOrPhoneFile(txtname.getText().toString(),txtname.getText().toString());
										
										if(dataArrayList.size()>0){
						            		setListMember(null,null,position);
										}
						            	else{
						            		try {
						            			String datamember = Api.getMemberByName(txtname.getText().toString());
						            			if(!datamember.equals("failed")){
							            			JSONArray jo = new JSONArray(datamember);
								            		for(int i = 0;i<jo.length();i++){
								            			JSONObject jobj = jo.getJSONObject(i);
								            			MemberJson.add(jobj.toString(), jobj.getString("phone"),jobj.getString("name"));
								            		}
						            			}
							            		setListMember(tableId,tableNo,position);
						            		} catch (Exception e) {
												// TODO: handle exception
											}
						            	}
									}
								}
								@Override
								public void beforeTextChanged(CharSequence s, int start, int count,
										int after) {
									// TODO Auto-generated method stub
									
								}
								
								@Override
								public void afterTextChanged(Editable s) {
									// TODO Auto-generated method stub
									
								}
							});
			            	
			            	lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {

								@Override
								public void onItemClick(
										AdapterView<?> parent, View view,
										int position, long id) {
										// TODO Auto-generated method stub
										Log.i("DATA-MEMBER-JSON", "DATA-MEMBER-JSON = "+dataArrayList.get(position).get("member_id"));
										memberid = dataArrayList.get(position).get("member_id");
										namemember = dataArrayList.get(position).get("name");
										phonemember = dataArrayList.get(position).get("phone");
										emailmember = dataArrayList.get(position).get("email");
										
										txtname.setText(namemember);
										txtmemberid.setText(memberid);
										txtmemberid.setVisibility(View.GONE);
								}
							});
			            	
			            	// set dialog message
							alertDialogBuilderParent
								.setPositiveButton("OK",
								  new DialogInterface.OnClickListener() {
								    public void onClick(DialogInterface dialog,int id) {
								    	Log.i("MEMBER", "MEMBER = "+txtmemberid.getText().toString());
									// get user input and set it to result
								    	if(txtmemberid.getText().length()>0){
									    	String a =	Db.setBookTables(tableId,tableNo,phonemember,emailmember,namemember,memberid);
											ArrayList<HashMap<String, String>>datamember = new ArrayList<HashMap<String,String>>();
											mapDataMember.clear();
											mapDataMember.put("member_id", memberid);
											mapDataMember.put("member_name", namemember);
											mapDataMember.put("member_email", emailmember);
											mapDataMember.put("member_phone", phonemember);
											datamember.add(mapDataMember);
											TableJson.setUpdateTables("", tableId, "booked", dataArrayMember,new ArrayList<HashMap<String,String>>());
									    	task = new initData();
											task.execute();	
								    	}
								    	else{
								    		Toast.makeText(getActivity().getApplicationContext(), "Member Harus Di Pilih", Toast.LENGTH_LONG);
								    	}
								    }
								  })
								.setNegativeButton("Cancel",
								  new DialogInterface.OnClickListener() {
								    public void onClick(DialogInterface dialog,int id) {
									dialog.cancel();
								    }
								  });
								// create alert dialog
								alertdgparent = alertDialogBuilderParent.create();
								// show it
								alertdgparent.show();
			            }
			        });
		    }
		    else{
		    	
		    }
		    	alertParent.show();
		}
		else if(statusTable.toUpperCase().equals("BOOKED")){
			AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
		    alert.setTitle("Booking Meja");
		    alert.setMessage("Silahkan Pilih");
		    alert.setPositiveButton("Order Baru", new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int whichButton) {
		            //New Order Action
		        	//String result = Db.setOrder(tableId,tableNo,descOrder,dataArrayMember.get(0).get("member_id"),dataArrayMember.get(0).get("member_name"),dataArrayMember.get(0).get("member_email"),dataArrayMember.get(0).get("member_phone"));
					taskneworder = new newOrder(getActivity(), tableNo, descOrder, tableId, dataArrayMember);
		        	taskneworder.execute();
		        	/*if(!result.equals("FAILED")){
						try {
							JSONObject jo = new JSONObject(result);
							String sales_id = jo.getString("sales_id");
							String queue = jo.getString("queue");
							Intent i = new Intent(getActivity(), Catalogue.class);
		                    Bundle b = new Bundle();
		                    b.putString("sales_id",sales_id);
		                    b.putString("table_id",tableId);
		                    b.putString("table_no",tableNo);
		                    b.putSerializable("member", dataArrayMember);
		                    b.putString("queue",queue);
		                    i.putExtras(b);
		                    if(Api.tables!=null){
		                    	if(Api.tables.get(position).get("statusnotif")!=null){
		                    		Api.tables.get(position).put("total_menu",total_menu);
		                    		
		                    	}
		                    }      
		        			getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
		                    startActivity(i);
		                    getActivity().finish();
		               } catch (JSONException e) {
							// TODO: handle exception
						}
					}*/
		        }
		    });
		    alert.setNegativeButton("Cancel Booking",
		        new DialogInterface.OnClickListener() {
		            public void onClick(DialogInterface dialog, int whichButton) {
		            	//action booking
		            	Api.cancelBooking(tableId);
		            	dialog.cancel();
		            	task = new initData();
		            	task.execute();
		            }
		        });
		    	alert.show();
		}
		else{
			final String sales_id = dataArray.get(position).get("sales_id");
			final String status = dataArray.get(position).get("status");
			AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
			 alert.setTitle("Pemesanan");
			    alert.setMessage("Silahkan Pilih");
			    alert.setPositiveButton("Lihat", new DialogInterface.OnClickListener() {
			        public void onClick(DialogInterface dialog, int whichButton) {
			            //New Order Action   	
			        	ArrayList<HashMap<String, String>> dataorder = Api.getListOrderDetail(sales_id);
						if(dataorder==null){
							Toast.makeText(getActivity().getApplicationContext(), "Periksa Kembali Koneksi Anda", Toast.LENGTH_LONG).show();
						}
						else{
							Intent i = new Intent(getActivity(), Catalogue.class);
		                    Bundle b = new Bundle();
		                    b.putString("sales_id",sales_id);
		                    b.putString("table_id",tableId);
		                    b.putString("table_no",tableNo);
		                    b.putString("quee","0");
		                    b.putSerializable("member", dataArrayMember);
		                    b.putSerializable("dataorder",dataorder);
		                    i.putExtras(b);
		                    if(Api.tables!=null){
		                    	if(Api.tables.get(position).get("statusnotif")!=null){
		                    		Api.tables.get(position).put("total_menu",total_menu);
		                    		
		                    	}
		                    }      
		                    getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
		                    startActivity(i);
		                    getActivity().finish();
						}
			        }
			    });
			    
			    alert.setNeutralButton("Pindah Meja" ,
				        new DialogInterface.OnClickListener() {
				            public void onClick(final DialogInterface dialog, int whichButton) {
				            	//action booking
				            	AlertDialog.Builder alertdialog = new AlertDialog.Builder(getActivity());
				        		LayoutInflater li = LayoutInflater.from(getActivity());
				        		View promptsView = li.inflate(R.layout.dialog_setting_printerbt, null);
				        		ArrayAdapter<String>adapterspinner;
				        		alertdialog.setTitle("Pilih Meja");
				        	    alertdialog.setCancelable(true);
				        	    final List<String>listtable = new ArrayList<String>();
				        	    if(Api.getCheckConnection().toUpperCase().equals("SUCCESS")){
				        	    	 dataTable = Api.getAvailableTableChange();
				        	    }
				        	    else{
				        	    	 dataTable = TableJson.getAvailableTableChange();
				        	    }
				        	    
				        	    for(int i =0;i<dataTable.size();i++){
				        	    	listtable.add(dataTable.get(i).get("no"));
				        	    }
				        	    
				        	    adapterspinner = new ArrayAdapter<String>(getActivity(), R.layout.spiner_table,R.id.txtnotable,listtable);
				        	    final Spinner spinnernamebluetooth = (Spinner)promptsView.findViewById(R.id.spinnernamebluetooth);
				        	    Button btn_connect = (Button)promptsView.findViewById(R.id.btnconnect);
				        	    alertdialog.setView(promptsView);
				        	    final AlertDialog alertd = alertdialog.create();
				        	    spinnernamebluetooth.setAdapter(adapterspinner);
				        		btn_connect.setText("Proses");
				        	    btn_connect.setOnClickListener(new View.OnClickListener() {
									
									@Override
									public void onClick(View v) {
										// TODO Auto-generated method stub
										int position = (int) spinnernamebluetooth.getSelectedItemId();
										String table_id = dataTable.get(position).get("table_id");
										String table_no = dataTable.get(position).get("no");
										String changetable = Db.changeTable(table_no,table_id, sales_id,dataArrayMember,new ArrayList<HashMap<String,String>>());
										if(changetable.toUpperCase().equals("UNAVAILABLE")){
											changetable = "Meja Terpakai";
										}
										Toast.makeText(getActivity(), changetable, Toast.LENGTH_LONG).show();
										task = new initData();
										task.execute();
										dialog.cancel();
										alertd.dismiss();
									}
								});
				        	    
				            	alertd.show();
				            	
				            }
				        });
			    
			    if(total_price<1){
			    	 alert.setNegativeButton("Cancel" ,
				        new DialogInterface.OnClickListener() {
				            public void onClick(DialogInterface dialog, int whichButton) {
				            	//action booking
				            	AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
				        		alert.setTitle("Konfirmasi Cancel Order");
				        		alert.setMessage("Apakah Anda Yakin Akan Ingin Mengcancel Order ?");
				        		alert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
				        			public void onClick(final DialogInterface dialog, int whichButton) {
				        	            //New Order Action
				        				LayoutInflater li = LayoutInflater.from(getActivity());
				        				View promptsView = li.inflate(R.layout.layoutnote, null);
				        		     	final TextView txtnote = (TextView)promptsView.findViewById(R.id.notes);
				        		     	Button btn_cancel = (Button)promptsView.findViewById(R.id.bCancel);
				        		     	
				        		     	AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
				        				alertDialogBuilder.setView(promptsView);
				        				final AlertDialog alertd = alertDialogBuilder.create();
				        		     	
				        		     	btn_cancel.setOnClickListener(new View.OnClickListener() {
											
											@Override
											public void onClick(View v) {
												// TODO Auto-generated method stub
												Db.cancelOrder(tableId,sales_id,txtnote.getText().toString());
						        				task = new initData();
								            	task.execute();
								            	alertd.dismiss();
											}
										});
				        		     	alertd.show();
				        				
				        			}
				        		   });
				        		   alert.setNegativeButton("Tidak",
				        		     new DialogInterface.OnClickListener() {
				        		       public void onClick(DialogInterface dialog, int whichButton) {
				        		            //action booking
				        		              dialog.cancel();
				        		            }
				        		        });
				        		 alert.show();
				            	dialog.cancel();
				            	
				            }
				        });
			    }
			    
			  
			   alert.show();
		}
	}
	
	private void dialogQueue(final int position){
		final String noQueue = dataArrayQuee.get(position).get("queue");
		final String sales_id = dataArrayQuee.get(position).get("sales_id");
		final String table_id = dataArrayQuee.get(position).get("table_id");
		
		String statusTable = dataArrayQuee.get(position).get("status");
		//String member_name = dataArray.get(position).get("member_name");
		final ArrayList<HashMap<String, String>> dataArrayMember = new ArrayList<HashMap<String, String>>(); 
		HashMap<String, String>mapDataMember = new HashMap<String, String>();
		mapDataMember.put("member_id", dataArrayQuee.get(position).get("member_id"));
		mapDataMember.put("member_name", dataArrayQuee.get(position).get("member_name"));
		mapDataMember.put("member_email", dataArrayQuee.get(position).get("member_email"));
		mapDataMember.put("member_phone", dataArrayQuee.get(position).get("member_phone"));
		dataArrayMember.add(mapDataMember);
		
		final String descOrder = null;
		int total_price = 0 ;
		
		AlertDialog.Builder alert = new AlertDialog.Builder(getActivity());
		 alert.setTitle("Pemesanan");
		    alert.setMessage("Silahkan Pilih");
		    alert.setPositiveButton("Lihat Order", new DialogInterface.OnClickListener() {
		        public void onClick(DialogInterface dialog, int whichButton) {
		            //New Order Action   	
		        	ArrayList<HashMap<String, String>> dataorder = new ArrayList<HashMap<String,String>>();
		        	
		        	if(Api.getCheckConnection().toUpperCase().equals("SUCCESS")){
		        		dataorder = Api.getListOrderDetail(sales_id);
		        	}
		        	else{
		        		dataorder = Db.getListOrderDetail(sales_id);
		        	}
		        	
					if(dataorder.size()<1){
						Toast.makeText(getActivity().getApplicationContext(), "Periksa Kembali Koneksi Anda", Toast.LENGTH_LONG).show();
					}
					else{
						Intent i = new Intent(getActivity(), Catalogue.class);
	                    Bundle b = new Bundle();
	                    b.putString("sales_id",sales_id);
	                    b.putString("queue",noQueue);
	                    b.putString("table_id",table_id);
	                    b.putSerializable("member", dataArrayMember);
	                    b.putSerializable("dataorder",dataorder);
	                    i.putExtras(b);
	                    getActivity().overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
	                    startActivity(i);
	                    getActivity().finish();
					}
		        }
		    });
	
		alert.show();
	}
	
	private class initDataConnection extends AsyncTask<String, Integer, Long> {
		@Override
	    protected void onPreExecute() { 
		}

	    @Override
	    protected Long doInBackground(String... urls) {
	     return null;
	    }
	    
	    @Override
	    protected void onPostExecute(Long result) {
	    	if(OutgoingData.getInstance().getStatusThread()==false){
	    		if(Api.getTypeApp()!=null){
    				if(Helper.checkCashierPrimary(mContext)){	
    					if(Chilkat.ping()){
    						OutgoingData.getInstance().setStart();
    					}
    				}
    			}
	    	}
	    	
	    	if(Api.getCheckConnection().toUpperCase().equals("SUCCESS")){
				Main.getInstance().imageButton.setImageResource(R.drawable.iconon);
			}
			else{
				Main.getInstance().imageButton.setImageResource(R.drawable.iconoff);
			}
	    	mHandlerConnection.postDelayed(mUpdateDownTaskConnection,5000);
	    }
	}
	
	private class initData extends AsyncTask<String, Integer, Long> {
    	@Override
        protected void onPreExecute() { 
    	}

        @Override
        protected Long doInBackground(String... urls) {
        	ArrayList<HashMap<String, String>> dataTempApi = new ArrayList<HashMap<String,String>>();
        	ArrayList<HashMap<String, String>> dataTempMenu = new ArrayList<HashMap<String,String>>();
        	if(Api.getCheckConnection().toUpperCase().equals("SUCCESS")){
	        	dataTempApi = Api.getUpdateTables();
        	}
        	else{
        		if(Helper.checkCashierPrimary(getActivity())){
        			dataTempApi = TableJson.getUpdateTables();
        		}
        	}
        	
        	if(dataTempApi!=null){
        		//Log.i("DATA", dataTempApi.toString());
        		if(dataTempApi!=null){
        			//Log.i("RESET ARRAY", "TEMP-API");
        			dataArrayTempAll.clear();
        			dataArrayTempAll.addAll(dataTempApi);
        		}
        		//Log.i("TEMP-ALL", dataArrayTempAll.toString());
        		if(dataArrayTempAll.size()>0){
	        		dataArrayTemp.clear();
	        		dataArrayTempQuee.clear();
	        		for(int i=0;i<dataArrayTempAll.size();i++){
	        			HashMap<String, String>map = new HashMap<String, String>();
	        			if(dataArrayTempAll.get(i).get("data_type").toUpperCase().equals("TABLE")){
	        				map.put("table_id", dataArrayTempAll.get(i).get("table_id"));
							map.put("no", dataArrayTempAll.get(i).get("no"));
							map.put("status", dataArrayTempAll.get(i).get("status"));
							map.put("total_price", dataArrayTempAll.get(i).get("total_price"));
							map.put("total_menu", dataArrayTempAll.get(i).get("total_menu"));
							String member_id = "";
							String total_menu = "0";
							if(dataArrayTempAll.get(i).get("member_id")!=null){
								member_id = dataArrayTempAll.get(i).get("member_id");
							}
							map.put("member_id", member_id);
							map.put("member_name", dataArrayTempAll.get(i).get("member_name"));
							map.put("member_email", dataArrayTempAll.get(i).get("member_email"));
							map.put("member_phone", dataArrayTempAll.get(i).get("member_phone"));
							map.put("sales_id", dataArrayTempAll.get(i).get("sales_id"));
							map.put("photo", dataArrayTempAll.get(i).get("photo"));
							map.put("capacity",dataArrayTempAll.get(i).get("capacity"));
							map.put("time_used", dataArrayTempAll.get(i).get("time_used"));
							map.put("data_type", dataArrayTempAll.get(i).get("data_type"));
							dataArrayTemp.add(map);
						}
	        			else if(dataArrayTempAll.get(i).get("data_type").toUpperCase().equals("QUEUE")){
	        				if(dataArrayTempAll.get(i).get("no").toUpperCase().equals("NONE")){
	        					map.put("table_id", dataArrayTempAll.get(i).get("table_id"));
								map.put("no", dataArrayTempAll.get(i).get("no"));
								map.put("queue", dataArrayTempAll.get(i).get("queue"));
								map.put("status", dataArrayTempAll.get(i).get("status"));
								map.put("total_price", dataArrayTempAll.get(i).get("total_price"));
								map.put("total_menu", dataArrayTempAll.get(i).get("total_menu"));
								map.put("member_id", dataArrayTempAll.get(i).get("member_id"));
								map.put("member_name", dataArrayTempAll.get(i).get("member_name"));
								map.put("member_email", dataArrayTempAll.get(i).get("member_email"));
								map.put("member_phone", dataArrayTempAll.get(i).get("member_phone"));
								map.put("sales_id", dataArrayTempAll.get(i).get("sales_id"));
								map.put("photo", "");
								map.put("capacity","");
								map.put("time_used", "0");
								map.put("status", "0");
								map.put("data_type", dataArrayTempAll.get(i).get("data_type"));
								dataArrayTempQuee.add(map);
	        				}
						}
	        			else if(dataArrayTempAll.get(i).get("data_type").toUpperCase().equals("MENU")){
	        				
	        				map.put("menu_id", dataArrayTempAll.get(i).get("menu_id"));
							map.put("menu_type", dataArrayTempAll.get(i).get("menu_type"));
							map.put("price", dataArrayTempAll.get(i).get("price"));
							map.put("status", dataArrayTempAll.get(i).get("status"));	
							dataTempMenu.add(map);
						}
	        			
	        			if(!dataArrayTempAll.get(i).get("data_type").toUpperCase().equals("MENU")){
	        				//set syncrhonize data
							if(Api.getCheckConnection().toUpperCase().equals("SUCCESS")){
								if(dataArrayTempAll.get(i).get("sales_id")!=null){
									HashMap<String, String> mapdata = new HashMap<String, String>();
									mapdata.put("total", dataArrayTempAll.get(i).get("total_price"));
									mapdata.put("total_menu", dataArrayTempAll.get(i).get("total_menu"));
									mapdata.put("member_id",  dataArrayTempAll.get(i).get("member_id"));
									mapdata.put("member_name",  dataArrayTempAll.get(i).get("member_name"));
									mapdata.put("member_email",  dataArrayTempAll.get(i).get("member_email"));
									mapdata.put("member_phone", dataArrayTempAll.get(i).get("member_phone"));
									Db.syncronizeData(mapdata,dataArrayTempAll.get(i).get("sales_id"), dataArrayTempAll.get(i).get("table_id"), dataArrayTempAll.get(i).get("table_no"), "", dataArrayTempAll.get(i).get("member_id"), dataArrayTempAll.get(i).get("member_name"), dataArrayTempAll.get(i).get("member_email"), dataArrayTempAll.get(i).get("member_phone"), dataArrayTempAll.get(i).get("status"));
								}
							}
	        			}
	        			
	        		}
	        		MenuJson.updateMenu(dataTempMenu);
        		}
        		
        		/*if(position == 0){
        			//Log.i("MASUK", "TEMP-TABLE");
        			//dataArrayTemp.clear();
        			for(int ii=0;ii<dataArrayTemp.size();ii++){
        				HashMap<String, String>map = new HashMap<String, String>();
        				if(Api.tables!=null){
							if(Api.tables.size()>1){
								//Log.i("MASUK1", "MASUK1");
								//Log.i("MASUK2", "MASUK2");
								String statusnotif = "0";
								if(!dataArrayTemp.get(ii).get("no").toLowerCase().equals("none")){
									//Log.i("DATA-TABLES-API",Api.tables.get(ii).get("total_menu") + " = " +dataArrayTemp.get(ii).get("total_menu"));
									if(!Api.tables.get(ii).get("total_menu").equals(dataArrayTemp.get(ii).get("total_menu"))){
										statusnotif = "1";
										//Log.i("MASUK3", "MASUK3");
									}
									
								}
									
								Api.tables.get(ii).put("statusnotif",statusnotif);
								map.put("statusnotif", Api.tables.get(ii).get("statusnotif"));
								dataArrayTemp.get(ii).putAll(map);
							}
						}
        			}
        		//Log.i("DATA-TEMP", dataArrayTemp.toString());
        		}*/
        	}
        	return (long) 0;
        }
        
        @Override
        protected void onPostExecute(Long result) {
	        
        		if(position == 0){
        			
	        		if(Main.search==false){
	        		
	        			if(!dataArray.toString().equals(dataArrayTemp.toString())){
	                		if(dataArrayTemp.size()>0){
	                			dataArray.clear();
		                		dataArray.addAll(dataArrayTemp);
		                		Collections.sort(dataArray,new Comparator<HashMap<String, String>>() {

									@Override
									public int compare(HashMap<String, String> lhs, HashMap<String, String> rhs) {
										// TODO Auto-generated method stub
										return lhs.get("no").compareTo(rhs.get("no"));
									}
								});
		                		if(Api.tables==null){
		            				Api.tables = new ArrayList<HashMap<String,String>>();
		            				Api.tables.addAll(dataArrayTemp);
		            			}
	                		}
	                		adapter.notifyDataSetChanged();
	                		if(Api.getTypeApp()!=null){
	                			if(Api.getTypeApp().toLowerCase().equals("cashier")){
	                				dataNotif = new ArrayList<HashMap<String,String>>();
	    	                		countnotif = 0;
	    	                		countallnotif = 0;
	    	                		if(dataArray!=null){
	    	                			for(int i =0;i<dataArray.size();i++){
	    	                				if(dataArray.get(i).get("statusnotif")!=null){
	    	                					if(dataArray.get(i).get("statusnotif").equals("1")){
	    	                						HashMap<String, String>hashMap = new HashMap<String, String>();
	    	                						hashMap.put("notif",String.valueOf(i));
	    	                						dataNotif.add(hashMap);
	    	                						countnotif++;
	    	                						countallnotif++;
	    	                					}
	    	                				}
	    	                			}
	    	                		}
	    	                		btn_notif.setText(String.valueOf(countallnotif));
	    	                		if(countallnotif>0){
	    	                			Helper.notifSound(Main.getContextMain().getApplicationContext());
	    	                		}
	                			}
	                		}
	                		
	                	}
	        		}
	        	}
	        	else{
	        		
	        		if(!dataArrayQuee.toString().equals(dataArrayTempQuee.toString())){
	            		if(dataArrayTempQuee.size()>0){
	            			
	            			dataArrayQuee.clear();
		            		dataArrayQuee.addAll(dataArrayTempQuee);
		            	   /* Collections.sort(dataArrayQuee,new Comparator<HashMap<String, String>>() {

								@Override
								public int compare(HashMap<String, String> lhs, HashMap<String, String> rhs) {
									// TODO Auto-generated method stub
									return lhs.get("table_no").compareTo(rhs.get("table_no"));
								}
							});*/
		            		//Log.i("DATA-QUEE", dataArrayQuee.toString());
	            		}
	            		adapterquee.notifyDataSetChanged();
	            	}
	        	}
	        	
	        	mHandler.postDelayed(mUpdateDownTask, 5000);
	        	
        }
    }
	

	
	@Override
	public void onConfigurationChanged(Configuration newConfig) {
	    super.onConfigurationChanged(newConfig);
	    // Checks the orientation of the screen
	    if (newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE) {
	    	if(Helper.isTablet(getActivity())){
	    		grid.setNumColumns(3);
	    	}
	    	else{
	    		grid.setNumColumns(2);
	    	}
	    	
	    } else if (newConfig.orientation == Configuration.ORIENTATION_PORTRAIT){
	    	if(Helper.isTablet(getActivity())){
	    		grid.setNumColumns(2);
	    	}
	    	else{
	    		grid.setNumColumns(1);
	    	}
	    	
	    }
	}
	
	private Runnable mUpdateDownTask = new Runnable() {
		@Override
		public void run() {
			task = new initData();
			task.execute();
		}
	};
	
	public  Runnable mUpdateDownTaskConnection = new Runnable() {
		@Override
		public void run() {
			taskConnection = new initDataConnection();
			taskConnection.execute();
		}
	};
	
	private void setListMember(final String tableid,final String tableno,final int positiongrid){
		if(dataArrayList.size()>0){
			adapterlist = new MemberAdapterList(getActivity().getApplicationContext(), R.layout.header, R.layout.list_name_phone, dataArrayList);
			adapterlist.notifyDataSetChanged();
			lv.setAdapter(adapterlist);
	    	pbar.setVisibility(View.GONE);
		}
		else{
			pbar.setVisibility(View.GONE);
			layoutlistnotfound.setVisibility(View.VISIBLE);
			btnregister.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					LayoutInflater li = LayoutInflater.from(getActivity().getApplicationContext());
					View promptsView = li.inflate(R.layout.dialog_register_member, null);
				    final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
				    
	            	alertDialogBuilder.setTitle("Register Member");
	            	alertDialogBuilder.setView(promptsView);
	            	btnregister = (Button)promptsView.findViewById(R.id.btnregister);
	            	txtregistername = (TextView)promptsView.findViewById(R.id.txtregistername);
	            	txtregisterphone = (TextView)promptsView.findViewById(R.id.txtregisterphone);
	            	edttxtregistername = (EditText)promptsView.findViewById(R.id.edttxtregistername);
	            	edttxtregisterphone = (EditText)promptsView.findViewById(R.id.edttxtregisterphone);

	                final AlertDialog dlg = alertDialogBuilder.create();
	                alertDialogBuilderParent.create().cancel();
	                alertdgparent.cancel();
	            	btnregister.setOnClickListener(new View.OnClickListener() {
				
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							String apiregistermember = Db.setBookAndRegisterMember(tableid,tableno,edttxtregistername.getText().toString(), edttxtregisterphone.getText().toString());
							if(apiregistermember.equals("SUCCESS")){
								task = new initData();
								task.execute();
								dlg.dismiss();
								
								Toast.makeText(getActivity().getApplicationContext(), "Booking Berhasil", Toast.LENGTH_LONG).show();
							}
							else{
								Toast.makeText(getActivity().getApplicationContext(), "Terjadi Kesalahan !!!", Toast.LENGTH_LONG).show();
							}
							
						}
					});
	            	dlg.show();
				}
			});
		}	
	}
}