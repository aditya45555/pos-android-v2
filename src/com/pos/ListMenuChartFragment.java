/*
 * Copyright (C) 2013 Andreas Stuetz <andreas.stuetz@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.adapter.ListChartAdapter;
import com.json.ListChartDataJson;
import com.lib.Helper;

import android.app.FragmentManager;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

public class ListMenuChartFragment extends Fragment  {
	
	private static final String ARG_POSITION = "position";
	private static final String ARG_STATUS = "status";
	private static final String ARG_SALES_ID = "sales_id";
	public static ArrayList<HashMap<String, String>> dataArrayOrderMenuChart = new ArrayList<HashMap<String, String>>();
	private int position;
	private  String status ;
	private  String sales_id ;
	public static   ListView list_menu_chart;
	public static   ListChartAdapter adapter;
	Boolean checkMenu;
	private static android.support.v4.app.FragmentManager fragmentmanagerpublic;
	//public static  initData task= new initData(); 
	private static ListMenuChartFragment sMainActivity;
	
	
	public static ListMenuChartFragment newInstance(int position,String id,String sales_id,android.support.v4.app.FragmentManager fragmentmanager) {
		ListMenuChartFragment f = new ListMenuChartFragment();
		Bundle b = new Bundle();
		b.putInt(ARG_POSITION, position);
		b.putString(ARG_STATUS, id);
		b.putString(ARG_SALES_ID, sales_id);
		fragmentmanagerpublic = fragmentmanager;
		f.setArguments(b);
		
		return f;
	}
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		sMainActivity = this;
		position = getArguments().getInt(ARG_POSITION);
		status = getArguments().getString(ARG_STATUS);
		sales_id = getArguments().getString(ARG_SALES_ID);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.grid_chart, container, false);
		if(position == 0){
			if(Api.getCheckConnection().toUpperCase().equals("SUCCESS")){
				dataArrayOrderMenuChart = Api.getOrderTemp(sales_id);
			}
			else{
				if(Helper.checkCashierPrimary(getActivity())){
					dataArrayOrderMenuChart = Db.getOrderTemp(sales_id);
				}
			}
		}
		else{
			if(Api.getCheckConnection().toUpperCase().equals("SUCCESS")){
				dataArrayOrderMenuChart = Api.getOrderConfirm(sales_id);
			}
			else{
				if(Helper.checkCashierPrimary(getActivity())){
					dataArrayOrderMenuChart = Db.getOrderConfirm(sales_id);
				}
				
			}
		}
		list_menu_chart = (ListView) v.findViewById(R.id.list_menu_chart);
    	adapter = new ListChartAdapter(getActivity().getApplicationContext(),
        R.layout.header, R.layout.list_menu_chart,dataArrayOrderMenuChart,list_menu_chart,fragmentmanagerpublic);
    	list_menu_chart.setAdapter(adapter);
    	
    	return v;
	}
	
}