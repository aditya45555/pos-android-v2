package com.pos;

import java.io.File;
import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.os.Environment;
import android.util.Log;
import android.widget.ImageView;

import com.asyntask.tasksplit;
import com.json.QueueJson;
import com.json.TableJson;
import com.lib.Helper;

public class Db {
	private static Db db;
	
	public static Db getInstance() {
		if (db == null)
			db = new Db();
		return db;
	}
	
	public static void syncronizeData(HashMap<String, String>datastatus,String sales_id,String table_id,String table_no,String descOrder,String member_id,String member_name,String member_email,String member_phone,String status){
		if(sales_id.length()>0){
			if(table_no!=null){
				if(table_no.toUpperCase().equals("NONE")){
					setOrderQueueHistory(sales_id, table_id, table_no, descOrder, member_id, member_name, member_email, member_phone);
				}
				else{
					setOrderHistory(sales_id, table_id, table_no, descOrder, member_id, member_name, member_email, member_phone, status);
				}
			}
			setOrderTempHistory(sales_id);
			confirmOrderHistory(sales_id);
			setStatusTables(sales_id, table_id, datastatus);
		}
	
	}
	
	public static String setOrder(String sales_id,String table_id,String table_no,String descOrder,String member_id,String member_name,String member_email,String member_phone){
		try{
			JSONObject param = new JSONObject(), jobj = new JSONObject(), jo = new JSONObject();
			String json,jsonreturn;
			String dataenc = null;
			if(sales_id.length()<1){
				sales_id = Helper.generateId("order");
			}
			long time = System.currentTimeMillis()/1000;
			param.put("sales_id", sales_id);
			param.put("table_id", table_id);
			param.put("table_no", table_no);
			param.put("member_id",member_id);
			param.put("member_name",member_name);
			param.put("member_email",member_email);
			param.put("member_phone",member_phone);
			param.put("status","new");
			param.put("queue", "0");
			param.put("created_by",Api.getUid());
			param.put("time_created",time);
			dataenc= param.toString();
			jobj.put("data", dataenc);
			json = jobj.toString();
			//Log.i("JSON", json);
			if (Helper.isJSONValid(json))
			{
				Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), sales_id+".json", json, "order","new");
				jo.put("queue", "0");
				jo.put("sales_id", sales_id);
				jsonreturn = jo.toString();
				return jsonreturn;
			}
    	}catch (JSONException e) { 
			Log.i("ERROR", e.toString());
		}
		
		return "failed";
	}
	
	public static String setOrderHistory(String sales_id,String table_id,String table_no,String descOrder,String member_id,String member_name,String member_email,String member_phone,String status){
		try{
			JSONObject param = new JSONObject(), jobj = new JSONObject(), jo = new JSONObject();
			String json,jsonreturn;
			String dataenc = null;
			long time = System.currentTimeMillis()/1000;
			param.put("sales_id", sales_id);
			param.put("table_id", table_id);
			param.put("table_no", table_no);
			param.put("member_id",member_id);
			param.put("member_name",member_name);
			param.put("member_email",member_email);
			param.put("member_phone",member_phone);
			param.put("status",status);
			param.put("queue", "0");
			param.put("created_by",Api.getUid());
			param.put("time_created",time);
			dataenc= param.toString();
			jobj.put("data", dataenc);
			json = jobj.toString();
			//Log.i("JSON", json);
			if (Helper.isJSONValid(json))
			{
				Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), sales_id+".json", json, "orderhistory","new");
				jo.put("queue", "0");
				jo.put("sales_id", sales_id);
				jsonreturn = jo.toString();
				return jsonreturn;
			}
			
    	}catch (JSONException e) { 
			Log.i("ERROR", e.toString());
		}
		
		return "failed";
	}
	
	public static String setOrderQueue(String table_id,String table_no,String descOrder,String member_id,String member_name,String member_email,String member_phone){
		try{
			JSONObject param = new JSONObject(), jobj = new JSONObject(), jo = new JSONObject();
			String json,jsonreturn;
			String dataenc = null;
			String sales_id = Helper.generateId("order");
			long time = System.currentTimeMillis()/1000;
			param.put("sales_id", sales_id);
			param.put("table_id", table_id);
			param.put("table_no", table_no);
			param.put("member_id",member_id);
			param.put("member_name",member_name);
			param.put("member_email",member_email);
			param.put("member_phone",member_phone);
			param.put("created_by",Api.getUid());
			param.put("status","new");
			String queue = String.valueOf(Integer.valueOf(QueueJson.getQueue())+1);
			param.put("queue", queue);
			param.put("time_created",time);
			dataenc= param.toString();
			
			jobj.put("data", dataenc);
			json = jobj.toString();
			//Log.i("JSON", json);
			if (Helper.isJSONValid(json))
			{
				Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), sales_id+".json", json, "order","newqueue");
				QueueJson.add(queue);
				jo.put("queue", queue);
				jo.put("sales_id", sales_id);
				jsonreturn = jo.toString();
				return jsonreturn;
			}
    	}catch (JSONException e) { 
			Log.i("ERROR", e.toString());
		}
		
		return "failed";
	}
	
	public static String setOrderQueueHistory(String sales_id,String table_id,String table_no,String descOrder,String member_id,String member_name,String member_email,String member_phone){
		try{
			JSONObject param = new JSONObject(), jobj = new JSONObject(), jo = new JSONObject();
			String json,jsonreturn;
			String dataenc = null;
			long time = System.currentTimeMillis()/1000;
			param.put("sales_id", sales_id);
			param.put("table_id", table_id);
			param.put("table_no", table_no);
			param.put("member_id",member_id);
			param.put("member_name",member_name);
			param.put("member_email",member_email);
			param.put("member_phone",member_phone);
			param.put("created_by",Api.getUid());
			param.put("status","new");
			String queue = String.valueOf(Integer.valueOf(QueueJson.getQueue())+1);
			param.put("queue", queue);
			param.put("time_created",time);
			dataenc= param.toString();
			
			jobj.put("data", dataenc);
			json = jobj.toString();
			//Log.i("JSON", json);
			if (Helper.isJSONValid(json))
			{
				Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), sales_id+".json", json, "orderhistory","newqueue");
				QueueJson.add(queue);
				jo.put("queue", queue);
				jo.put("sales_id", sales_id);
				jsonreturn = jo.toString();
				return jsonreturn;
			}
    	}catch (JSONException e) { 
			Log.i("ERROR", e.toString());
		}
		
		return "failed";
	}
	
	public static String setBookTables(String tableid,String tableno,String member_phone,String member_email,String member_name,String member_id){
		try{
			JSONObject param = new JSONObject(), jobj = new JSONObject(), jo = new JSONObject();
			String json,jsonreturn;
			String dataenc = null;
			String id = Helper.generateId("bookedtable");
			long time = System.currentTimeMillis()/1000;
			param.put("table_booked_id", id);
			param.put("table_id", tableid);
			param.put("member_id",member_id);
			param.put("name",member_name);
			param.put("phone",member_phone);
			param.put("email",member_email);
			param.put("status","new");
			param.put("time_arrival","0");
			param.put("time_created",time);
			param.put("created_by",Api.getUid());
			dataenc= param.toString();
			jobj.put("data", dataenc);
			json = jobj.toString();
			//Log.i("JSON", json);
			if (Helper.isJSONValid(json))
			{
				Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), id+".json", json, "order","book");
				//jo.put("id", sales_id);
				jsonreturn = jo.toString();
				return jsonreturn;
			}
    	}catch (JSONException e) { 
			Log.i("ERROR", e.toString());
		}
		
		return "failed";
	}
	
	public static String setBookAndRegisterMember(String tableid,String tableno,String name,String phone){
		try {
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray params = new JSONArray();
			String json;
			String datajson;
			String id = Helper.generateId("member");
			long time = System.currentTimeMillis()/1000;
			jo.put("member_id",id);
			jo.put("name",name);
			jo.put("phone",phone);
			jo.put("email","");
			jo.put("address","");
			jo.put("time_created",time);
			jo.put("created_by",Api.getUid());
			jobj.put("data", jo.toString());
			jobj.put("token", Api.getToken());
			jobj.put("app_id",Api.pappid);
			json = jobj.toString();
			Log.i("SET-JSON-MEMBER",json);
			if (Helper.isJSONValid(json))
			{
				Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), id+".json", json, "member","register");
				setBookTables(tableid, tableno, "", "",name, id);
				return "SUCCESS";
			}
		} catch (JSONException e) {
			// TODO: handle exception
		}
		return "failed";
	} 
	
	public static void setOrderTemp(String sales_id,String id,ArrayList<HashMap<String, String>> dataArrayOrderMenu){
		try {
			JSONObject jobj = new JSONObject(), jo = new JSONObject(),jodata = new JSONObject(),joxdata = new JSONObject();
			String file = sales_id+"_"+id+".json";
			String filename = file.replace(".json","");
			String strsplit = new String(filename);
			String[] datafilename = strsplit.split("_");
			String pathFile = Helper.checkFileOrder(Environment.getExternalStorageDirectory(), "order", datafilename, "temp");
			if(!pathFile.toUpperCase().equals("FAILED")){
				String jsonfile = Helper.getFileContentDir(pathFile);
				if(Helper.isJSONValid(jsonfile)){
					
					//jika file ada (Edit Quantity dari file lama)
					jobj = new JSONObject(jsonfile);
					jobj = new JSONObject(jobj.getString("data"));
					jodata  = new JSONObject();
					int quantity = Integer.valueOf(dataArrayOrderMenu.get(0).get("quantity"))+jobj.getInt("quantity");
					jodata.put("sales_id", jobj.get("sales_id"));
					jodata.put("sales_detail_id", jobj.get("sales_detail_id"));
					jodata.put("id", jobj.get("id"));
					jodata.put("name", jobj.get("name"));
					jodata.put("price", jobj.get("price"));
					jodata.put("type", jobj.get("type"));
					jodata.put("quantity", quantity);
					jodata.put("discount", jobj.get("discount"));
					joxdata.put("data", jodata.toString());
					file = jobj.getString("sales_id")+"_"+jobj.getString("sales_detail_id")+"_"+jobj.getString("id")+".json";
					Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), file, joxdata.toString(), "order","temp");	
				}
			}
			else{
				//jika file tidak ada (Buat Baru)
				pathFile = Helper.checkFileOrder(Environment.getExternalStorageDirectory(), "orderhistory", datafilename, "detail");
				String sales_detail_id = "";
				//get sales_detail_id
				if(!pathFile.toUpperCase().equals("FAILED")){
					String jsonfile = Helper.getFileContentDir(pathFile);
					if(Helper.isJSONValid(jsonfile)){
						jobj = new JSONObject(jsonfile);
						jobj = new JSONObject(jobj.getString("data"));
						sales_detail_id = jobj.getString("sales_detail_id");
					}
					else{
						sales_detail_id = Helper.generateId("sales_detail_temp");
					}
				}
				else{
					pathFile = Helper.checkFileOrder(Environment.getExternalStorageDirectory(), "order", datafilename, "detail");
					if(!pathFile.toUpperCase().equals("FAILED")){
						String jsonfile = Helper.getFileContentDir(pathFile);
						if(Helper.isJSONValid(jsonfile)){
							jobj = new JSONObject(jsonfile);
							jobj = new JSONObject(jobj.getString("data"));
							sales_detail_id = jobj.getString("sales_detail_id");
						}
						else{
							sales_detail_id = Helper.generateId("sales_detail_temp");
						}
						
					}
					else{
						sales_detail_id = Helper.generateId("sales_detail_temp");
					}
				}
				file = sales_id+"_"+sales_detail_id+"_"+id+".json";
				jodata  = new JSONObject();
				jodata.put("sales_id", dataArrayOrderMenu.get(0).get("sales_id"));
				jodata.put("sales_detail_id", sales_detail_id);
				jodata.put("id", dataArrayOrderMenu.get(0).get("id"));
				jodata.put("name", dataArrayOrderMenu.get(0).get("name"));
				jodata.put("price", dataArrayOrderMenu.get(0).get("price"));
				jodata.put("type", dataArrayOrderMenu.get(0).get("menu_type"));
				jodata.put("quantity", dataArrayOrderMenu.get(0).get("quantity"));
				jodata.put("discount", dataArrayOrderMenu.get(0).get("discount"));
				joxdata.put("data", jodata.toString());
				Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), file, joxdata.toString(), "order","temp");
			}
		} catch (JSONException e) {
			// TODO: handle exception
			Log.i("JSON-ERROR-ORDER-TEMP", e.toString());
		}
		}
	
	public static void setOrderTempHistory(String sales_id){
		try {
			JSONObject jobj = new JSONObject(), jo = new JSONObject(),jodata = new JSONObject(),joxdata = new JSONObject();
			ArrayList<HashMap<String, String>> dataOrder = new ArrayList<HashMap<String,String>>();
			dataOrder = Api.getOrderTemp(sales_id);
			if(dataOrder.size()>0){
				for(int i=0;i<dataOrder.size();i++){
					String file = sales_id+"_"+dataOrder.get(i).get("id")+".json";
					String filename = file.replace(".json","");
					String strsplit = new String(filename);
					String[] datafilename = strsplit.split("_");
					String pathFile = Helper.checkFileOrder(Environment.getExternalStorageDirectory(), "orderhistory", datafilename, "temp");
					if(!pathFile.toUpperCase().equals("FAILED")){
						String jsonfile = Helper.getFileContentDir(pathFile);
						if(Helper.isJSONValid(jsonfile)){
							Log.i("ADA","ADA");
							//jika file ada (Edit Quantity dari file lama)
							jobj = new JSONObject(jsonfile);
							jobj = new JSONObject(jobj.getString("data"));
							jodata  = new JSONObject();
							int quantity = jobj.getInt("quantity");
							jodata.put("sales_id", jobj.get("sales_id"));
							jodata.put("sales_detail_id", jobj.get("sales_detail_id"));
							jodata.put("id", jobj.get("id"));
							jodata.put("name", jobj.get("name"));
							jodata.put("price", jobj.get("price"));
							jodata.put("type", jobj.get("type"));
							jodata.put("quantity", quantity);
							jodata.put("discount", jobj.get("discount"));
							joxdata.put("data", jodata.toString());
							file = jobj.getString("sales_id")+"_"+jobj.getString("sales_detail_id")+"_"+jobj.getString("id")+".json";
							Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), file, joxdata.toString(), "orderhistory","temp");	
						}
					}
					else{
						Log.i("TIDAK ADA","TIDAK ADA");
						//jika file tidak ada (Buat Baru)
						String sales_detail_id = dataOrder.get(i).get("sales_detail_id");
						file = sales_id+"_"+sales_detail_id+"_"+dataOrder.get(i).get("id")+".json";
						jodata  = new JSONObject();
						jodata.put("sales_id", dataOrder.get(i).get("sales_id"));
						jodata.put("sales_detail_id", sales_detail_id);
						jodata.put("id", dataOrder.get(i).get("id"));
						jodata.put("name", dataOrder.get(i).get("name"));
						jodata.put("price", dataOrder.get(i).get("price"));
						jodata.put("type", dataOrder.get(i).get("type"));
						jodata.put("quantity", dataOrder.get(i).get("quantity"));
						jodata.put("discount", dataOrder.get(i).get("discount"));
						joxdata.put("data", jodata.toString());
						Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), file, joxdata.toString(), "orderhistory","temp");
					}
				}
			}
			
		} catch (JSONException e) {
			// TODO: handle exception
			Log.i("JSON-ERROR-ORDER-HISTORY-TEMP", e.toString());
		}
		}
	
	public static void setStatusTables(String salesId,String tableId,HashMap<String, String> datastatus){
		try {
			
			JSONObject jobj = new JSONObject(), jo = new JSONObject(),jodata = new JSONObject(),joxdata = new JSONObject();
			JSONArray jadata;
			ArrayList<HashMap<String, String>>datamember = new ArrayList<HashMap<String,String>>();
			HashMap<String, String>map = new HashMap<String, String>();
			ArrayList<HashMap<String, String>>datatotal = new ArrayList<HashMap<String,String>>();
			
			String file = salesId+"_"+tableId+".json";
			jo = new JSONObject(datastatus);
			jobj.put("data", jo);
			jobj.put("sales_id",salesId);
			jobj.put("table_id",tableId);
			map.put("member_id", datastatus.get("member_id"));
			map.put("member_name", datastatus.get("member_name"));
			map.put("member_email", datastatus.get("member_email"));
			map.put("member_phone", datastatus.get("member_phone"));
			datamember.add(map);
			map = new HashMap<String, String>();
			map.put("total_menu",datastatus.get("total_menu"));
			map.put("total_price",datastatus.get("total_price"));
			datatotal.add(map);
			
			
			TableJson.setUpdateTables(salesId, tableId, datastatus.get("status"), datamember,datatotal);
			Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), file, jobj.toString(), "order", "status");
			
		} catch (JSONException e) {
			// TODO: handle exception
			Log.i("JSON-ERROR-ORDER-STATUS", e.toString());
		}
	}
	
	public static String confirmOrder(String salesId){
		try {
			JSONObject jobj = new JSONObject(),jodata = new JSONObject(),joxdata = new JSONObject();
			ArrayList<HashMap<String, String>>datatemporder = new ArrayList<HashMap<String,String>>();
			
			if(Api.getCheckConnection().toUpperCase().equals("SUCCESS")){
				datatemporder = Api.getOrderTemp(salesId);
			}
			else{
				datatemporder = Db.getOrderTemp(salesId);
			}
			
			if(datatemporder.size()>0){
				for(int i =0;i<datatemporder.size();i++){
					String file = salesId+"_"+datatemporder.get(i).get("id")+".json";
					String filename = file.replace(".json","");
					String strsplit = new String(filename);
					String[] datafilename = strsplit.split("_");
					String pathFile = Helper.checkFileOrder(Environment.getExternalStorageDirectory(), "order", datafilename, "detail");
					
					if(!pathFile.toUpperCase().equals("FAILED")){
						//jika ada file confirm order (TAMBAH QUANTITY)
						String jsonfile = Helper.getFileContentDir(pathFile);
						if(Helper.isJSONValid(jsonfile)){
							jobj = new JSONObject(jsonfile);
							jobj = new JSONObject(jobj.getString("data"));
							jodata  = new JSONObject();
							int quantity = Integer.valueOf(datatemporder.get(i).get("quantity"))+jobj.getInt("quantity");
							Log.i("QUANTITY", datatemporder.get(i).get("quantity")+"+"+jobj.getInt("quantity")+" = "+quantity);
							jodata.put("sales_id", jobj.get("sales_id"));
							jodata.put("sales_detail_id", jobj.get("sales_detail_id"));
							jodata.put("id", jobj.get("id"));
							jodata.put("name", jobj.get("name"));
							jodata.put("price", jobj.get("price"));
							jodata.put("type", jobj.get("type"));
							jodata.put("quantity", quantity);
							jodata.put("discount", jobj.get("discount"));
							joxdata.put("data", jodata.toString());
							file = jobj.getString("sales_id")+"_"+jobj.getString("sales_detail_id")+"_"+jobj.getString("id")+".json";
							Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), file, joxdata.toString(), "order","detail");	
						}
						
					}
					else{
						Log.i("FILE","TIDAK ADA");
						//jika tidak ada file confirm order BUAT BARU
						file = salesId+"_"+datatemporder.get(i).get("sales_detail_id")+"_"+datatemporder.get(i).get("id")+".json";
						jodata  = new JSONObject();
						jodata.put("sales_id", datatemporder.get(i).get("sales_id"));
						jodata.put("sales_detail_id", datatemporder.get(i).get("sales_detail_id"));
						jodata.put("id", datatemporder.get(i).get("id"));
						jodata.put("name", datatemporder.get(i).get("name"));
						jodata.put("type", datatemporder.get(i).get("type"));
						jodata.put("price",datatemporder.get(i).get("price"));
						jodata.put("quantity", datatemporder.get(i).get("quantity"));
						jodata.put("discount", datatemporder.get(i).get("discount"));
						joxdata.put("data", jodata.toString());
						Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), file, joxdata.toString(), "order","detail");
					}
				}
				
				//hapus file order temp dan order history temp
				ArrayList<HashMap<String, String>>dataFileOrderTempArray = Helper.checkListFileOrder(Environment.getExternalStorageDirectory(), "order", "temp", salesId);
				ArrayList<HashMap<String, String>>dataFileOrderHistoryTempArray = Helper.checkListFileOrder(Environment.getExternalStorageDirectory(), "order", "temp", salesId);
				
				if(dataFileOrderTempArray.size()>0){
					for(int i=0;i<dataFileOrderTempArray.size();i++){
						String pathfile = dataFileOrderTempArray.get(i).get("pathfile");
						File ffile = new File(pathfile);
						if(ffile.exists())
							ffile.delete();
					}
				}
				
				if(dataFileOrderHistoryTempArray.size()>0){
					for(int i=0;i<dataFileOrderHistoryTempArray.size();i++){
						String pathfile = dataFileOrderHistoryTempArray.get(i).get("pathfile");
						File ffile = new File(pathfile);
						if(ffile.exists())
							ffile.delete();
					}
				}
			}
			
		} catch (JSONException e) {
			// TODO: handle exception
			Log.i("JSON-ERROR-ORDER-CONFIRM", e.toString());
		}
		return "failed";
	} 
	
	public static String confirmOrderHistory(String salesId){
		try {
			JSONObject jobj = new JSONObject(),jodata = new JSONObject(),joxdata = new JSONObject();
			ArrayList<HashMap<String, String>>datatemporder = new ArrayList<HashMap<String,String>>();
			datatemporder = Api.getOrderConfirm(salesId);
			if(datatemporder.size()>0){
				for(int i =0;i<datatemporder.size();i++){
					String file = salesId+"_"+datatemporder.get(i).get("id")+".json";
					String filename = file.replace(".json","");
					String strsplit = new String(filename);
					String[] datafilename = strsplit.split("_");
					String pathFile = Helper.checkFileOrder(Environment.getExternalStorageDirectory(), "orderhistory", datafilename, "detail");
					
					if(!pathFile.toUpperCase().equals("FAILED")){
						//jika ada file confirm order (TAMBAH QUANTITY)
						String jsonfile = Helper.getFileContentDir(pathFile);
						if(Helper.isJSONValid(jsonfile)){
							jobj = new JSONObject(jsonfile);
							jobj = new JSONObject(jobj.getString("data"));
							jodata  = new JSONObject();
							int quantity = jobj.getInt("quantity");
							jodata.put("sales_id", jobj.get("sales_id"));
							jodata.put("sales_detail_id", jobj.get("sales_detail_id"));
							jodata.put("id", jobj.get("id"));
							jodata.put("name", jobj.get("name"));
							jodata.put("price", jobj.get("price"));
							jodata.put("type", jobj.get("type"));
							jodata.put("quantity", quantity);
							jodata.put("discount", jobj.get("discount"));
							joxdata.put("data", jodata.toString());
							file = jobj.getString("sales_id")+"_"+jobj.getString("sales_detail_id")+"_"+jobj.getString("id")+".json";
							Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), file, joxdata.toString(), "orderhistory","detail");	
						}
						
					}
					else{
						Log.i("FILE","TIDAK ADA");
						//jika tidak ada file confirm order BUAT BARU
						file = salesId+"_"+datatemporder.get(i).get("sales_detail_id")+"_"+datatemporder.get(i).get("id")+".json";
						jodata  = new JSONObject();
						jodata.put("sales_id", datatemporder.get(i).get("sales_id"));
						jodata.put("sales_detail_id", datatemporder.get(i).get("sales_detail_id"));
						jodata.put("id", datatemporder.get(i).get("id"));
						jodata.put("name", datatemporder.get(i).get("name"));
						jodata.put("type", datatemporder.get(i).get("type"));
						jodata.put("price",datatemporder.get(i).get("price"));
						jodata.put("quantity", datatemporder.get(i).get("quantity"));
						jodata.put("discount", datatemporder.get(i).get("discount"));
						joxdata.put("data", jodata.toString());
						Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), file, joxdata.toString(), "orderhistory","detail");
					}
				}	
			}
			
		} catch (JSONException e) {
			// TODO: handle exception
			Log.i("JSON-ERROR-ORDER-HISTORY-CONFIRM", e.toString());
		}
		return "failed";
	} 
	
	public static String sendDataJual(String sales_id,String table_id,String time_sales,JSONObject data) {
		String code = "failed";	
		try {
			JSONObject jodata = new JSONObject(),joxdata = new JSONObject();
			JSONObject jodatakredit = new JSONObject(),jodatacash = new JSONObject(),jodatavoucher = new JSONObject();
			JSONArray jadata = new JSONArray(),jadatakredit = new JSONArray(),jadatacash = new JSONArray(),jadatavoucher = new JSONArray();
			
			int countfile = Helper.getCountFileOutgoing(Environment.getExternalStorageDirectory(), "order","paid")+Helper.getCountFileOutgoing(Environment.getExternalStorageDirectory(), "orderhistory","paid")+1;
			String countcode = String.valueOf(countfile);
			SimpleDateFormat sdf =new SimpleDateFormat("ddMMyy");
			Date d = new Date();
			long time = System.currentTimeMillis()/1000;
			String prefix = sdf.format(d);
			switch(countcode.length()){
			case 1: {
				code= prefix+"0000"+countcode;
				break;
			}
			case 2:	{
				code= prefix+"000"+countcode;
				break;
			}
			case 3: {
				code= prefix+"00"+countcode;
				break;
			}
			case 4: {
				code= prefix+"0"+countcode;
				break;
			}
			case 5: {
				code= prefix+""+countcode;
				break;
			}
		}
			
			if(Helper.isJSONValid(data.toString())){
				jodata = data;
				
				//check array cash
				if(jodata.getString("cash")!=null){
					jadatacash = new JSONArray(jodata.getString("cash"));
				}
				
				//check array credit
				if(jodata.getString("kredit")!=null){
					jadatakredit = new JSONArray(jodata.getString("kredit"));
				}
				
				//check array voucher
				if(jodata.getString("voucher")!=null){
					jadatavoucher = new JSONArray(jodata.getString("voucher"));
				}
				
				
				//put cash
				if(jadatacash.length()>0){
					String sales_payment_id = Helper.generateId("sales_payment_id"); 
					JSONObject ddataobject = jadatacash.getJSONObject(0);
					if(ddataobject.getInt("total")>0){
						joxdata = new JSONObject();
						joxdata.put("sales_payment_id", sales_payment_id);
						joxdata.put("code", code);
						joxdata.put("payment_type", "cash");
						joxdata.put("reference_number", ddataobject.get("reference_number"));
						joxdata.put("bank_id", ddataobject.get("bank_id"));
						joxdata.put("tips", ddataobject.get("tips"));
						joxdata.put("discount", ddataobject.get("discount"));
						joxdata.put("tax", ddataobject.get("tax"));
						joxdata.put("total", ddataobject.get("total"));
						joxdata.put("time_sales", time_sales);
						joxdata.put("created_by", Api.getUid());
						jadata.put(joxdata);
					}
				}
				
				//put kredit
				if(jadatakredit.length()>0){
					String sales_payment_id = Helper.generateId("sales_payment_id"); 
					JSONObject ddataobject = jadatakredit.getJSONObject(0);
					
					if(ddataobject.getInt("total")>0){
						joxdata = new JSONObject();
						joxdata.put("sales_payment_id", sales_payment_id);
						joxdata.put("code", code);
						joxdata.put("payment_type", "kredit");
						joxdata.put("reference_number", ddataobject.get("reference_number"));
						joxdata.put("bank_id", ddataobject.get("bank_id"));
						joxdata.put("tips", ddataobject.get("tips"));
						joxdata.put("discount", ddataobject.get("discount"));
						joxdata.put("tax", ddataobject.get("tax"));
						joxdata.put("total", ddataobject.get("total"));
						joxdata.put("time_sales", time_sales);
						joxdata.put("created_by", Api.getUid());
						jadata.put(joxdata);
					}
				}
				
				//put voucher
				if(jadatavoucher.length()>0){
					String sales_payment_id = Helper.generateId("sales_payment_id"); 
					JSONObject ddataobject = jadatavoucher.getJSONObject(0);
					
					if(ddataobject.getInt("total")>0){
						joxdata = new JSONObject();
						joxdata.put("sales_payment_id", sales_payment_id);
						joxdata.put("code", code);
						joxdata.put("payment_type", "voucher");
						joxdata.put("reference_number", ddataobject.get("reference_number"));
						joxdata.put("bank_id", ddataobject.get("bank_id"));
						joxdata.put("tips", ddataobject.get("tips"));
						joxdata.put("discount", ddataobject.get("discount"));
						joxdata.put("tax", ddataobject.get("tax"));
						joxdata.put("total", ddataobject.get("total"));
						joxdata.put("time_sales", time_sales);
						joxdata.put("created_by", Api.getUid());
						jadata.put(joxdata);
					}
				}
				jodata = new JSONObject();
				jodata.put("data",jadata);
				jodata.put("sales_id",sales_id);
				jodata.put("table_id",table_id);
				Log.i("DATA-PAID", jodata.toString());
				if(Helper.isJSONValid(jodata.toString())){
					Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), sales_id+".json",jodata.toString(), "order","paid");	
				}
				else{
					code = "failed";
				}
			}
				
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}			
		
		return code;
	}
	
	public static ArrayList<HashMap<String, String>> getOrderTemp(String sales_id){
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String,String>>();
		ArrayList<HashMap<String, String>>dataFileArrayHistory = Helper.checkListFileOrder(Environment.getExternalStorageDirectory(), "orderhistory", "temp", sales_id);
		ArrayList<HashMap<String, String>>dataFileArrayOrder = Helper.checkListFileOrder(Environment.getExternalStorageDirectory(), "order", "temp", sales_id);
		ArrayList<HashMap<String, String>>dataFileArray = new ArrayList<HashMap<String,String>>();
		
		if(dataFileArrayOrder.size()>0){
			dataFileArray.addAll(dataFileArrayOrder);
		}
		try {
			JSONObject jobj = new JSONObject(), jo = new JSONObject(),jodata = new JSONObject();
			JSONArray data = new JSONArray(),datatype = new JSONArray();
			JSONArray jArrayDetail = new JSONArray();
			String json;
			String error = null;
			String foto_menu = null;
			String datajsonobject;
			if(dataFileArray.size()>0){
				for(int i =0;i<dataFileArray.size();i++){
					String jsonfile = Helper.getFileContentDir(dataFileArray.get(i).get("pathfile"));
					Log.i("JSON-FILE", "JSONFILE = "+jsonfile);
					if(Helper.isJSONValid(jsonfile)){	 
						jobj = new JSONObject(jsonfile);
						jobj = new JSONObject(jobj.getString("data"));
						HashMap<String, String>map= new HashMap<String, String>();
						map.put("sales_id", jobj.getString("sales_id"));
						map.put("sales_detail_id", jobj.getString("sales_detail_id"));
						map.put("id", jobj.getString("id"));
						map.put("name", jobj.getString("name"));
						map.put("price", jobj.getString("price"));
						map.put("type",jobj.getString("type"));
						map.put("quantity", jobj.getString("quantity"));
						map.put("discount", jobj.getString("discount"));
						map.put("status", "not confirm");
						tempData.add(map);
					}
				}
			}
		} catch (JSONException e) {
			// TODO: handle exception
			Log.i("ERROR-JSON", e.toString());
		}
		return tempData;
	}
	
	public static ArrayList<HashMap<String, String>> getOrderConfirm(String sales_id){
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String,String>>();
		ArrayList<HashMap<String, String>>dataFileArrayOrder = Helper.checkListFileOrder(Environment.getExternalStorageDirectory(), "order", "detail", sales_id);
		ArrayList<HashMap<String, String>>dataFileArrayHistory = Helper.checkListFileOrder(Environment.getExternalStorageDirectory(), "orderhistory", "detail", sales_id);
		JSONObject jobj = new JSONObject(),jobjOrder = new JSONObject(), jo = new JSONObject(),jodata = new JSONObject();
		JSONArray data = new JSONArray(),datatype = new JSONArray();
		JSONArray jArrayDetail = new JSONArray();	
		try {
			
			if(dataFileArrayOrder.size()>0){
				for(int i =0;i<dataFileArrayOrder.size();i++){
					String jsonfile = Helper.getFileContentDir(dataFileArrayOrder.get(i).get("pathfile"));			
					if(Helper.isJSONValid(jsonfile)){
						jobj = new JSONObject(jsonfile);
						jobj = new JSONObject(jobj.getString("data"));
						HashMap<String, String>map= new HashMap<String, String>();
						map.put("sales_id", jobj.getString("sales_id"));
						map.put("sales_detail_id", jobj.getString("sales_detail_id"));
						map.put("id", jobj.getString("id"));
						map.put("name", jobj.getString("name"));
						map.put("price", jobj.getString("price"));
						map.put("quantity", jobj.getString("quantity"));
						map.put("type", jobj.getString("type"));
						map.put("discount", jobj.getString("discount"));
						map.put("status", "confirm");
						map.put("choicesplit", "0");
						map.put("qtysplit", "0");
						tempData.add(map);
					}
					
				}
			}
			
			if(dataFileArrayHistory.size()>0){
				for(int i =0;i<dataFileArrayHistory.size();i++){
					String jsonfile = Helper.getFileContentDir(dataFileArrayHistory.get(i).get("pathfile"));
					if(Helper.isJSONValid(jsonfile)){
						jobj = new JSONObject(jsonfile);
						jobj = new JSONObject(jobj.getString("data"));
						int quantity = 0;
						for(int a=0;a<tempData.size();a++){
							if(jobj.getString("sales_detail_id").equals(tempData.get(a).get("sales_detail_id"))){
								//jika ada ubah qty
								quantity = jobj.getInt("quantity") + Integer.valueOf(tempData.get(a).get("quantity"));
								tempData.get(a).put("quantity", String.valueOf(quantity));
							}
						}
						if(quantity<1){
							//jika tidak ada tambah
							HashMap<String, String>map = new HashMap<String, String>();
							map.put("sales_id", jobj.getString("sales_id"));
							map.put("sales_detail_id", jobj.getString("sales_detail_id"));
							map.put("id", jobj.getString("id"));
							map.put("name", jobj.getString("name"));
							map.put("price", jobj.getString("price"));
							map.put("quantity", jobj.getString("quantity"));
							map.put("type", jobj.getString("type"));
							map.put("discount", jobj.getString("discount"));
							map.put("status", "confirm");
							map.put("choicesplit", "0");
							map.put("qtysplit", "0");
							tempData.add(map);
						}
						
					}
				}
			}
			
		} catch (JSONException e) {
			// TODO: handle exception
			Log.i("ERROR-JSON", e.toString());
		}
		return tempData;
	}
	
	public static ArrayList<HashMap<String, String>> getListOrderDetail(String sales_id){
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String,String>>();
		try{
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray jArrayDetail = new JSONArray();
			String json = "";
			String error = null;
			String foto_menu = null;
			String name_file = sales_id+".json";
			File file= new File(Helper.getDirDboxOutFile()+"/listorderdetail/");
			json = Helper.getFileContent(file, name_file);
			
			if (Helper.isJSONValid(json))
			{
				data = new JSONArray(json);
				for(int i = 0; i < data.length(); i++){
					JSONObject d = data.getJSONObject(i);
					HashMap<String, String>  map = new HashMap<String, String>();
					map.put("sales_id", d.getString("sales_id"));
					map.put("id", d.getString("id"));
					map.put("name", d.getString("name"));
					map.put("price", d.getString("price"));
					map.put("quantity", d.getString("quantity"));
					map.put("status", "confirm");
					map.put("photo", d.getString("photo"));
					tempData.add(map);
				}
			}
    	}catch (JSONException e) { 
			Log.i("ERROR-JSON",e.toString());
		}
		return tempData;
	}
	
	public static void cancelOrder(String tableid,String salesid,String notes){
		try{
			ArrayList<HashMap<String, String>>datamember = new ArrayList<HashMap<String,String>>();
			ArrayList<HashMap<String, String>>datatotal = new ArrayList<HashMap<String,String>>();
			
			JSONObject param = new JSONObject(), jobj = new JSONObject(), jo = new JSONObject();
			String json;
			String dataenc = null;
			jo.put("sales_id", salesid);
			jo.put("note", notes);
			jo.put("table_id", tableid);
			jobj.put("data", jo.toString());
			json = jobj.toString();
			String filename = salesid+"_"+tableid+".json";
			if(Helper.isJSONValid(json)){
				Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), filename, json, "order", "cancelnew");
				TableJson.setUpdateTables(salesid, tableid, "available", datamember,datatotal);
				Helper.deleteFileOutgoing(Environment.getExternalStorageDirectory(), salesid+".json", "order", "new");
			}
    	}catch (JSONException e) { 
			Log.i("JSON-ERROR-CANCEL-NEW-ORDER", e.toString());
		}
	}
	
	public static void cancelOrderTemp(String salesid,String salesdetailid,String id){
		try{
			ArrayList<HashMap<String, String>>datamember = new ArrayList<HashMap<String,String>>();
			JSONObject param = new JSONObject(), jobj = new JSONObject(), jo = new JSONObject();
			String json;
			String dataenc = null;
			String filename = salesid+"_"+salesdetailid+"_"+id+".json";
			Helper.deleteFileOutgoing(Environment.getExternalStorageDirectory(), filename, "order", "temp");
    	}catch (Exception e) { 
			Log.i("ERROR-CANCEL-TEMP-ORDER", e.toString());
		}
	}
	
	public static String changeTable(String table_no,String table_id,String sales_id,ArrayList<HashMap<String, String>>datamember,ArrayList<HashMap<String, String>>datatotal){
		String strreturn = "failed";
		try{
			JSONObject param = new JSONObject(), jobj = new JSONObject(), jo = new JSONObject();
			String json;
			String dataenc = null;
			jo.put("sales_id", sales_id);
			jo.put("table_id", table_id);
			jobj.put("data", jo.toString());
			json = jobj.toString();
			
			String tableidtemp = "";
			if(Helper.isJSONValid(json)){
				boolean chekfile = Helper.checkFile(Environment.getExternalStorageDirectory(), "order", sales_id+".json", "new");
				if(chekfile){
					String jsondata = Helper.getFileContentOutgoing(Environment.getExternalStorageDirectory(), sales_id+".json", "order", "new");
					if(Helper.isJSONValid(jsondata)){
						param = new JSONObject(jsondata);
						param = new JSONObject(param.getString("data"));
						tableidtemp = param.getString("table_id");
						param.put("table_no", table_no);
						param.put("table_id", table_id);
						String member_id = "";
						String member_name = "";
						String member_email = "";
						String member_phone = "";
						
						if(datamember.size()>0){
							member_id = datamember.get(0).get("member_id");
							member_name = datamember.get(0).get("member_name");
							member_email = datamember.get(0).get("member_email");
							member_phone = datamember.get(0).get("member_phone");
						}
						
						setOrder(sales_id,table_id, table_no, "", member_id, member_name, member_email, member_phone);
						TableJson.setUpdateTables(sales_id, tableidtemp, "available", new ArrayList<HashMap<String,String>>(),new ArrayList<HashMap<String,String>>());
						chekfile = Helper.checkFile(Environment.getExternalStorageDirectory(), "order",tableidtemp+".json", "changetable");
						if(chekfile){
							Helper.deleteFileOutgoing(Environment.getExternalStorageDirectory(), tableidtemp+".json", "order","changetable");
						}
					}
				}
				String filename = table_id+".json";
				Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), filename, json, "order", "changetable");
				TableJson.setUpdateTables(sales_id, table_id, "not available", datamember,new ArrayList<HashMap<String,String>>());
				strreturn = "success";
			}
		} catch (JSONException e) {
			// TODO: handle exception
		}
		return strreturn;
	}
	
	public static ArrayList<HashMap<String, String>> getListOrder(String sales_id){
		ArrayList<HashMap<String, String>>tempData = new ArrayList<HashMap<String,String>>();
		ArrayList<HashMap<String, String>>dataTable = new ArrayList<HashMap<String,String>>();
		
		try{
			JSONObject param = new JSONObject(), jobj = new JSONObject(), jo = new JSONObject();
			String json;
			String dataenc = null;
			dataTable = TableJson.getNotAvailableTable();
			
			for(int i = 0; i < dataTable.size(); i++){
				HashMap<String, String>  map = new HashMap<String, String>();
				if(!sales_id.equals(dataTable.get(i).get("sales_id"))){
					map.put("table_id", dataTable.get(i).get("table_id"));
					map.put("no", dataTable.get(i).get("no"));
					map.put("queue", dataTable.get(i).get("queue"));
					map.put("status", dataTable.get(i).get("status"));
					map.put("total_price", dataTable.get(i).get("total_price"));
					map.put("total_menu", dataTable.get(i).get("total_menu"));
					map.put("member_name", dataTable.get(i).get("member_name"));
					map.put("sales_id", dataTable.get(i).get("sales_id"));
					map.put("status", "0");
					tempData.add(map);
				}
			}
			
			Log.i("TEMP-DATA", tempData.toString());
    	}catch (Exception e) { 
			Log.i("GET-LIST-ORDER", e.toString());
		}
		return tempData;
	}
	
	public static String setMergeTransaction(String sales_id,ArrayList<HashMap<String, String>> dataArrayOrderMenu){
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		
		try{
			ArrayList<HashMap<String, String>> dataDetailMenu = new ArrayList<HashMap<String, String>>();
			ArrayList<HashMap<String, String>> dataMember = new ArrayList<HashMap<String, String>>();
			
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray jArrayDetail = new JSONArray();
			String json;
			String error = null;
			String foto_menu = null;
			boolean checkfile = false;
			for(int i=0;i<dataArrayOrderMenu.size();i++){
				JSONObject joArrayDetail = new JSONObject();
				if(dataArrayOrderMenu.get(i).get("status").equals("1")){
					joArrayDetail.put("sales_id",dataArrayOrderMenu.get(i).get("sales_id"));
					jArrayDetail.put(joArrayDetail);
					dataDetailMenu = getOrderConfirm(dataArrayOrderMenu.get(i).get("sales_id"));
					for(int a=0;a<dataDetailMenu.size();a++){
						String sales_id_temp = dataDetailMenu.get(a).get("sales_id");
						String filenamedelete = sales_id_temp+"_"+dataDetailMenu.get(a).get("sales_detail_id")+"_"+dataDetailMenu.get(a).get("id")+".json";
						String filenamesave = sales_id+dataDetailMenu.get(a).get("sales_detail_id")+"_"+dataDetailMenu.get(a).get("id")+".json";
						ArrayList<HashMap<String, String>>dataArrayMenu = new ArrayList<HashMap<String,String>>();
						HashMap<String, String>mapMenu = new HashMap<String, String>();
						mapMenu.put("sales_id", sales_id);
						mapMenu.put("sales_detail_id",dataDetailMenu.get(a).get("sales_detail_id") );
						mapMenu.put("id", dataDetailMenu.get(a).get("id"));
						mapMenu.put("name", dataDetailMenu.get(a).get("name"));
						mapMenu.put("price", dataDetailMenu.get(a).get("price"));
						mapMenu.put("menu_type", dataDetailMenu.get(a).get("type"));
						mapMenu.put("quantity", dataDetailMenu.get(a).get("quantity"));
						mapMenu.put("discount", dataDetailMenu.get(a).get("discount"));
						dataArrayMenu.add(mapMenu);
						setOrderTemp(sales_id, dataDetailMenu.get(a).get("sales_detail_id"),dataArrayMenu);
						confirmOrder(sales_id);
						
						checkfile = Helper.checkFile(Environment.getExternalStorageDirectory(), "order", filenamedelete, "detail");
						if(checkfile){
							Helper.deleteFileOutgoing(Environment.getExternalStorageDirectory(), filenamedelete, "order", "detail");
						}
						
						checkfile = Helper.checkFile(Environment.getExternalStorageDirectory(), "order", filenamedelete, "temp");
						if(checkfile){
							Helper.deleteFileOutgoing(Environment.getExternalStorageDirectory(), filenamedelete, "order", "temp");
						}	
					}
					checkfile = Helper.checkFile(Environment.getExternalStorageDirectory(), "order", dataArrayOrderMenu.get(i).get("sales_id")+".json", "new");
					if(checkfile){
						Helper.deleteFileOutgoing(Environment.getExternalStorageDirectory(), dataArrayOrderMenu.get(i).get("sales_id")+".json","order", "new");
					}
					TableJson.setUpdateTables(dataArrayOrderMenu.get(i).get("sales_id"),dataArrayOrderMenu.get(i).get("table_id"), "available", dataMember,new ArrayList<HashMap<String,String>>());
				}
			}
			
			jobj.put("sales_id", sales_id);
			jobj.put("data", jArrayDetail);
			json = jobj.toString();
			Log.i("MERGE", json);
			if(Helper.isJSONValid(json)){
				Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), sales_id+".json", json, "order", "merge");
			}
			
    	}catch (JSONException e) { 
			Log.i("JSON-ERROR-MERGE-TRANSACTION", e.toString());
		}
		return null;
	}
	
	public static String setSplitTransaction(String table_no,String table_id,String sales_id,ArrayList<HashMap<String, String>> dataArrayOrderMenu){
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		try{
			ArrayList<HashMap<String, String>> dataMember = new ArrayList<HashMap<String, String>>();
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray jArrayDetail = new JSONArray();
			String json;
			String error = null;
			String foto_menu = null;
			
			for(int i=0;i<dataArrayOrderMenu.size();i++){
				JSONObject joArrayDetail = new JSONObject();
				if(dataArrayOrderMenu.get(i).get("choicesplit").equals("1")){
					String salesJson = setOrder("",table_id,table_no, "","", "","", "");
					JSONObject josales = new JSONObject(salesJson);
					String salesIdbef = josales.getString("sales_id");
					
					ArrayList<HashMap<String, String>>dataArrayMenu = new ArrayList<HashMap<String,String>>();
					HashMap<String, String>mapMenu = new HashMap<String, String>();
					mapMenu.put("sales_id", salesIdbef);
					mapMenu.put("id", dataArrayOrderMenu.get(i).get("id"));
					mapMenu.put("name", dataArrayOrderMenu.get(i).get("name"));
					mapMenu.put("price", dataArrayOrderMenu.get(i).get("price"));
					mapMenu.put("menu_type", dataArrayOrderMenu.get(i).get("type"));
					mapMenu.put("quantity", dataArrayOrderMenu.get(i).get("qtysplit"));
					mapMenu.put("discount", dataArrayOrderMenu.get(i).get("discount"));
					dataArrayMenu.add(mapMenu);
					
					joArrayDetail.put("id",dataArrayOrderMenu.get(i).get("id"));
					joArrayDetail.put("quantity",dataArrayOrderMenu.get(i).get("qtysplit"));
					jArrayDetail.put(joArrayDetail);
					
					HashMap<String, String>mapMenuBef = new HashMap<String, String>();
					int total_quantity = Integer.valueOf(dataArrayOrderMenu.get(i).get("quantity")) - Integer.valueOf(dataArrayOrderMenu.get(i).get("qtysplit")) ;
					if(total_quantity>=1){
						dataArrayOrderMenu.get(i).put("quantity", String.valueOf(total_quantity));
						dataArrayOrderMenu.get(i).remove("qtysplit");
						dataArrayOrderMenu.get(i).remove("choicesplit");
						dataArrayOrderMenu.get(i).remove("status");
						dataArrayOrderMenu.get(i).put("split", "1");
						mapMenuBef = dataArrayOrderMenu.get(i);
						JSONObject joOrderMenu = new JSONObject(mapMenuBef);
						JSONObject joConfrimOrderMenu = new JSONObject();
						joConfrimOrderMenu.put("data", joOrderMenu.toString());
						Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), sales_id+"_"+dataArrayOrderMenu.get(i).get("sales_detail_id")+"_"+dataArrayOrderMenu.get(i).get("id")+".json", joConfrimOrderMenu.toString(),"order","detail");	
					}
					else{
						boolean checkfile = Helper.checkFile(Environment.getExternalStorageDirectory(), "order", sales_id+"_"+dataArrayOrderMenu.get(i).get("sales_detail_id")+"_"+dataArrayOrderMenu.get(i).get("id")+".json", "detail");
						if(checkfile){
							Helper.deleteFileOutgoing(Environment.getExternalStorageDirectory(), sales_id+"_"+dataArrayOrderMenu.get(i).get("sales_detail_id")+"_"+dataArrayOrderMenu.get(i).get("id")+".json","order", "detail");
						}
						checkfile = Helper.checkFile(Environment.getExternalStorageDirectory(), "orderhistory", sales_id+"_"+dataArrayOrderMenu.get(i).get("sales_detail_id")+"_"+dataArrayOrderMenu.get(i).get("id")+".json", "detail");
						if(checkfile){
							Helper.deleteFileOutgoing(Environment.getExternalStorageDirectory(),  sales_id+"_"+dataArrayOrderMenu.get(i).get("sales_detail_id")+"_"+dataArrayOrderMenu.get(i).get("id")+".json","order", "detail");
						}
					}
					tasksplit task= new tasksplit(salesIdbef, dataArrayOrderMenu.get(i).get("id"), dataArrayMenu);
					task.execute();
					TableJson.setUpdateTables(salesIdbef, table_id, "not available", dataMember,new ArrayList<HashMap<String,String>>());
				}
			}
		
			jobj.put("table_id", table_id);
			jobj.put("sales_id", sales_id);
			jobj.put("data", jArrayDetail);
			
			Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), sales_id+".json",jobj.toString(), "order", "split");
			
			
    	}catch (JSONException e) { 
			
		}
		return null;
	}
	
	public static void setStatusTable(){
		ArrayList<HashMap<String, String>>dataStatusTable = new ArrayList<HashMap<String,String>>();
		dataStatusTable = TableJson.getNotAvailableTableId();
		try {
			JSONObject jo= new JSONObject();
			JSONArray ja= new JSONArray();
			if(dataStatusTable.size()>0){
				for(int i=0;i<dataStatusTable.size();i++){
					JSONObject d= new JSONObject();
					d.put("table_id", dataStatusTable.get(i).get("table_id"));
					d.put("sales_id", dataStatusTable.get(i).get("sales_id"));
					ja.put(d);
				}
				jo.put("data", ja);
				Log.i("DATA-STATUS-TABLE", jo.toString());
				Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), "statustable.json", jo.toString(), "order", "tablestatus");
			}
		} catch (JSONException e) {
			// TODO: handle exception
			Log.i("ERROR-JSON-STATUS-TABLE", e.toString());
		}
	}
	
	public static void saveTable(String table_id,String table_no,String capacity,String imgname,ImageView imgview){
		try {
			JSONObject jo = new JSONObject(),joxdata = new JSONObject();
			long time = System.currentTimeMillis()/1000;
			jo.put("table_id",table_id);
			jo.put("no",table_no);
			jo.put("capacity",capacity);
			jo.put("photo",imgname);
			jo.put("time_created", time);
			jo.put("created_by", Api.getUid());
			jo.put("status", "available");
			joxdata.put("data", jo);
			Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), table_id+".json", joxdata.toString(), "table","data");
			Helper.saveImageFileOutgoing(Environment.getExternalStorageDirectory(), imgname, imgview, "table", "image");
		} catch (JSONException e) {
			// TODO: handle exception
			Log.i("ERROR-SAVE-IMAGE", e.toString());
		}
	}
	
}
