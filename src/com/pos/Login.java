package com.pos;

import com.lib.Helper;
import com.pos.R;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends Activity {
    EditText txtUsername, txtPassword;
    Button btnLogin;
    Context mContext;
   private SessionManager session;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
		setContentView(R.layout.login);
        session = new SessionManager(getApplicationContext());               
        txtUsername = (EditText) findViewById(R.id.txtUsername);
        txtPassword = (EditText) findViewById(R.id.txtPassword);
        btnLogin = (Button) findViewById(R.id.btnLogin);
        
        int w  = getResources().getDisplayMetrics().widthPixels;
        int sWidth  = (w/4) + (w/2);
        txtUsername.getLayoutParams().width=sWidth;
        txtPassword.getLayoutParams().width=sWidth;
        btnLogin.getLayoutParams().width=sWidth;
        
        if(session.isLoggedIn()==false){
        	if(Api.registerAppId()==true){
        		session.createAppIdSession(Api.resultAppId());
        	}  
        }
        
        
		btnLogin.setOnClickListener(new View.OnClickListener() {
        	@Override
            public void onClick(View arg0) {
            	String username = txtUsername.getText().toString();
            	String password = txtPassword.getText().toString();
            	
                if(username.trim().length() > 0 && password.trim().length() > 0){
                	if(Chilkat.ping()){
                		//Log.i("LOGIN", String.valueOf(Api.login(username, password)));
	                	if (Api.login(username, password,Login.this) == 1) {
	                		Api.getProfile();
	                		session.createLoginSession(
	                        		Api.puserid, 
	                        		Api.pusername, 
	                        		Api.pname, 
	                        		Api.pemail, 
	                        		Api.pimage,
	                        		Api.pbranchid,
	                        		Api.pclientid,
	                        		Api.pcode,
	                        		Api.plogo,
	                        		Api.paddress,
	                        		Api.pphone,
	                        		Api.pnameclient,
	                        		Api.pfax
	                            );
	                            Intent i = new Intent(Login.this, Welcome.class);
	                            overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
	                            startActivity(i);
	                            finish();
	                	}else Toast.makeText(Login.this, "Login failed", Toast.LENGTH_LONG).show();
	                } else Toast.makeText(Login.this, "Login failed", Toast.LENGTH_LONG).show();
                }
                else{
                	Toast.makeText(Login.this, Helper.getAlertNoConnection(), Toast.LENGTH_LONG).show();
                }
        	}
        });
        
    }
    
    @Override
    public void onBackPressed() {
		finish();
    }
}