package com.pos;
import java.io.Console;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import com.adapter.ListChartAdapter;
import com.adapter.ListConfirmAdapter;
import com.adapter.ListConfirmAdapterSplit;
import com.adapter.ListMenuCashierAdapter;
import com.adapter.TableConfirmAdapter;
import com.astuetz.PagerSlidingTabStrip;
import com.asyntask.bayarDialog;
import com.asyntask.cancelOrderTemp;
import com.asyntask.confirmAct;
import com.asyntask.confirmDialog;
import com.asyntask.listMenu;
import com.asyntask.mergeAct;
import com.asyntask.mergeDialog;
import com.asyntask.printBill;
import com.asyntask.printOrder;
import com.asyntask.splitAct;
import com.asyntask.splitDialog;
import com.asyntask.totalOrder;
import com.dialog.DialogDiskon;
import com.dialog.DialogPayment;
import com.dialog.DialogTip;
import com.json.CategoryJson;
import com.json.ListChartDataJson;
import com.json.MenuJson;
import com.lib.Helper;
import com.lib.Print;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelSlideListener;
import com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelState;

import android.R.menu;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Debug;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.format.Time;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;

@SuppressLint("NewApi")
public class Catalogue extends ActionBarActivity {
	
	public PagerSlidingTabStrip tabs_chart;
	public PagerSlidingTabStrip tabs;
	public static ViewPager pager_chart,pager;
	public PagerAdapterChart adapter_chart;
	private MyPagerAdapter adapter;
	private SlidingUpPanelLayout mLayout;
	public static Context getcontext;
	private static Catalogue sMainActivity;
	TextView txt_total_harga,txt_total_menu,txt_title_list_order,txt_subtotal_list,txt_diskon_list,txt_total_harga_list,txt_total_belum_confirm,txt_total_confirm;
	TextView txt_menu_title,txt_paket_title;
	Button btn_confirm_order,btn_print_menu,btn_buy_menu,btn_cancel,btn_diskon,btn_merge,btn_split,btn_print_bill;
	LinearLayout layout_total_menu,layout_slide,layout_list,layout_cashier;
	public LinearLayout layoutslideup,layout_title;
	private LinearLayout layout_menu_title,layout_paket_title;
	public ArrayList<HashMap<String, String>> dataOrder,dataOrderConfirm,dataTable,dataArrayMember;
	public ArrayList<HashMap<String, String>> dataArrayOrderConfirm = new ArrayList<HashMap<String, String>>();
	public ArrayList<HashMap<String, String>> dataArrayOrderNotConfirm = new ArrayList<HashMap<String, String>>();
	private int gltotalconfirm,gltotalmenuconfirm;
	
	RelativeLayout layout_total_harga;
	public String sales_id,quee,table_id,table_no;
	int dataPosition = 0;
	
	
	private byte Align_LEFT = (byte)0x30; //������
	private byte Align_CENTER = (byte)0x31;//����
	private byte Align_RIGHT = (byte)0x32;
	private final static byte B_ESC = (byte) 0x1B;
	//lines feed
	private byte[] lfs = new byte[]{0x1B,'d', 5};
	//cut paper
	private byte[] cut = new byte[]{0x1B,'i'};
	private Print print;
	//-----------asyntask----------------//
	private confirmDialog taskConfirmDialog;
	private confirmAct taskConfirmAct;
	private bayarDialog taskBayarDialog;
	private mergeDialog taskMergeDialog;
	private mergeAct taskmergeAct;
	private splitDialog taskSplitDialog;
	private splitAct taskSplitAct;
	private printOrder taskprintOrder;
	private printBill taskprintbill;
	
	private int pageMargin;
	private int title;
	public String menu_type;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.catalogue);
		sMainActivity = this;
		getcontext = Catalogue.this;
		tabs = (PagerSlidingTabStrip) findViewById(R.id.tabs);
		pager = (ViewPager) findViewById(R.id.pager);
		tabs_chart = (PagerSlidingTabStrip)findViewById(R.id.tab_chart);		
		pager_chart = (ViewPager) findViewById(R.id.pager_chart);
		if(CategoryJson.getCount()>0){
			adapter_chart = new PagerAdapterChart(getSupportFragmentManager());
			pager_chart.setAdapter(adapter_chart);
			pageMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 2, getResources()
					.getDisplayMetrics());
			pager_chart.setPageMargin(pageMargin);
			tabs_chart.setShouldExpand(true);
			tabs_chart.setViewPager(pager_chart);
		}
		else{
			 Toast.makeText(Catalogue.this, "Data Category Belum Tersedia", Toast.LENGTH_LONG).show();
			 Intent i = new Intent(Catalogue.this, Main.class);
             overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
             startActivity(i);
             finish();
		}
		
		mLayout = (SlidingUpPanelLayout)findViewById(R.id.sliding_layout);
		txt_total_harga = (TextView)findViewById(R.id.txt_total_harga);
		txt_total_menu = (TextView)findViewById(R.id.txt_total_menu);
		txt_menu_title = (TextView)findViewById(R.id.txt_menu_title);
		txt_paket_title = (TextView)findViewById(R.id.txt_paket_title);
		btn_confirm_order = (Button)findViewById(R.id.btn_confirm_menu);
		btn_cancel = (Button)findViewById(R.id.btn_cancel_order);
		btn_print_menu = (Button)findViewById(R.id.btn_print_menu);
		layout_total_harga = (RelativeLayout)findViewById(R.id.layout_total_harga);
		layout_total_menu = (LinearLayout)findViewById(R.id.layout_total_menu);
		layout_slide = (LinearLayout)findViewById(R.id.layout_slide);
		layout_list = (LinearLayout)findViewById(R.id.layout_list);
		layoutslideup = (LinearLayout)findViewById(R.id.layoutslideup);
		layout_cashier = (LinearLayout)findViewById(R.id.layout_cashier);
		layout_title = (LinearLayout)findViewById(R.id.layout_title);
		layout_menu_title = (LinearLayout)findViewById(R.id.layout_menu_title);
		layout_paket_title = (LinearLayout)findViewById(R.id.layout_paket_title);
		txt_subtotal_list = (TextView)findViewById(R.id.txt_subtotal_list);
		txt_diskon_list = (TextView)findViewById(R.id.txt_diskon_list);
		txt_total_harga_list = (TextView)findViewById(R.id.txt_total_harga_list);
		txt_total_belum_confirm = (TextView)findViewById(R.id.txt_total_belum_confirm);
		txt_total_confirm = (TextView)findViewById(R.id.txt_total_confirm);
		btn_buy_menu = (Button)findViewById(R.id.btn_buy);
		btn_diskon = (Button)findViewById(R.id.btn_diskon);
		btn_merge = (Button)findViewById(R.id.btn_merge);
		btn_split = (Button)findViewById(R.id.btn_split);
		btn_print_bill = (Button)findViewById(R.id.btn_print_bill);
		
		layout_cashier.setVisibility(View.GONE);
		
		layout_menu_title.getLayoutParams().width = Helper.getWidthScreen(getApplicationContext())/2;
		layout_paket_title.getLayoutParams().width = Helper.getWidthScreen(getApplicationContext())/2;
		layout_paket_title.setBackgroundColor(Color.LTGRAY);
		
		savedInstanceState = getIntent().getExtras();
		sales_id = savedInstanceState.getString("sales_id");
		table_id = savedInstanceState.getString("table_id");
		quee = savedInstanceState.getString("queue");
		table_no = savedInstanceState.getString("table_no");
		dataArrayMember = (ArrayList<HashMap<String, String>>) savedInstanceState.getSerializable("member");
		
		if(quee == null || quee.equals("0")){
			getSupportActionBar().setTitle("Table No : "+table_no);
		}
		else{
			getSupportActionBar().setTitle("No Antrian : "+quee);
		}
		
		menu_type = txt_menu_title.getText().toString();
		pager.setPageMargin(pageMargin);
		adapter = new MyPagerAdapter(getSupportFragmentManager(),txt_menu_title.getText().toString(),CategoryJson.getParent());
		pager.setAdapter(adapter);
		tabs.setShouldExpand(true);
		tabs.setViewPager(pager);
		title = 0;
		
		layout_menu_title.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
					menu_type = txt_menu_title.getText().toString();
					adapter.notifyDataSetChanged();
					pager.setAdapter(adapter);
					tabs.setViewPager(pager);
					layout_menu_title.setBackgroundColor(Color.parseColor("#F8F4E3"));
					layout_paket_title.setBackgroundColor(Color.LTGRAY);
			}
		});
		
		layout_paket_title.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
					menu_type = txt_paket_title.getText().toString();
					adapter.notifyDataSetChanged();
					pager.setAdapter(adapter);
					tabs.setViewPager(pager);
					layout_paket_title.setBackgroundColor(Color.parseColor("#F8F4E3"));
					layout_menu_title.setBackgroundColor(Color.LTGRAY);
			}
		});
		
		mLayout.setPanelSlideListener(new PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                Log.i("A", "onPanelSlide, offset " + slideOffset);
               
                if(layout_slide.getHeight()>0){
                	layout_list.getLayoutParams().height = layout_slide.getHeight()/2;
                }
                
                if(layout_slide.getWidth()>0){
                	pager_chart.getLayoutParams().width = layout_slide.getWidth();
                	tabs_chart.getLayoutParams().width = layout_slide.getWidth();
                }
                
                ListMenuChartFragment.list_menu_chart.getLayoutParams().height = Catalogue.pager_chart.getHeight();
                
            }

            @Override
            public void onPanelExpanded(View panel) {
                Log.i("B", "onPanelExpanded");
               
                layout_total_harga.setVisibility(View.GONE);
                layout_total_menu.setVisibility(View.GONE);
                layoutslideup.setVisibility(View.GONE);
            }

            @Override
            public void onPanelCollapsed(View panel) {
                Log.i("C", "onPanelCollapsed");
                layout_total_harga.setVisibility(View.VISIBLE);
                layout_total_menu.setVisibility(View.VISIBLE);
                layoutslideup.setVisibility(View.VISIBLE);
               
            }

            @Override
            public void onPanelAnchored(View panel) {
                Log.i("D", "onPanelAnchored");
               
            }

            @Override
            public void onPanelHidden(View panel) {
                Log.i("E", "onPanelHidden");
               
            }
        });
		
		if(Api.getActivePrinter() == true){
			btn_print_bill.setVisibility(View.VISIBLE);
			btn_print_menu.setVisibility(View.VISIBLE);
		}
		else{
			btn_print_bill.setVisibility(View.GONE);
			btn_print_menu.setVisibility(View.GONE);
		}
		
		//action confirm menu
		btn_confirm_order.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(btn_confirm_order.getText().toString().toUpperCase().equals("KONFIRMASI")){
					AlertDialog.Builder alert = new AlertDialog.Builder(Catalogue.this);
					LayoutInflater li = LayoutInflater.from(Catalogue.this);
					alert.setTitle("List Menu Yang Di Pesan");
					View promptsView = li.inflate(R.layout.grid_confirm, null);
					ListView listviewconfirm = (ListView)promptsView.findViewById(R.id.list_menu_confirm);
					Button btn_confirm_list = (Button)promptsView.findViewById(R.id.btn_confirm_list);
					TextView txt_total_menu_confirm = (TextView)promptsView.findViewById(R.id.txt_total_menu_confirm);
					TextView txt_total_harga_confirm = (TextView)promptsView.findViewById(R.id.txt_total_harga_confirm);
					listviewconfirm.getLayoutParams().height = Helper.getHeightScreen(getApplicationContext())/2;
					alert.setView(promptsView);
					final AlertDialog alertd = alert.create();
					
					taskConfirmDialog = new confirmDialog(alertd, sales_id, listviewconfirm, Catalogue.this,txt_total_menu_confirm,txt_total_harga_confirm);
					taskConfirmDialog.execute();
					
					//print = new Print(Catalogue.getContext());
					btn_confirm_list.setOnClickListener(new View.OnClickListener() {
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
						//	printConfirm();
							taskConfirmAct = new confirmAct(alertd, sales_id);
							taskConfirmAct.execute();
							
						}
					});	
			}
			}
		});
	
		//action btn print menu
		btn_print_menu.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(Api.getPrinterBluetooth() == null){
            		Helper.dialogSettingPrinter(Catalogue.this);
            	}
            	else{
            		print = new Print(getApplicationContext());
            		taskprintOrder = new printOrder(sales_id,Catalogue.this);
            		taskprintOrder.execute();
            	}
				
			}
		});
		
		//action btn buy menu
		btn_buy_menu.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Double Total = Double.valueOf(Helper.toIntData(txt_total_harga.getText().toString()));
				Double Total_Confirm = Double.valueOf(Helper.toIntData(txt_total_confirm.getText().toString()));
				Double disc_total = (Double.valueOf(txt_diskon_list.getText().toString())/100) * Total_Confirm;
				Double tax_total = (Double.valueOf(Api.getTax())/100) * Total_Confirm;
				
				ArrayList<HashMap<String, String>> itemListPay = new ArrayList<HashMap<String, String>>();
				HashMap<String, String> item  = new HashMap<String, String>();
				item.put("name", "Cash");
				
				item.put("discount", String.valueOf(disc_total));
				item.put("tax",String.valueOf( tax_total));
				item.put("tip", "0");
				item.put("total", "0");
				item.put("pay", "0");
				item.put("member_id", dataArrayMember.get(0).get("member_id"));
				item.put("member_name", dataArrayMember.get(0).get("member_name"));
				item.put("member_email", dataArrayMember.get(0).get("member_email"));
				item.put("member_phone", dataArrayMember.get(0).get("member_phone"));
				
				itemListPay.add(item);
				
				HashMap<String, String> item2  = new HashMap<String, String>();
				item2.put("name", "Kredit");
				item2.put("discount", String.valueOf(disc_total));
				item2.put("tax",String.valueOf( tax_total));
				item2.put("tip", "0");
				item2.put("total", "0");
				item2.put("reference", "");
				item2.put("pay", "0");
				item2.put("member_id", dataArrayMember.get(0).get("member_id"));
				item2.put("member_name", dataArrayMember.get(0).get("member_name"));
				item2.put("member_email", dataArrayMember.get(0).get("member_email"));
				item2.put("member_phone", dataArrayMember.get(0).get("member_phone"));
				itemListPay.add(item2);
				taskBayarDialog = new bayarDialog(itemListPay, sales_id, table_id, txt_total_harga, Total, getSupportFragmentManager());
				taskBayarDialog.execute();
			}
		});
		
		//act diskon
		btn_diskon.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Double amnt = Double.valueOf(Helper.toIntData(txt_subtotal_list.getText().toString()));
				if(amnt > 0) {
					DialogDiskon newFragment = new DialogDiskon(txt_diskon_list);
					Bundle bundle = new Bundle();
					bundle.putString("ammount", String.valueOf(amnt));
					newFragment.setArguments(bundle);
					newFragment.show(getSupportFragmentManager(), "dialogqty");
				}
				
			}
		});
		
		//act merge
		btn_merge.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				AlertDialog.Builder alert = new AlertDialog.Builder(Catalogue.this);
				LayoutInflater li = LayoutInflater.from(Catalogue.this);
				alert.setTitle("List Order Yang Akan Di Merge");
				View promptsView = li.inflate(R.layout.grid_table_confirm, null);
				ListView listviewconfirm = (ListView)promptsView.findViewById(R.id.list_table_confirm);
				Button btn_confirm_table = (Button)promptsView.findViewById(R.id.btn_confirm_table);
				//set height listview
				listviewconfirm.getLayoutParams().height = Helper.getHeightScreen(getApplicationContext())/2;
				alert.setView(promptsView);
				final AlertDialog alertd = alert.create();
				taskMergeDialog = new mergeDialog(getApplicationContext(), alertd, sales_id, listviewconfirm, dataOrder);
				taskMergeDialog.execute();
				btn_confirm_table.setOnClickListener(new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						AlertDialog.Builder alert = new AlertDialog.Builder(Catalogue.this);
						alert.setTitle("Konfirmasi Merge");
						alert.setMessage("Apakah Anda Yakin Akan melakukan Merge,Sesuai dengan table yang sudah di pilih ? ");
						alert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int whichButton) {
					            //New Order Action
								taskmergeAct = new mergeAct(alertd, sales_id, dataOrder);
								taskmergeAct.execute();
							}
						   });
						   alert.setNegativeButton("Tidak",
						     new DialogInterface.OnClickListener() {
						       public void onClick(DialogInterface dialog, int whichButton) {
						            //action booking
						              dialog.cancel();
						            }
						        });
						 alert.show();
					}
				});
			}
		});
		
		//btn split
		btn_split.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				AlertDialog.Builder alert = new AlertDialog.Builder(Catalogue.this);
				LayoutInflater li = LayoutInflater.from(Catalogue.this);
				alert.setTitle("List Menu Yang Bisa di Split");
				
				View promptsView = li.inflate(R.layout.layout_split_bill, null);
				ListView listviewconfirm = (ListView)promptsView.findViewById(R.id.list_menu_confirm);
				final Spinner spinertable = (Spinner)promptsView.findViewById(R.id.spiner_table);
				Button btn_confirm_table = (Button)promptsView.findViewById(R.id.btn_confirm_table);
				
				listviewconfirm.getLayoutParams().height = Helper.getHeightScreen(getApplicationContext())/2;
				alert.setView(promptsView);
				final AlertDialog alertd = alert.create();
				taskSplitDialog = new splitDialog(getApplicationContext(), alertd, sales_id, listviewconfirm, spinertable);
				taskSplitDialog.execute();
				
				btn_confirm_table.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						int position = (int) spinertable.getSelectedItemId();
						final String table_id = dataTable.get(position).get("table_id");
						final String table_no = dataTable.get(position).get("no");
						
						AlertDialog.Builder alert = new AlertDialog.Builder(Catalogue.this);
						alert.setTitle("Konfirmasi Split Menu");
						alert.setMessage("Apakah Anda Yakin Akan Mensplit Menu yang dipilih Ke Table "+table_no+" ?");
						alert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
							public void onClick(DialogInterface dialog, int whichButton) {
					            //New Order Action
					        	taskSplitAct = new splitAct(alertd, table_no,table_id, sales_id, dataOrderConfirm);
								taskSplitAct.execute();
							}
						   });
						   alert.setNegativeButton("Tidak",
						     new DialogInterface.OnClickListener() {
						       public void onClick(DialogInterface dialog, int whichButton) {
						            //action booking
						              dialog.cancel();
						            }
						        });
						 alert.show();
					}
				});
			}
		});
		
		
		//act print bill
		btn_print_bill.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(Api.getPrinterBluetooth() == null){
            		Helper.dialogSettingPrinter(Catalogue.this);
            	}
            	else{
            		print = new Print(getApplicationContext());
            		taskprintbill = new printBill(sales_id, Catalogue.this);
            		taskprintbill.execute();
    				
            	}
			}
		});
		if(Api.getCheckConnection().toUpperCase().equals("SUCCESS")){
			dataArrayOrderNotConfirm = Api.getOrderTemp(sales_id);
			dataArrayOrderConfirm = Api.getOrderConfirm(sales_id);
			setDataTotalMenu();
		}
		else{
			if(Helper.checkCashierPrimary(Catalogue.this)){
				dataArrayOrderNotConfirm = Db.getOrderTemp(sales_id);
				dataArrayOrderConfirm = Db.getOrderConfirm(sales_id);
				setDataTotalMenu();
			}
		}
		
	}
	
	public static Catalogue getInstance() {
        return sMainActivity;
	}
	
	public static Context getContext(){
		return getcontext;
	}
	
	public void setDataOrderMenu(){
		adapter_chart.notifyDataSetChanged();
		pager_chart.setAdapter(adapter_chart);
		tabs_chart.setViewPager(pager_chart);
		setDataTotalMenu();
		ArrayList<HashMap<String, String>>datastatus = new ArrayList<HashMap<String,String>>();
		HashMap<String, String>map = new HashMap<String, String>();
		map.put("total_price", String.valueOf(gltotalconfirm));
		map.put("total_menu", String.valueOf(gltotalmenuconfirm));
		map.put("member_id", dataArrayMember.get(0).get("member_id"));
		map.put("member_name", dataArrayMember.get(0).get("member_name"));
		map.put("member_email", dataArrayMember.get(0).get("member_email"));
		map.put("member_phone", dataArrayMember.get(0).get("member_phone"));
		Db.setStatusTables(sales_id, table_id, map);
	}
	
	public void setDataTotalMenu(){
		double harga=0,subtotal = 0,totalharga=0,totalnotconfirm=0,totalconfirm=0;
		double total_diskon = 0,total_tax = 0;
		int totalmenu = 0;
		
		if(Api.getCheckConnection().toUpperCase().equals("SUCCESS")){
			dataArrayOrderConfirm = Api.getOrderConfirm(sales_id);
			dataArrayOrderNotConfirm = Api.getOrderTemp(sales_id);
		}
		else{
			if(Helper.checkCashierPrimary(Catalogue.this)){
				dataArrayOrderConfirm = Db.getOrderConfirm(sales_id);
				dataArrayOrderNotConfirm = Db.getOrderTemp(sales_id);
			}
		}
		
		if(dataArrayOrderNotConfirm.size()>0){
			btn_confirm_order.setVisibility(View.VISIBLE);
			btn_cancel.setVisibility(View.VISIBLE);
			btn_confirm_order.setVisibility(View.VISIBLE);
			layout_cashier.setVisibility(View.GONE);
		}
		else{
			btn_confirm_order.setVisibility(View.GONE);
			btn_cancel.setVisibility(View.GONE);
			if(Helper.checkCashierApp(Catalogue.this)==true||Helper.checkCashierPrimary(Catalogue.this)==true){
				if(dataArrayOrderConfirm.size()>0){
					layout_cashier.setVisibility(View.VISIBLE);
				}
				else{
					layout_cashier.setVisibility(View.GONE);
				}
			}
			
		}
		
		for(int i = 0;i<dataArrayOrderConfirm.size();i++){
			harga = Integer.valueOf( dataArrayOrderConfirm.get(i).get("price"))*Integer.valueOf( dataArrayOrderConfirm.get(i).get("quantity"));
 			totalmenu += Integer.valueOf( dataArrayOrderConfirm.get(i).get("quantity"));
 			subtotal += harga;
 			totalconfirm += harga;
		}
		gltotalconfirm = (int) totalconfirm;
		gltotalmenuconfirm = (int) totalmenu;
		
		
		for(int i = 0;i<dataArrayOrderNotConfirm.size();i++){
			harga = Integer.valueOf( dataArrayOrderNotConfirm.get(i).get("price"))*Integer.valueOf( dataArrayOrderNotConfirm.get(i).get("quantity"));
 			totalmenu += Integer.valueOf( dataArrayOrderNotConfirm.get(i).get("quantity"));
 			subtotal += harga;
 			totalnotconfirm +=harga;
		}
		total_tax = Double.valueOf(Api.getTax())/100;
		total_diskon = subtotal * (Double.valueOf(txt_diskon_list.getText().toString())/100);
		totalharga = (double)((subtotal + (subtotal*total_tax)) - total_diskon);
		
		txt_total_belum_confirm.setText(Helper.toRupiahFormat(String.valueOf(totalnotconfirm)));
		txt_total_confirm.setText(Helper.toRupiahFormat(String.valueOf(totalconfirm)));
		
    	txt_total_harga.setText(Helper.toRupiahFormat(String.valueOf(totalharga)));
		txt_total_menu.setText(String.valueOf(totalmenu));
		txt_total_harga_list.setText(Helper.toRupiahFormat(String.valueOf(totalharga)));
        txt_subtotal_list.setText(Helper.toRupiahFormat(String.valueOf(subtotal)));
    }
	
	
	
	@Override
	public void onBackPressed()
	{
		if(mLayout.getPanelState() != PanelState.EXPANDED){
			Intent i = new Intent(Catalogue.this, Main.class);
			overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
			startActivity(i);
	        finish();
	        Api.phash = "";
		}
		else{
			mLayout.setPanelState(PanelState.COLLAPSED);
		}
		
    }
	
	private class MyPagerAdapter extends FragmentPagerAdapter {

		ArrayList<HashMap<String, String>> cd;
		
		public MyPagerAdapter(FragmentManager fm,String menu_type,ArrayList<HashMap<String, String>> data) {
			super(fm);
			cd = data;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return cd.get(position).get("name");
		}

		@Override
		public int getCount() {
			return cd.size();
		}

		@Override
		public Fragment getItem(int position) {
			return ParentCatalogueFragment.newInstance(position, cd.get(position).get("category_id"),sales_id,dataArrayOrderNotConfirm,dataArrayOrderConfirm,table_id);
		}
	}
	
	public class PagerAdapterChart extends FragmentPagerAdapter {

		ArrayList<HashMap<String, String>> chart;
		
		public PagerAdapterChart(FragmentManager fm) {
			super(fm);
			chart = Helper.getTabMenuChart(sales_id);
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return chart.get(position).get("title_tab");
		}

		@Override
		public int getCount() {
			return chart.size();
		}

		@Override
		public Fragment getItem(int position) {
			return  ListMenuChartFragment.newInstance(position, chart.get(position).get("title_tab"),sales_id,getSupportFragmentManager());
		}

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_setting_search));
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        if(null!=searchManager ) {   
         searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }
        searchView.setIconifiedByDefault(false);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			
			@Override
			public boolean onQueryTextSubmit(String query) {
				// TODO Auto-generated method stub
				//CatalogueFragment.adapter.filter(query);
				//listMenu.getInstance().filter(query);
				return false;
			}
			
			@Override
			public boolean onQueryTextChange(String newText) {
				// TODO Auto-generated method stub
				//CatalogueFragment.adapter.filter(newText);
				//listMenu.adapter.filter(newText);
				return false;
			}
		});
        
        return true;
	}
	
	

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if(Api.getTypeApp().toLowerCase().equals("cashier")){
			if(Api.statusStore().toUpperCase().equals("OPEN")){
				if(id == R.id.action_setting_closestore){
					Helper.dialogConfirmCloseStore(Catalogue.this);
					return true;
				}
			}
		}
		if (id == R.id.action_settings_app) {
			Helper.dialogSettingApp(Catalogue.this);
			return true;
		}
		else if (id== R.id.action_settings_printer){
			Helper.dialogSettingPrinter(Catalogue.this);
			return true;
		}
		else if (id== R.id.action_settings_numcopy_bill){
			Helper.dialogSetNumCopyBill(Catalogue.this);
			return true;
		}
		else if (id == R.id.action_settings_activeprinter){
			Helper.dialogConfirmActivePrinter(Catalogue.this);
			return true;
		}
		else if (id== R.id.action_settings_displaymenu){
			Helper.dialogSettingDisplayDisableMenu(Catalogue.this);
			return true;
		}
		else if (id== R.id.action_settings_complain){
			Helper.dialogComplain(Catalogue.this);
			return true;
		}
		else if (id== R.id.action_settings_logout){
			Helper.dialogConfirmLogout(Catalogue.this);
			return true;
		}
		else{
			
			return true;
		}
		
	}
	
	public void printConfirm(ArrayList<HashMap<String, String>> dataConfirm){
		
		try {
			Time today = new Time(Time.getCurrentTimezone());
			today.setToNow();	
			Long datetime = today.toMillis(true)/1000;
			byte[] lft = new byte[]{B_ESC,(byte)0x61,Align_LEFT};
        	byte[] cntr = new byte[]{B_ESC,(byte)0x61,Align_CENTER};
        	byte[] rght = new byte[]{B_ESC,(byte)0x61,Align_RIGHT};
			//mulai print text
			//header text bisa diambil dari referensi data server atau dari settingan local
        	String l = new String(lft);
        	String r = new String(rght);
        	String c = new String(cntr);
        	String f = new String(lfs);
        	
        	String msg = c;
			msg += "Order\n";			
			msg += table_no;			
			msg += "\n\n";
			msg += r;
			msg += today.format("%d-%m-%Y %H:%M:%S");
			msg += l;
			
			
			for(int i=0; i<dataConfirm.size(); i++) {
				msg += "\n________________________________\n";
				HashMap<String, String> map = new HashMap<String, String>();
				map = dataConfirm.get(i);
				msg += l;
				msg += map.get("name")+"\n";
				msg += r;
				msg += map.get("quantity")+"x";
				msg += r;
				msg += "\n________________________________\n";
			}
			msg += l;
			msg += f;
			
			byte[] data = msg.getBytes();
			print.sendData(data);
			
								
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}
	
	public void printBilll(ArrayList<HashMap<String, String>> dataConfirm){
		
		try {
			Time today = new Time(Time.getCurrentTimezone());
			today.setToNow();	
			Long datetime = today.toMillis(true)/1000;
			byte[] lft = new byte[]{B_ESC,(byte)0x61,Align_LEFT};
        	byte[] cntr = new byte[]{B_ESC,(byte)0x61,Align_CENTER};
        	byte[] rght = new byte[]{B_ESC,(byte)0x61,Align_RIGHT};
			//mulai print text
			//header text bisa diambil dari referensi data server atau dari settingan local
        	String l = new String(lft);
        	String r = new String(rght);
        	String c = new String(cntr);
        	String f = new String(lfs);
        	
        	
        	String msg = c;
        	msg += Api.getNameClient()+"\n";			
			msg += Api.getAddress()+"\n";
			msg += "Phone : \t "+Api.getPhone()+"\n";
			msg += "Fax : \t "+Api.getFax()+"\n";		
			msg += "\n";

			msg += r;
			msg += today.format("%d-%m-%Y %H:%M:%S");
			msg += l;
			msg += "\n________________________________\n";
			int subtotal = 0;
			for(int i=0; i<dataConfirm.size(); i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				map = dataConfirm.get(i);	
				
				msg += l;
				msg += map.get("name")+"\n";
				
				int qty = (int) Math.round(Double.valueOf(map.get("quantity")));
				int price = (int) Math.round(Double.valueOf(map.get("price")));
				int dsc = (int) Math.round(Double.valueOf(map.get("discount")));
				msg += l;
				msg += map.get("quantity")+"x \t "+Helper.formatDecimal(Double.valueOf(map.get("price")))+" \t "+String.valueOf(dsc)+"% \t";				
					
				int totalMenu = (price-((price*dsc)/100))*qty;
				msg += r;
				msg += Helper.formatDecimal(Double.valueOf(totalMenu))+"\n";
				
				subtotal = subtotal+totalMenu;				
			}
			//	msg += l;
			msg += "\n________________________________\n";
			
			msg += l;
			msg += "subTotal:"+Helper.getTabPrint("subTotal:", Helper.formatDecimal(Double.valueOf(subtotal)));
			msg += r;
			msg += String.valueOf( Helper.formatDecimal(Double.valueOf(subtotal)));
			msg += "\n";
			
			
			double tax = Double.valueOf(Api.getTax());
        	double taxtotal = (tax/100) * subtotal;
			int disc =  (int) Math.round(Double.valueOf(txt_diskon_list.getText().toString()));
			double disctotal = (disc/100) * subtotal;
			
			msg += l;
			msg += "Diskon("+String.valueOf(disc)+"%):"+Helper.getTabPrint("Diskon("+disc+"%):",String.valueOf(disctotal));
			msg += r;
			msg += String.valueOf(disctotal);
			msg += "\n";
			
			msg += l;
			msg += "Tax("+tax+"%):"+Helper.getTabPrint("Tax("+tax+"%):",String.valueOf(taxtotal));
			msg += r;
			msg += String.valueOf(taxtotal);
			msg += "\n";
			
			double total = subtotal-((subtotal*disc)/100);
			total = total+(total*(tax/100));
			msg += l;
			msg += "Total:"+Helper.getTabPrint("Total:", Helper.formatDecimal(Double.valueOf(total)));
			msg += r;
			msg += String.valueOf(Helper.formatDecimal(Double.valueOf(total)));
			
			msg += l;
			msg += "\n________________________________\n";
			msg += c;
			msg += "Bill\n";
			msg += table_no;
			msg += f;
			byte[] data = msg.getBytes();
			print.sendData(data);			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	
	
	public void closePrint(){
		try {
			print.closeBT();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
