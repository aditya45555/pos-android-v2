package com.pos;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.dialog.DialogPayment.MyPagerAdapter;
import com.lib.Helper;

import android.content.Context;
import android.opengl.Visibility;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;
import android.widget.LinearLayout.LayoutParams;
import android.widget.Spinner;
import android.widget.TextView;

public class PaymentFragment extends Fragment {
	private static final String ARG_TOTAL = "total_amount";
	private static final String ARG_POSITION = "position";
	private static final String ARG_NAME = "name";
	private static MyPagerAdapter adapter;	
	private static ArrayList<HashMap<String, String>> dataAdapter;
	private static TextView tChange;
	private double total;
	private double changedue;
	private int pos;
	private String name;
	private String cash;
	private String kredit;
	private String ref;
	
	private boolean isFloatCash;
	private boolean isFloatKredit;
	private boolean isCash;
	private boolean isKredit;
	private boolean isRef;
	
	private LinearLayout lc;
	private LinearLayout lk;
	private LinearLayout lr;
	GridView lHolder;
	
	private static TextView tCash;
	private static TextView tKredit;
	private Spinner spinerbank;
	private static TextView tRef;
	Double dchange;
	static Double change;
	private DecimalFormat format;
	private double AllTotalCash = 0 ;
	public static PaymentFragment newInstance(int position, double tAmount, String name, 
			ArrayList<HashMap<String, String>> data, MyPagerAdapter adpt, TextView txtChange) {
		PaymentFragment p = new PaymentFragment();
		
		Bundle b = new Bundle();
		b.putDouble(ARG_TOTAL, tAmount);
		b.putInt(ARG_POSITION, position);
		b.putString(ARG_NAME, name);
		p.setArguments(b);
		
		setAdapter(adpt);
		setDataAdapter(data);
		settChange(txtChange);
		
		return p;
	}		
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		
		total = getArguments().getDouble(ARG_TOTAL);
		pos = getArguments().getInt(ARG_POSITION);
		name = getArguments().getString(ARG_NAME);	
		
		isFloatCash = false;
		isFloatKredit = false;
		isCash = false;
		isKredit = false;
		isRef = false;
		cash = "0";
		kredit = "0";
		changedue = (double)0;
		ref = "";
		format = new DecimalFormat("#,###,###,###.###");
	}
	
	public static void settPref(TextView tRef) {
		
		PaymentFragment.tRef = tRef;
	}
	
	public static TextView gettRef() {
		return tRef;
	}
	
	public static void settChange(TextView tChange) {
		PaymentFragment.tChange = tChange;
	}
	
	public static TextView gettChange() {
		return tChange;
	}
	
	public static void setdChange(double change){
		PaymentFragment.change = Double.valueOf(change);
	}
	
	public static Double getChange() {
		return change;
	}
	
	public static void setDataAdapter(ArrayList<HashMap<String, String>> dataAdapter) {
		PaymentFragment.dataAdapter = dataAdapter;
	}
	
	public static ArrayList<HashMap<String, String>> getDataAdapter() {
		return dataAdapter;
	}
	
	public static void setAdapter(MyPagerAdapter adapter) {
		PaymentFragment.adapter = adapter;
	}
	
	public static TextView getTCash(){
		return tCash;
	}
	
	public static TextView getTKredit(){
		return tKredit;
	}
	
	public static MyPagerAdapter getAdapter() {
		return adapter;
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		LayoutParams params = new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT);
		FrameLayout fl = new FrameLayout(getActivity());
		fl.setLayoutParams(params);
		
		final int margin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, getResources()
				.getDisplayMetrics());
		
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		int wmlp = (int) (metrics.widthPixels * 0.95);
		if(!Helper.isTablet(getActivity()))
			wmlp = (int) (metrics.widthPixels * 0.95);
		
		int wn = (int)Math.round(wmlp*0.80);
		
		LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(wn, LinearLayout.LayoutParams.WRAP_CONTENT);
		
		switch(pos) {
			case 1: {
				isKredit = true;
				View v = inflater.inflate(R.layout.kredit_item, container, false);
				tKredit = (TextView)v.findViewById(R.id.txtkredit);
				tKredit.setLayoutParams(llp);				
				tRef = (TextView)v.findViewById(R.id.txtref);
				tRef.setLayoutParams(llp);
				List<String>listbank = new ArrayList<String>();
				for(int i=0;i<Api.getarraybank().size();i++){
					listbank.add(Api.getarraybank().get(i).get("name"));
				}
				ArrayAdapter<String>adapterbank = new  ArrayAdapter<String>(getActivity(), R.layout.spiner_table,R.id.txtnotable,listbank);
				spinerbank = (Spinner)v.findViewById(R.id.spinnerbank);
				spinerbank.setAdapter(adapterbank);
				spinerbank.setLayoutParams(llp);
				spinerbank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
					@Override
					public void onItemSelected(AdapterView<?> parent,
							View view, int position, long id) {
						// TODO Auto-generated method stub
						getDataAdapter().get(1).put("bank_id", Api.getarraybank().get(position).get("bank_id"));
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {
						// TODO Auto-generated method stub
						
					}
				});
				lk = (LinearLayout)v.findViewById(R.id.layoutkredit);
				lk.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						isKredit = true;
						isCash = false;
						isRef = false;
					}
				});
				
				lr = (LinearLayout)v.findViewById(R.id.layoutref);
				lr.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						isRef = true;
						isKredit = false;
						isCash = false;
					}
				});
				
				initKredit(v);								
				fl.addView(v);
				break;
			}
			default : {
				isCash = true;
				View vcash = inflater.inflate(R.layout.cash_item, container, false);
				tCash = (TextView)vcash.findViewById(R.id.txtcash);
				tCash.setLayoutParams(llp);
				
				lc = (LinearLayout)vcash.findViewById(R.id.layoutcash);
				lc.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						isCash = true;
						isKredit = false;
						isRef = false;
					}
				});
				
				initCash(vcash);
				fl.addView(vcash);
				break;
			}
		}

		return fl;
	}
	
	private void initCash(View v) {
		final ImageView tdel = (ImageView)v.findViewById(R.id.hapuscash);
	    final TextView tsatu = (TextView)v.findViewById(R.id.satu);
	    final TextView tdua = (TextView)v.findViewById(R.id.dua);
	    final TextView ttiga = (TextView)v.findViewById(R.id.tiga);
	    final TextView tempat = (TextView)v.findViewById(R.id.empat);
	    final TextView tlima = (TextView)v.findViewById(R.id.lima);
	    final TextView tenam = (TextView)v.findViewById(R.id.enam);
	    final TextView ttujuh = (TextView)v.findViewById(R.id.tujuh);
	    final TextView tdelapan = (TextView)v.findViewById(R.id.delapan);
	    final TextView tsembilan = (TextView)v.findViewById(R.id.sembilan);
	    final TextView tnol = (TextView)v.findViewById(R.id.nol);
	    final TextView tdblnol = (TextView)v.findViewById(R.id.doublenol);
	    TextView ttitik = (TextView)v.findViewById(R.id.titik);
	    
	    tsatu.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				rubahCash(tsatu.getText().toString());
			}
		});
	    tdua.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				rubahCash(tdua.getText().toString());
			}
		});
	    ttiga.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				rubahCash(ttiga.getText().toString());
			}
		});
	    tempat.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				rubahCash(tempat.getText().toString());
			}
		});
	    tlima.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				rubahCash(tlima.getText().toString());
			}
		});
	    tenam.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				rubahCash(tenam.getText().toString());
			}
		});
	    ttujuh.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				rubahCash(ttujuh.getText().toString());
			}
		});
	    tdelapan.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				rubahCash(tdelapan.getText().toString());
			}
		});
	    tsembilan.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				rubahCash(tsembilan.getText().toString());
			}
		});
	    tnol.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				rubahCash(tnol.getText().toString());
			}
		});
	    tdblnol.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				rubahCash(tdblnol.getText().toString());
			}
		});
	    ttitik.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!isFloatCash) {
					isFloatCash = true;
					cash = cash+".";
					Double hsl = Double.valueOf(cash);
					tCash.setText(format.format(hsl));
				}				
			}
		});	
	    tdel.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(cash.length() > 0) {
					String nn = cash.substring(0, cash.length() -1);
					if(nn.length() == 0)
						nn = "0";
					cash = nn;
					
					isFloatCash = false;
					if(cash.indexOf(".") > -1)
						isFloatCash = true;
					
					
					
					Double hsl = Double.valueOf(cash);
					tCash.setText(format.format(hsl).replace(".", ","));
					getDataAdapter().get(0).put("total", String.valueOf(total));
					getDataAdapter().get(0).put("pay", String.valueOf(cash));
					Double nk = (double)0;
					if(Double.valueOf(getDataAdapter().get(1).get("pay")) > 0){
						getDataAdapter().get(1).put("pay", "0");
						getTKredit().setText("0");
						nk = Double.valueOf(getDataAdapter().get(1).get("pay"));
					}
					dchange = (hsl+nk)-total;
					setdChange(Double.valueOf(String.valueOf(dchange).replace(".","")));
					String nch = format.format(dchange);
					gettChange().setText("Kembali "+nch);
				}
			}
		});
	}
	
	private void initKredit(View v) {
		final ImageView tdel = (ImageView)v.findViewById(R.id.hapuskredit);
		final ImageView tdelR = (ImageView)v.findViewById(R.id.hapusref);
	    final TextView tsatu = (TextView)v.findViewById(R.id.satu);
	    final TextView tdua = (TextView)v.findViewById(R.id.dua);
	    final TextView ttiga = (TextView)v.findViewById(R.id.tiga);
	    final TextView tempat = (TextView)v.findViewById(R.id.empat);
	    final TextView tlima = (TextView)v.findViewById(R.id.lima);
	    final TextView tenam = (TextView)v.findViewById(R.id.enam);
	    final TextView ttujuh = (TextView)v.findViewById(R.id.tujuh);
	    final TextView tdelapan = (TextView)v.findViewById(R.id.delapan);
	    final TextView tsembilan = (TextView)v.findViewById(R.id.sembilan);
	    final TextView tnol = (TextView)v.findViewById(R.id.nol);
	    final TextView tdblnol = (TextView)v.findViewById(R.id.doublenol);
	    TextView ttitik = (TextView)v.findViewById(R.id.titik);
	    
	    tsatu.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isKredit)
					rubahKredit(tsatu.getText().toString());
				if(isRef)
					rubahRef(tsatu.getText().toString());					
			}
		});
	    tdua.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isKredit)
					rubahKredit(tdua.getText().toString());
				if(isRef)
					rubahRef(tdua.getText().toString());
			}
		});
	    ttiga.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isKredit)
					rubahKredit(ttiga.getText().toString());
				if(isRef)
					rubahRef(ttiga.getText().toString());
			}
		});
	    tempat.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isKredit)
					rubahKredit(tempat.getText().toString());
				if(isRef)
					rubahRef(tempat.getText().toString());
			}
		});
	    tlima.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isKredit)
					rubahKredit(tlima.getText().toString());
				if(isRef)
					rubahRef(tlima.getText().toString());
			}
		});
	    tenam.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isKredit)
					rubahKredit(tenam.getText().toString());
				if(isRef)
					rubahRef(tenam.getText().toString());
			}
		});
	    ttujuh.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isKredit)
					rubahKredit(ttujuh.getText().toString());
				if(isRef)
					rubahRef(ttujuh.getText().toString());
			}
		});
	    tdelapan.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isKredit)
					rubahKredit(tdelapan.getText().toString());
				if(isRef)
					rubahRef(tdelapan.getText().toString());
			}
		});
	    tsembilan.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isKredit)
					rubahKredit(tsembilan.getText().toString());
				if(isRef)
					rubahRef(tsembilan.getText().toString());
			}
		});
	    tnol.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isKredit)
					rubahKredit(tnol.getText().toString());
				if(isRef)
					rubahRef(tnol.getText().toString());
			}
		});
	    tdblnol.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isKredit)
					rubahKredit(tdblnol.getText().toString());
				if(isRef)
					rubahRef(tdblnol.getText().toString());
			}
		});
	    ttitik.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!isRef && !isCash) {
					if(!isFloatKredit) {
						isFloatKredit = true;
						kredit = kredit+".";
						Double hsl = Double.valueOf(kredit);
						tKredit.setText(format.format(hsl));
					}
				}							
			}
		});	
	    tdel.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(kredit.length() > 0) {
					String nn = kredit.substring(0, kredit.length() -1);
					if(nn.length() == 0)
						nn = "0";
					kredit = nn;
					
					isFloatKredit = false;
					if(kredit.indexOf(".") > -1)
						isFloatKredit = true;
					Double kreditcash = Double.valueOf(kredit);
					Double totalcash = Double.valueOf(total);
					if(kreditcash<=totalcash){
						getDataAdapter().get(1).put("total", String.valueOf(total));
						getDataAdapter().get(1).put("pay", String.valueOf(kredit));
						if(Double.valueOf(getDataAdapter().get(1).get("pay"))>0){
							AllTotalCash = totalcash - kreditcash;
							getDataAdapter().get(0).put("pay", String.valueOf(AllTotalCash));
							getTCash().setEnabled(false);
							getTCash().setText(String.valueOf(format.format(AllTotalCash)));
						}
						else{
							getTCash().setEnabled(true);
							getTCash().setText(String.valueOf(format.format(totalcash)));
							getDataAdapter().get(0).put("pay", String.valueOf(totalcash));
						}
						Double nc = (double)0;
						if(Double.valueOf(getDataAdapter().get(0).get("pay")) > 0)
							nc = Double.valueOf(getDataAdapter().get(0).get("pay"));
						
						getDataAdapter().get(1).put("pay", String.valueOf(kredit));
						Double hsl = Double.valueOf(kredit);
						dchange = (hsl+nc)-total;
						tKredit.setText(format.format(hsl));
						getDataAdapter().get(1).put("total", String.valueOf(total));
						getDataAdapter().get(1).put("pay", String.valueOf(kredit));
						
						if(Double.valueOf(getDataAdapter().get(0).get("pay")) > 0)
							nc = Double.valueOf(getDataAdapter().get(0).get("pay"));
						
						int dichange = Integer.valueOf(String.valueOf(dchange).replace(".",""));
						setdChange(dichange);
						String nch = format.format(dchange);
						gettChange().setText("Kembali "+nch);
						
					}
					else{
						Toast.makeText(getActivity(), "Untuk Pembayaran Secara Kredit Harus Sama Dengan Total", Toast.LENGTH_LONG).show();
					}
					
					
				}
			}
		});
	    tdelR.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(ref.length() > 0) {
					String nn = ref.substring(0, ref.length() -1);					
					ref = nn;
				}
				else {
					ref = "";
				}
				
				tRef.setText(ref);
			}
		});
	}
	
	private void rubahCash(String value) {
		if(cash.indexOf(".") > -1)
			isFloatCash = true;
		String q = "";
		q = cash+value;
		
		Double nm = Double.parseDouble(q);
		String nl = format.format(nm).replace(".",",");
		cash = nl.replace(",", "");
		tCash.setText(nl);
		
		getDataAdapter().get(0).put("total", String.valueOf(total));
		getDataAdapter().get(0).put("pay", String.valueOf(cash));
		
		Double nk = (double)0;
		if(Double.valueOf(getDataAdapter().get(1).get("pay")) > 0){
			getDataAdapter().get(1).put("pay", "0");
			getTKredit().setText("0");
			nk = Double.valueOf(getDataAdapter().get(1).get("pay"));
		}
		dchange = (nm+nk)-total;
		
		Double dichange = Double.valueOf(String.valueOf(dchange).replace(".",""));
		setdChange(dichange);
		String nch = format.format(dchange);
		gettChange().setText("Kembali "+nch);
	}
	
	private void rubahKredit(String value) {
		if(kredit.indexOf(".") > -1)
			isFloatKredit = true;
		
		String q = kredit+value;
		Double nm = Double.valueOf(q);
		
		String nl = format.format(nm).replace(".",",");
		kredit = nl.replace(",", "");
		Double kreditcash = Double.valueOf(kredit);
		Double totalcash = Double.valueOf(total);
		
		if(kreditcash<=totalcash){
			
			tKredit.setText(format.format(nm));
			getDataAdapter().get(1).put("total", String.valueOf(total));
			getDataAdapter().get(1).put("pay", String.valueOf(kredit));
			
			if(Double.valueOf(getDataAdapter().get(1).get("pay"))>0){
				AllTotalCash = totalcash - kreditcash;
				getDataAdapter().get(0).put("pay", String.valueOf(AllTotalCash));
				getTCash().setEnabled(false);
				getTCash().setText(String.valueOf(format.format(AllTotalCash)));
				
			}
			else{
				getTCash().setEnabled(true);
				getTCash().setText(String.valueOf(format.format(totalcash)));
				getDataAdapter().get(0).put("pay", String.valueOf(totalcash));
			}
			
			Double nc = (double)0;
			if(Double.valueOf(getDataAdapter().get(0).get("pay")) > 0)
				nc = Double.valueOf(getDataAdapter().get(0).get("pay"));
			
			dchange = (nm+nc)-total;
			int dichange = Integer.valueOf(String.valueOf(dchange).replace(".",""));
			setdChange(dichange);
			String nch = format.format(dchange);
			gettChange().setText("Kembali "+nch);
			getDataAdapter().get(1).put("pay", String.valueOf(kredit));
		}
		else{
			Toast.makeText(getActivity(), "Untuk Pembayaran Secara Kredit Harus Sama Dengan Total", Toast.LENGTH_LONG).show();
		}
	}
	
	private void rubahRef(String value) {
		ref = ref+value;		
		tRef.setText(ref);
		int positionspinner = spinerbank.getSelectedItemPosition();
		getDataAdapter().get(1).put("bank_id", Api.getarraybank().get(positionspinner).get("bank_id"));
	}
}
