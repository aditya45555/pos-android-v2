package com.pos;

import android.util.Log;

import com.chilkatsoft.CkByteData;
import com.chilkatsoft.CkCrypt2;
import com.chilkatsoft.CkHttp;
import com.chilkatsoft.CkHttpRequest;
import com.chilkatsoft.CkHttpResponse;
import com.chilkatsoft.CkSsh;
import com.chilkatsoft.CkUpload;
import com.lib.Helper;

public class Chilkat {
	static final String BASE_URL = "http://admin.kaskul.com/api/";
	private static CkHttp shttp = null;
	private static CkHttpResponse resp;
	private static CkSsh ssh = null;
	private static CkHttp http = null;
	private static  CkHttpRequest req = null;
	private static CkUpload ckupload = null;
	private static String getUrl(String relativeUrl) {
        return BASE_URL + relativeUrl;
    }
	
	public static String encryptLogin(String str,String key){
		CkCrypt2 crypt = new CkCrypt2();
		crypt.UnlockComponent("WILLAWCrypt_KM8tJZPHMRLn");
		crypt.put_CryptAlgorithm("aes");
		crypt.put_CipherMode("cbc");
		crypt.put_KeyLength(256);
		crypt.SetEncodedKey(key, "hex");
		crypt.SetEncodedIV("000102030405060708090A0B0C0D0E0F", "hex");
		return crypt.encryptStringENC(str);
	}
	
	private static void sHttpInit() {
		shttp = new CkHttp();
		shttp.UnlockComponent("WILLAWHttp_QxVksXRMURBF");
		shttp.SetRequestHeader("Content-type", "application/json");
		shttp.put_AcceptCharset("");
		shttp.put_UserAgent("");
		shttp.put_AcceptLanguage("");
		shttp.put_AllowGzip(false);
				
		shttp.put_CookieDir("memory");
	    shttp.put_SaveCookies(true);
	    shttp.put_SendCookies(true);
	    
	   
	    
	}
	private static void HttpInit(){
		http = new CkHttp();
		http.UnlockComponent("WILLAWHttp_QxVksXRMURBF");
		req = new CkHttpRequest();
		req.put_HttpVerb("POST");
	    req.put_ContentType("multipart/form-data");
	    req.put_Path("api/insert/inserttableimage");
	}
	public static String SendHttpFile(String filename,String path,String url){
		try {
			if(http==null)
				HttpInit();
			
			req.AddParam("token", Api.getToken());
			req.AddParam("app_id", Api.pappid);
			req.AddParam("filename", filename);
			req.AddFileForUpload2("foto", path,"image/jpg");
			resp = http.SynchronousRequest("admin.kaskul.com", 80, false, req);
			
		} catch (Exception e) {
			// TODO: handle exception
		}
		return resp.bodyStr();
	}
	
	public static String SendHttpGet(String json, String url) {
		if (shttp==null)
			sHttpInit();
		
 		resp = shttp.QuickGetObj(getUrl(url));
 	    if (resp == null )
 	    	return null;

 	    return resp.bodyStr();
	}
	
	public static String SendHttp(String json, String url) {
		if (shttp==null)
			sHttpInit();
		
		resp = shttp.PostJson(getUrl(url), json);
 		
 	    if (resp == null )
 	    	return null;
 	    
		return resp.bodyStr();
	}
	
	public static boolean ping() {
		ssh = new CkSsh();
		ssh.UnlockComponent("WILLAWSSH_5DhsPcaABRn8");
	//  Connect to an SSH server:
	    String hostname;
	    int port;

	    //  Hostname may be an IP address or hostname:
	    hostname = "103.232.30.61";
	    port = 2022;

	    if(!ssh.Connect(hostname,port)) {
	    	ssh.Disconnect();
	    	return false;
	    }

	    //  Wait a max of 5 seconds when reading responses..
	    ssh.put_IdleTimeoutMs(3000);
	    
	    ssh.Disconnect();
		return true;
	}
	
	public static boolean pingTimeOut() {
		ssh = new CkSsh();
		ssh.UnlockComponent("WILLAWSSH_5DhsPcaABRn8");
	//  Connect to an SSH server:
	    String hostname;
	    int port;

	    //  Hostname may be an IP address or hostname:
	    hostname = "103.232.30.61";
	    port = 2022;

	    if(!ssh.Connect(hostname,port)) {
	    	ssh.Disconnect();
	    	return false;
	    }

	    //  Wait a max of 5 seconds when reading responses..
	    ssh.put_IdleTimeoutMs(5000);
	    
	    ssh.Disconnect();
		return true;
	}
	
	
	static {
        System.loadLibrary("chilkat");
    }
}
