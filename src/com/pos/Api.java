package com.pos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.json.MenuJson;
import com.json.TokenJson;
import com.lib.Gps;
import com.lib.Helper;

import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageInstaller.Session;
import android.os.Build;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

public class Api {
	
	private static Api api;
	static String puserid, pname, pusername, pemail, pimage, pclientid,pbranchid;
	public static String pcode,plogo,paddress,pphone,pnameclient,pfax,ptax;
	static String ptoken,pappid;
	static boolean pdisplaydisablemenu,pactiveprinter;
    static String typeapp,pprinterbluetooth,psetactiveprinter,pnumcopybill;
    static String pcheckconnection = "no connection";
    public static String phash = "";
	public static ArrayList<HashMap<String, String>> tables;
	static Helper h =new Helper();
	public static  ArrayList<HashMap<String, String>> arraybank = new ArrayList<HashMap<String,String>>();
	private Api(){

	}
	
	public static Api getInstance() {
		if (api == null)
			api = new Api();
		return api;
	}
	
	public static String getCheckConnection(){
		return pcheckconnection;
	}
	
	
	public static String getUid() {
		return puserid;
	}
	
	public static ArrayList<HashMap<String, String>>  getarraybank(){
		return arraybank;
	}
	
	public static String getUname() {
		return pname;
	}
	public static String getUsername() {
		return pusername;
	}
	public static String getTax(){
		return ptax;
	}
	
	public static String getClientId() {
		return pclientid;
	}
	
	public static String getBranchId() {
		return pbranchid;
	}
	
	public static String getCode() {
		return pcode;
	}
	
	public static boolean disablemenu(){
		return pdisplaydisablemenu;
	}
	
	public static String getLogo() {
		return plogo;
	}
	
	public static String getAddress() {
		return paddress;
	}
	
	public static String getPhone() {
		return pphone;
	}
	
	public static String getNameClient() {
		return pnameclient;
	}
	
	public static String getFax() {
		return pfax;
	}
	
	public static String resultAppId() {
		return pappid;
	}
	
	public static String getTypeApp() {
		return typeapp;
	}
	
	public static void  setTypeApp(String type) {
		typeapp = type;
	}
	
	public static void  setPrinterBluetooth(String printbluetooth) {
		pprinterbluetooth = printbluetooth;
	}
	
	public static void  setDisplayDisableMenu(boolean active) {
		pdisplaydisablemenu = active;
	}
	
	public static Boolean getDisplayDisableMenu(){
		return pdisplaydisablemenu;
	}
	
	public static void  setActivePrinterCheck(String active) {
		psetactiveprinter = active;
	}
	
	public static String getActivePrinterCheck(){
		return psetactiveprinter;
	}
	
	
	public static void  setActivePrinter(boolean active) {
		pactiveprinter = active;
	}
	
	public static void  setNumCopyBill(String active) {
		pnumcopybill = active;
	}
	
	public static String getNumCopyBill() {
		return pnumcopybill;
	}
	
	public static Boolean getActivePrinter(){
		return pactiveprinter;
	}
	
	public static String getPrinterBluetooth() {
		return pprinterbluetooth;
	}
	
	public static String getToken(){
		if(ptoken==null){
			if(getCheckConnection().toUpperCase().equals("SUCCESS")){
				String token = getTokenApi();
				if(!token.toUpperCase().equals("FAILED")){
					ptoken = token;
				}
			}
			else{
				String token = TokenJson.getToken();
				if(!token.toUpperCase().equals("FAILED")){
					ptoken = token;
				}
			}
			
		}
		return ptoken;
	}
	
	public static int login(String email, String password,Context context) {
		
		try {
			JSONObject jobj = new JSONObject();
			jobj.put("email",email);
			String encpassword = password;
			jobj.put("password", encpassword);
			jobj.put("app_id", resultAppId());
			Gps gps = new Gps(context);
			double latitude = -6.8769466666667;
			double longtitude = 107.560625;
			
			if(gps.getLatitude()>0&&gps.getLongitude()>0){
				latitude = gps.getLatitude();
				longtitude = gps.getLongitude();
			}
			
			jobj.put("latitude", latitude);
			jobj.put("longitude", longtitude);
			
			Log.i("CAKTOPA-SEND-JSON",jobj.toString());
			String json = Chilkat.SendHttp(jobj.toString(), "account/login");
			Log.i("CAKTOPA-JSON-LOGIN",json);
			if (Helper.isJSONValid(json))
			{
				JSONObject jo = new JSONObject(json);
				int error = 0;
				try {
					error = jo.getInt("error");
				} catch (JSONException e) {
					error = 0;
				}
				
				if (error==0){
					JSONObject data = new JSONObject(jo.getString("result"));
					//Log.i("CAKTOPA", data.getString("user_id"));
					puserid = data.getString("user_id");
		            pname = data.getString("name");
		            pusername = data.getString("email");
		            pemail = data.getString("email");
		            pclientid = data.getString("client_id");
		            String branchid = data.getString("branch_id");
					pbranchid = branchid;
					String cookie = data.getString("token");
					ptoken = cookie;
					return 1;
				}
			}
			return 0;
		} catch (JSONException e) {
			
		}
		return 0;
	}
	
	public static int getProfile(){
		try {
			
			JSONArray data;
			JSONObject jobj = new JSONObject(),jo = new JSONObject();
			JSONArray params = new JSONArray();
			String json;
			String datajson;
			
			jobj.put("token", getToken());
			jobj.put("app_id", resultAppId());
			Log.i("SEND-JSON-PROFILE",jobj.toString());
			json = Chilkat.SendHttp(jobj.toString(), "account/getprofile");
			Log.i("GET-JSON-PROFILE",json);
			
			if (Helper.isJSONValid(json))
			{
				try {
					jo = new JSONObject(json);
					JSONObject d = new JSONObject(jo.getString("result"));
					plogo = d.getString("logo");
					pnameclient = d.getString("name");
					paddress = d.getString("address");
					pphone = d.getString("phone");
					pfax = d.getString("fax");
					pcode = d.getString("code");
					ptax = d.getString("tax");
					int banksize = 0;
					banksize = d.getJSONArray("bank").length();
					for(int i=0;i<banksize;i++){
						jobj = d.getJSONArray("bank").getJSONObject(i);
						HashMap<String, String>map = new HashMap<String, String>();
						map.put("bank_id", jobj.getString("bank_id"));
						map.put("name", jobj.getString("name"));
						arraybank.add(map);
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				return 1;
			}
			
		} catch (JSONException e) {
			// TODO: handle exception
		}
		return 0;
	}
	

	public static String getProfileFile(){
		try {
			
			JSONArray data;
			JSONObject jobj = new JSONObject(),jo = new JSONObject();
			JSONArray params = new JSONArray();
			String json;
			String datajson;
			
			jobj.put("token", getToken());
			jobj.put("app_id", resultAppId());
			Log.i("SEND-JSON-PROFILE",jobj.toString());
			json = Chilkat.SendHttp(jobj.toString(), "account/getprofile");
			Log.i("GET-JSON-PROFILE",json);
			
			if (Helper.isJSONValid(json))
			{
				return json;
			}
			
		} catch (JSONException e) {
			// TODO: handle exception
		}
		return "";
	}
	
	public static String getTables(){
		try{
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray data = new JSONArray();
			String json;
			String error = null;
			
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			json = Chilkat.SendHttp(jobj.toString(), "order/getAvailableTables");
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				data = jo.getJSONArray("result");
				error = jo.getString("error");
				if (!error.equals("")){
					for(int i = 0; i < data.length(); i++){
						JSONObject d = data.getJSONObject(i);
						HashMap<String, String>  map = new HashMap<String, String>();
						map.put("table_id", d.getString("table_id"));
						map.put("no", d.getString("no"));
						
					}
					return "true";
				}
			}
    	}catch (JSONException e) { 
			
		}
		
		return "failed";
		
	}
	
	public static String setOrder(String tableid,String descorder){
		try{
			JSONObject param = new JSONObject(), jobj = new JSONObject(), jo = new JSONObject();
			String json;
			String dataenc = null;
			param.put("table_id", tableid);
			param.put("desc_order", descorder);
			
			dataenc= param.toString();
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			
			jobj.put("data", dataenc);
			Log.i("SEND_JSON-new-order", jobj.toString());
			json = Chilkat.SendHttp(jobj.toString(), "order/createneworder");
			Log.i("get-json-new-order", json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				String orderid = jo.getString("result");
				
				return orderid;
			}
    	}catch (JSONException e) { 
			
		}
		
		return "failed";
		
	}
	
	public static String cancelOrder(String orderid,String notes){
		try{
			JSONObject param = new JSONObject(), jobj = new JSONObject(), jo = new JSONObject();
			String json;
			String dataenc = null;
			param.put("order_id", orderid);
			param.put("note", notes);
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			
			jobj.put("data", param.toString());
			Log.i("send-CANCELORDER", jobj.toString());
			json = Chilkat.SendHttp(jobj.toString(), "order/cancelneworder");
			Log.i("JSON-CANCELORDER", json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				String result = jo.getString("result");
				return result;
			}
    	}catch (JSONException e) { 
			
		}
		
		return "failed";
		
	}
	
	
	public static String getCategory(){
		try{
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray params = new JSONArray();
			String json;
			String datajson;
			
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			json = Chilkat.SendHttp(jobj.toString(), "order/getcategories");
			//Log.i("PARENT", json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				params = jo.getJSONArray("result");
				datajson = String.valueOf(params);
				return datajson;
			}
    	}catch (JSONException e) {
    		
		}
		
		return "failed";
	}
	
	public static  ArrayList<HashMap<String, String>> getMenuByCategory(String catid,String menu_type){
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		try {
			JSONArray data;
			JSONObject jobj = new JSONObject(),jo = new JSONObject();
			JSONArray params = new JSONArray();
			String json;
			String datajson;
			jo.put("category_id",catid);
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			jobj.put("data", jo.toString());
			
			json = Chilkat.SendHttp(jobj.toString(), "order/getmenubycategory");
			
			Log.i("JSON-MENU",json);
			
			if (Helper.isJSONValid(json))
			{
				try {
					jo = new JSONObject(json);
					data = jo.getJSONArray("result");
					for(int i = 0; i < data.length(); i++){
						JSONObject d = data.getJSONObject(i);
						HashMap<String, String>  map = new HashMap<String, String>();
						if(d.get("menu_type").toString().toUpperCase().equals(menu_type.toUpperCase())){
							if(pdisplaydisablemenu == true){
								map.put("menu_id", d.getString("menu_id"));
								map.put("category_id", d.getString("category_id"));
								map.put("description", d.getString("description"));
								map.put("name", d.getString("name"));
								map.put("price", d.getString("price"));
								map.put("status", d.getString("status"));
								map.put("short_description",d.getString("short_description"));
								map.put("event", d.getString("event"));
								map.put("color", d.getString("color"));
								map.put("bcolor", d.getString("bcolor"));
								map.put("photo", d.getString("photo"));
								tempData.add(map);
							}
							else{
								if(d.getString("status").toUpperCase().equals("AVAILABLE")){
									map.put("menu_id", d.getString("menu_id"));
									map.put("category_id", d.getString("category_id"));
									map.put("description", d.getString("description"));
									map.put("name", d.getString("name"));
									map.put("price", d.getString("price"));
									map.put("status", d.getString("status"));
									map.put("short_description",d.getString("short_description"));
									map.put("event", d.getString("event"));
									map.put("color", d.getString("color"));
									map.put("bcolor", d.getString("bcolor"));
									map.put("photo", d.getString("photo"));
									tempData.add(map);
								}
							}
							
						}
					}
				} catch (JSONException e) {
					e.printStackTrace();
				}
				return tempData;
			}
			
		} catch (JSONException e) {
			// TODO: handle exception
		}
		return tempData;
	}
	
	public static String getMenu(){
		try {
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray params = new JSONArray();
			String json;
			String datajson;
			
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			jobj.put("data", 0);
			
			json = Chilkat.SendHttp(jobj.toString(), "order/getmenus");
			//Log.i("JSON-MENU", json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				params = jo.getJSONArray("result");
				datajson = String.valueOf(params);
				return datajson;
			}
		} catch (JSONException e) {
			// TODO: handle exception
		}
		return "failed";
	}
	
	public static String getPackage(){
		try {
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray params = new JSONArray();
			String json = "TEST";
			String datajson;
			
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			jobj.put("data", 0);
			json = Chilkat.SendHttp(jobj.toString(), "order/getpackages");
			//Log.i("JSON-API", json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				params = jo.getJSONArray("result");
				datajson = String.valueOf(params);
				return datajson;
			}
		} catch (JSONException e) {
			// TODO: handle exception
		}
		return "failed";
	}

	public static ArrayList<HashMap<String, String>> getAllTables(){
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		try{
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray data = new JSONArray();
			String json;
			String error = null;
			
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			//Log.i("SEND-JSON", jobj.toString());
			json = Chilkat.SendHttp(jobj.toString(), "tables/getAll");
			//Log.i("JSON GET ALL TABLES", json);
			if(tables!=null){
				//Log.i("COUNT", String.valueOf(tables));
			}
			
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				data = jo.getJSONArray("result");
				error = jo.getString("error");
				if (!error.equals("")){
					for(int i = 0; i < data.length(); i++){
						JSONObject d = data.getJSONObject(i);
						HashMap<String, String>  map = new HashMap<String, String>();
						map.put("table_id", d.getString("table_id"));
						map.put("no", d.getString("no"));
						map.put("status", d.getString("status"));
						map.put("total_price", d.getString("total_price"));
						map.put("total_menu", d.getString("total_menu"));
						map.put("member_id", d.getString("member_id"));
						map.put("member_name", d.getString("member_name"));
						map.put("member_email", d.getString("member_email"));
						map.put("member_phone", d.getString("member_phone"));
						
						map.put("sales_id", d.getString("sales_id"));
						map.put("photo", d.getString("photo"));
						map.put("capacity", d.getString("capacity"));
						map.put("time_used", d.getString("time_used"));
						
						if(tables!=null){
							if(tables.size()>1){
								//Log.i("MASUK1", "MASUK1");
								if(tables.size() == data.length()){
									//Log.i("MASUK2", "MASUK2");
									String statusnotif = "0";
									if(!d.getString("no").toLowerCase().equals("none")){
										if(!d.getString("total_menu").equals("0")){
											if(!tables.get(i).get("total_menu").equals(d.getString("total_menu"))){
												statusnotif = "1";
												//Log.i("MASUK3", "MASUK3");
											}
										}
									}
									
								tables.get(i).put("statusnotif",statusnotif);
								map.put("statusnotif", tables.get(i).get("statusnotif"));
								}
							}
						}
						tempData.add(map);
					}
					
					if(tables==null){
						tables = new ArrayList<HashMap<String,String>>();
						tables.addAll(tempData);
					}
					
					return tempData;
					
			}
			}
		}catch (JSONException e) { 
			
		}
		
		return null;
	}
	
	public static String getAllTablesFile(){
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		try{
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray data = new JSONArray();
			String json;
			String error = null;
			
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			//Log.i("SEND-JSON", jobj.toString());
			json = Chilkat.SendHttp(jobj.toString(), "tables/getAll");
			//Log.i("JSON GET ALL TABLES", json);
			if (Helper.isJSONValid(json))
			{
				return json;
			}
		}catch (JSONException e) { 
			
		}
		
		return "failed";
	}
	
	public static ArrayList<HashMap<String, String>> getUpdateTables(){
		ArrayList<HashMap<String, String>> tempData = null;
		ArrayList<HashMap<String, String>> tempDataMenu = new ArrayList<HashMap<String, String>>();
		try{
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray datatype = new JSONArray();
			JSONObject arraydata = new JSONObject();
			String json;
			String error = null;
			String hash = phash;
			
			arraydata.put("hash",hash);
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			jobj.put("data", arraydata.toString());
			
			//Log.i("SEND-JSON", jobj.toString());
			json = Chilkat.SendHttp(jobj.toString(), "tables/getUpdate");
			//Log.i("JSON GET UPDATE TABLES", json);
			if (Helper.isJSONValid(json))
			{
				tempData = new ArrayList<HashMap<String, String>>();
				jo = new JSONObject(json);
				datatype = jo.getJSONArray("result");
				error = jo.getString("error");
				if(datatype.length()>0){
					phash = jo.getString("hash");
				}
				
				for(int i = 0; i < datatype.length(); i++){
					JSONObject d = datatype.getJSONObject(i);
					HashMap<String, String>  map = new HashMap<String, String>();
					if(d.getString("data_type").toUpperCase().equals("TABLE")){
						map.put("table_id", d.getString("table_id"));
						map.put("no", d.getString("no"));
						map.put("status", d.getString("status"));
						map.put("total_price", d.getString("total_price"));
						map.put("total_menu", d.getString("total_menu"));
						String member_id = "";
						String total_menu = "0";
						if(d.getString("member_id")!=null){
							member_id = d.getString("member_id");
						}
						map.put("member_id", member_id);
						map.put("member_name", d.getString("member_name"));
						map.put("member_email", d.getString("member_email"));
						map.put("member_phone", d.getString("member_phone"));
						map.put("sales_id", d.getString("sales_id"));
						map.put("photo", d.getString("photo"));
						map.put("capacity",d.getString("capacity"));
						map.put("time_used", d.getString("time_used"));
						map.put("data_type", d.getString("data_type"));
						tempData.add(map);
					}
					else if(d.getString("data_type").toUpperCase().equals("QUEUE")){
						map.put("table_id", d.getString("table_id"));
						map.put("no", d.getString("table_no"));
						map.put("queue", d.getString("queue"));
						map.put("status", d.getString("status"));
						map.put("total_price", d.getString("total"));
						String queue = "0";
						if(d.getString("quantity")!=null){
							queue = d.getString("quantity");
						}
						map.put("total_menu",queue);
						map.put("member_id", d.getString("member_id"));
						map.put("member_name", d.getString("member_name"));
						map.put("member_email", d.getString("member_email"));
						map.put("member_phone", d.getString("member_phone"));
						map.put("sales_id", d.getString("sales_id"));
						map.put("photo", "");
						map.put("capacity","");
						map.put("time_used", "0");
						map.put("status", "0");
						map.put("data_type", d.getString("data_type"));
						tempData.add(map);
					}
					else if(d.getString("menu_type").toUpperCase().equals("MENU")){
						map.put("menu_id", d.getString("menu_id"));
						map.put("menu_type", d.getString("menu_type"));
						map.put("price", d.getString("price"));
						map.put("status", d.getString("status"));
						map.put("data_type", d.getString("data_type"));
						tempData.add(map);
					}
				}	
			}
		}catch (JSONException e) { 
			Log.i("ERROR-JSON-API-TABLE", e.toString());
		}
		
		return tempData;
	}
	
	public static String getUpdateTablesJsonFile(){
		//ArrayList<HashMap<String, String>> tempData = null;
		//ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		String json = "";
		try{
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray datatype = new JSONArray();
			JSONObject arraydata = new JSONObject();
			
			String error = null;
			String hash = phash;
			
			arraydata.put("hash",hash);
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			jobj.put("data", arraydata.toString());
			//Log.i("SEND-JSON", jobj.toString());
			json = Chilkat.SendHttp(jobj.toString(), "tables/getUpdate");
			Log.i("JSON GET UPDATE TABLES", json);
			if (Helper.isJSONValid(json))
			{
				return json;
			}
					
		}catch (JSONException e) { 
			Log.i("ERROR-JSON", e.toString());
		}
		
		return "";
	}
	
	
	
	public static ArrayList<HashMap<String, String>> getListOrderAll(){
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		try {
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray params = new JSONArray();
			String json;
			String datajson;
			JSONArray data = new JSONArray();
			String error = null;
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			json = Chilkat.SendHttp(jobj.toString(), "order/listorder");
			//Log.i("JSON GET ALL LIST ORDER", json);
			
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				data = jo.getJSONArray("result");
				error = jo.getString("error");
				if (!error.equals("")){
					for(int i = 0; i < data.length(); i++){
						JSONObject d = data.getJSONObject(i);
						HashMap<String, String>  map = new HashMap<String, String>();
						if(d.getString("table_no").toUpperCase().equals("NONE")){
							map.put("table_id", d.getString("table_id"));
							map.put("no", d.getString("table_no"));
							map.put("queue", d.getString("queue"));
							map.put("status", d.getString("status"));
							map.put("total_price", d.getString("total"));
							map.put("total_menu", d.getString("quantity"));
							map.put("member_id", d.getString("member_id"));
							map.put("member_name", d.getString("member_name"));
							map.put("member_email", d.getString("member_email"));
							map.put("member_phone", d.getString("member_phone"));
							map.put("sales_id", d.getString("sales_id"));
							map.put("photo", "");
							map.put("capacity","");
							map.put("time_used", "0");
							map.put("status", "0");
							tempData.add(map);
						}
					}
					return tempData;
			}
			}		
		} catch (JSONException e) {
			// TODO: handle exception
		}
		return null;
	}
	
	public static ArrayList<HashMap<String, String>> getListOrder(String sales_id){
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		try {
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray params = new JSONArray();
			String json;
			String datajson;
			JSONArray data = new JSONArray();
			String error = null;
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			jobj.put("sales_id", sales_id);
			json = Chilkat.SendHttp(jobj.toString(), "order/listorder");
			Log.i("JSON", json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				data = jo.getJSONArray("result");
				error = jo.getString("error");
				if (!error.equals("")){
					for(int i = 0; i < data.length(); i++){
						JSONObject d = data.getJSONObject(i);
						HashMap<String, String>  map = new HashMap<String, String>();
						map.put("table_id", d.getString("table_id"));
						map.put("no", d.getString("table_no"));
						map.put("queue", d.getString("queue"));
						map.put("status", d.getString("status"));
						map.put("total_price", d.getString("total"));
						map.put("total_menu", d.getString("quantity"));
						map.put("member_name", d.getString("member_name"));
						map.put("sales_id", d.getString("sales_id"));
						map.put("status", "0");
						tempData.add(map);
					}
					
					return tempData;
			}
			}		
		} catch (JSONException e) {
			// TODO: handle exception
		}
		return null;
	}
	
	public static ArrayList<HashMap<String, String>> getListOrderDetail(String sales_id){
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		try{
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray data = new JSONArray();
			String json;
			String error = null;
			String foto_menu = null;
			
			
			jo.put("sales_id", sales_id);
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			jobj.put("data", jo.toString());
			
			json = Chilkat.SendHttp(jobj.toString(), "order/listorderdetail");
			//Log.i("GET-JSON", json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				data = jo.getJSONArray("result");
				error = jo.getString("error");
				if (!error.equals("")){
					for(int i = 0; i < data.length(); i++){
						JSONObject d = data.getJSONObject(i);
						HashMap<String, String>  map = new HashMap<String, String>();
						map.put("sales_id", d.getString("sales_id"));
						map.put("id", d.getString("id"));
						map.put("name", d.getString("name"));
						map.put("price", d.getString("price"));
						map.put("quantity", d.getString("quantity"));
						map.put("status", "confirm");
						map.put("photo", d.getString("image"));
						tempData.add(map);
					}
					
				
			}
			}
			
    	}catch (JSONException e) { 
			
		}
		return tempData;
	}
	
	public static String getListOrderDetailFile(){
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		try{
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray data = new JSONArray();
			String json;
			String error = null;
			String foto_menu = null;
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			json = Chilkat.SendHttp(jobj.toString(), "order/listorderdetailall");
			//Log.i("GET-JSON", json);
			if (Helper.isJSONValid(json))
			{
				return json;
			}
			
    	}catch (JSONException e) { 
			
		}
		return "failed";
	}
	
	public static String setBookTables(String tableid,String name,String member_id){
		try {
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray params = new JSONArray();
			String json;
			String datajson;
			jo.put("table_id", tableid);
			jo.put("name", name);
			jo.put("member_id", member_id);
			
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			jobj.put("data", jo.toString());
			Log.i("CAKTOPA-JSON-SEND", jobj.toString());
			json = Chilkat.SendHttp(jobj.toString(), "tables/book");
			Log.i("CAKTOPA-JSON", json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				params = jo.getJSONArray("result");
				datajson = String.valueOf(params);
				return datajson;
			}
		} catch (JSONException e) {
			// TODO: handle exception
		}
		return "failed";
		
	} 
	
	public static String getMemberByName(String key){
		try {
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray params = new JSONArray();
			String json;
			String datajson;
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			jobj.put("key", key);
			json = Chilkat.SendHttp(jobj.toString(), "member/search");
			//Log.i("GET-JSON-MEMBER",json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				params = jo.getJSONArray("result");
				datajson = params.toString();
				return datajson;
			}
		} catch (JSONException e) {
			// TODO: handle exception
		}
		return "failed";
		
	} 
	
	public static String getMemberAllFile(){
		try {
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray params = new JSONArray();
			String json;
			String datajson;
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			json = Chilkat.SendHttp(jobj.toString(), "member/getall");
			Log.i("GET-JSON-MEMBER",json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				params = jo.getJSONArray("result");
				datajson = params.toString();
				return datajson;
			}
		} catch (JSONException e) {
			// TODO: handle exception
		}
		return "failed";
		
	} 
	
	public static String setBookAndRegisterMember(String tableid,String name,String phone){
		try {
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray params = new JSONArray();
			String json;
			String datajson;
			
			jo.put("name",name);
			jo.put("phone",phone);
			jo.put("table_id",tableid);
			
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			jobj.put("data", jo.toString());
			json = Chilkat.SendHttp(jobj.toString(), "tables/bookandregister");
			Log.i("SET-JSON-MEMBER",json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				datajson = jo.getString("result");
				Log.i("SUCCESS-JSON", datajson);
				return datajson;
			}
		} catch (JSONException e) {
			// TODO: handle exception
		}
		return "failed";
	} 
	
	public static String cancelBooking(String tableid){
		try {
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray params = new JSONArray();
			String json;
			String datajson;
			jo.put("table_id",tableid);
			
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			jobj.put("data", jo.toString());
			json = Chilkat.SendHttp(jobj.toString(), "tables/cancelbook");
			//Log.i("JSON-LOG", json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				params = jo.getJSONArray("result");
				datajson = params.toString();
				Log.i("SUCCESS-JSON", datajson);
				return datajson;
			}
		} catch (JSONException e) {
			// TODO: handle exception
		}
		return "failed";
	} 
	
	public static String confirmOrder(String salesId){
		try {
			JSONObject jobj = new JSONObject(), jo = new JSONObject(),jodetail = new JSONObject();
			JSONObject joResultDetail = new JSONObject();
			JSONArray params = new JSONArray();
			JSONArray jArrayDetail = new JSONArray();
			
			String json;
			String datajson;
			
			jo.put("sales_id",salesId);
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			jobj.put("data", jo.toString());
			
			json = Chilkat.SendHttp(jobj.toString(), "order/confirmorder");
			Log.i("GET-JSON-CONFIRM", json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				datajson = jo.getString("result");
				return datajson;
			}
		} catch (JSONException e) {
			// TODO: handle exception
		}
		return "failed";
	} 
	
	
	
	public static String printMenu(String salesId){
		try {
			JSONObject jobj = new JSONObject(), jo = new JSONObject(),jodetail = new JSONObject();
			JSONObject joResultDetail = new JSONObject();
			JSONArray params = new JSONArray();
			JSONArray jArrayDetail = new JSONArray();
			String json;
			String datajson;
			jo.put("sales_id",salesId);
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			jobj.put("data", jo.toString());
		//	Log.i("SEND-JSON-PRINT", jobj.toString());
			json = Chilkat.SendHttp(jobj.toString(), "order/printorder");
		//	Log.i("JSON-RESULT-PRint", json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				params = jo.getJSONArray("result");
				datajson = params.toString();
				Log.i("SUCCESS-JSON", datajson);
				return datajson;
			}
		} catch (JSONException e) {
			// TODO: handle exception
		}
		return "failed";
	}
	
	
	public static String setOrderTemp(String sales_id,ArrayList<HashMap<String, String>> dataArrayOrderMenu){
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		try{
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray jArrayDetail = new JSONArray();
			String json;
			String error = null;
			String foto_menu = null;
			
			
			for(int i=0;i<dataArrayOrderMenu.size();i++){
				JSONObject joArrayDetail = new JSONObject();
				
				joArrayDetail.put("id",dataArrayOrderMenu.get(i).get("id"));
				joArrayDetail.put("quantity",dataArrayOrderMenu.get(i).get("quantity"));
				jArrayDetail.put(joArrayDetail);
			}
			
			jo.put("sales_id", sales_id);
			jo.put("sales_detail", jArrayDetail);
			
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			jobj.put("data", jo.toString());
			Log.i("SEND-JSON", jobj.toString());
			json = Chilkat.SendHttp(jobj.toString(), "order/addordermenu");
			//Log.i("GET-JSON", json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				String result = jo.getString("result");
				return result;
			}
			
    	}catch (JSONException e) { 
			
		}
		return null;
	}
	
	public static ArrayList<HashMap<String, String>> getOrderTemp(String sales_id){
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		try{
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray jArrayDetail = new JSONArray();
			String json;
			String error = null;
			String foto_menu = null;
			
		
			jobj.put("sales_id", sales_id);
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			
			Log.i("SEND-JSON", jobj.toString());
			json = Chilkat.SendHttp(jobj.toString(), "order/gettemporder");
			//Log.i("GET-JSON-NOT-CONFIRM", json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				data = jo.getJSONArray("result");
				error = jo.getString("error");
				if (!error.equals("")){
					for(int i = 0; i < data.length(); i++){
						JSONObject d = data.getJSONObject(i);
						HashMap<String, String>  map = new HashMap<String, String>();
						
						map.put("sales_id", d.getString("sales_id"));
						map.put("sales_detail_id", d.getString("sales_detail_id"));
						map.put("id", d.getString("id"));
						map.put("name", d.getString("name"));
						map.put("price", d.getString("price"));
						map.put("type", d.getString("type"));
						map.put("quantity", d.getString("quantity"));
						map.put("discount", "0");
						map.put("status", "not confirm");
						
						map.put("photo", d.getString("image"));
						tempData.add(map);
					}
					return tempData;
				
			}
			}
			
    	}catch (JSONException e) { 
			
		}
		return tempData;
	}
	
	public static String getOrderTempAllFile(){
		try{
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray jArrayDetail = new JSONArray();
			String json;
			String error = null;
			String foto_menu = null;
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			json = Chilkat.SendHttp(jobj.toString(), "order/gettemporderall");
			Log.i("JSON-TEMP-FILE",json );
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				data = jo.getJSONArray("result");
				error = jo.getString("error");
				if (!error.equals("")){
					return json;
			}
			}
			
    	}catch (JSONException e) { 
			
		}
		return "failed";
	}
	
	public static String setOrderTempQuantity(String sales_id,ArrayList<HashMap<String, String>> dataArrayOrderMenu){
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		try{
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray jArrayDetail = new JSONArray();
			String json;
			String error = null;
			String foto_menu = null;
			
			for(int i=0;i<dataArrayOrderMenu.size();i++){
				JSONObject joArrayDetail = new JSONObject();
				joArrayDetail.put("id",dataArrayOrderMenu.get(i).get("id"));
				joArrayDetail.put("quantity",dataArrayOrderMenu.get(i).get("quantity"));
				jArrayDetail.put(joArrayDetail);
			}
			
			jo.put("sales_id", sales_id);
			jo.put("sales_detail", jArrayDetail);
			
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			jobj.put("data", jo.toString());
			Log.i("SEND-JSON", jobj.toString());
			json = Chilkat.SendHttp(jobj.toString(), "order/addordermenuwithquantity");
			Log.i("GET-JSON", json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				String result = jo.getString("result");
				return result;
			}
			
    	}catch (JSONException e) { 
			
		}
		return null;
	}
	
	public static String deleteOrderTemp(String sales_detail_id){
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		try{
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray jArrayDetail = new JSONArray();
			String json;
			String error = null;
			String foto_menu = null;
			
			
			jobj.put("sales_detail_id", sales_detail_id);
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			Log.i("SEND-JSON", jobj.toString());
			json = Chilkat.SendHttp(jobj.toString(), "order/deletetemporder");
			Log.i("GET-JSON", json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				String result = jo.getString("result");
				return result;
			}
			
    	}catch (JSONException e) { 
			
		}
		return "failed";
	}
	
	public static ArrayList<HashMap<String, String>> getOrderConfirm(String sales_id){
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		try{
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray jArrayDetail = new JSONArray();
			String json;
			String error = null;
			String foto_menu = null;
		
			jobj.put("sales_id", sales_id);
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			//Log.i("SEND-JSON-CONFIRM", jobj.toString());
			json = Chilkat.SendHttp(jobj.toString(), "order/getconfirmorder");
			//Log.i("GET-JSON-CONFIRM", json);
			if (Helper.isJSONValid(json))
			{
				
				jo = new JSONObject(json);
				data = jo.getJSONArray("result");
				error = jo.getString("error");
				if (!error.equals("")){
					for(int i = 0; i < data.length(); i++){
						JSONObject d = data.getJSONObject(i);
						HashMap<String, String>  map = new HashMap<String, String>();
						
						map.put("sales_id", d.getString("sales_id"));
						map.put("sales_detail_id", d.getString("sales_detail_id"));
						map.put("id", d.getString("id"));
						map.put("name", d.getString("name"));
						map.put("price", d.getString("price"));
						map.put("type", d.getString("type"));
						map.put("quantity", d.getString("quantity"));
						map.put("discount", "0");
						map.put("status", "confirm");
						map.put("choicesplit", "0");
						map.put("qtysplit", "0");
						map.put("photo", d.getString("image"));
						tempData.add(map);
					}
			//		Log.i("GET-JSON-CONFIRM",tempData.toString());
					return tempData;
				
			}
			}
			
    	}catch (JSONException e) { 
			
		}
		return tempData;
	}
	
	
	public static String getOrderConfirmAllFile(){
		try{
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray jArrayDetail = new JSONArray();
			String json;
			String error = null;
			String foto_menu = null;
		
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			//Log.i("SEND-JSON-CONFIRM", jobj.toString());
			json = Chilkat.SendHttp(jobj.toString(), "order/getconfirmorderall");
			Log.i("GET-JSON-CONFIRM", json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				data = jo.getJSONArray("result");
				error = jo.getString("error");
				if (!error.equals("")){
					return json;
			}
			}
			
    	}catch (JSONException e) { 
			
		}
		return "failed";
	}
	
	
	public static String sendDataJual(String data) {
		Log.i("data param", data);
		JSONObject jo = new JSONObject();
		Log.i("data jual kirim", data);
		
		String hasil = Chilkat.SendHttp(data, "order/payment");	
		Log.i("data jual", hasil);
		if (Helper.isJSONValid(hasil))
		{
			try {
				jo = new JSONObject(hasil);
				String code = jo.getString("code");
				if(jo.getString("result").toLowerCase().toString().equals("success") || jo.getString("result").toLowerCase().toString().equals("data exists"))
					return code;
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				return "failed";
			}			
		}
		return "failed";
	}
	
	public static String setMergeTransaction(String sales_id,ArrayList<HashMap<String, String>> dataArrayOrderMenu){
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		try{
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray jArrayDetail = new JSONArray();
			String json;
			String error = null;
			String foto_menu = null;
			
			for(int i=0;i<dataArrayOrderMenu.size();i++){
				JSONObject joArrayDetail = new JSONObject();
				if(dataArrayOrderMenu.get(i).get("status").equals("1")){
					joArrayDetail.put("sales_id",dataArrayOrderMenu.get(i).get("sales_id"));
					jArrayDetail.put(joArrayDetail);
				}
			}
			
			jobj.put("sales_id", sales_id);
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			jobj.put("data", jArrayDetail.toString());
			Log.i("SEND-DATA-MERGE",jobj.toString());
			json = Chilkat.SendHttp(jobj.toString(), "order/mergeorder");
			Log.i("GET-DATA-MERGE",json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				String result = jo.getString("result");
				return result;
			}
			
    	}catch (JSONException e) { 
			
		}
		return null;
	}
	
	public static String setSplitTransaction(String table_id,String sales_id,ArrayList<HashMap<String, String>> dataArrayOrderMenu){
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		try{
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray data = new JSONArray();
			JSONArray jArrayDetail = new JSONArray();
			String json;
			String error = null;
			String foto_menu = null;
			
			
			for(int i=0;i<dataArrayOrderMenu.size();i++){
				JSONObject joArrayDetail = new JSONObject();
				if(dataArrayOrderMenu.get(i).get("choicesplit").equals("1")){
					joArrayDetail.put("id",dataArrayOrderMenu.get(i).get("id"));
					joArrayDetail.put("quantity",dataArrayOrderMenu.get(i).get("qtysplit"));
					jArrayDetail.put(joArrayDetail);
				}
			}
			jobj.put("table_id", table_id);
			jobj.put("sales_id", sales_id);
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			jobj.put("data", jArrayDetail.toString());
			Log.i("SEND-JSON", jobj.toString());
			json = Chilkat.SendHttp(jobj.toString(), "order/splitorder");
			Log.i("GET-JSON", json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				String result = jo.getString("result");
				return result;
			}
			
    	}catch (JSONException e) { 
			
		}
		return null;
	}
	
	public static ArrayList<HashMap<String, String>> getAvailableTable(){
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		try{
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray data = new JSONArray();
			String json;
			String error = null;
			
			
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			
			json = Chilkat.SendHttp(jobj.toString(), "tables/getavailable");
			Log.i("JSON-GET",json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				data = jo.getJSONArray("result");
				error = jo.getString("error");
				if (!error.equals("")){
					for(int i = 0; i < data.length(); i++){
						JSONObject d = data.getJSONObject(i);
						HashMap<String, String>  map = new HashMap<String, String>();
						map.put("table_id", d.getString("table_id"));
						map.put("no", d.getString("no"));
						map.put("status", d.getString("status"));	
						map.put("photo", d.getString("photo"));
						map.put("capacity", d.getString("capacity"));
						map.put("time_used", d.getString("time_used"));
						tempData.add(map);
					}
					return tempData;
			}
			}
			
    	}catch (JSONException e) { 
			
		}
		return tempData;
	}
	
	public static ArrayList<HashMap<String, String>> getAvailableTableChange(){
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		try{
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray data = new JSONArray();
			String json;
			String error = null;
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			json = Chilkat.SendHttp(jobj.toString(), "tables/getavailable");
			Log.i("JSON-GET",json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				data = jo.getJSONArray("result");
				error = jo.getString("error");
				if (!error.equals("")){
					for(int i = 0; i < data.length(); i++){
						JSONObject d = data.getJSONObject(i);
						HashMap<String, String>  map = new HashMap<String, String>();
						if(!d.getString("no").toUpperCase().equals("NONE")){
							map.put("table_id", d.getString("table_id"));
							map.put("no", d.getString("no"));
							map.put("status", d.getString("status"));	
							map.put("photo", d.getString("photo"));
							map.put("capacity", d.getString("capacity"));
							map.put("time_used", d.getString("time_used"));
							tempData.add(map);
						}
					}
					return tempData;
			}
			}
			
    	}catch (JSONException e) { 
			
		}
		return tempData;
	}
	
	public static boolean registerAppId(){
		try{
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			String json;
			String error = null;
			
			String mcaddress = Helper.getMACAddress("wlan0");
			if(mcaddress == "")
			    mcaddress = Helper.getMACAddress("eth0");
			String androidOS = Build.VERSION.RELEASE;
			String deviceName = Helper.getDeviceName();
			String versionName = Build.VERSION.CODENAME;
			
			jobj.put("mac_address", mcaddress);
			jobj.put("os", androidOS);
			jobj.put("version", versionName);
			jobj.put("device_name", deviceName);
			//Log.i("SEND-JSON-REGISTER",jobj.toString());
			json = Chilkat.SendHttp(jobj.toString(), "app/register");
			//Log.i("GET-JSON-REGISTER", json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				error = jo.getString("error");
				if (!error.equals("")){
					pappid = jo.getString("result");
					return true;
				}
			}
			
    	}catch (JSONException e) { 
			
		}
		return false;
	}
	
	public static boolean getAppId(){
		try{
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			String json;
			String error = null;
			
			jobj.put("app_id", pappid);
			//Log.i("SEND-JSON",jobj.toString());
			json = Chilkat.SendHttp(jobj.toString(), "app/getappid");
			//Log.i("GET-JSON", json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				error = jo.getString("error");
				if (!error.equals("")){
					if(jo.getString("result").toUpperCase().equals("FAILED")){
						return false;
					}
					else{
						pappid = jo.getString("result");
						return true;
					}
					
				}
			}
			
    	}catch (JSONException e) { 
			
		}
		return false;
	}
	
	public static String getTokenApi(){
		try{
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			String json;
			String error = null;
			
			jobj.put("app_id", pappid);
			jobj.put("user_id", puserid);			
			jobj.put("client_id", pclientid);
			jobj.put("branch_id", pbranchid);
			
			Log.i("SEND-JSON-TOKEN",jobj.toString());
			json = Chilkat.SendHttp(jobj.toString(), "token/gettoken");
			Log.i("GET-JSON-TOKEN", json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				error = jo.getString("error");
				if (!error.equals("")){
					if(jo.getString("result").toUpperCase().equals("FAILED")){
						return "failed";
					}
					else{
						ptoken = jo.getString("result");
						return ptoken;
					}
					
				}
			}
			
    	}catch (JSONException e) { 
			
		}
		return "failed";
	}
	
	public static String getTokenApiFile(){
		
		try{
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			String error = null;
			String json;
			jobj.put("app_id", pappid);
			jobj.put("user_id", puserid);			
			jobj.put("client_id", pclientid);
			jobj.put("branch_id", pbranchid);
			
			Log.i("SEND-JSON-TOKEN",jobj.toString());
			json = Chilkat.SendHttp(jobj.toString(), "token/gettoken");
			Log.i("GET-JSON-TOKEN", json);
			if (Helper.isJSONValid(json))
			{
				return json;
			}
			
    	}catch (JSONException e) { 
			
		}
		return "failed";
	}
	
	public static boolean openStore(){
		try {
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray params = new JSONArray();
			String json;
			String datajson;
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			json = Chilkat.SendHttp(jobj.toString(), "store/open");
			//Log.i("JSON-OPENSTORE", json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				params = jo.getJSONArray("result");
				datajson = String.valueOf(params);
				if(datajson.toUpperCase().equals("SUCCESS")){
					return true;
				}
			}
		} catch (JSONException e) {
			// TODO: handle exception
		}
		return false;
	}
	
	public static boolean closeStore(){
		try {
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray params = new JSONArray();
			String json;
			String datajson;
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			json = Chilkat.SendHttp(jobj.toString(), "store/close");
			Log.i("JSON-CLOSESTORE", json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				datajson = jo.getString("result");
				
				if(datajson.toUpperCase().equals("SUCCESS")){
					return true;
				}
			}
		} catch (JSONException e) {
			// TODO: handle exception
		}
		return false;
	}
	
	public static String statusStore(){
		try {
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray params = new JSONArray();
			String json;
			String datajson;
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			json = Chilkat.SendHttp(jobj.toString(), "store/status");
			//Log.i("JSON-STATUSSTORE", json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				datajson = jo.getString("result");
				return datajson;
			}
		} catch (JSONException e) {
			// TODO: handle exception
		}
		return "failed";
	}
	
	public static ArrayList<HashMap<String, String>> printOrder(String sales_id){
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		try{
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray data = new JSONArray();
			String json;
			String datajson;
			String error;
			jobj.put("sales_id", sales_id);
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			Log.i("SEND-JSON-PRINT", jobj.toString());
			json = Chilkat.SendHttp(jobj.toString(), "printer/setprinted");
			Log.i("JSON-PRINT", json);
			
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				data = jo.getJSONArray("result");
				error = jo.getString("error");
				if (!error.equals("")){
					for(int i = 0; i < data.length(); i++){
						JSONObject d = data.getJSONObject(i);
						HashMap<String, String>  map = new HashMap<String, String>();
						int qty = 0;
						qty = d.getInt("quantity") - d.getInt("printed");
						if(qty>0){
							map.put("sales_id", d.getString("sales_id"));
							map.put("sales_detail_id", d.getString("sales_detail_id"));
							map.put("id", d.getString("id"));
							map.put("name", d.getString("name"));
							map.put("price", d.getString("price"));
							map.put("type", d.getString("type"));
							map.put("quantity", String.valueOf(qty));
							tempData.add(map);
						}
					}
					return tempData;
				
			}
			}
		} catch (JSONException e) {
			// TODO: handle exception
		}
		return null;
	}
	
	public static ArrayList<HashMap<String, String>> getCategoryComplain(){
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		try{
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray data = new JSONArray();
			String json;
			String datajson;
			String error;
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			Log.i("SEND-JSON-COMPLAIN", jobj.toString());
			json = Chilkat.SendHttp(jobj.toString(), "member/getcomplaincategory");
			Log.i("JSON-COMPLAIN", json);
			
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				data = jo.getJSONArray("result");
				error = jo.getString("error");
				if (!error.equals("")){
					for(int i = 0; i < data.length(); i++){
						JSONObject d = data.getJSONObject(i);
						HashMap<String, String>  map = new HashMap<String, String>();
						map.put("category_id", d.getString("category_id"));
						map.put("name", d.getString("name"));
						tempData.add(map);
					}
					return tempData;
				
			}
			}
		} catch (JSONException e) {
			// TODO: handle exception
		}
		return null;
	}
	
	public static String logout(String note){
		try{
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray data = new JSONArray();
			String json;
			String datajson;
			String error;
			jo.put("note", note);
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			jobj.put("data", jo.toString());
			Log.i("SEND-LOGOUT", jobj.toString());
			json = Chilkat.SendHttp(jobj.toString(), "account/logout");
			Log.i("JSON-LOGOUT", json);
			
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				datajson = jo.getString("result");
				return datajson;
			}
		} catch (JSONException e) {
			// TODO: handle exception
		}
		return "failed";
	}
	
	public static String changeTable(String table_id,String sales_id){
		try{
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray data = new JSONArray();
			String json;
			String datajson;
			String error;
			jo.put("table_id", table_id);
			jo.put("sales_id", sales_id);
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			jobj.put("data", jo.toString());
			Log.i("SEND-CHANGETABLE", jobj.toString());
			json = Chilkat.SendHttp(jobj.toString(), "order/changetable");
			Log.i("JSON-CHANGETABLE", json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				datajson = jo.getString("result");
				return datajson;
			}
		} catch (JSONException e) {
			// TODO: handle exception
		}
		return "failed";
	}
	
	public static String complainSend(String category_id,String note_complain){
		try{
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray data = new JSONArray();
			String json;
			String datajson;
			String error;
			jo.put("category_id", category_id);
			jo.put("description", note_complain);
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			jobj.put("data", jo.toString());
			Log.i("SEND-COMPLAINSEND", jobj.toString());
			json = Chilkat.SendHttp(jobj.toString(), "member/insertcomplain");
			Log.i("JSON-COMPLAINSEND", json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				datajson = jo.getString("result");
				return datajson;
			}
		} catch (JSONException e) {
			// TODO: handle exception
		}
		return "failed";
	}
	
	public static String getQueue(){
		try{
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray data = new JSONArray();
			String json;
			String datajson;
			String error;
			jobj.put("token", getToken());
			jobj.put("app_id", pappid);
			json = Chilkat.SendHttp(jobj.toString(), "get/queue");
			Log.i("JSON-QUEUE", json);
			if (Helper.isJSONValid(json))
			{
				jo = new JSONObject(json);
				datajson = jo.getString("result");
				if(!datajson.toUpperCase().equals("FAILED")){
					return datajson;
				}
				
			}
		} catch (JSONException e) {
			// TODO: handle exception
		}
		return "0";
	}
	
	public static void chekcconnection(Context context){
		try {
			JSONObject jobj = new JSONObject(), jo = new JSONObject();
			JSONArray data = new JSONArray();
			String json = "";
			String datajson;
			String error;
			json = Chilkat.SendHttp("", "app/heartbeat");
			//Log.i("JSON-HEARTBEAT", json);
			if(json!=null){
				if (Helper.isJSONValid(json))
				{
					jo = new JSONObject(json);
					datajson = jo.getString("result");
					pcheckconnection = datajson;
				}
			}
			else{
				pcheckconnection = "No Connection";
			}
			
			
			SessionManager session = new SessionManager(context);
			session.createCheckConnection(pcheckconnection);
		} catch (JSONException e) {
			// TODO: handle exception
			Log.i("JSON-CHEKCCONNECTION", e.toString());
		}
	}
	
	//-------------------------Send FILE-----------------//
	
	public static void sendOrderNew(){
		try {
			JSONObject jo = new JSONObject();
			JSONObject jobj  = new JSONObject();
			BufferedReader br = null;
			String path = Helper.getDirContentOutgoing(Environment.getExternalStorageDirectory(), "order","new");
			File filepath = new File(path);
			File lisFile[] = filepath.listFiles();
			if(filepath.listFiles().length>0){
				for(int i=0;i<filepath.listFiles().length;i++){
					String filejson = "";
					br = new BufferedReader(new FileReader(lisFile[i].getAbsoluteFile()));
					filejson = br.readLine();
					jobj = new JSONObject(filejson);
					jobj.put("app_id", Api.pappid);
					jobj.put("token", Api.getToken());
					filejson = jobj.toString();
					if(Helper.isJSONValid(filejson)){
						String json = Chilkat.SendHttp(filejson, "insert/neworder");
						Log.i("JSON-SEND-NEW-ORDER", filejson);
						Log.i("JSON-RES-NEW-ORDER", json);
						jo = new JSONObject(json);
						if(jo.getString("result").toUpperCase().equals("SUCCESS")){
							Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), lisFile[i].getName(), filejson, "orderhistory","new");
							if(lisFile[i].getAbsoluteFile().exists())
								lisFile[i].getAbsoluteFile().delete();
						}
					}
				}
			}	
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			Log.i("FILE-NOT-FOUND", e.toString());
		}
		 catch (IOException e) {
			// TODO Auto-generated catch block
			 Log.i("IO-ERROR", e.toString());
		}
		catch (JSONException e) {
			// TODO: handle exception
			 Log.i("JSON-ERROR", e.toString());
				
		}
	}
	
	public static void sendcancelOrderNew(){
		try {
			JSONObject jo = new JSONObject();
			JSONObject jobj  = new JSONObject();
			BufferedReader br = null;
			String path = Helper.getDirContentOutgoing(Environment.getExternalStorageDirectory(), "order","cancelnew");
			File filepath = new File(path);
			File lisFile[] = filepath.listFiles();
			if(filepath.listFiles().length>0){
				for(int i=0;i<filepath.listFiles().length;i++){
					String filejson = "";
					br = new BufferedReader(new FileReader(lisFile[i].getAbsoluteFile()));
					filejson = br.readLine();
					jobj = new JSONObject(filejson);
					jobj.put("app_id", Api.pappid);
					jobj.put("token", Api.getToken());
					filejson = jobj.toString();
					if(Helper.isJSONValid(filejson)){
						String json = Chilkat.SendHttp(filejson, "insert/cancelorder");
						Log.i("JSON-SEND-CANCEL-ORDER", filejson);
						Log.i("JSON-RES-CANCEL-ORDER", json);
						jo = new JSONObject(json);
						if(jo.getString("result").toUpperCase().equals("SUCCESS")){
							if(lisFile[i].getAbsoluteFile().exists())
								lisFile[i].getAbsoluteFile().delete();
						}
					}
				}
			}	
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			Log.i("FILE-NOT-FOUND", e.toString());
		}
		 catch (IOException e) {
			// TODO Auto-generated catch block
			 Log.i("IO-ERROR", e.toString());
		}
		catch (JSONException e) {
			// TODO: handle exception
			 Log.i("JSON-ERROR", e.toString());
				
		}
	}
	
	public static void sendRegisterMember(){
		try {
			JSONObject jo = new JSONObject();
			BufferedReader br = null;
			String path = Helper.getDirContentOutgoing(Environment.getExternalStorageDirectory(), "member","register");
			File filepath = new File(path);
			File lisFile[] = filepath.listFiles();
			if(filepath.listFiles().length>0){
				for(int i=0;i<filepath.listFiles().length;i++){
					String filejson;
					br = new BufferedReader(new FileReader(lisFile[i].getAbsoluteFile()));
					filejson = br.readLine();
					JSONObject jobj = new JSONObject(filejson);
					jobj.put("app_id", Api.pappid);
					jobj.put("token", Api.getToken());
					filejson = jobj.toString();
					if(Helper.isJSONValid(filejson)){
						
						String json = Chilkat.SendHttp(filejson, "insert/member");
						Log.i("JSON-SEND-REGISTER-MEMBER", json);
						jo = new JSONObject(json);
						if(jo.getString("result").toUpperCase().equals("SUCCESS")){
							Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), lisFile[i].getName(), filejson, "memberhistory","register");
							if(lisFile[i].getAbsoluteFile().exists())
								lisFile[i].getAbsoluteFile().delete();
						}
					}
				}
			}	
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			Log.i("FILE-NOT-FOUND", e.toString());
		}
		 catch (IOException e) {
			// TODO Auto-generated catch block
			 Log.i("IO-ERROR", e.toString());
		}
		catch (JSONException e) {
			// TODO: handle exception
			 Log.i("JSON-ERROR", e.toString());
				
		}
	}
	
	public static void sendOrderBook(){
		try {
			JSONObject jo = new JSONObject();
			BufferedReader br = null;
			String path = Helper.getDirContentOutgoing(Environment.getExternalStorageDirectory(), "order","book");
			File filepath = new File(path);
			File lisFile[] = filepath.listFiles();
			if(filepath.listFiles().length>0){
				for(int i=0;i<filepath.listFiles().length;i++){
					String filejson;
					br = new BufferedReader(new FileReader(lisFile[i].getAbsoluteFile()));
					filejson = br.readLine();
					JSONObject jobj = new JSONObject(filejson);
					jobj.put("app_id", Api.pappid);
					jobj.put("token", Api.getToken());
					filejson = jobj.toString();
					if(Helper.isJSONValid(filejson)){
						String json = Chilkat.SendHttp(filejson, "insert/book");
						Log.i("JSON-SEND-BOOK", json);
						jo = new JSONObject(json);
						if(jo.getString("result").toUpperCase().equals("SUCCESS")){
							Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), lisFile[i].getName(), filejson, "orderhistory","book");
							if(lisFile[i].getAbsoluteFile().exists())
								lisFile[i].getAbsoluteFile().delete();
						}
					}
				}
			}	
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			Log.i("FILE-NOT-FOUND", e.toString());
		}
		 catch (IOException e) {
			// TODO Auto-generated catch block
			 Log.i("IO-ERROR", e.toString());
		}
		catch (JSONException e) {
			// TODO: handle exception
			 Log.i("JSON-ERROR", e.toString());
				
		}
	}
	
	public static void sendOrderTemp(){
		try {
			JSONObject jodata,joxdata = new JSONObject(),jo = new JSONObject(),jotempdata = new JSONObject(),jobj = new JSONObject();
			BufferedReader br = null;
			String path = Helper.getDirContentOutgoing(Environment.getExternalStorageDirectory(), "order","temp");
			File filepath = new File(path);
			File lisFile[] = filepath.listFiles();
			if(filepath.listFiles().length>0){
				for(int i=0;i<filepath.listFiles().length;i++){
					String filejson;
					br = new BufferedReader(new FileReader(lisFile[i].getAbsoluteFile()));
					filejson = br.readLine();
					jobj = new JSONObject(filejson);
					jobj = new JSONObject(jobj.getString("data"));
					
					String file = jobj.getString("sales_id")+"_"+jobj.getString("id")+".json";
					String filename = file.replace(".json","");
					String strsplit = new String(filename);
					String[] datafilename = strsplit.split("_");
					String pathFile = Helper.checkFileOrder(Environment.getExternalStorageDirectory(), "orderhistory", datafilename, "temp");
					
					if(!pathFile.toUpperCase().equals("FAILED")){
						String jsonfile = Helper.getFileContentDir(pathFile);
						if(Helper.isJSONValid(jsonfile)){
							//jika file history ada (Edit Quantity dari file lama)
							jotempdata = new JSONObject(jsonfile);
							jotempdata = new JSONObject(jotempdata.getString("data"));
							jodata  = new JSONObject();
							int quantity = jobj.getInt("quantity");
							Log.i("QUANTITY ORDER-HISTORY TEMP", jotempdata.getString("quantity"));
							Log.i("QUANTITY ORDER TEMP", jobj.getString("quantity"));
							jodata.put("sales_id", jotempdata.get("sales_id"));
							jodata.put("sales_detail_id", jotempdata.get("sales_detail_id"));
							jodata.put("id", jotempdata.get("id"));
							jodata.put("name", jotempdata.get("name"));
							jodata.put("price", jotempdata.get("price"));
							jodata.put("quantity", quantity);
							jodata.put("discount", jobj.get("discount"));
							jobj = new JSONObject();
							jobj.put("data",jodata.toString());
							//Log.i("ADA", "ADA");
						}
					}
					else{
						//Log.i("TIDAK ADA", "TIDAK ADA");
						//jika file history tidak ada
						jobj = new JSONObject(filejson);
					}
					
					jobj.put("app_id", Api.pappid);
					jobj.put("token", Api.getToken());
					filejson = jobj.toString();
					Log.i("FILE-JSON", filejson);
					if(Helper.isJSONValid(filejson)){
						String json = Chilkat.SendHttp(filejson, "insert/temporder");
						Log.i("JSON-SEND-TEMP", "JSON TEMP = "+json);
						jo = new JSONObject(json);
						if(jo.getString("result").toUpperCase().equals("SUCCESS")){
							//pindah ke history folder
							if(lisFile[i].getAbsoluteFile().exists()){
								jotempdata = new JSONObject(filejson);
								jotempdata = new JSONObject(jotempdata.getString("data"));
								file = jotempdata.getString("sales_id")+"_"+jotempdata.getString("id")+".json";
								filename = file.replace(".json","");
								strsplit = new String(filename);
								datafilename = strsplit.split("_");
								pathFile = Helper.checkFileOrder(Environment.getExternalStorageDirectory(), "orderhistory", datafilename, "temp");
								if(!pathFile.toUpperCase().equals("FAILED")){
									String jsonfile = Helper.getFileContentDir(pathFile);
									if(Helper.isJSONValid(jsonfile)){
										//jika file ada (Edit Quantity dari file lama)
										jobj = new JSONObject(jsonfile);
										jobj = new JSONObject(jobj.getString("data"));
										jodata  = new JSONObject();
										int quantity = jotempdata.getInt("quantity")+jobj.getInt("quantity");
										jodata.put("sales_id", jobj.get("sales_id"));
										jodata.put("sales_detail_id", jobj.get("sales_detail_id"));
										jodata.put("id", jobj.get("id"));
										jodata.put("name", jobj.get("name"));
										jodata.put("price", jobj.get("price"));
										jodata.put("quantity", quantity);
										jodata.put("discount", jobj.get("discount"));
										joxdata.put("data", jodata.toString());
										file = jobj.getString("sales_id")+"_"+jobj.getString("sales_detail_id")+"_"+jobj.getString("id")+".json";
										Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), file, joxdata.toString(), "orderhistory","temp");	
									}
								}
								else{
									//jika file tidak ada (Buat Baru)
									Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), lisFile[i].getName(), filejson, "orderhistory","temp");
								}
								lisFile[i].getAbsoluteFile().delete();
							}
						}
					}
				}
			}	
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			Log.i("FILE-NOT-FOUND", e.toString());
		}
		 catch (IOException e) {
			// TODO Auto-generated catch block
			 Log.i("IO-ERROR", e.toString());
		 }
		catch (JSONException e) {
			// TODO: handle exception
			 Log.i("JSON-ERROR", e.toString());
				
		}
	}
	
	public static void sendOrderConfirm(){
		try {
			JSONObject jodata,joxdata = new JSONObject(),jo = new JSONObject(),jotempdata = new JSONObject(),jobj = new JSONObject();
			BufferedReader br = null;
			String path = Helper.getDirContentOutgoing(Environment.getExternalStorageDirectory(), "order","detail");
			File filepath = new File(path);
			File lisFile[] = filepath.listFiles();
			if(filepath.listFiles().length>0){
				for(int i=0;i<filepath.listFiles().length;i++){
					String filejson;
					br = new BufferedReader(new FileReader(lisFile[i].getAbsoluteFile()));
					filejson = br.readLine();
					jobj = new JSONObject(filejson);
					jobj = new JSONObject(jobj.getString("data"));
					
					String file = jobj.getString("sales_id")+"_"+jobj.getString("id")+".json";
					String filename = file.replace(".json","");
					String strsplit = new String(filename);
					String[] datafilename = strsplit.split("_");
					String pathFile = Helper.checkFileOrder(Environment.getExternalStorageDirectory(), "orderhistory", datafilename, "detail");
					
					if(!pathFile.toUpperCase().equals("FAILED")){
						String jsonfile = Helper.getFileContentDir(pathFile);
						if(Helper.isJSONValid(jsonfile)){
							//jika file history ada (Edit Quantity dari file lama)
							jotempdata = new JSONObject(jsonfile);
							jotempdata = new JSONObject(jotempdata.getString("data"));
							jodata  = new JSONObject();
							
							int quantity = jobj.getInt("quantity");
							
							jodata.put("sales_id", jotempdata.get("sales_id"));
							jodata.put("sales_detail_id", jotempdata.get("sales_detail_id"));
							jodata.put("id", jotempdata.get("id"));
							jodata.put("name", jotempdata.get("name"));
							jodata.put("price", jotempdata.get("price"));
							jodata.put("quantity", quantity);
							jodata.put("discount", jobj.get("discount"));
							if(!jobj.isNull("split")){
								jodata.put("split", jobj.get("split"));
							}
							jobj = new JSONObject();
							jobj.put("data",jodata.toString());
							//Log.i("ADA", "ADA");
						}
					}
					else{
						//Log.i("TIDAK ADA", "TIDAK ADA");
						//jika file history tidak ada
						jobj = new JSONObject(filejson);
					}
					
					jobj.put("app_id", Api.pappid);
					jobj.put("token", Api.getToken());
					filejson = jobj.toString();
					Log.i("FILE-JSON-CONFIRM", filejson);
					if(Helper.isJSONValid(filejson)){
						String json = Chilkat.SendHttp(filejson, "insert/confirmorder");
						Log.i("JSON-SEND-CONFIRM", "JSON CONFIRM = "+json);
						jo = new JSONObject(json);
						if(jo.getString("result").toUpperCase().equals("SUCCESS")){
							//pindah ke history folder
							if(lisFile[i].getAbsoluteFile().exists()){
								jotempdata = new JSONObject(filejson);
								jotempdata = new JSONObject(jotempdata.getString("data"));
								file = jotempdata.getString("sales_id")+"_"+jotempdata.getString("id")+".json";
								filename = file.replace(".json","");
								strsplit = new String(filename);
								datafilename = strsplit.split("_");
								pathFile = Helper.checkFileOrder(Environment.getExternalStorageDirectory(), "orderhistory", datafilename, "detail");
								if(!pathFile.toUpperCase().equals("FAILED")){
									String jsonfile = Helper.getFileContentDir(pathFile);
									if(Helper.isJSONValid(jsonfile)){
										//jika file ada (Edit Quantity dari file lama)
										jobj = new JSONObject(jsonfile);
										jobj = new JSONObject(jobj.getString("data"));
										jodata  = new JSONObject();
										int quantity = 0;
										if(!jobj.isNull("split")){
											quantity = jobj.getInt("quantity");
										}
										else{
											quantity = jotempdata.getInt("quantity")+jobj.getInt("quantity");
										}
										
										jodata.put("sales_id", jobj.get("sales_id"));
										jodata.put("sales_detail_id", jobj.get("sales_detail_id"));
										jodata.put("id", jobj.get("id"));
										jodata.put("name", jobj.get("name"));
										jodata.put("price", jobj.get("price"));
										jodata.put("quantity", quantity);
										jodata.put("discount", jobj.get("discount"));
										joxdata.put("data", jodata.toString());
										file = jobj.getString("sales_id")+"_"+jobj.getString("sales_detail_id")+"_"+jobj.getString("id")+".json";
										Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), file, joxdata.toString(), "orderhistory","detail");	
									}
								}
								else{
									//jika file tidak ada (Buat Baru)
									Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), lisFile[i].getName(), filejson, "orderhistory","detail");
								}
								lisFile[i].getAbsoluteFile().delete();
							}
						}
					}
				}
			}	
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			Log.i("FILE-NOT-FOUND", e.toString());
		}
		 catch (IOException e) {
			// TODO Auto-generated catch block
			 Log.i("IO-ERROR", e.toString());
		 }
		catch (JSONException e) {
			// TODO: handle exception
			 Log.i("JSON-ERROR", e.toString());
				
		}
	}
	
	public static void sendOrderPayment(){
		try {
			BufferedReader br = null;
			JSONObject jo;
			String path = Helper.getDirContentOutgoing(Environment.getExternalStorageDirectory(), "order","paid");
			File filepath = new File(path);
			File lisFile[] = filepath.listFiles();
			
			if(filepath.listFiles().length>0){
				for(int i=0;i<filepath.listFiles().length;i++){
					String filejson;
					br = new BufferedReader(new FileReader(lisFile[i].getAbsoluteFile()));
					filejson = br.readLine();
					JSONObject jobj = new JSONObject(filejson);
					jobj.put("app_id", Api.pappid);
					jobj.put("token", Api.getToken());
					filejson = jobj.toString();
					Log.i("JSON-RESPON-PAYMENT", filejson);
					if(Helper.isJSONValid(filejson)){
						String json = Chilkat.SendHttp(filejson, "insert/paidorder");
						Log.i("JSON-SEND-PAYMENT", json);
						jo = new JSONObject(json);
						if(jo.getString("result").toUpperCase().equals("SUCCESS")){
							Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), lisFile[i].getName(), filejson, "orderhistory","paid");
							if(lisFile[i].getAbsoluteFile().exists())
								lisFile[i].getAbsoluteFile().delete();
						}
					}
				}
			}	
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			Log.i("FILE-NOT-FOUND", e.toString());
		}
		 catch (IOException e) {
			// TODO Auto-generated catch block
			 Log.i("IO-ERROR", e.toString());
		}
		catch (JSONException e) {
			// TODO Auto-generated catch block
			 Log.i("JSON-ERROR", e.toString());
		}	
	}
	
	public static void sendOrderQueue(){
		try {
			BufferedReader br = null;
			JSONObject jo;
			String path = Helper.getDirContentOutgoing(Environment.getExternalStorageDirectory(), "order","newqueue");
			File filepath = new File(path);
			File lisFile[] = filepath.listFiles();
			if(filepath.listFiles().length>0){
				for(int i=0;i<filepath.listFiles().length;i++){
					String filejson;
					br = new BufferedReader(new FileReader(lisFile[i].getAbsoluteFile()));
					filejson = br.readLine();
					JSONObject jobj = new JSONObject(filejson);
					jobj.put("app_id", Api.pappid);
					jobj.put("token", Api.getToken());
					filejson = jobj.toString();
					if(Helper.isJSONValid(filejson)){
						String json = Chilkat.SendHttp(filejson, "insert/neworder");
						Log.i("JSON-SEND-ORDER-QUEUE", json);
						jo = new JSONObject(json);
						if(jo.getString("result").toUpperCase().equals("SUCCESS")){
							Helper.saveFileOutgoing(Environment.getExternalStorageDirectory(), lisFile[i].getName(), filejson, "orderhistory","paid");
							if(lisFile[i].getAbsoluteFile().exists())
								lisFile[i].getAbsoluteFile().delete();
						}
					}
				}
			}	
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			Log.i("FILE-NOT-FOUND", e.toString());
		}
		 catch (IOException e) {
			// TODO Auto-generated catch block
			 Log.i("IO-ERROR", e.toString());
		}
		catch (JSONException e) {
			// TODO Auto-generated catch block
			 Log.i("JSON-ERROR", e.toString());
		}	
	}
	
	public static void sendChangeTable(){
		try {
			BufferedReader br = null;
			JSONObject jo;
			String path = Helper.getDirContentOutgoing(Environment.getExternalStorageDirectory(), "order","changetable");
			File filepath = new File(path);
			File lisFile[] = filepath.listFiles();
			if(filepath.listFiles().length>0){
				for(int i=0;i<filepath.listFiles().length;i++){
					String filejson;
					br = new BufferedReader(new FileReader(lisFile[i].getAbsoluteFile()));
					filejson = br.readLine();
					JSONObject jobj = new JSONObject(filejson);
					jobj.put("app_id", Api.pappid);
					jobj.put("token", Api.getToken());
					filejson = jobj.toString();
					if(Helper.isJSONValid(filejson)){
						String json = Chilkat.SendHttp(filejson, "insert/changetable");
						Log.i("JSON-SEND-ORDER-CHANGE-TABLE", json);
						jo = new JSONObject(json);
						if(jo.getString("result").toUpperCase().equals("SUCCESS")){
							if(lisFile[i].getAbsoluteFile().exists())
								lisFile[i].getAbsoluteFile().delete();
						}
					}
				}
			}	
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			Log.i("FILE-NOT-FOUND", e.toString());
		}
		 catch (IOException e) {
			// TODO Auto-generated catch block
			 Log.i("IO-ERROR", e.toString());
		}
		catch (JSONException e) {
			// TODO Auto-generated catch block
			 Log.i("JSON-ERROR", e.toString());
		}	
	}
	
	public static void sendMerge(){
		try {
			BufferedReader br = null;
			JSONObject jo;
			String path = Helper.getDirContentOutgoing(Environment.getExternalStorageDirectory(), "order","merge");
			File filepath = new File(path);
			File lisFile[] = filepath.listFiles();
			if(filepath.listFiles().length>0){
				for(int i=0;i<filepath.listFiles().length;i++){
					String filejson;
					br = new BufferedReader(new FileReader(lisFile[i].getAbsoluteFile()));
					filejson = br.readLine();
					JSONObject jobj = new JSONObject(filejson);
					jobj.put("app_id", Api.pappid);
					jobj.put("token", Api.getToken());
					filejson = jobj.toString();
					if(Helper.isJSONValid(filejson)){
						String json = Chilkat.SendHttp(filejson, "insert/mergeorder");
						Log.i("JSON-SEND-ORDER-MERGE", json);
						jo = new JSONObject(json);
						if(jo.getString("result").toUpperCase().equals("SUCCESS")){
							if(lisFile[i].getAbsoluteFile().exists())
								lisFile[i].getAbsoluteFile().delete();
						}
					}
				}
			}	
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			Log.i("FILE-NOT-FOUND", e.toString());
		}
		 catch (IOException e) {
			// TODO Auto-generated catch block
			 Log.i("IO-ERROR", e.toString());
		}
		catch (JSONException e) {
			// TODO Auto-generated catch block
			 Log.i("JSON-ERROR", e.toString());
		}	
	}
	
	public static void sendSplit(){
		try {
			BufferedReader br = null;
			JSONObject jo;
			String path = Helper.getDirContentOutgoing(Environment.getExternalStorageDirectory(), "order","split");
			File filepath = new File(path);
			File lisFile[] = filepath.listFiles();
			if(filepath.listFiles().length>0){
				for(int i=0;i<filepath.listFiles().length;i++){
					String filejson;
					br = new BufferedReader(new FileReader(lisFile[i].getAbsoluteFile()));
					filejson = br.readLine();
					JSONObject jobj = new JSONObject(filejson);
					jobj.put("app_id", Api.pappid);
					jobj.put("token", Api.getToken());
					filejson = jobj.toString();
					if(Helper.isJSONValid(filejson)){
						String json = Chilkat.SendHttp(filejson, "insert/splitorder");
						Log.i("JSON-SEND-ORDER-SPLIT", json);
						jo = new JSONObject(json);
						if(jo.getString("result").toUpperCase().equals("SUCCESS")){
							if(lisFile[i].getAbsoluteFile().exists())
								lisFile[i].getAbsoluteFile().delete();
						}
					}
				}
			}	
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			Log.i("FILE-NOT-FOUND", e.toString());
		}
		 catch (IOException e) {
			// TODO Auto-generated catch block
			 Log.i("IO-ERROR", e.toString());
		}
		catch (JSONException e) {
			// TODO Auto-generated catch block
			 Log.i("JSON-ERROR", e.toString());
		}	
	}
	
	public static void sendStatusTable(){
		try {
			BufferedReader br = null;
			JSONObject jo;
			String path = Helper.getDirContentOutgoing(Environment.getExternalStorageDirectory(), "order","tablestatus");
			File filepath = new File(path);
			File lisFile[] = filepath.listFiles();
			if(filepath.listFiles().length>0){
				for(int i=0;i<filepath.listFiles().length;i++){
					String filejson = "";
					if(filepath.listFiles().length>0){
						br = new BufferedReader(new FileReader(lisFile[i].getAbsoluteFile()));
						filejson = br.readLine();
						if(filejson!=null){
							JSONObject jobj = new JSONObject(filejson);
							jobj.put("app_id", Api.pappid);
							jobj.put("token", Api.getToken());
							filejson = jobj.toString();
							if(Helper.isJSONValid(filejson)){
								String json = Chilkat.SendHttp(filejson, "insert/setstatustable");
								Log.i("JSON-SEND-TABLE-STATUS", json);
								jo = new JSONObject(json);
								if(jo.getString("result").toUpperCase().equals("SUCCESS")){
									if(lisFile[i].getAbsoluteFile().exists())
										lisFile[i].getAbsoluteFile().delete();
								}
							}
						}
					}
				}
			}	
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			Log.i("FILE-NOT-FOUND", e.toString());
		}
		 catch (IOException e) {
			// TODO Auto-generated catch block
			 Log.i("IO-ERROR", e.toString());
		}
		catch (JSONException e) {
			// TODO Auto-generated catch block
			 Log.i("JSON-ERROR", e.toString());
		}
		catch (Exception e) {
			// TODO Auto-generated catch block
			 Log.i("ERROR-BUFFER", e.toString());
		}	
	}
	
	
	public static void sendAddTable(){
		try {
			BufferedReader br = null;
			JSONObject jo;
			String path = Helper.getDirContentOutgoing(Environment.getExternalStorageDirectory(), "table","data");
			String pathImage = Helper.getDirContentOutgoing(Environment.getExternalStorageDirectory(), "table","image");
			
			File filepath = new File(path);
			File lisFile[] = filepath.listFiles();
			//data table
			if(filepath.listFiles().length>0){
				for(int i=0;i<filepath.listFiles().length;i++){
					String filejson = "";
					if(filepath.listFiles().length>0){
						br = new BufferedReader(new FileReader(lisFile[i].getAbsoluteFile()));
						filejson = br.readLine();
						if(filejson!=null){
							JSONObject jobj = new JSONObject(filejson);
							jobj.put("app_id", Api.pappid);
							jobj.put("token", Api.getToken());
							filejson = jobj.toString();
							if(Helper.isJSONValid(filejson)){
								String json = Chilkat.SendHttp(filejson, "insert/inserttabledata");
								Log.i("JSON-SEND-TABLE-DATA", json);
								jo = new JSONObject(json);
								if(jo.getString("result").toUpperCase().equals("SUCCESS")){
									if(lisFile[i].getAbsoluteFile().exists())
										lisFile[i].getAbsoluteFile().delete();
								}
								
							}
						}
					}
				}
			}
			//image table
			filepath = new File(pathImage);
			File lisFileImage[] = filepath.listFiles();
			if(filepath.listFiles().length>0){
				for(int i=0;i<filepath.listFiles().length;i++){
					if(filepath.listFiles().length>0){	
						String json = Chilkat.SendHttpFile(lisFileImage[i].getName().toString(),lisFileImage[i].getPath(), "insert/inserttableimage");
						Log.i("JSON-SEND-TABLE-IMAGE", json);
						jo = new JSONObject(json);
						if(jo.getString("result").toUpperCase().equals("SUCCESS")){
							if(lisFileImage[i].getAbsoluteFile().exists())
								lisFileImage[i].getAbsoluteFile().delete();
						}
					}
				}
			}
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			Log.i("FILE-NOT-FOUND", e.toString());
		}
		 catch (IOException e) {
			// TODO Auto-generated catch block
			 Log.i("IO-ERROR", e.toString());
		}
		catch (JSONException e) {
			// TODO Auto-generated catch block
			 Log.i("JSON-ERROR", e.toString());
		}
		
	}
}
