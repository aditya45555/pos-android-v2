package com.pos;

import android.app.Application;
import android.content.Context;
import android.widget.Toast;

public class MyApp extends Application {
	private static Context context;

    public void onCreate(){
        super.onCreate();
        MyApp.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return MyApp.context;
    }
   
   
   
}
