package com.pos;

import java.util.HashMap;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
@SuppressLint("CommitPrefEdits")
public class SessionManager {
    SharedPreferences pref;
    Editor editor;
    Context _context;
    int PRIVATE_MODE = 0;
    private static final String PREF_NAME = "restoPref";
    private static final String IS_LOGIN = "IsLoggedIn";
   
    public static final String KEY_USER_ID = "user_id";
    public static final String KEY_USERNAME = "username";
    public static final String KEY_NAME = "name";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_IMAGE = "image";
    public static final String KEY_BRANCH_ID = "branch_id";
    public static final String KEY_CLIENT_ID = "client_id";
    
    public static final String KEY_CODE_BRANCH = "code_branch";
    public static final String KEY_LOGO = "logo";
    public static final String KEY_ADDRESS = "address";
    public static final String KEY_PHONE = "phone";
    public static final String KEY_NAME_CLIENT = "name_client";
    public static final String KEY_FAX = "fax";
    public static final String KEY_APP_ID = "app_id";
    public static final String KEY_APP_TYPE = "app_type";
    public static final String KEY_BLUETOOTH_NAME = "bluetooth_name";
    public static final String KEY_DISPLAY_DISABLE_MENU = "false";
    public static final String KEY_ACTIVE_PRINTER = "false";
    public static final String KEY_ACTIVE_PRINTER_CHECK = "false";
    public static final String KEY_NUM_COPY_BILL = "0";
    public static final String KEY_CHECK_CONNECTION = "check_connection";
    
    // Constructor
    public SessionManager(Context context){
        this._context = context;
        pref = _context.getSharedPreferences(PREF_NAME, PRIVATE_MODE);
        editor = pref.edit();
    }

    public void createLoginSession(String user_id, String username, String name, 
    		String email, String image, String branch_id,String client_id,String code,String logo,String address,String phone,String name_client,String fax){

        editor.putBoolean(IS_LOGIN, true);

        editor.putString(KEY_USER_ID, user_id);
        editor.putString(KEY_USERNAME, username);
        editor.putString(KEY_NAME, name);
        editor.putString(KEY_EMAIL, email);
        editor.putString(KEY_IMAGE, image);
        editor.putString(KEY_BRANCH_ID, branch_id);
        editor.putString(KEY_CLIENT_ID, client_id);
        
        editor.putString(KEY_CODE_BRANCH, code);
        editor.putString(KEY_LOGO, logo);
        editor.putString(KEY_ADDRESS, address);
        editor.putString(KEY_PHONE, phone);
        editor.putString(KEY_NAME_CLIENT, name_client);
        editor.putString(KEY_FAX, fax);
        editor.commit();
    }  
    
    public void createAppIdSession(String app_id){
    	editor.putString(KEY_APP_ID, app_id);
        editor.commit();
    }
    
    public void createTypeAppSession(String type){
    	editor.putString(KEY_APP_TYPE, type);
        editor.commit();
    }
    
    public void createPrinterBluetoothSession(String type){
    	editor.putString(KEY_BLUETOOTH_NAME, type);
        editor.commit();
    }
    
    public void createDisplayDisableMenu(Boolean active){
    	editor.putBoolean(KEY_DISPLAY_DISABLE_MENU, active);
        editor.commit();
    }
    
    public void createActivePrinter(Boolean active){
    	editor.putBoolean(KEY_ACTIVE_PRINTER, active);
        editor.commit();
    }
    
    public void createActivePrinterCheck(String active){
    	editor.putString(KEY_ACTIVE_PRINTER_CHECK, active);
        editor.commit();
    }
    
    public void createNumCopyBill(String active){
    	editor.putString(KEY_NUM_COPY_BILL, active);
        editor.commit();
    }
    
    public void createCheckConnection(String active){
    	editor.putString(KEY_CHECK_CONNECTION, active);
        editor.commit();
    }
    
    public void checkLogin(){
        if(!this.isLoggedIn()){
            Intent i = new Intent(_context, Login.class);
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            _context.startActivity(i);   
        }
    }
    
    public HashMap<String, String> getUserDetails(){
        HashMap<String, String> user = new HashMap<String, String>();
        
        user.put(KEY_USER_ID, pref.getString(KEY_USER_ID, null));
        user.put(KEY_USERNAME, pref.getString(KEY_USERNAME, null));
        user.put(KEY_NAME, pref.getString(KEY_NAME, null));
        user.put(KEY_EMAIL, pref.getString(KEY_EMAIL, null));
        user.put(KEY_IMAGE, pref.getString(KEY_IMAGE, null));
        user.put(KEY_BRANCH_ID, pref.getString(KEY_BRANCH_ID, null));
        user.put(KEY_CLIENT_ID, pref.getString(KEY_CLIENT_ID, null));
        
        user.put(KEY_CODE_BRANCH, pref.getString(KEY_CODE_BRANCH, null));
        user.put(KEY_LOGO, pref.getString(KEY_LOGO, null));
        user.put(KEY_ADDRESS, pref.getString(KEY_ADDRESS, null));
        user.put(KEY_PHONE, pref.getString(KEY_PHONE, null));
        user.put(KEY_NAME_CLIENT,pref.getString(KEY_NAME_CLIENT, null));
        user.put(KEY_FAX,pref.getString(KEY_FAX, null));
        user.put(KEY_APP_ID,pref.getString(KEY_APP_ID, null));
        user.put(KEY_APP_TYPE,pref.getString(KEY_APP_TYPE, null));
        user.put(KEY_BLUETOOTH_NAME,pref.getString(KEY_BLUETOOTH_NAME, null));
        user.put(KEY_CHECK_CONNECTION, pref.getString(KEY_CHECK_CONNECTION,"No Connection"));
        return user;
    }
     
    public void logoutUser(){
        editor.clear();
        editor.commit();
        Intent i = new Intent(_context, Login.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        _context.startActivity(i);
        TableFragment.mHandler.removeCallbacksAndMessages(null);
    }

    public boolean isLoggedIn(){
        return pref.getBoolean(IS_LOGIN, false);
    }
}
