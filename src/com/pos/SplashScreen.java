package com.pos;

import java.io.File;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;

import com.json.CategoryJson;
import com.json.MemberJson;
import com.json.MenuJson;
import com.json.OutGoingJson;
import com.json.PackageJson;
import com.json.ProfileJson;
import com.json.QueueJson;
import com.json.TableJson;
import com.json.TokenJson;
import com.lib.Gps;
import com.lib.Helper;
import com.lib.Print;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.print.PrintJob;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

public class SplashScreen extends Activity {
	static int SPLASH_TIME_OUT = 1000;
	ProgressBar pg;
	private initData task = null;
	public static Print print;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splashscreen);
		pg = (ProgressBar) findViewById(R.id.progressBar);
		//ImageView logo = (ImageView) findViewById(R.id.logo);
		int w = getResources().getDisplayMetrics().widthPixels;
		int wi = w/4;
		RelativeLayout.LayoutParams lp = new RelativeLayout.LayoutParams(wi, wi);
		lp.addRule(RelativeLayout.CENTER_IN_PARENT);
		//logo.setLayoutParams(lp);
		task = new initData(); 
		task.execute();
		
	}
	
	private class initData extends AsyncTask<String, Integer, Long> {
    	
    	@Override
        protected void onPreExecute() { 
    		pg.setProgress(0);
    	}

        @Override
        protected Long doInBackground(String... urls) {
        	if(Chilkat.ping()){
        		String token = Api.getTokenApiFile();
            	TokenJson.add(token);
            	String datamember = Api.getMemberAllFile();
            	MemberJson.addAll(datamember);
            	String dataqueue = Api.getQueue();
            	Log.i("DATA-QUEUE", dataqueue);
            	QueueJson.add(dataqueue);
        		String datacategory = Api.getCategory();
            	CategoryJson.add(datacategory);
            	pg.setProgress(20);
            	String datatable = Api.getAllTablesFile();
            	TableJson.add(datatable);
            	pg.setProgress(30);
            	String datamenu = Api.getMenu();
            	MenuJson.add(datamenu);
            	pg.setProgress(40);
            	String profile = Api.getProfileFile();
            	ProfileJson.add(profile	);
            	ProfileJson.setProfile();
            	//Api.sendAddTable();
            	pg.setProgress(50);
            	pg.setProgress(100);
        	}
        	else{
        		pg.setProgress(50);
        		ProfileJson.setProfile();
        		pg.setProgress(100);
        	}
        	return null;
        }
        
        @Override
        protected void onPostExecute(Long result) {
        	//CategoryJson.getAll();
        	Helper.createFolderOutgoing(Environment.getExternalStorageDirectory(), "order");
        	Helper.createFolderOutgoing(Environment.getExternalStorageDirectory(), "orderhistory");
        	Helper.createFolderOutgoing(Environment.getExternalStorageDirectory(), "member");
        	Helper.createFolderOutgoing(Environment.getExternalStorageDirectory(), "memberhistory");
        	Helper.createFolderOutgoing(Environment.getExternalStorageDirectory(), "table");
        	
        	//THREAD
        	OutgoingData.getInstance().setStart();
        	
        	if(Chilkat.ping()){
        		TableJson.saveImageTable(SplashScreen.this);
        		MenuJson.saveImageMenu(SplashScreen.this);
        	}
        	Intent i = new Intent(SplashScreen.this, Main.class);
			startActivity(i);
			overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
			finish();
        }
    }
}