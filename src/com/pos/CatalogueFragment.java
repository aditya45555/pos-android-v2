/*
 * Copyright (C) 2013 Andreas Stuetz <andreas.stuetz@gmail.com>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.pos;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONException;
import org.json.JSONObject;

import com.adapter.ListChartAdapter;
import com.adapter.MenuAdapterList;
import com.asyntask.listMenu;
import com.json.CategoryJson;
import com.json.ListChartDataJson;
import com.json.MenuJson;
import com.json.PackageJson;
import com.lib.Helper;
import com.pos.Catalogue.PagerAdapterChart;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.sothree.slidinguppanel.SlidingUpPanelLayout.PanelSlideListener;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.view.ScrollingView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AlertDialog.Builder;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

@SuppressLint("NewApi")
public class CatalogueFragment extends Fragment {

	private static final String ARG_POSITION = "position";
	private static final String ARG_CATEGORY = "category";
	private static final String ARG_SALES_ID = "sales_id";
	private static final String ARG_TABLE_ID = "table_id";
	private static final String ARG_DATA_ORDERTEMP = "order_temp";
	private static final String ARG_DATA_ORDERCONFIRM = "order_confirm";
	private ArrayList<HashMap<String, String>> DataArrayMenu = new ArrayList<HashMap<String, String>>();
	private  ArrayList<HashMap<String, String>> DataArrayOrderTempMenu = new ArrayList<HashMap<String, String>>();
	private  ArrayList<HashMap<String, String>> DataArrayOrderConfirmMenu = new ArrayList<HashMap<String, String>>();
	private int position;
	private String catid ;
	private String sales_id,table_id ;
	
	private listMenu task = null;
	public static GridView grid;
	public static MenuAdapterList adapter;

	public static CatalogueFragment newInstance(int position,String id,String sales_id,ArrayList<HashMap<String, String>> dataarrayordertempmenu,ArrayList<HashMap<String, String>> dataarrayorderconfirmmenu,String tableId) {
		CatalogueFragment f = new CatalogueFragment();
		Bundle b = new Bundle();
		b.putInt(ARG_POSITION, position);
		b.putString(ARG_CATEGORY, id);
		b.putString(ARG_SALES_ID, sales_id);
		b.putString(ARG_TABLE_ID, tableId);
		b.putSerializable(ARG_DATA_ORDERTEMP, dataarrayordertempmenu);
		b.putSerializable(ARG_DATA_ORDERCONFIRM, dataarrayorderconfirmmenu);
		f.setArguments(b);
		
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		position = getArguments().getInt(ARG_POSITION);
		catid = getArguments().getString(ARG_CATEGORY);
		sales_id = getArguments().getString(ARG_SALES_ID);
		table_id = getArguments().getString(ARG_TABLE_ID);
		DataArrayOrderTempMenu = (ArrayList<HashMap<String, String>>) getArguments().getSerializable(ARG_DATA_ORDERTEMP);
		DataArrayOrderConfirmMenu = (ArrayList<HashMap<String, String>>) getArguments().getSerializable(ARG_DATA_ORDERCONFIRM);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v = inflater.inflate(R.layout.grid_holder, container, false);
		grid = (GridView) v.findViewById(R.id.grid);
		
		task = new listMenu(grid, sales_id, catid, getActivity().getApplicationContext(),Catalogue.getInstance().menu_type,DataArrayOrderTempMenu,DataArrayOrderConfirmMenu,table_id);
		task.execute();
		
		int lenghtheight = 40;
		if(Helper.isTablet(getActivity().getApplicationContext())){
			lenghtheight = 20;
		}
		grid.getLayoutParams().height = (((((((Helper.getHeightScreen(getActivity())-Catalogue.getInstance().layout_title.getHeight()) - Catalogue.getInstance().layoutslideup.getHeight()))-grid.getVerticalSpacing())-Catalogue.getInstance().tabs.getHeight())-Catalogue.getInstance().getSupportActionBar().getHeight())-ParentCatalogueFragment.tab.getHeight())-lenghtheight;
		
		
		return v;
	}
	
	
	
	
}