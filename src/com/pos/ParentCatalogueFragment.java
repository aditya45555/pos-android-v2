package com.pos;

import java.util.ArrayList;
import java.util.HashMap;

import com.adapter.MenuAdapterList;
import com.astuetz.PagerSlidingTabStrip;
import com.asyntask.listMenu;
import com.json.CategoryJson;
import com.lib.Helper;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;

public class ParentCatalogueFragment extends Fragment{
	private static final String ARG_POSITION = "position";
	private static final String ARG_CATEGORY = "category";
	private static final String ARG_SALES_ID = "sales_id";
	private static final String ARG_TABLE_ID = "table_id";
	private static final String ARG_DATA_ORDERTEMP = "order_temp";
	private static final String ARG_DATA_ORDERCONFIRM = "order_confirm";
	
	private  ArrayList<HashMap<String, String>> DataArrayOrderTempMenu = new ArrayList<HashMap<String, String>>();
	private  ArrayList<HashMap<String, String>> DataArrayOrderConfirmMenu = new ArrayList<HashMap<String, String>>();
	
	private MyPagerAdapter adapterCatMenu;
	private int position;
	private String catid ;
	private String sales_id,table_id ;
	
	private listMenu task = null;
	public static GridView grid;
	LinearLayout parent_category;
	public static PagerSlidingTabStrip tab;
	ViewPager pager;
	public static ParentCatalogueFragment newInstance(int position,String id,String sales_id,ArrayList<HashMap<String, String>> dataarrayordertempmenu,ArrayList<HashMap<String, String>> dataarrayorderconfirmmenu,String tableId) {
		ParentCatalogueFragment f = new ParentCatalogueFragment();
		Bundle b = new Bundle();
		b.putInt(ARG_POSITION, position);
		b.putString(ARG_CATEGORY, id);
		b.putString(ARG_SALES_ID, sales_id);
		b.putSerializable(ARG_DATA_ORDERTEMP, dataarrayordertempmenu);
		b.putSerializable(ARG_DATA_ORDERCONFIRM, dataarrayorderconfirmmenu);
		b.putString(ARG_TABLE_ID, tableId);
		f.setArguments(b);
		
		return f;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		position = getArguments().getInt(ARG_POSITION);
		catid = getArguments().getString(ARG_CATEGORY);
		sales_id = getArguments().getString(ARG_SALES_ID);
		DataArrayOrderTempMenu = (ArrayList<HashMap<String, String>>) getArguments().getSerializable(ARG_DATA_ORDERTEMP);
		DataArrayOrderConfirmMenu = (ArrayList<HashMap<String, String>>) getArguments().getSerializable(ARG_DATA_ORDERCONFIRM);
	}
	
	@SuppressLint("NewApi")
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View v ;
		ArrayList<HashMap<String, String>> cd = CategoryJson.getAll(catid);
		if(cd.size()>0){
			v = inflater.inflate(R.layout.parent_category, container, false);
			//parent_category = (LinearLayout)v.findViewById(R.id.parentcategory);
			tab = (PagerSlidingTabStrip)v.findViewById(R.id.tabs);
			pager = (ViewPager)v.findViewById(R.id.pager);
			tab.setVisibility(View.VISIBLE);
			adapterCatMenu = new MyPagerAdapter(getChildFragmentManager(), Catalogue.getInstance().menu_type,catid);
			pager.setAdapter(adapterCatMenu);
			tab.setViewPager(pager);
		}
		else{
			v = inflater.inflate(R.layout.grid_holder, container, false);
			grid = (GridView) v.findViewById(R.id.grid);
			task = new listMenu(grid, sales_id, catid, getActivity().getApplicationContext(),Catalogue.getInstance().menu_type,DataArrayOrderTempMenu,DataArrayOrderConfirmMenu,table_id);
			task.execute();
			int lenghtheight = 40;
			if(Helper.isTablet(getActivity().getApplicationContext())){
				lenghtheight = 20;
			}
			
			//grid.getLayoutParams().height = (((((((Helper.getHeightScreen(getActivity())-Catalogue.getInstance().layout_title.getHeight()) - Catalogue.getInstance().layoutslideup.getHeight()))-grid.getVerticalSpacing())-Catalogue.getInstance().tabs.getHeight())-Catalogue.getInstance().getSupportActionBar().getHeight()))-lenghtheight;
			
			
		}
		
		
		return v;
	}
	
	private class MyPagerAdapter extends FragmentPagerAdapter {

		ArrayList<HashMap<String, String>> cd;
		
		public MyPagerAdapter(FragmentManager fm,String menu_type,String catid) {
			super(fm);
			cd = CategoryJson.getAll(catid);
			
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return cd.get(position).get("name");
		}

		@Override
		public int getCount() {
			return cd.size();
		}

		@Override
		public Fragment getItem(int position) {
			return CatalogueFragment.newInstance(position, cd.get(position).get("category_id"),sales_id,DataArrayOrderTempMenu,DataArrayOrderConfirmMenu,table_id);
		}
	}
	
	
	
}
