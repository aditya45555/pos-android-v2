package com.pos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.adapter.MemberAdapterList;
import com.adapter.TableAdapter;
import com.astuetz.PagerSlidingTabStrip;
import com.json.CategoryJson;
import com.json.MemberJson;
import com.lib.FloatingActionButton;
import com.lib.Helper;

import android.annotation.SuppressLint;
import android.app.SearchManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.GradientDrawable;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Layout;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SearchView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.OnItemClickListener;

@SuppressLint("NewApi")
public class Main extends ActionBarActivity {

	private static Context mContext;
	private PagerSlidingTabStrip tabs;
	public ViewPager pager;
	private LinearLayout layouttable;
	public ImageButton imageButton;
	private MyPagerAdapter adapter;
	public static boolean search;
	private static Main sMainActivity;
	private static final int CAMERA_REQUEST = 1888; 
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		Main.mContext = getApplicationContext();
		int w = getResources().getDisplayMetrics().widthPixels;
		int h = getResources().getDisplayMetrics().heightPixels;
		tabs = (PagerSlidingTabStrip)findViewById(R.id.tabs);
		pager = (ViewPager)findViewById(R.id.pager);
		layouttable = (LinearLayout)findViewById(R.id.layouttable);
		sMainActivity = this;
		adapter = new MyPagerAdapter(getSupportFragmentManager());
		pager.setAdapter(adapter);
		tabs.setShouldExpand(true);
		tabs.setViewPager(pager);
		LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(w-(w*(10/100)),LinearLayout.LayoutParams.WRAP_CONTENT);
		search = false;
	
		if(Api.getActivePrinterCheck() == null){
			Helper.dialogConfirmActivePrinter(Main.this);
		}
		
		if(Api.getTypeApp() == null){
			Helper.dialogSettingApp(Main.this);
		}
		getSupportActionBar().setTitle("");
		
		//ACTION BAR
		ActionBar mActionBar = getSupportActionBar();
		mActionBar.setDisplayShowHomeEnabled(false);
		mActionBar.setDisplayShowTitleEnabled(false);
		LayoutInflater mInflater = LayoutInflater.from(this);
		View mCustomView = mInflater.inflate(R.layout.custom_action_bar, null);
		imageButton = (ImageButton) mCustomView.findViewById(R.id.imgstatuson);
		mActionBar.setCustomView(mCustomView);
		mActionBar.setDisplayShowCustomEnabled(true);
		
	}
	
	public static Context getContextMain(){
		return Main.mContext;
	}
	
	
	public static Main getInstance() {
        return sMainActivity;
	}
	
	@Override
	public void onBackPressed() 
	{
		Helper.dialogConfirmExit(Main.this);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_setting_search));
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        if(null!=searchManager ) {   
         searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }
        searchView.setIconifiedByDefault(false);
        
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
			
			@Override
			public boolean onQueryTextSubmit(String query) {
				// TODO Auto-generated method stub
				TableFragment.adapter.filter(query);
				
				return false;
			}
			
			@Override
			public boolean onQueryTextChange(String newText) {
				// TODO Auto-generated method stub
				TableFragment.adapter.filter(newText);
				
				return false;
			}
		});
        
        return true;

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if(Api.getTypeApp()!=null){
			if(Api.getTypeApp().toLowerCase().equals("cashier")){
				if(Api.statusStore().toUpperCase().equals("OPEN")){
					if(id == R.id.action_setting_closestore){
						Helper.dialogConfirmCloseStore(Main.this);
						return true;
					}
				}
			}
		}
		if(id == R.id.action_settings_tambahmeja){
			Helper.dialogCameraAddTable(Main.this,CAMERA_REQUEST);
			return true;
		}
		else if (id == R.id.action_settings_app) {
			Helper.dialogSettingApp(Main.this);
			return true;
		}
		else if (id== R.id.action_settings_printer){
			Helper.dialogSettingPrinter(Main.this);
			return true;
		}
		else if (id== R.id.action_settings_numcopy_bill){
			Helper.dialogSetNumCopyBill(Main.this);
			return true;
		}
		else if (id == R.id.action_settings_activeprinter){
			Helper.dialogConfirmActivePrinter(Main.this);
			return true;
		}
		else if (id== R.id.action_settings_displaymenu){
			Helper.dialogSettingDisplayDisableMenu(Main.this);
			return true;
		}
		else if (id== R.id.action_settings_complain){
			Helper.dialogComplain(Main.this);
			return true;
		}
		else if (id== R.id.action_settings_logout){
			Helper.dialogConfirmLogout(Main.this);
			return true;
		}
		else{
			return true;
		}
		
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		 if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
			 	Bitmap photo = (Bitmap) data.getExtras().get("data"); 
	            Helper.dialogAddTable(Main.this,photo);
	     }  
	};
	
	private class MyPagerAdapter extends FragmentPagerAdapter {

		ArrayList<HashMap<String, String>> cd;
		
		public MyPagerAdapter(FragmentManager fm) {
			super(fm);
			cd = Helper.getTabQueeTable();
			
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return cd.get(position).get("title_tab");
		}

		@Override
		public int getCount() {
			return cd.size();
		}

		@Override
		public Fragment getItem(int position) {
			return TableFragment.newInstance(position, cd.get(position).get("title_tab"));
		}
	}

	
}
