package com.pos;

import java.util.ArrayList;
import java.util.HashMap;

import com.lib.Helper;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

public class OutgoingData  {
	private Context mContext;
	private Thread sbuffer;
	private String data;
	private boolean isBuffering;
	private static OutgoingData outgoing;
	
	private OutgoingData() {
		// TODO Auto-generated constructor stub
		mContext =  MyApp.getAppContext();
		isBuffering = false;
		
	}

	private class BufferThread implements Runnable {
	    public void run() {
	    	while (isBuffering){
	    			new sendData().execute();
	    		try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
	    	}
	    } 
	}
	
	public Boolean getStatusThread(){
		return isBuffering;
	}
	
	public void setStart(){
		isBuffering = true;
		if(sbuffer==null){
			sbuffer = new Thread(new BufferThread());
			sbuffer.start();
		}
	}
	
	public void setStop(){
		isBuffering = false;
		sbuffer = null;
	}
	
	public static OutgoingData getInstance() {
		if (outgoing == null)
			outgoing = new OutgoingData();
		return outgoing;
	}
	
	private class sendData extends AsyncTask<String, Integer, Long> {
		@Override
	    protected void onPreExecute() {
			Api.chekcconnection(mContext);
        	Db.setStatusTable();
		}

	    @Override
	    protected Long doInBackground(String... urls) {
    		//Log.i("TEST", Api.getCheckConnection());
    		if(Api.getCheckConnection().equals("SUCCESS")){
    			Api.sendAddTable();
    			Api.sendStatusTable();
    			Api.sendOrderNew();
    			Api.sendcancelOrderNew();
    			Api.sendChangeTable();
    			Api.sendOrderQueue();
	    		Api.sendRegisterMember();
	    		Api.sendOrderBook();
	    		Api.sendOrderTemp();
	    		Api.sendOrderConfirm();
	    		Api.sendMerge();
	    		Api.sendSplit();
	    		Api.sendOrderPayment();
    		}
    		
		return null;
	    }
	    
	    @Override
	    protected void onPostExecute(Long result) {
	    	if(!Api.getCheckConnection().toUpperCase().equals("SUCCESS")){
    			if(Api.getTypeApp()!=null){
    				if(!Helper.checkCashierPrimary(mContext)){	
    					Intent mIntent = new Intent(mContext,NoConnection.class);
    					mIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    					mContext.startActivity(mIntent);
    					setStop();
    				}
    			}
    		}
	    }
	   }
	
	
}
