package com.pos;

import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;

public class Welcome extends Activity {
	static int SPLASH_TIME_OUT = 1000;
	SessionManager session;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.splashscreen);
		
		session = new SessionManager(this);
		
		if (session.isLoggedIn()!=false){
			HashMap<String, String> user = session.getUserDetails();
			
			Api.puserid = user.get(SessionManager.KEY_USER_ID);
	        Api.pname = user.get(SessionManager.KEY_NAME);
	        Api.pusername = user.get(SessionManager.KEY_USERNAME);
	        Api.pemail = user.get(SessionManager.KEY_EMAIL);
	        Api.pimage = user.get(SessionManager.KEY_IMAGE);
	        Api.pclientid = user.get(SessionManager.KEY_CLIENT_ID);
	        Api.pbranchid = user.get(SessionManager.KEY_BRANCH_ID);
	        
	        Api.pcode = user.get(SessionManager.KEY_CODE_BRANCH);
	        Api.plogo = user.get(SessionManager.KEY_LOGO);
	        Api.paddress = user.get(SessionManager.KEY_ADDRESS);
	        Api.pphone = user.get(SessionManager.KEY_PHONE);
	        Api.pnameclient = user.get(SessionManager.KEY_NAME_CLIENT);
	        Api.pfax = user.get(SessionManager.KEY_FAX);
	        Api.pappid = user.get(SessionManager.KEY_APP_ID);
	        
	        Intent i = new Intent(Welcome.this, SplashScreen.class);
			startActivity(i);
			overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
			finish();	
		}
    	else
    	{
        	Intent i = new Intent(Welcome.this, Login.class);
			startActivity(i);
			overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
			finish();	        		
    	}
	}
}