package com.json;

import java.io.InputStream;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;

import com.lib.Helper;
import com.pos.Chilkat;

public class TableJson {
	private static TableJson tablejson;
	static String filename = "table.json";

	public static TableJson getInstance() {
		if (tablejson == null)
			tablejson = new TableJson();
		
		return tablejson;
	}
	
	public static String add(String table) {
    	if(table.length() < 1)
    		return null;
    	if(Helper.isJSONValid(table)){
    		Helper.saveFile(Helper.getDirDboxInFile(), filename, table);
    	}
    	return filename;
    }
	
	public static void saveImageTable(Context context){
		ArrayList<HashMap<String, String>>data = getImage();
		for(int i = 0;i<data.size();i++){
			String filename = data.get(i).get("photo");
			if(filename.contains(".")){
				String url = Helper.getImageUrl(filename+"?w=200&h=200&t=w");
				String extension = filename.substring(filename.lastIndexOf("."));
				Helper.saveFileImage(Helper.getDirDboxInFile(), filename, url, extension, context,"imagetable");
			}
		}
	}
	
	public static ArrayList<HashMap<String, String>> getUpdateTables(){
		JSONArray data,datatype;
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String,String>>();
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		JSONObject jo;
		String error;
		
		if (Helper.isJSONValid(json))
		{
			try {
				jo = new JSONObject(json);
				datatype = jo.getJSONArray("result");
				//error = jo.getString("error");
				for(int i = 0; i < datatype.length(); i++){
					JSONObject d = datatype.getJSONObject(i);
					HashMap<String, String>  map = new HashMap<String, String>();
					map.put("table_id", d.getString("table_id"));
					map.put("no", d.getString("no"));
					map.put("status", d.getString("status"));
					map.put("total_price", d.getString("total_price"));
					map.put("total_menu", d.getString("total_menu"));
					String member_id = "";
					String total_menu = "0";
					if(d.getString("member_id")!=null){
						member_id = d.getString("member_id");
					}
					map.put("member_id", member_id);
					map.put("member_name", d.getString("member_name"));
					map.put("member_email", d.getString("member_email"));
					map.put("member_phone", d.getString("member_phone"));
					map.put("sales_id", d.getString("sales_id"));
					map.put("photo", d.getString("photo"));
					map.put("capacity",d.getString("capacity"));
					map.put("time_used", d.getString("time_used"));
					map.put("data_type", "TABLE");
					
					tempData.add(map);	
				}
			} catch (JSONException e) {
				Log.i("JSON-ERROR", e.toString());
			}
		}
		return tempData;
	}
	
	public static ArrayList<HashMap<String, String>> setUpdateTables(String sales_id,String table_id,String status,ArrayList<HashMap<String, String>> datamember,ArrayList<HashMap<String, String>> datatotal){
		JSONArray data,datatype;
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String,String>>();
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		JSONObject jo,jobj = new JSONObject();
		String error;
		Log.i("DATA-ARRAY-MEMBER", datamember.toString());
		if (Helper.isJSONValid(json))
		{
			try {
				jo = new JSONObject(json);
				datatype = jo.getJSONArray("result");
				error = jo.getString("error");
				for(int i = 0; i < datatype.length(); i++){
					JSONObject d = datatype.getJSONObject(i);
					HashMap<String, String>  map = new HashMap<String, String>();
					String statustable = d.getString("status");
					String sales_idtable = d.getString("sales_id");
					String member_id = "";
					String member_name = "";
					String member_email = "";
					String member_phone = "";
					String total_menu = d.getString("total_menu");
					String total_price = d.getString("total_price");
					if(table_id.equals(d.getString("table_id"))){
						statustable = status;
						sales_idtable = sales_id;
						if(datamember.size()>0){
							member_id = datamember.get(0).get("member_id");
							member_name = datamember.get(0).get("member_name");
							member_email = datamember.get(0).get("member_email");
							member_phone = datamember.get(0).get("member_phone");
						}
						
						if(datatotal.size()>0){
							total_menu = datatotal.get(0).get("total_menu");
							total_price = datatotal.get(0).get("total_price");
						}
					}
					map.put("table_id", d.getString("table_id"));
					map.put("no", d.getString("no"));
					map.put("status", statustable);
					map.put("total_price", total_price);
					map.put("total_menu", total_menu);
					
					if(datamember.size()<1&&d.getString("member_id")!=null){
						member_id = d.getString("member_id");
					}
					map.put("member_id", member_id);
					map.put("member_name",member_name);
					map.put("member_email", member_email);
					map.put("member_phone", member_phone);
					map.put("sales_id", sales_idtable);
					map.put("photo", d.getString("photo"));
					map.put("capacity",d.getString("capacity"));
					map.put("time_used", d.getString("time_used"));
					tempData.add(map);	
				}
				data = new JSONArray(tempData);
				jobj.put("result", data);
				jobj.put("error", "null");
				jobj.put("id", "200");
				Log.i("JSON-UPDATE-TABLE", jobj.toString());
				add(jobj.toString());
			} catch (JSONException e) {
				Log.i("JSON-ERROR-UPDATE-TABLES", e.toString());
			}
		}
		return tempData;
	}
	
	private static ArrayList<HashMap<String, String>> getImage(){
		JSONArray data,datatype;
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String,String>>();
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		JSONObject jo;
		String error;
		
		if (Helper.isJSONValid(json))
		{
			try {
				jo = new JSONObject(json);
				datatype = jo.getJSONArray("result");
				error = jo.getString("error");
				for(int i = 0; i < datatype.length(); i++){
					JSONObject d = datatype.getJSONObject(i);
					HashMap<String, String>  map = new HashMap<String, String>();
					
					map.put("photo", d.getString("photo"));
					tempData.add(map);	
					
					
				}
			} catch (JSONException e) {
				Log.i("JSON-ERROR", e.toString());
			}
		}
		return tempData;
	}
	
	public static ArrayList<HashMap<String, String>> getAvailableTableChange(){
		JSONArray data,datatype;
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String,String>>();
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		JSONObject jo;
		String error;
		
		if (Helper.isJSONValid(json))
		{
			try {
				jo = new JSONObject(json);
				datatype = jo.getJSONArray("result");
				//error = jo.getString("error");
				for(int i = 0; i < datatype.length(); i++){
					JSONObject d = datatype.getJSONObject(i);
					HashMap<String, String>  map = new HashMap<String, String>();
					if(!d.getString("no").toUpperCase().equals("NONE")){
						if(d.getString("status").toUpperCase().equals("AVAILABLE")){
							map.put("table_id", d.getString("table_id"));
							map.put("no", d.getString("no"));
							map.put("status", d.getString("status"));
							map.put("total_price", d.getString("total_price"));
							map.put("total_menu", d.getString("total_menu"));
							String member_id = "";
							String total_menu = "0";
							if(d.getString("member_id")!=null){
								member_id = d.getString("member_id");
							}
							map.put("member_id", member_id);
							map.put("member_name", d.getString("member_name"));
							map.put("member_email", d.getString("member_email"));
							map.put("member_phone", d.getString("member_phone"));
							map.put("sales_id", d.getString("sales_id"));
							map.put("photo", d.getString("photo"));
							map.put("capacity",d.getString("capacity"));
							map.put("time_used", d.getString("time_used"));
							map.put("data_type", "TABLE");
							tempData.add(map);
						}
					}
				}
			} catch (JSONException e) {
				Log.i("JSON-ERROR", e.toString());
			}
		}
		return tempData;
	}
	
	public static ArrayList<HashMap<String, String>> getNotAvailableTable(){
		JSONArray data,datatype;
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String,String>>();
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		JSONObject jo;
		String error;
		
		if (Helper.isJSONValid(json))
		{
			try {
				jo = new JSONObject(json);
				datatype = jo.getJSONArray("result");
				//error = jo.getString("error");
				for(int i = 0; i < datatype.length(); i++){
					JSONObject d = datatype.getJSONObject(i);
					HashMap<String, String>  map = new HashMap<String, String>();
					if(!d.getString("no").toUpperCase().equals("NONE")){
						if(!d.getString("status").toUpperCase().equals("AVAILABLE")){
							String sales_id = d.getString("sales_id");
							String total_price = d.getString("total_price");
							String total_menu = d.getString("total_menu");
							String member_id = d.getString("member_id");
							String member_name = d.getString("member_name");
							String member_email = d.getString("member_email");
							String member_phone = d.getString("member_phone");
							
							boolean chekcfilestatus = Helper.checkFile(Environment.getExternalStorageDirectory(), "order", sales_id+"_"+d.getString("table_id")+".json", "status");
							if(chekcfilestatus){
								String datajson = Helper.getFileContentOutgoing(Environment.getExternalStorageDirectory(),sales_id+"_"+d.getString("table_id")+".json", "order", "status");
								if(Helper.isJSONValid(datajson)){
									jo = new JSONObject(datajson);
									jo = new JSONObject(jo.getString("data"));
									if(jo.getString("total_menu")!=null){
										total_menu = jo.getString("total_menu");
									}
									else{
										total_menu = "0";
									}
									
									if(jo.getString("total")!=null){
										total_price = jo.getString("total");
									}
									else{
										total_price = "0";
									}
									
									member_id = jo.getString("member_id");
									member_name = jo.getString("member_name");
									member_phone = jo.getString("member_phone");
									member_email = jo.getString("member_email");
								}
							}
							
							map.put("table_id", d.getString("table_id"));
							map.put("no", d.getString("no"));
							map.put("status", d.getString("status"));
							map.put("total_price", total_price);
							map.put("total_menu", total_menu);
							
							map.put("member_id", member_id);
							map.put("member_name", member_name);
							map.put("member_email", member_email);
							map.put("member_phone", member_phone);
							map.put("sales_id", d.getString("sales_id"));
							map.put("photo", d.getString("photo"));
							map.put("capacity",d.getString("capacity"));
							map.put("time_used", d.getString("time_used"));
							map.put("data_type", "TABLE");
							tempData.add(map);
						}
					}
				}
			} catch (JSONException e) {
				Log.i("JSON-ERROR-TABLE-JSON", e.toString());
			}
		}
		return tempData;
	}
	
	public static ArrayList<HashMap<String, String>> getNotAvailableTableId(){
		JSONArray data,datatype;
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String,String>>();
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		JSONObject jo;
		String error;
		
		if (Helper.isJSONValid(json))
		{
			try {
				jo = new JSONObject(json);
				datatype = jo.getJSONArray("result");
				//error = jo.getString("error");
				for(int i = 0; i < datatype.length(); i++){
					JSONObject d = datatype.getJSONObject(i);
					HashMap<String, String>  map = new HashMap<String, String>();
					if(!d.getString("no").toUpperCase().equals("NONE")){
						if(!d.getString("status").toUpperCase().equals("AVAILABLE")){
							map.put("table_id", d.getString("table_id"));
							map.put("sales_id", d.getString("sales_id"));
							tempData.add(map);
						}
					}
				}
			} catch (JSONException e) {
				Log.i("JSON-ERROR-TABLE-JSON", e.toString());
			}
		}
		return tempData;
	}
	
	public static ArrayList<HashMap<String, String>> getAvailableTable(){
		JSONArray data,datatype;
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String,String>>();
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		JSONObject jo;
		String error;
		
		if (Helper.isJSONValid(json))
		{
			try {
				jo = new JSONObject(json);
				datatype = jo.getJSONArray("result");
				//error = jo.getString("error");
				for(int i = 0; i < datatype.length(); i++){
					JSONObject d = datatype.getJSONObject(i);
					HashMap<String, String>  map = new HashMap<String, String>();
					if(d.getString("status").toUpperCase().equals("AVAILABLE")){
						map.put("table_id", d.getString("table_id"));
						map.put("no", d.getString("no"));
						map.put("status", d.getString("status"));
						map.put("total_price", d.getString("total_price"));
						map.put("total_menu", d.getString("total_menu"));
						String member_id = "";
						String total_menu = "0";
						if(d.getString("member_id")!=null){
							member_id = d.getString("member_id");
						}
						map.put("member_id", member_id);
						map.put("member_name", d.getString("member_name"));
						map.put("member_email", d.getString("member_email"));
						map.put("member_phone", d.getString("member_phone"));
						map.put("sales_id", d.getString("sales_id"));
						map.put("photo", d.getString("photo"));
						map.put("capacity",d.getString("capacity"));
						map.put("time_used", d.getString("time_used"));
						map.put("data_type", "TABLE");
						tempData.add(map);
					}
				}
			} catch (JSONException e) {
				Log.i("JSON-ERROR", e.toString());
			}
		}
		return tempData;
	}
	
	public static ArrayList<HashMap<String, String>> getStatusTab(){
		JSONArray data,datatype;
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String,String>>();
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		JSONObject jo;
		String error;
		
		if (Helper.isJSONValid(json))
		{
			try {
				jo = new JSONObject(json);
				datatype = jo.getJSONArray("result");
				//error = jo.getString("error");
				for(int i = 0; i < datatype.length(); i++){
					JSONObject d = datatype.getJSONObject(i);
					HashMap<String, String>  map = new HashMap<String, String>();
					if(d.getString("status").toUpperCase().equals("AVAILABLE")){
						map.put("table_id", d.getString("table_id"));
						map.put("no", d.getString("no"));
						map.put("status", d.getString("status"));
						map.put("total_price", d.getString("total_price"));
						map.put("total_menu", d.getString("total_menu"));
						String member_id = "";
						String total_menu = "0";
						if(d.getString("member_id")!=null){
							member_id = d.getString("member_id");
						}
						map.put("member_id", member_id);
						map.put("member_name", d.getString("member_name"));
						map.put("member_email", d.getString("member_email"));
						map.put("member_phone", d.getString("member_phone"));
						map.put("sales_id", d.getString("sales_id"));
						map.put("photo", d.getString("photo"));
						map.put("capacity",d.getString("capacity"));
						map.put("time_used", d.getString("time_used"));
						map.put("data_type", "TABLE");
						tempData.add(map);
					}
				}
			} catch (JSONException e) {
				Log.i("JSON-ERROR", e.toString());
			}
		}
		return tempData;
	}
	
	
	public static int getCount(){
		JSONArray data;
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		if (Helper.isJSONValid(json))
		{
			try {
				data = new JSONArray(json);
				return data.length();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return 0;
	}
}
