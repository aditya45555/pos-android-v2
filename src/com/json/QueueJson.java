package com.json;

import com.lib.Helper;

public class QueueJson {

	private static QueueJson quejson;
	static String filename = "queue.txt";

	public static QueueJson getInstance() {
		if (quejson == null)
			quejson = new QueueJson();
		
		return quejson;
	}
	
	public static String add(String cat) {
    	if(cat.length() < 1)
    		return null;
    	Helper.saveFiletxt(Helper.getDirDboxInFile(), filename, cat);
    	return filename;
    }
	
	public static String getQueue(){
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		return json;
	}
}
