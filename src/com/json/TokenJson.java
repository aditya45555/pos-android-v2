package com.json;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.lib.Helper;
import com.pos.Api;

public class TokenJson {
	
	private static TokenJson mj;
	static String filename = "token.json";

	public static TokenJson getInstance() {
		if (mj == null)
			mj = new TokenJson();
		
		return mj;
	}
	
	public static String add(String cat) {
    	if(cat.length() < 1)
    		return null;
    	if(Helper.isJSONValid(cat)){
    		Helper.saveFile(Helper.getDirDboxInFile(), filename, cat);
    	}
    	return filename;
    }
	
	public static String getToken(){
		JSONArray data;
		JSONObject jo;
		String error;
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		String token = "failed";
		if (Helper.isJSONValid(json))
		{
			try {
				jo = new JSONObject(json);
				error = jo.getString("error");
				if (!error.equals("")){
					if(jo.getString("result").toUpperCase().equals("FAILED")){
						return "failed";
					}
					else{
						token = jo.getString("result");
					}
					
				}
				
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return token;
	}
	
	public static int getCount(){
		JSONArray data;
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		if (Helper.isJSONValid(json))
		{
			try {
				data = new JSONArray(json);
				return data.length();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return 0;
	}
	
}
