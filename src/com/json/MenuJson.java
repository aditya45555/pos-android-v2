package com.json;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.content.Context;
import android.util.Log;

import com.lib.Helper;
import com.pos.Api;
import com.pos.Chilkat;

public class MenuJson {
	
	private static MenuJson mj;
	static String filename = "menu.json";

	public static MenuJson getInstance() {
		if (mj == null)
			mj = new MenuJson();
		
		return mj;
	}
	
	public static String add(String cat) {
    	if(cat.length() < 1)
    		return null;
    	if(Helper.isJSONValid(cat)){
    		Helper.saveFile(Helper.getDirDboxInFile(), filename, cat);
    	}
    	return filename;
    }
	
	public static void updateMenu(ArrayList<HashMap<String, String>> tempDataP){
		JSONArray data,datajson = new JSONArray();
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String,String>>();
		
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		
		if (Helper.isJSONValid(json))
		{
			try {
				data = new JSONArray(json);
				//Log.i("JSON",json);
				
				if(tempDataP.size()==data.length()){
					String[] status = new String[data.length()];
					String[] price = new String[data.length()];
					for(int i = 0; i < data.length(); i++){
						JSONObject d = data.getJSONObject(i);
						HashMap<String, String>  map = new HashMap<String, String>();
						for(int a = 0;a<tempDataP.size();a++){
							if(d.getString("menu_id").equals(tempDataP.get(a).get("menu_id"))){
								status[i] = tempDataP.get(a).get("status");
								price[i] = tempDataP.get(a).get("price");
							}
						}
						if(Api.disablemenu() == true){	
							map.put("menu_id", d.getString("menu_id"));
							map.put("category_id", d.getString("category_id"));
							map.put("description", d.getString("description"));
							map.put("name", d.getString("name"));
							map.put("price", price[i]);
							map.put("status", status[i]);
							map.put("short_description",d.getString("short_description"));
							map.put("event", d.getString("event"));
							map.put("color", d.getString("color"));
							map.put("bcolor", d.getString("bcolor"));
							map.put("photo", d.getString("photo"));
							map.put("menu_type", d.getString("menu_type"));
							
							tempData.add(map);
						}
						else{
							if(d.getString("status").toUpperCase().equals("AVAILABLE")){
								map.put("menu_id", d.getString("menu_id"));
								map.put("category_id", d.getString("category_id"));
								map.put("description", d.getString("description"));
								map.put("name", d.getString("name"));
								map.put("price", price[i]);
								map.put("status", status[i]);
								map.put("short_description",d.getString("short_description"));
								map.put("event", d.getString("event"));
								map.put("color", d.getString("color"));
								map.put("bcolor", d.getString("bcolor"));
								map.put("photo", d.getString("photo"));
								map.put("menu_type", d.getString("menu_type"));
								tempData.add(map);
							}
						}
						
						}
					
					datajson = new JSONArray(tempData);
					add(datajson.toString());
				}
				else{
					if(Api.getCheckConnection().toUpperCase().equals("SUCCESS")){
						String datamenu = Api.getMenu();
						add(datamenu);
					}
				}
			
			} catch (JSONException e) {
				Log.i("ERROR-JSON-MENU-UPDATE", e.toString());
			}
			
		}
		
	}
	
	public static void saveImageMenu(Context context){
		ArrayList<HashMap<String, String>>data = getImage();
		for(int i = 0;i<data.size();i++){
			String filename = data.get(i).get("photo");	
			if(filename.length()>0){
				String url = Helper.getImageUrl(filename+"?w=100&h=100&t=w");
				String extension = filename.substring(filename.lastIndexOf("."));
				Helper.saveFileImage(Helper.getDirDboxInFile(), filename, url, extension, context,"imagemenu");
			}
		}
	}
	
	public static ArrayList<HashMap<String, String>> getAll(){
		JSONArray data;
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		if (Helper.isJSONValid(json))
		{
			try {
				data = new JSONArray(json);
				for(int i = 0; i < data.length(); i++){
					JSONObject d = data.getJSONObject(i);
					HashMap<String, String>  map = new HashMap<String, String>();
					map.put("menu_id", d.getString("menu_id"));
					map.put("category_id", d.getString("category_id"));
					map.put("description", d.getString("description"));
					map.put("name", d.getString("name"));
					map.put("photo", d.getString("photo"));
					map.put("price", d.getString("price"));
					tempData.add(map);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return tempData;
		}
		return null;
	}
	
	public static HashMap<String, String> getById(String id){
		JSONArray data;
		HashMap<String, String>  map = new HashMap<String, String>();
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		if (Helper.isJSONValid(json))
		{
			try {
				data = new JSONArray(json);
				for(int i = 0; i < data.length(); i++){
					JSONObject d = data.getJSONObject(i);
					if (id.equals(d.getString("menu_id")))
					{
						map.clear();
						map.put("menu_id", d.getString("menu_id"));
						map.put("category_id", d.getString("category_id"));
						map.put("description", d.getString("description"));
						map.put("name", d.getString("name"));
						map.put("photo", d.getString("photo"));
						map.put("price", d.getString("price"));
						map.put("status", d.getString("status"));
						
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return map;
		}
		return null;
	}
	
	public static HashMap<String, String> getByName(String name){
		JSONArray data;
		HashMap<String, String>  map = new HashMap<String, String>();
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		if (Helper.isJSONValid(json))
		{
			try {
				data = new JSONArray(json);
				for(int i = 0; i < data.length(); i++){
					JSONObject d = data.getJSONObject(i);
					if (name.equals(d.getString("name")))
					{
						map.clear();
						map.put("menu_id", d.getString("menu_id"));
						map.put("category_id", d.getString("category_id"));
						map.put("description", d.getString("description"));
						map.put("name", d.getString("name"));
						map.put("photo", d.getString("photo"));
						map.put("price", d.getString("price"));
						map.put("status", d.getString("status"));
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return map;
		}
		return null;
	}
	
	public static ArrayList<HashMap<String, String>> getByCategoryID(String cid,String menu_type){
		JSONArray data;
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		
		if (Helper.isJSONValid(json))
		{
			try {
				data = new JSONArray(json);
				for(int i = 0; i < data.length(); i++){
					JSONObject d = data.getJSONObject(i);
					
					if (cid.equals(d.getString("category_id")))
					{
						
						if(d.get("menu_type").toString().toUpperCase().equals(menu_type.toUpperCase())){
						HashMap<String, String>  map = new HashMap<String, String>();
						
						if(Api.disablemenu() == true){
							map.put("menu_id", d.getString("menu_id"));
							map.put("category_id", d.getString("category_id"));
							map.put("description", d.getString("description"));
							map.put("name", d.getString("name"));
							map.put("price", d.getString("price"));
							map.put("status", d.getString("status"));
							map.put("short_description",d.getString("short_description"));
							map.put("event", d.getString("event"));
							map.put("color", d.getString("color"));
							map.put("bcolor", d.getString("bcolor"));
							map.put("photo", d.getString("photo"));
							map.put("menu_type", d.getString("menu_type"));
							tempData.add(map);
						}
						else{
							if(d.getString("status").toUpperCase().equals("AVAILABLE")){
								map.put("menu_id", d.getString("menu_id"));
								map.put("category_id", d.getString("category_id"));
								map.put("description", d.getString("description"));
								map.put("name", d.getString("name"));
								map.put("price", d.getString("price"));
								map.put("status", d.getString("status"));
								map.put("short_description",d.getString("short_description"));
								map.put("event", d.getString("event"));
								map.put("color", d.getString("color"));
								map.put("bcolor", d.getString("bcolor"));
								map.put("photo", d.getString("photo"));
								map.put("menu_type", d.getString("menu_type"));
								tempData.add(map);
							}
						}
						
						}
					}
				}
			} catch (JSONException e) {
				Log.i("ERROR-JSON-MENU-CATEGORY", e.toString());
			}
			return tempData;
		}
		return null;
	}
	
	private static ArrayList<HashMap<String, String>> getImage(){
		JSONArray data,datatype;
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String,String>>();
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		JSONObject jo;
		String error;
		
		if (Helper.isJSONValid(json))
		{
			try {
				datatype = new JSONArray(json);
				for(int i = 0; i < datatype.length(); i++){
					JSONObject d = datatype.getJSONObject(i);
					HashMap<String, String>  map = new HashMap<String, String>();	
					map.put("photo", d.getString("photo"));
					tempData.add(map);	
				}
			} catch (JSONException e) {
				Log.i("JSON-ERROR", e.toString());
			}
		}
		return tempData;
	}
	
	public static int getCount(){
		JSONArray data;
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		if (Helper.isJSONValid(json))
		{
			try {
				data = new JSONArray(json);
				return data.length();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return 0;
	}
	
}
