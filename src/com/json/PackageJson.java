package com.json;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.lib.Helper;
import com.pos.Api;

public class PackageJson {
	
	private static PackageJson pj;
	static String filename = "package.json";

	public static PackageJson getInstance() {
		if (pj == null)
			pj = new PackageJson();
		
		return pj;
	}
	
	public static String add(String cat) {
    	if(cat.length() < 1)
    		return null;
    	if(Helper.isJSONValid(cat)){
    		Helper.saveFile(Helper.getDirDboxInFile(), filename, cat);
    	}
    	
    	return filename;
    }
	
	public static ArrayList<HashMap<String, String>> getAll(){
		JSONArray data;
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		if (Helper.isJSONValid(json))
		{
			try {
				data = new JSONArray(json);
				for(int i = 0; i < data.length(); i++){
					JSONObject d = data.getJSONObject(i);
					HashMap<String, String>  map = new HashMap<String, String>();
					map.put("menu_id", d.getString("menu_id"));
					map.put("category_id", d.getString("category_id"));
					map.put("description", d.getString("description"));
					map.put("name", d.getString("name"));
					map.put("photo", d.getString("photo"));
					map.put("price", d.getString("price"));
					tempData.add(map);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return tempData;
		}
		return null;
	}
	
	public static HashMap<String, String> getById(String id){
		JSONArray data;
		HashMap<String, String>  map = new HashMap<String, String>();
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		if (Helper.isJSONValid(json))
		{
			try {
				data = new JSONArray(json);
				for(int i = 0; i < data.length(); i++){
					JSONObject d = data.getJSONObject(i);
					if (id.equals(d.getString("menu_id")))
					{
						map.clear();
						map.put("menu_id", d.getString("menu_id"));
						map.put("category_id", d.getString("category_id"));
						map.put("description", d.getString("description"));
						map.put("name", d.getString("name"));
						map.put("photo", d.getString("photo"));
						map.put("price", d.getString("price"));
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return map;
		}
		return null;
	}
	
	public static HashMap<String, String> getByName(String name){
		JSONArray data;
		HashMap<String, String>  map = new HashMap<String, String>();
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		if (Helper.isJSONValid(json))
		{
			try {
				data = new JSONArray(json);
				for(int i = 0; i < data.length(); i++){
					JSONObject d = data.getJSONObject(i);
					if (name.equals(d.getString("name")))
					{
						map.clear();
						map.put("menu_id", d.getString("menu_id"));
						map.put("category_id", d.getString("category_id"));
						map.put("description", d.getString("description"));
						map.put("name", d.getString("name"));
						map.put("photo", d.getString("photo"));
						map.put("price", d.getString("price"));
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return map;
		}
		return null;
	}
	
	public static ArrayList<HashMap<String, String>> getByCategoryID(String cid){
		JSONArray data;
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		if (Helper.isJSONValid(json))
		{
			Log.i("GET-JSON", json);
			try {
				data = new JSONArray(json);
				for(int i = 0; i < data.length(); i++){
					JSONObject d = data.getJSONObject(i);
					if (cid.equals(d.getString("category_id")))
					{
						HashMap<String, String>  map = new HashMap<String, String>();
						if(Api.disablemenu() == true){
							map.put("menu_id", d.getString("menu_id"));
							map.put("category_id", d.getString("category_id"));
							map.put("description", d.getString("description"));
							map.put("name", d.getString("name"));
							map.put("price", d.getString("price"));
							map.put("status", d.getString("status"));
							map.put("short_description",d.getString("short_description"));
							map.put("event", d.getString("event"));
							map.put("color", d.getString("color"));
							map.put("bcolor", d.getString("bcolor"));
							map.put("photo", d.getString("photo"));
							tempData.add(map);
						}
						else{
							if(d.getString("status").toUpperCase().equals("AVAILABLE")){
								map.put("menu_id", d.getString("menu_id"));
								map.put("category_id", d.getString("category_id"));
								map.put("description", d.getString("description"));
								map.put("name", d.getString("name"));
								map.put("price", d.getString("price"));
								map.put("status", d.getString("status"));
								map.put("short_description",d.getString("short_description"));
								map.put("event", d.getString("event"));
								map.put("color", d.getString("color"));
								map.put("bcolor", d.getString("bcolor"));
								map.put("photo", d.getString("photo"));
								tempData.add(map);
							}
						}
						tempData.add(map);
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return tempData;
		}
		return null;
	}
	
	public static int getCount(){
		JSONArray data;
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		if (Helper.isJSONValid(json))
		{
			try {
				data = new JSONArray(json);
				return data.length();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return 0;
	}
	
}
