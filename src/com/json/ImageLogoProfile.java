package com.json;

import com.lib.Helper;

public class ImageLogoProfile {
	private static ImageLogoProfile img;
	static String filename = "logoprofile.png";

	public static ImageLogoProfile getInstance() {
		if (img == null)
			img = new ImageLogoProfile();
		
		return img;
	}
	
	public static String add(String imagename) {
    	if(imagename.length() < 1)
    		return null;
    	Helper.saveFile(Helper.getDirDbox(), filename, imagename);
    	return filename;
    }
	
	//public static void getImage
	
}
