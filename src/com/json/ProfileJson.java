package com.json;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.lib.Helper;
import com.pos.Api;

public class ProfileJson {
	private static ProfileJson profilejson;
	static String filename = "profile.json";

	public static ProfileJson getInstance() {
		if (profilejson == null)
			profilejson = new ProfileJson();
		
		return profilejson;
	}
	
	public static String add(String cat) {
    	if(cat.length() < 1)
    		return null;
    	Helper.saveFile(Helper.getDirDboxInFile(), filename, cat);
    	return filename;
    }
	
	public static void setProfile(){
		JSONArray data,datatype;
		JSONObject jobj;
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String,String>>();
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		JSONObject jo;
		String error;
		
		if (Helper.isJSONValid(json))
		{
			try {
				jo = new JSONObject(json);
				JSONObject d = new JSONObject(jo.getString("result"));
				
				Api.plogo = d.getString("logo");
				Api.pnameclient = d.getString("name");
				Api.paddress = d.getString("address");
				Api.pphone = d.getString("phone");
				Api.pfax = d.getString("fax");
				Api.pcode = d.getString("code");
				Api.ptax = d.getString("tax");
				int banksize = 0;
				banksize = d.getJSONArray("bank").length();
				for(int i=0;i<banksize;i++){
					jobj = d.getJSONArray("bank").getJSONObject(i);
					HashMap<String, String>map = new HashMap<String, String>();
					map.put("bank_id", jobj.getString("bank_id"));
					map.put("name", jobj.getString("name"));
					Api.arraybank.add(map);
				}
				
			} catch (JSONException e) {
				Log.i("JSON-ERROR", e.toString());
			}
		}
		
	}
	
	public static int getCount(){
		JSONArray data;
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		if (Helper.isJSONValid(json))
		{
			try {
				data = new JSONArray(json);
				return data.length();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return 0;
	}
	
}
