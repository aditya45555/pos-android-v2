package com.json;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.lib.Helper;

public class CategoryJson {
	
	private static CategoryJson catjson;
	static String filename = "category.json";

	public static CategoryJson getInstance() {
		if (catjson == null)
			catjson = new CategoryJson();
		
		return catjson;
	}
	
	public static String add(String cat) {
    	if(cat.length() < 1)
    		return null;
    	Helper.saveFile(Helper.getDirDboxInFile(), filename, cat);
    	return filename;
    }
	
	public static ArrayList<HashMap<String, String>> getAll(String parent_id){
		JSONArray data;
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		if (Helper.isJSONValid(json))
		{
			try {
				data = new JSONArray(json);
				for(int i = 0; i < data.length(); i++){
					JSONObject d = data.getJSONObject(i);
					HashMap<String, String>  map = new HashMap<String, String>();
					if(!d.getString("parent_id").equals("")){
						if(d.getString("parent_id").toLowerCase().equals(parent_id.toLowerCase())){
							map.put("category_id", d.getString("category_id"));
							map.put("description", d.getString("description"));
							map.put("name", d.getString("name"));
							tempData.add(map);
						}
					}
					
					
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return tempData;
		}
		return null;
	}
	
	public static ArrayList<HashMap<String, String>> getParent(){
		JSONArray data;
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		if (Helper.isJSONValid(json))
		{
			try {
				data = new JSONArray(json);
				for(int i = 0; i < data.length(); i++){
					JSONObject d = data.getJSONObject(i);
					HashMap<String, String>  map = new HashMap<String, String>();
					if(d.getString("parent_id").equals("")){
						map.put("category_id", d.getString("category_id"));
						map.put("description", d.getString("description"));
						map.put("name", d.getString("name"));
						tempData.add(map);
					}
					
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return tempData;
		}
		return null;
	}
	
	
	public static HashMap<String, String> getById(String id){
		JSONArray data;
		HashMap<String, String>  map = new HashMap<String, String>();
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		if (Helper.isJSONValid(json))
		{
			try {
				data = new JSONArray(json);
				for(int i = 0; i < data.length(); i++){
					JSONObject d = data.getJSONObject(i);
					if (id.equals(d.getString("category_id")))
					{
						map.clear();
						map.put("category_id", d.getString("category_id"));
						map.put("description", d.getString("description"));
						map.put("name", d.getString("name"));
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return map;
		}
		return null;
	}
	
	public static HashMap<String, String> getByName(String name){
		JSONArray data;
		HashMap<String, String>  map = new HashMap<String, String>();
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		if (Helper.isJSONValid(json))
		{
			try {
				data = new JSONArray(json);
				for(int i = 0; i < data.length(); i++){
					JSONObject d = data.getJSONObject(i);
					if (name.equals(d.getString("name")))
					{
						map.clear();
						map.put("category_id", d.getString("category_id"));
						map.put("description", d.getString("description"));
						map.put("name", d.getString("name"));
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return map;
		}
		return null;
	}
	
	public static int getCount(){
		JSONArray data;
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		if (Helper.isJSONValid(json))
		{
			try {
				data = new JSONArray(json);
				return data.length();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return 0;
	}
	
}
