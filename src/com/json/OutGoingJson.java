package com.json;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.lib.Helper;

public class OutGoingJson {
	private static OutGoingJson outgoingjson;
	
	public static OutGoingJson getInstance() {
		if (outgoingjson == null)
			outgoingjson = new OutGoingJson();
		
		return outgoingjson;
	}
	
	public static String add(String api,String jsonfile) {
    	if(api.length() < 1)
    		return null;
    	 Calendar calendar = Calendar.getInstance();
    	 java.sql.Timestamp ourJavaTimestampObject = new java.sql.Timestamp(calendar.getTime().getTime());
    	String file = api+"-"+ourJavaTimestampObject;
    	if(CheckFile(file) == false){
    		Helper.saveFile(Helper.getDirDboxOutFile(), file, jsonfile);
        }
    	return file;
    }
	
	private static boolean CheckFile(String file){
		File f =new File(Helper.getDirDboxOutFile(),file);
		return f.isFile();
		
	}
	
	
	/*public static int getCount(){
		JSONArray data;
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		if (Helper.isJSONValid(json))
		{
			try {
				data = new JSONArray(json);
				return data.length();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return 0;
	}*/
}
