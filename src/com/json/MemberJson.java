package com.json;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.lib.Helper;

public class MemberJson {
	
	private static MemberJson memjson;
	static String filename = "member.json";
	
	public static MemberJson getInstance() {
		if (memjson == null)
			memjson = new MemberJson();
		
		return memjson;
	}
	
	public static String add(String cat,String phone,String name) {
    	if(cat.length() < 1)
    		return null;
    	String file = name+"_"+phone+".json";
    	if(CheckFile(file) == false){
    		Helper.saveFile(Helper.getDirDboxInFile(), file, cat);
        }
    	return file;
    }
	
	public static String addAll(String cat) {
		Log.i("DATA-FILE-MEMBER", cat);
    	if(cat.length() < 1)
    		return null;
    	String file = filename;
    	if(Helper.isJSONValid(cat)){
    		Helper.saveFile(Helper.getDirDboxInFile(), file, cat);
        }
    	return file;
    }
	
	private static boolean CheckFile(String file){
		File f =new File(Helper.getDirDboxInFile(),file);
		return f.isFile();
		
	}
	
	public static ArrayList<HashMap<String, String>> getAll(){
		JSONArray data;
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		if (Helper.isJSONValid(json))
		{
			try {
				data = new JSONArray(json);
				for(int i = 0; i < data.length(); i++){
					JSONObject d = data.getJSONObject(i);
					HashMap<String, String>  map = new HashMap<String, String>();
					map.put("name", d.getString("name"));
					map.put("email", d.getString("email"));
					map.put("phone", d.getString("phone"));
					
					tempData.add(map);
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return tempData;
		}
		return null;
	}
	
	public static HashMap<String, String> getById(String id){
		JSONArray data;
		HashMap<String, String>  map = new HashMap<String, String>();
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		if (Helper.isJSONValid(json))
		{
			try {
				data = new JSONArray(json);
				for(int i = 0; i < data.length(); i++){
					JSONObject d = data.getJSONObject(i);
					if (id.equals(d.getString("category_id")))
					{
						map.clear();
						map.put("name", d.getString("name"));
						map.put("email", d.getString("email"));
						map.put("phone", d.getString("phone"));
					}
				}
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return map;
		}
		return null;
	}
	
	public static ArrayList<HashMap<String, String>> getByNameOrPhone(String name,String phone){
		JSONArray data;
		ArrayList<HashMap<String, String>>  tempdata = new ArrayList<HashMap<String, String>>();
		HashMap<String, String>  map = new HashMap<String, String>();
		
		
		String files;
		File[] slistOfFiles = Helper.getDirDboxInFile().listFiles(); 
		
		for (int i = 0; i < slistOfFiles.length; i++) {
			if (slistOfFiles[i].length()==0)
				slistOfFiles[i].delete();
			else
			{
			   files = slistOfFiles[i].getName();
		       if (files.endsWith(".json") || files.endsWith(".JSON"))
		       {
		    	   if(files.contains("_")){
		    		   String[] filesplit = files.split("_");
		    		   String namefile = filesplit[0];
		    		   String phonefile = filesplit[1].toUpperCase().replace(".JSON","");
		    		   if(namefile.toUpperCase().contains(name.toUpperCase())||phone.contains(phonefile)){
		    			   String json = Helper.getFileContent(Helper.getDirDbox(), files); 
		    			   if (Helper.isJSONValid(json))
		    				{
		    					try {
		    						JSONObject jo = new JSONObject(json);
		    						
		    							if (jo.getString("name").length()>0)
		    							{
		    								map.clear();
		    								map.put("member_id", jo.getString("member_id"));
		    								map.put("name", jo.getString("name"));
		    								map.put("email", jo.getString("email"));
		    								map.put("phone", jo.getString("phone"));
		    								tempdata.add(map);
		    							}
		    						
		    					} catch (JSONException e) {
		    						e.printStackTrace();
		    					}
		    					return tempdata;
		    				}
		    			   
		    		   }
		    	   }
		       }
			}
		}
		return tempdata;
	}
	
	public static ArrayList<HashMap<String, String>> getByNameOrPhoneFile(String name,String phone){
		JSONArray data;
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		if (Helper.isJSONValid(json))
		{
			Log.i("DATA-FILE", json);
			try {
				data = new JSONArray(json);
				for(int i = 0; i < data.length(); i++){
					JSONObject d = data.getJSONObject(i);
					HashMap<String, String>  map = new HashMap<String, String>();
					if(name.toUpperCase().contains(d.getString("name").toUpperCase())){
						map.put("member_id", d.getString("member_id"));
						map.put("name", d.getString("name"));
						map.put("email", d.getString("email"));
						map.put("phone", d.getString("phone"));
						tempData.add(map);
					}
					
				}
			} catch (JSONException e) {
				Log.i("ERROR-MEMBER_JSON", e.toString());
			}
			Log.i("DATA-MEMBER", tempData.toString());
		}
		return tempData;
	}
	
	public static int getCount(){
		JSONArray data;
		String json = Helper.getFileContent(Helper.getDirDboxInFile(), filename);
		if (Helper.isJSONValid(json))
		{
			try {
				data = new JSONArray(json);
				return data.length();
			} catch (JSONException e) {
				e.printStackTrace();
			}
		}
		return 0;
	}
	
}
