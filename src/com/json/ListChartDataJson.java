package com.json;

import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.lib.Helper;
import com.pos.Api;
import com.pos.Catalogue;

public class ListChartDataJson {
	
	private static ListChartDataJson listchart;
	public static ListChartDataJson getInstance() {
		if (listchart == null)
			listchart = new ListChartDataJson();
		
		return listchart;
	}
	
	public static ArrayList<HashMap<String, String>> getListChart(String status,String sales_id){
		JSONArray data;
		ArrayList<HashMap<String, String>> tempData = new ArrayList<HashMap<String, String>>();
		
		if(status.toUpperCase().equals("BELUM CONFIRM")){
			tempData = Api.getOrderTemp(sales_id);
		}
		else{
			tempData = Api.getOrderConfirm(sales_id);
		}
		return tempData;
	}
	
	
	
	
	
}
