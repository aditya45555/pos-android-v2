package com.dialog;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import com.adapter.ListChartAdapter;
import com.adapter.ListConfirmAdapter;
import com.lib.Helper;
import com.pos.Catalogue;
import com.pos.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DialogDiskon extends DialogFragment {

	private int position;
	private String nilai;
	private String persen;
	private boolean isFloatNilai;
	private boolean isFloatPersen;
	private boolean isNilai;
	private boolean isPersen;
	private ArrayList<HashMap<String, String>> dataAdapter;
	private ListChartAdapter adapter;
	private TextView tDisc;
	private int baseAdapter;
	private String ammount;
	private double am;
	
	private LinearLayout layn;
	private LinearLayout layp;
	private TextView txtnilai;
	private TextView txtpersen;
	private ImageView deln;	    
    private ImageView delp;
    
    private DecimalFormat format;
    private DecimalFormat formatp;
    
    public DialogDiskon(ArrayList<HashMap<String, String>> data, ListChartAdapter Dadapter){
		dataAdapter = data;
		adapter = Dadapter;
		baseAdapter = 1;
	}
	
	public DialogDiskon(TextView disc){
		tDisc = disc;
		baseAdapter = 0;
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		final Activity activity = getActivity();
		Bundle a = this.getArguments();
		position = a.getInt("position");
		ammount = a.getString("ammount");
		am = Double.valueOf(ammount);
		
		format = new DecimalFormat("#,###,###.###");
	    formatp = new DecimalFormat("###.#");
		
		LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = inflater.inflate(R.layout.dialogdiskon, null);
		
		isNilai = true;
		isPersen = false;
		
		layn = (LinearLayout)v.findViewById(R.id.layoutnilai);	    
	    layp = (LinearLayout)v.findViewById(R.id.layoutpersen);	    
	    
	    txtnilai = (TextView)v.findViewById(R.id.txtnilai);
	    
	    layn.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				isNilai = true;
				isPersen = false;
			}
		});
	    
	    txtpersen = (TextView)v.findViewById(R.id.txtpersen);
	    
	    layp.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				isNilai = false;
				isPersen = true;
			}
		});
	    
	    deln = (ImageView)v.findViewById(R.id.backnilai);	    
	    delp = (ImageView)v.findViewById(R.id.backpersen);
	    
	    isFloatNilai = false;
	    isFloatPersen = false;
	    nilai = "";
	    persen = "";
		
		initView(v);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(activity);
		builder.setTitle("Rubah Diskon");
		builder.setView(v);	
			    	    	   
	    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				
				switch(baseAdapter) {
					case 1: {
						dataAdapter.get(position).put("discount", persen);
						adapter.notifyDataSetChanged();
						break;
					}
					case 0: {
						tDisc.setText(persen);
						Catalogue.getInstance().setDataTotalMenu();
						break;
					}
				}
			}
		});
	    builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				dialog.cancel();
			}
		});	    
	    
		return builder.create();
	}
	
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		
		if (getDialog() == null) {
			 Log.i("get dialog", "false");
			 return;
		 }
		 
		 DisplayMetrics metrics = getResources().getDisplayMetrics();
		 int wmlp = (int) (metrics.widthPixels * 0.525);
		 if(!Helper.isTablet(getActivity()))
			 wmlp = (int) (metrics.widthPixels * 0.95);
		 int wn = (int)Math.round(wmlp*0.65);
		 int wp = (int)Math.round(wmlp*0.35);
		 
		 LinearLayout.LayoutParams lln = new LinearLayout.LayoutParams(wn, LinearLayout.LayoutParams.WRAP_CONTENT);
		 layn.setLayoutParams(lln);
		 
		 int wnl = (int)Math.round(wn*0.8);
		 LinearLayout.LayoutParams llnl = new LinearLayout.LayoutParams(wnl, LinearLayout.LayoutParams.WRAP_CONTENT);
		 txtnilai.setLayoutParams(llnl);
		 
		 LinearLayout.LayoutParams llp = new LinearLayout.LayoutParams(wp, LinearLayout.LayoutParams.WRAP_CONTENT);
		 layp.setLayoutParams(llp);
		 
		 int wnp = (int)Math.round(wp*0.6);
		 LinearLayout.LayoutParams llpr = new LinearLayout.LayoutParams(wnp, LinearLayout.LayoutParams.WRAP_CONTENT);
		 txtpersen.setLayoutParams(llpr);
	}
	
	private void initView(View v) {
		final TextView tsatu = (TextView)v.findViewById(R.id.satu);
	    final TextView tdua = (TextView)v.findViewById(R.id.dua);
	    final TextView ttiga = (TextView)v.findViewById(R.id.tiga);
	    final TextView tempat = (TextView)v.findViewById(R.id.empat);
	    final TextView tlima = (TextView)v.findViewById(R.id.lima);
	    final TextView tenam = (TextView)v.findViewById(R.id.enam);
	    final TextView ttujuh = (TextView)v.findViewById(R.id.tujuh);
	    final TextView tdelapan = (TextView)v.findViewById(R.id.delapan);
	    final TextView tsembilan = (TextView)v.findViewById(R.id.sembilan);
	    final TextView tnol = (TextView)v.findViewById(R.id.nol);
	    final TextView tdblnol = (TextView)v.findViewById(R.id.doublenol);
	    TextView ttitik = (TextView)v.findViewById(R.id.titik);
	    
	    tsatu.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isNilai)
					rubahNilai(tsatu.getText().toString());
				if(isPersen)
					rubahPersen(tsatu.getText().toString());
			}
		});	    
	    tdua.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isNilai)
					rubahNilai(tdua.getText().toString());
				if(isPersen)
					rubahPersen(tdua.getText().toString());
			}
		});
	    ttiga.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isNilai)
					rubahNilai(ttiga.getText().toString());
				if(isPersen)
					rubahPersen(ttiga.getText().toString());
			}
		});
	    tempat.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isNilai)
					rubahNilai(tempat.getText().toString());
				if(isPersen)
					rubahPersen(tempat.getText().toString());
			}
		});
	    tlima.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isNilai)
					rubahNilai(tlima.getText().toString());
				if(isPersen)
					rubahPersen(tlima.getText().toString());
			}
		});
	    tenam.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isNilai)
					rubahNilai(tenam.getText().toString());
				if(isPersen)
					rubahPersen(tenam.getText().toString());
			}
		});
	    ttujuh.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isNilai)
					rubahNilai(ttujuh.getText().toString());
				if(isPersen)
					rubahPersen(ttujuh.getText().toString());
			}
		});
	    tdelapan.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isNilai)
					rubahNilai(tdelapan.getText().toString());
				if(isPersen)
					rubahPersen(tdelapan.getText().toString());
			}
		});
	    tsembilan.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isNilai)
					rubahNilai(tsembilan.getText().toString());
				if(isPersen)
					rubahPersen(tsembilan.getText().toString());
			}
		});
	    tnol.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isNilai)
					rubahNilai(tnol.getText().toString());
				if(isPersen)
					rubahPersen(tnol.getText().toString());
			}
		});
	    tdblnol.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isNilai)
					rubahNilai(tdblnol.getText().toString());
				if(isPersen)
					rubahPersen(tdblnol.getText().toString());
			}
		});
	    ttitik.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(isNilai) {
					if(!isFloatNilai) {
						isFloatNilai = true;
						nilai = nilai+".";
						Double hsl = Double.valueOf(nilai);
						txtnilai.setText(format.format(hsl));
					}					
				}
				if(isPersen) {
					if(!isFloatPersen) {
						isFloatPersen = true;
						int p = Integer.parseInt(persen);
						if(p < 100) {
							persen = persen+".";
							Double hsl = Double.valueOf(persen);
							txtpersen.setText(formatp.format(hsl));
						}
					}					
				}	
			}
		});
	    deln.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(nilai.length() > 0) {										
					String nn = nilai.substring(0, nilai.length() -1);
					nilai = nn;
					
					isFloatNilai = false;
					if(nilai.indexOf(".") > -1)
						isFloatNilai = true;
					
					Double hsl = Double.valueOf(nilai);
					txtnilai.setText(format.format(hsl));
					
					Double np = (hsl/am)*100;
					persen = formatp.format(np);
					
					isFloatPersen = false;
					if(persen.indexOf(".") > -1)
						isFloatPersen = true;
					
					txtpersen.setText(persen);
				}
				else {
					nilai = "0";
					persen = "0";
				}	
			}
		});
	    delp.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(persen.length() > 0) {
					String nn = persen.substring(0, persen.length() -1);
					persen = nn;										
					
					Double hsl = Double.valueOf(persen);
					persen = formatp.format(hsl);
					
					isFloatPersen = false;
					if(persen.indexOf(".") > -1)
						isFloatPersen = true;
					
					txtpersen.setText(persen);
					
					Double np = hsl/100;
					Double nnp = am*np;
					String sn = format.format(nnp);					
					nilai = sn.replace(",", "");
					
					isFloatNilai = false;
					if(nilai.indexOf(".") > -1)
						isFloatNilai = true;
					
					txtnilai.setText(sn);
				}
				else {
					persen = "0";
					nilai = "0";
				}		
			}
		});
	}		
	
	private void rubahNilai(String value) {
		if(nilai.indexOf(".") > -1)
			isFloatNilai = true;
		
		String bn = nilai+value;
		Double nm = Double.valueOf(bn);
		
		String nl = format.format(nm).replace(".",",");
		Log.i("NL",nl);
		nilai = nl.replace(",", "");
		
		if(nm >= am) {
			nm = am;
			nilai = ammount;
		}
		
		txtnilai.setText(format.format(nm));
		
		Double np = (nm/am)*100;
		persen = formatp.format(np).replace(",", ".");
		if(persen.indexOf(".") > -1)
			isFloatPersen = true;
		
		txtpersen.setText(persen);
	}
	
	private void rubahPersen(String value) {
		if(persen.indexOf(".") > -1)
			isFloatPersen = true;
		
		Double pm = Double.valueOf(persen+value);
		persen = formatp.format(pm);
		
		if(pm >= 100) {
			pm = (double)100;
			persen = "100";
		}
		
		txtpersen.setText(persen);
		
		Double nm = pm/100;
		Double nnm = am*nm;
		String nlm = format.format(nnm);
		nilai = nlm.replace(",", "");
		if(nilai.indexOf(".") > -1)
			isFloatNilai = true;
		
		txtnilai.setText(nlm);
	}
}
