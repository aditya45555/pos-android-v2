package com.dialog;

import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.astuetz.PagerSlidingTabStrip;
import com.lib.Helper;
import com.lib.Print;
import com.pos.Api;
import com.pos.Chilkat;
import com.pos.PaymentFragment;
import com.pos.R;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.text.format.Time;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class DialogPayment extends DialogFragment {
	
	private int position;
	private TextView tTotal;
	private TextView tChange,txt_total_harga;
	private String dTitle;
	private AlertDialog.Builder builder;
	private double dTotal;
	
	private DecimalFormat format;
	
	private MyPagerAdapter adapter;
	private PagerSlidingTabStrip tabs;
	private ViewPager pager;
	private String table_id;
	private int wmlp;
	
	private ArrayList<HashMap<String, String>> dataAdapter;
	private ArrayList<HashMap<String, String>> itemMenu;
	private Print print;
	
	public DialogPayment(TextView txttotal, Double total, ArrayList<HashMap<String, String>> dtmenu, ArrayList<HashMap<String, String>> itmenu,String tid) {		
		tTotal = txttotal;
		dTotal = total;
		dataAdapter = dtmenu;
		itemMenu = itmenu;
		table_id = tid;
	}
	
	@Override
	public void onCreate(@Nullable Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		super.onCreate(savedInstanceState);
		Bundle a = this.getArguments();
		position = a.getInt("position");
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		getDialog().getWindow().requestFeature(DialogFragment.STYLE_NO_TITLE);
		inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = inflater.inflate(R.layout.dialog_payment, null);
		
		final Button bCancel = (Button)v.findViewById(R.id.bCancel);	
		final Button bOK = (Button)v.findViewById(R.id.bOK);
		tChange = (TextView)v.findViewById(R.id.kembali);
		txt_total_harga = (TextView)v.findViewById(R.id.txt_total_harga);
		format = new DecimalFormat("#,###,###.###");
		String nch = format.format(dTotal);
		txt_total_harga.setText("Total Harga "+String.valueOf(nch));
		
		tabs = (PagerSlidingTabStrip) v.findViewById(R.id.tabs);
		pager = (ViewPager) v.findViewById(R.id.pager);
		adapter = new MyPagerAdapter(getChildFragmentManager(), itemMenu);
		pager.setAdapter(adapter);		
		
		tabs.setViewPager(pager);				
		
		bCancel.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Toast.makeText(getActivity(), bCancel.getText().toString(), Toast.LENGTH_SHORT).show();
				getDialog().dismiss();
			}
		});
		
		bOK.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				//Toast.makeText(getActivity(), String.valueOf(PaymentFragment.getChange()),Toast.LENGTH_LONG).show();
				if(Double.valueOf(PaymentFragment.getChange())>=0){
					if(Api.getActivePrinter() == true){
						if(Api.getPrinterBluetooth() == null){
		            		Helper.dialogSettingPrinter(getActivity());
		            	}
						else{
							if(Api.getNumCopyBill() == null){
								Helper.dialogSetNumCopyBill(getActivity());
							}
							else{
								itemMenu.get(1).put("reference", PaymentFragment.gettRef().getText().toString());
								DialogTip tip = new DialogTip(dataAdapter, itemMenu,table_id,dTotal);
								tip.show(getFragmentManager(), "dialogtip");	
							}
							
						}
					}
					else{
						//Log.i("ACTIVE-PRINTER","Tidak");
						if(Api.getNumCopyBill() == null){
							Helper.dialogSetNumCopyBill(getActivity());
						}
						else{
							itemMenu.get(1).put("reference", PaymentFragment.gettRef().getText().toString());
							DialogTip tip = new DialogTip(dataAdapter, itemMenu,table_id,dTotal);
							tip.show(getFragmentManager(), "dialogtip");	
						}
					}
					
				}
				else{
					Toast.makeText(getActivity(), "Pembayaran Kurang", Toast.LENGTH_LONG).show();
				}	
			}
		});		
		
		return v;
	}		
	
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		
		if (getDialog() == null) {
			Log.i("get dialog", "false");
			return;
		}												
		 
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		wmlp = (int) (metrics.widthPixels * 0.95);
		if(!Helper.isTablet(getActivity()))
			wmlp = (int) (metrics.widthPixels * 0.95);
				
		getDialog().getWindow().setLayout(wmlp, ViewGroup.LayoutParams.WRAP_CONTENT);
		
	}
	
	public class MyPagerAdapter extends FragmentPagerAdapter {

		ArrayList<HashMap<String, String>> d;

		public MyPagerAdapter(FragmentManager fm, ArrayList<HashMap<String, String>> data) {
			super(fm);
			d = data;
		}			
		
		@Override
		public long getItemId(int position) {
			// TODO Auto-generated method stub
			return position;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			return d.get(position).get("name");
		}

		@Override
		public int getCount() {
			return d.size();
		}			

		@Override
		public Fragment getItem(int position) {
			return PaymentFragment.newInstance(position, dTotal, d.get(position).get("name"), d, adapter, tChange);
		}
	}
}
