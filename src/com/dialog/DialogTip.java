package com.dialog;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.asyntask.printBuy;
import com.lib.Helper;
import com.pos.Api;
import com.pos.Catalogue;
import com.pos.Chilkat;
import com.pos.Main;
import com.pos.PaymentFragment;
import com.pos.R;
import com.pos.SplashScreen;
import com.lib.Print;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v4.print.PrintHelper;
import android.text.format.Time;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class DialogTip extends DialogFragment {
	
	private ArrayList<HashMap<String, String>> dataAdapter;
	private ArrayList<HashMap<String, String>> itemMenu;
	String sales_id;
	private boolean isFloat;
	private TextView txtqty;
	private LinearLayout lq;
	private DecimalFormat format;
	private String sTip,table_id;
	private Double dtotal;
	private byte Align_LEFT = (byte)0x30; //������
	private byte Align_CENTER = (byte)0x31;//����
	private byte Align_RIGHT = (byte)0x32;
	private final static byte B_ESC = (byte) 0x1B;
	//lines feed
	private byte[] lfs = new byte[]{0x1B,'d', 5};
	//cut paper
	private byte[] cut = new byte[]{0x1B,'i'};
	//crash drawer
	private byte[] openCD={27,112,0,60,120};
	private Print print;
	public String codetransaction;
	private static DialogTip sMainActivity;
	private printBuy taskPrintBuy;
	Intent i;
    Bundle b;
    private JSONArray member;
    private JSONObject objectMember;
	
	public DialogTip(ArrayList<HashMap<String, String>> dtmenu, ArrayList<HashMap<String, String>> itmenu,String tableid,Double total) {
		dataAdapter = dtmenu;
		itemMenu = itmenu;
		table_id = tableid;
		dtotal = total;
		sMainActivity = this;
	}
	
	public static DialogTip getinstance(){
		return sMainActivity;
	}
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		LayoutInflater inflater = (LayoutInflater)getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View v = inflater.inflate(R.layout.dialogqty, null);
		
		isFloat = false;
	    
		lq = (LinearLayout)v.findViewById(R.id.layoutqty);
	    txtqty = (TextView)v.findViewById(R.id.txtqty);
	    
	    format = new DecimalFormat("#,###,###.###");
	    sTip = "0";
	   
	    initView(v);
		
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(v);	   	    	    	    	    	    
	    
	    builder.setTitle("Tip");
	   
	    builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				// TODO Auto-generated method stub
				itemMenu.get(0).put("tip", sTip);
				itemMenu.get(1).put("tip", sTip);
				try {
					//print.sendData2();
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				/*try {
					   Bitmap bitmap = BitmapFactory.decodeResource(getActivity().getResources(),R.drawable.caktopa);
				       ByteArrayOutputStream stream = new ByteArrayOutputStream();
				       bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
				       byte[] data = stream.toByteArray();
				        
					print.sendData2(data);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}*/
				sendDataJual();
				dialog.dismiss();
			}
		});
	    if(Api.getActivePrinter() == true){
	    	print = new Print(getActivity());
	    }
	    
		return builder.create();
	}
	
	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		
		// safety check
		if (getDialog() == null) {
			Log.i("get dialog", "false");
			return;
		}
		 
		DisplayMetrics metrics = getResources().getDisplayMetrics();
		int wmlp = (int) (metrics.widthPixels * 0.525);
		if(!Helper.isTablet(getActivity()))
			wmlp = (int) (metrics.widthPixels * 0.95);
		int wn = (int)Math.round(wmlp*0.75);
		 
		LinearLayout.LayoutParams lln = new LinearLayout.LayoutParams(wn, LinearLayout.LayoutParams.WRAP_CONTENT);
		txtqty.setLayoutParams(lln);
	}
	
	private void initView(View v) {
		final ImageView tdel = (ImageView)v.findViewById(R.id.hapusqty);
	    final TextView tsatu = (TextView)v.findViewById(R.id.satu);
	    final TextView tdua = (TextView)v.findViewById(R.id.dua);
	    final TextView ttiga = (TextView)v.findViewById(R.id.tiga);
	    final TextView tempat = (TextView)v.findViewById(R.id.empat);
	    final TextView tlima = (TextView)v.findViewById(R.id.lima);
	    final TextView tenam = (TextView)v.findViewById(R.id.enam);
	    final TextView ttujuh = (TextView)v.findViewById(R.id.tujuh);
	    final TextView tdelapan = (TextView)v.findViewById(R.id.delapan);
	    final TextView tsembilan = (TextView)v.findViewById(R.id.sembilan);
	    final TextView tnol = (TextView)v.findViewById(R.id.nol);
	    final TextView tdblnol = (TextView)v.findViewById(R.id.doublenol);
	    TextView ttitik = (TextView)v.findViewById(R.id.titik);
	    
	    tsatu.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				rubahTip(tsatu.getText().toString());
			}
		});
	    tdua.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				rubahTip(tdua.getText().toString());
			}
		});
	    ttiga.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				rubahTip(ttiga.getText().toString());
			}
		});
	    tempat.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				rubahTip(tempat.getText().toString());
			}
		});
	    tlima.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				rubahTip(tlima.getText().toString());
			}
		});
	    tenam.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				rubahTip(tenam.getText().toString());
			}
		});
	    ttujuh.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				rubahTip(ttujuh.getText().toString());
			}
		});
	    tdelapan.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				rubahTip(tdelapan.getText().toString());
			}
		});
	    tsembilan.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				rubahTip(tsembilan.getText().toString());
			}
		});
	    tnol.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				rubahTip(tnol.getText().toString());
			}
		});
	    tdblnol.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				rubahTip(tdblnol.getText().toString());
			}
		});
	    ttitik.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(!isFloat) {
					isFloat = true;
					sTip = sTip+".";
					Double hsl = Double.valueOf(sTip);
					txtqty.setText(format.format(hsl));
				}				
			}
		});	
	    tdel.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(sTip.length() > 0) {
					String nn = sTip.substring(0, sTip.length() -1);
					sTip = nn;
					
					isFloat = false;
					if(sTip.indexOf(".") > -1)
						isFloat = true;
					
					Double hsl = Double.valueOf(sTip);
					txtqty.setText(format.format(hsl));
				}
			}
		});
	}
	
	private void rubahTip(String value) {
		if(sTip.indexOf(".") > -1)
			isFloat = true;
		
		String q = sTip+value;
		Double nm = Double.valueOf(q);
		
		String nl = format.format(nm);
		sTip = nl.replace(",", "");
		
		txtqty.setText(format.format(nm));
	}
	
	private void sendDataJual() {		
		Time today = new Time(Time.getCurrentTimezone());
		today.setToNow();	
		Long datetime = today.toMillis(true)/1000;
		double tax = 0,disc = 0;
		sales_id = dataAdapter.get(0).get("sales_id");
		JSONArray menu = new JSONArray();
		
		for(int i=0; i<dataAdapter.size(); i++) {
			HashMap<String, String> map = new HashMap<String, String>();
			map = dataAdapter.get(i);					
			JSONObject jm = new JSONObject();					
			try {
				jm.put("sales_detail_id", map.get("sales_detail_id"));
				jm.put("sales_id", map.get("sales_id"));
				jm.put("id", map.get("id"));
				jm.put("type", map.get("type"));
				jm.put("name", map.get("name"));
				jm.put("image", map.get("image"));
				jm.put("quantity", map.get("quantity"));
				jm.put("discount", map.get("discount"));
				jm.put("price", map.get("price"));
				menu.put(jm);
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}				
		}
		
		String user_id = Api.getUid();
		String client_id = Api.getClientId();
		String branch_id = Api.getBranchId();
		
		
		String total_kredit = "";
		String total_cash = "";
		String total_voucher = "0";
		if(Double.valueOf(itemMenu.get(1).get("pay"))>0){
			total_kredit = String.valueOf(itemMenu.get(1).get("pay"));
			total_cash = String.valueOf(itemMenu.get(0).get("pay"));
		}
		else{
			total_kredit = String.valueOf(itemMenu.get(1).get("total"));
			total_cash = String.valueOf(itemMenu.get(0).get("total"));
		}
		
		JSONObject js = new JSONObject();
		try {
			JSONArray cash = new JSONArray();
			JSONObject jc = new JSONObject();
			String taxcash = "0";
			String discountcash = "0";
			
			taxcash = String.valueOf(Double.valueOf(total_cash) * (Double.valueOf(Api.getTax())/100));
			discountcash = itemMenu.get(0).get("discount");
			jc.put("discount", discountcash);
			jc.put("tax", taxcash);
			jc.put("total", total_cash);
			jc.put("tips", itemMenu.get(0).get("tip"));
			jc.put("bank_id", "");
			jc.put("reference_number", "");
			
			cash.put(jc);
			js.put("cash", cash.toString());
			
			JSONArray kredit = new JSONArray();
			JSONObject jk = new JSONObject();
			
			String taxkredit = String.valueOf(Double.valueOf(total_kredit) * (Double.valueOf(Api.getTax())/100));
			jk.put("total", total_kredit);
			jk.put("reference_number", itemMenu.get(1).get("reference"));
			itemMenu.get(1).put("discount", String.valueOf(disc));
			jk.put("discount", itemMenu.get(1).get("discount"));
			jk.put("tax", taxkredit);
			jk.put("tips", itemMenu.get(1).get("tip"));
			jk.put("bank_id", itemMenu.get(1).get("bank_id"));
			kredit.put(jk);
			js.put("kredit", kredit.toString());
			
			JSONArray voucher = new JSONArray();
			JSONObject jv = new JSONObject();

			jv.put("total", "0");
			jv.put("reference_number", "");
			jv.put("discount", "0");
			jv.put("tax", "0");
			jv.put("bank_id", "");
			jv.put("tips", "0");
			voucher.put(jv);
			js.put("voucher", voucher.toString());
			
		
			String member_id = null;
			String member_name = null;
			String member_phone = null;
			String member_email = null;
			member = new JSONArray();
			objectMember = new JSONObject();
			if(Double.valueOf(itemMenu.get(0).get("total"))>0){
				member_id = itemMenu.get(0).get("member_id");
				member_name = itemMenu.get(0).get("member_name");
				member_phone = itemMenu.get(0).get("member_phone");
				member_email = itemMenu.get(0).get("member_email");
			}
			
			if(Double.valueOf(itemMenu.get(1).get("total"))>0){
				member_id = itemMenu.get(1).get("member_id");
				member_name = itemMenu.get(1).get("member_name");
				member_phone = itemMenu.get(1).get("member_phone");
				member_email = itemMenu.get(1).get("member_email");
			}
			
			objectMember.put("member_id", member_id);
			objectMember.put("member_name", member_name);
			objectMember.put("member_phone", member_phone);
			objectMember.put("member_email", member_email);
			member.put(objectMember);
			//js.put("member", member.toString());
			
			taskPrintBuy = new printBuy(sales_id,table_id,String.valueOf(datetime),js,getActivity());
			taskPrintBuy.execute();
				
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void openCd()
	{
		try {
			print.sendData(openCD);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void closePrint(){
		try {
			print.closeBT();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void olahPrint() {		
		try {
			Time today = new Time(Time.getCurrentTimezone());
			today.setToNow();	
			Long datetime = today.toMillis(true)/1000;
			byte[] lft = new byte[]{B_ESC,(byte)0x61,Align_LEFT};
        	byte[] cntr = new byte[]{B_ESC,(byte)0x61,Align_CENTER};
        	byte[] rght = new byte[]{B_ESC,(byte)0x61,Align_RIGHT};
			//mulai print text
			//header text bisa diambil dari referensi data server atau dari settingan local
        	String l = new String(lft);
        	String r = new String(rght);
        	String c = new String(cntr);
        	String f = new String(lfs);
        	
        	String msg = c;
        	
			msg += Api.getNameClient()+"\n";			
			msg += Api.getAddress()+"\n";
			msg += "Phone : \t "+Api.getPhone()+"\n";
			msg += "Fax : \t "+Api.getFax()+"\n";
			
			msg += "\n";

			msg += l;
			msg += today.format("%d-%m-%Y %H:%M:%S")+" \t ";
			msg += codetransaction;
			msg += r ;
			
			msg += "\n________________________________\n";
			int subtotal = 0;
			for(int i=0; i<dataAdapter.size(); i++) {
				HashMap<String, String> map = new HashMap<String, String>();
				map = dataAdapter.get(i);	
				
				msg += l;
				msg += map.get("name")+"\n";
				
				int qty = (int) Math.round(Double.valueOf(map.get("quantity")));
				int price = (int) Math.round(Double.valueOf(map.get("price")));
				int dsc = (int) Math.round(Double.valueOf(map.get("discount")));
				msg += l;
				msg += map.get("quantity")+"x \t "+Helper.formatDecimal(Double.valueOf(map.get("price")))+" \t "+String.valueOf(dsc)+"% \t";				
					
				int totalMenu = (price-((price*dsc)/100))*qty;
				msg += r;
				msg += Helper.formatDecimal(Double.valueOf(totalMenu))+"\n";
				
				subtotal = subtotal+totalMenu;				
			}
		//	msg += l;
			msg += "\n________________________________\n";
			
			msg += l;
			msg += "subTotal:"+Helper.getTabPrint("subTotal:", Helper.formatDecimal(Double.valueOf(subtotal)));
			msg += r;
			msg += Helper.formatDecimal(Double.valueOf(subtotal));
			msg += "\n";
			
			int disc = 0;
			
			if(!itemMenu.get(0).get("discount").equals("0")){
				disc =  (int) Math.round(Double.valueOf(itemMenu.get(0).get("discount")));
			}
			else if (!itemMenu.get(0).get("discount").equals("0")&&!itemMenu.get(1).get("discount").equals("0")){
				disc =  (int) Math.round(Double.valueOf(itemMenu.get(0).get("discount")));
			}
			else if(!itemMenu.get(1).get("discount").equals("0")){
				disc =  (int) Math.round(Double.valueOf(itemMenu.get(1).get("discount")));
			}
			double tax = Double.valueOf(Api.getTax());
			double taxtotal = (tax/100) * subtotal;
			double disctotal = (disc/100) * subtotal;
			
			msg += l;
			msg += "Diskon("+disc+"%):"+Helper.getTabPrint("Diskon("+disc+"%):",String.valueOf(disctotal));
			msg += r;
			msg += String.valueOf(disctotal);
			msg += "\n";
			
			msg += l;
			msg += "Tax("+tax+"%):"+Helper.getTabPrint("Tax("+tax+"%):",String.valueOf(taxtotal));
			msg += r;
			msg += String.valueOf(taxtotal);
			msg += "\n";
			
			Double total = (double) (subtotal-((subtotal*disc)/100));
			total = total + (total*(tax/100));
			msg += l;
			msg += "Total:"+Helper.getTabPrint("Total:",Helper.formatDecimal(Double.valueOf(total)));
			msg += r;
			msg += Helper.formatDecimal(Double.valueOf(total));
			msg += "\n\n";
			int cash = 0;
			
			if(!itemMenu.get(0).get("pay").equals("0")){
			    cash = (int) Math.round(Double.valueOf(itemMenu.get(0).get("pay")));
				msg += l;
				msg += "Cash:"+Helper.getTabPrint("Cash:",Helper.formatDecimal(Double.valueOf(itemMenu.get(0).get("pay"))));
				msg += r;
				msg += Helper.formatDecimal(Double.valueOf(itemMenu.get(0).get("pay")));
				msg += "\n"; 
			}
			
			int kredit = 0;
			if(!itemMenu.get(1).get("pay").equals("0")) {
				kredit = (int) Math.round(Double.valueOf(itemMenu.get(1).get("pay")));
				msg += l;
				msg += "Kredit:"+Helper.getTabPrint("Kredit:",Helper.formatDecimal(Double.valueOf(itemMenu.get(1).get("pay"))));
				msg += r;
				msg += Helper.formatDecimal(Double.valueOf(itemMenu.get(1).get("pay")));
				msg += "\n";
			}				
			
			Double changeDue = (cash+kredit)-total;	
			msg += l;
			msg += "Kembali:"+Helper.getTabPrint("Kembali:",Helper.formatDecimal(Double.valueOf(changeDue)));
			msg += r;
			msg += Helper.formatDecimal(Double.valueOf(changeDue));
			
			try {
				
				if(objectMember.getString("member_name").length()>0){
						msg +="\n";
						msg += l;
						msg += "Member:"+Helper.getTabPrint("Member:",objectMember.getString("member_name"));
						msg += r;
						msg += objectMember.getString("member_name");
				}
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			msg += "\n________________________________\n";
			msg += l;
			msg += "Kasir: \t "+Api.getUname()+"\n\n";
			msg += c;
			msg += "TERIMA KASIH\n";
			msg += "Harga sudah termasuk PPn";
			msg += f;
			msg += "\n";
			byte[] data = msg.getBytes();
			
			int numcopy = Integer.valueOf(Api.getNumCopyBill());
			for(int i=0;i<numcopy;i++){
				print.sendData(data);
			}
			
			msg = null;
			
			//Toast.makeText(getActivity(), "Data transaksi berhasil dikirim!", Toast.LENGTH_SHORT).show();
			
			
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}
