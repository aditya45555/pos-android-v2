package com.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.R.color;
import android.content.ClipData.Item;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.opengl.Visibility;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lib.Helper;
import com.pos.Api;
import com.pos.Catalogue;
import com.pos.Main;
import com.pos.R;

public class TableConfirmAdapter extends BaseAdapter {  //implements StickyGridHeadersSimpleAdapter {
    private int mHeaderResId;
    private LayoutInflater mInflater;
    private int mItemResId;
    private ArrayList<HashMap<String, String>> DataArray;
    private int count = 0;
    private Context con;
    View vi;
    public TableConfirmAdapter(Context context,int headerResId, int itemResId, ArrayList<HashMap<String, String>> data) {
    	init(context, headerResId, itemResId, data);
    }
    
    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public int getCount() {
    	return DataArray.size();
    }

    public CharSequence getHeaderId(int position) {
    	HashMap<String, String> item  = getItem(position);
        CharSequence value;
        if (item.get("name") instanceof CharSequence) {
            value = (CharSequence) item.get("name").toUpperCase();
        } else {
            value = item.get("name").toString().toUpperCase();
        }
        return value.subSequence(0, 1);
    }

    public View getHeaderView(int position, View convertView, ViewGroup parent) {
    	View vi=convertView;
		if(convertView==null)
			vi = mInflater.inflate(mHeaderResId, parent, false);
		TextView textView = (TextView) vi.findViewById(android.R.id.text1);
		
		HashMap<String, String> item = getItem(position);
		textView.setText(item.get("name").toUpperCase().subSequence(0, 1));
		
        return vi;
    }

    @Override
    public HashMap<String, String> getItem(int position) {
        return DataArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
    	vi=convertView;
    	if(convertView == null)
    	{
    		vi = mInflater.inflate(mItemResId, parent, false);
    	}
		
    	TextView name_table = (TextView)vi.findViewById(R.id.txt_name);
		TextView total_price = (TextView)vi.findViewById(R.id.txt_total_price);
		TextView total_menu = (TextView)vi.findViewById(R.id.txt_total_menu);
		final ImageView grid_image = (ImageView)vi.findViewById(R.id.grid_image);
		grid_image.setVisibility(View.GONE);
		final HashMap<String, String> item = getItem(position);
		
		if(item.get("no").toUpperCase().equals("NONE")){
			name_table.setText("No Antrian : "+item.get("queue"));
		}
		else{
			name_table.setText(item.get("no"));
		}
		
		
		total_price.setText(Helper.toRupiahFormat(item.get("total_price")));
		total_menu.setText("Total Menu : "+item.get("total_menu"));
		
		vi.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				if(item.get("status").equals("1")){
					grid_image.setVisibility(View.GONE);
					item.put("status", "0");
				}
				else{
					grid_image.setVisibility(View.VISIBLE);
					item.put("status", "1");
				}
				
				Log.i("ITEM", item.get("name")+" "+item.get("status"));
				
			}
		});
		
		return vi;		
    }
    
    private void init(Context context, int headerResId, int itemResId, ArrayList<HashMap<String, String>> data) {
    	this.mHeaderResId = headerResId;
        this.mItemResId = itemResId;
        this.DataArray = data;
        this.con = context;
        mInflater = LayoutInflater.from(context);
    }
    
}
