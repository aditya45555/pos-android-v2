package com.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.json.MenuJson;
import com.lib.Helper;
import com.pos.Api;
import com.pos.Catalogue;
import com.pos.R;

public class ListConfirmAdapterSplit extends BaseAdapter {  //implements StickyGridHeadersSimpleAdapter {
    private int mHeaderResId;
    private LayoutInflater mInflater;
    private int mItemResId;
    private ArrayList<HashMap<String, String>> DataArray;
    private Context pcon;
    private GridView pgrid;
    private String psales_id;
    public ListConfirmAdapterSplit(Context context,int headerResId, int itemResId, ArrayList<HashMap<String, String>> data) {
        init(context, headerResId, itemResId, data);
       
    }
    
    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public int getCount() {
        return DataArray.size();
    }

    public CharSequence getHeaderId(int position) {
    	HashMap<String, String> item  = getItem(position);
        CharSequence value;
        if (item.get("name") instanceof CharSequence) {
            value = (CharSequence) item.get("name").toUpperCase();
        } else {
            value = item.get("name").toString().toUpperCase();
        }
        return value.subSequence(0, 1);
    }

    public View getHeaderView(int position, View convertView, ViewGroup parent) {
    	View vi=convertView;
		if(convertView==null)
			vi = mInflater.inflate(mHeaderResId, parent, false);
		TextView textView = (TextView) vi.findViewById(android.R.id.text1);
		
		HashMap<String, String> item = getItem(position);
		textView.setText(item.get("name").toUpperCase().subSequence(0, 1));
        return vi;
    }

    @Override
    public HashMap<String, String> getItem(int position) {
        return DataArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
    	
		final HashMap<String, String>mapchart = new HashMap<String, String>();
		
    	View vi=convertView;
		if(convertView==null)
		vi = mInflater.inflate(mItemResId, parent, false);
		TextView txtname = (TextView)vi.findViewById(R.id.txt_name);
		TextView txtprice = (TextView)vi.findViewById(R.id.txt_price);
		final TextView txtqty = (TextView)vi.findViewById(R.id.txt_qty);
		final TextView txtqtysplit = (TextView)vi.findViewById(R.id.txt_qty_split);
		
		final ImageView gridimage = (ImageView)vi.findViewById(R.id.grid_image);
		gridimage.setVisibility(View.GONE);
		final HashMap<String, String> map = getItem(position);
		txtname.setText(map.get("name"));
		txtqty.setText("Total Menu : "+map.get("quantity"));
		txtqtysplit.setText("Total Split Menu : "+0);
		
		txtprice.setText(Helper.toRupiahFormat(map.get("price")));
		vi.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				
				if(map.get("choicesplit").equals("0")){
					final int quantiy =Integer.valueOf(map.get("quantity"));
					
					if(quantiy>=1){
						LayoutInflater li = LayoutInflater.from(Catalogue.getContext());
	     				View promptsView = li.inflate(R.layout.dialog_set_quantity, null);
	                 	final TextView txt_qty = (TextView)promptsView.findViewById(R.id.txt_qty);
	                 	Button btn_set_qty = (Button)promptsView.findViewById(R.id.btn_set_qty);
	                 	
	     				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Catalogue.getContext());
	                 	alertDialogBuilder.setTitle("Jumlah Pesanan Menu Yang di Split");
	     				alertDialogBuilder.setView(promptsView);
	     				final AlertDialog alertd = alertDialogBuilder.create();
	     				
	                 	btn_set_qty.setOnClickListener(new View.OnClickListener() {
							
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								int lenghthtxtqty = txt_qty.getText().length();
			                 	if(lenghthtxtqty>0 && txt_qty.getText().toString()!="0"){
				                 	int quantitytext = Integer.valueOf(txt_qty.getText().toString());
				                 	 
				                 	if(quantitytext>quantiy){
				                 		Toast.makeText(Catalogue.getContext(),"Pesanan Menu Yang Di Split lebih dari yang di pesan", Toast.LENGTH_LONG).show();
				                 	}
				                 	else{
				                 		gridimage.setVisibility(View.VISIBLE);
				    					map.put("choicesplit", "1");
				    					map.put("qtysplit", String.valueOf(quantitytext));
				    					txtqtysplit.setText("Total Split Menu : "+String.valueOf(quantitytext));
				                 	}
				                 	
				                 	alertd.cancel();
			                 	}
							}
						});
	                 	alertd.show();
					}
					else{
						Toast.makeText(pcon, "Split Menu Hanya Bisa Di lakukan minimal satu quantity 1", Toast.LENGTH_LONG).show();
					}

					
				}
				else{
					gridimage.setVisibility(View.GONE);
					map.put("choicesplit", "0");
					map.put("qtysplit", String.valueOf(0));
					txtqty.setText(String.valueOf(0));
				}
			}
		});
    	return vi;
    }
    
    private void init(Context context, int headerResId, int itemResId, ArrayList<HashMap<String, String>> data) {
    	this.mHeaderResId = headerResId;
        this.mItemResId = itemResId;
        this.DataArray = data;
        this.pcon = context;
        mInflater = LayoutInflater.from(context);
    }
    
}