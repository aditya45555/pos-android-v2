package com.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.pos.R;

public class MemberAdapterList extends BaseAdapter {  //implements StickyGridHeadersSimpleAdapter {
    private int mHeaderResId;
    private LayoutInflater mInflater;
    private int mItemResId;
    private ArrayList<HashMap<String, String>> DataArrayList;
    public MemberAdapterList(Context context,int headerResId, int itemResId, ArrayList<HashMap<String, String>> data) {
        init(context, headerResId, itemResId, data);
    }
    
    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public int getCount() {
        return DataArrayList.size();
    }

    @Override
    public HashMap<String, String> getItem(int position) {
        return DataArrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
    	View vi=convertView;
		if(convertView==null)
		{
			vi = mInflater.inflate(mItemResId, parent, false);
			TextView phone = (TextView)vi.findViewById(R.id.txtlistname);
			TextView name = (TextView)vi.findViewById(R.id.txtlistphone);
			HashMap<String, String> item = getItem(position);
			LinearLayout llayout = (LinearLayout)vi.findViewById(R.id.layoutlist);
			phone.setText(item.get("phone"));
			name.setText(item.get("name"));
		}	
		return vi;
    }

    private void init(Context context, int headerResId, int itemResId, ArrayList<HashMap<String, String>> data) {
    	this.mHeaderResId = headerResId;
        this.mItemResId = itemResId;
        this.DataArrayList = data;
        mInflater = LayoutInflater.from(context);
    }
    
}
