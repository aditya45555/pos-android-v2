package com.adapter;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import android.content.ClipData.Item;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.opengl.Visibility;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.lib.Helper;
import com.pos.Api;
import com.pos.Main;
import com.pos.R;
import com.pos.TableFragment;

public class TableAdapter extends BaseAdapter  {  //implements StickyGridHeadersSimpleAdapter {
    private int mHeaderResId;
    private LayoutInflater mInflater;
    private int mItemResId;
    private ArrayList<HashMap<String, String>> DataArray;
    private ArrayList<HashMap<String, String>> DataArrayTemp;
    private int count = 0;
    private Context con;
    public TableAdapter(Context context,int headerResId, int itemResId, ArrayList<HashMap<String, String>> data) {
    	init(context, headerResId, itemResId, data);
    }
    
    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public int getCount() {
    	return DataArray.size();
    }

    public CharSequence getHeaderId(int position) {
    	HashMap<String, String> item  = getItem(position);
        CharSequence value;
        if (item.get("name") instanceof CharSequence) {
            value = (CharSequence) item.get("name").toUpperCase();
        } else {
            value = item.get("name").toString().toUpperCase();
        }
        return value.subSequence(0, 1);
    }

    public View getHeaderView(int position, View convertView, ViewGroup parent) {
    	View vi=convertView;
		if(convertView==null)
			vi = mInflater.inflate(mHeaderResId, parent, false);
		TextView textView = (TextView) vi.findViewById(android.R.id.text1);
		
		HashMap<String, String> item = getItem(position);
		textView.setText(item.get("name").toUpperCase().subSequence(0, 1));
		
        return vi;
    }

    @Override
    public HashMap<String, String> getItem(int position) {
        return DataArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
    	View vi=convertView;
    	
    	if(convertView == null)
    	{
    		vi = mInflater.inflate(mItemResId, parent, false);
    	}
		
    	TextView name_table = (TextView)vi.findViewById(R.id.name_table);
		TextView time = (TextView)vi.findViewById(R.id.time);
		TextView total = (TextView)vi.findViewById(R.id.total);
		TextView menu = (TextView)vi.findViewById(R.id.menu);
		TextView capacity = (TextView)vi.findViewById(R.id.capacity);
		TextView member = (TextView)vi.findViewById(R.id.member);
		
		ImageView grid_image_table = (ImageView)vi.findViewById(R.id.grid_image_table);
		ImageView grid_image_status = (ImageView)vi.findViewById(R.id.grid_image_status);
		
		HashMap<String, String> item = getItem(position);
		if(Api.getTypeApp()!=null){
			if(Api.getTypeApp().toLowerCase().equals("cashier")){
				if(item.get("statusnotif")!=null){
					if(item.get("statusnotif").equals("1")){
						vi.setBackgroundColor(Color.YELLOW);
					}
					else{
						vi.setBackgroundColor(Color.WHITE);
					}
				}
				
			}
		}
		String urlphoto = "";
		/*if(Helper.isTablet(con)){
			urlphoto = item.get("photo")+"?w=150&h=150&t=w";
		}
		else{
			urlphoto=item.get("photo")+"?w=200&h=200&t=w";
		}*/
		//Helper.loadImage(item.get("photo"), con, grid_image_table);
		Bitmap btimap = BitmapFactory.decodeFile(Helper.getDirDboxInFile()+"/imagetable/"+item.get("photo"));
		grid_image_table.setImageBitmap(btimap);
		//set size image tabel
		grid_image_table.getLayoutParams().height = Helper.getHeightScreen(con)/4;
		grid_image_table.getLayoutParams().width = Helper.getWidthScreen(con);
		
		name_table.setText(item.get("no"));
		
		if(name_table.getText().toString().toUpperCase().equals("NONE") || item.get("status").toUpperCase().equals("AVAILABLE")){
			total.setText("0");
			time.setText("");
			menu.setText("0");
		}
		
		if(item.get("time_used")!="0"){
			time.setText(Helper.timeAgo(Long.valueOf(item.get("time_used"))));
		}
		
		if(item.get("total_menu").equals("null")){
			menu.setText("0");
		}
		else{
			menu.setText(item.get("total_menu"));
		}
		
		if(item.get("member_name").equals("")){
			member.setText("Umum");
		}
		else{
			member.setText(item.get("member_name"));
		}
		
		capacity.setText(item.get("capacity"));
		
		int txttotal= 0;
		if(!item.get("total_price").equals("null")){
			txttotal = Integer.valueOf(item.get("total_price"));
		}
		
		if(item.get("status").toUpperCase().equals("AVAILABLE")){
			grid_image_status.setImageResource(R.drawable.kosong);
		}
		else if(item.get("status").toUpperCase().equals("BOOKED")){
			grid_image_status.setImageResource(R.drawable.book);
		}
		else{
			grid_image_status.setImageResource(R.drawable.isi);
			total.setText(Helper.toRupiahFormat(String.valueOf(txttotal)));
		}
		return vi;		
    }
    
    private void init(Context context, int headerResId, int itemResId, ArrayList<HashMap<String, String>> data) {
    	this.mHeaderResId = headerResId;
        this.mItemResId = itemResId;
        this.DataArray = data;
        this.DataArrayTemp = data;
        this.con = context;
        mInflater = LayoutInflater.from(context);
    }
    
    public  void filter(String charText) {
    	
		if (charText.length()== 0) {
			Main.search = false;
			DataArray = Api.getAllTables();
			
		} else {
			Main.search = true;
			for(int i =0;i<DataArrayTemp.size();i++){
				if(DataArrayTemp.get(i).get("no").toLowerCase().contains(charText.toLowerCase())){
					HashMap<String, String>map = new HashMap<String, String>();
					map.put("table_id", DataArrayTemp.get(i).get("table_id"));
					map.put("no",  DataArrayTemp.get(i).get("no"));
					map.put("status",  DataArrayTemp.get(i).get("status"));
					map.put("total_price",  DataArrayTemp.get(i).get("total_price"));
					map.put("total_menu", DataArrayTemp.get(i).get("total_menu"));
					map.put("member_id",  DataArrayTemp.get(i).get("member_id"));
					map.put("member_name",  DataArrayTemp.get(i).get("member_name"));
					map.put("member_email",  DataArrayTemp.get(i).get("member_email"));
					map.put("member_phone",  DataArrayTemp.get(i).get("member_phone"));
					map.put("sales_id",  DataArrayTemp.get(i).get("sales_id"));
					map.put("photo",  DataArrayTemp.get(i).get("photo"));
					map.put("capacity",  DataArrayTemp.get(i).get("capacity"));
					map.put("time_used",  DataArrayTemp.get(i).get("time_used"));
					DataArray.clear();
					DataArray.add(map);
				}
				
			}
			
		}
		notifyDataSetChanged();
	}
	

	
	
    

}
