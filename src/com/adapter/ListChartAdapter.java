package com.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Dialog;
import android.app.FragmentManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.asyntask.cancelOrderTemp;
import com.asyntask.setQtyTemp;
import com.dialog.DialogDiskon;
import com.lib.Helper;
import com.pos.Api;
import com.pos.Catalogue;
import com.pos.Chilkat;
import com.pos.R;

public class ListChartAdapter extends BaseAdapter {  //implements StickyGridHeadersSimpleAdapter {
    private int mHeaderResId;
    private LayoutInflater mInflater;
    private int mItemResId;
    private ArrayList<HashMap<String, String>> DataArray;
    private Context con;
    private ListView list_menu_chart;
    Dialog dialogloading;
    private cancelOrderTemp task;
    private setQtyTemp taskqtytemp;
    private ListChartAdapter adapter;
    private android.support.v4.app.FragmentManager fragmentmanager;
    
    public ListChartAdapter(Context context,int headerResId, int itemResId, ArrayList<HashMap<String, String>> data,ListView list_chart,android.support.v4.app.FragmentManager fragmentManager) {
        init(context, headerResId, itemResId, data,list_chart,fragmentManager);
        
    }
    
    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public int getCount() {
        return DataArray.size();
    }

    public CharSequence getHeaderId(int position) {
    	HashMap<String, String> item  = getItem(position);
        CharSequence value;
        if (item.get("name") instanceof CharSequence) {
            value = (CharSequence) item.get("name").toUpperCase();
        } else {
            value = item.get("name").toString().toUpperCase();
        }
        return value.subSequence(0, 1);
    }

    public View getHeaderView(int position, View convertView, ViewGroup parent) {
    	View vi=convertView;
		if(convertView==null)
			vi = mInflater.inflate(mHeaderResId, parent, false);
		TextView textView = (TextView) vi.findViewById(android.R.id.text1);
		
		HashMap<String, String> item = getItem(position);
		textView.setText(item.get("name").toUpperCase().subSequence(0, 1));
		
        return vi;
    }

    @Override
    public HashMap<String, String> getItem(int position) {
        return DataArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
    	View vi=convertView;
    	
		if(convertView==null)
			//Log.i("LIST-MENU-CHART", String.valueOf(list_menu_chart.getLayoutParams().width));
			vi = mInflater.inflate(mItemResId, parent, false);
			final HashMap<String, String> item = getItem(position);
    		TextView txtname = (TextView)vi.findViewById(R.id.txt_name);
    		TextView txtprice = (TextView)vi.findViewById(R.id.txt_price);
    		final TextView txtqty = (TextView)vi.findViewById(R.id.txt_qty);
    		Button btn_cancel_order = (Button)vi.findViewById(R.id.btn_cancel_order);
    		//ImageView gridimage = (ImageView)vi.findViewById(R.id.grid_image);
    
    		if(DataArray.size()>0){
    			//list_menu_chart.getLayoutParams().height =Helper.getHeightScreen(con);
    			//gridimage.getLayoutParams().width = list_menu_chart.getWidth()/3;
    			//gridimage.getLayoutParams().height = list_menu_chart.getHeight()/3;
	    		txtname.getLayoutParams().width = list_menu_chart.getWidth()/4;
	    		String urlphoto = "";
	    		if(Helper.isTablet(con)){
	    			urlphoto = item.get("photo")+"?w=200&h=200&t=w";
	    		}
	    		else{
	    			urlphoto=item.get("photo")+"?w=100&h=100&t=w";
	    		}
	    		//Helper.loadImage(urlphoto, con, gridimage);
	    		txtname.setText(item.get("name"));
	    		txtprice.setText(Helper.toRupiahFormat(item.get("price")));
	    		txtqty.setText("Total Menu : "+item.get("quantity"));
    			
	    		if(item.get("status").toUpperCase().equals("CONFIRM")){
	    			btn_cancel_order.setVisibility(View.GONE);
	    			vi.setOnClickListener(new View.OnClickListener() {
						
						@Override
						public void onClick(View v) {
							// TODO Auto-generated method stub
							/*DialogDiskon dialogdiskon= new DialogDiskon(DataArray, adapter);
							Bundle bundle = new Bundle();
							bundle.putString("ammount", String.valueOf(item.get("price")));
							bundle.putString("position",String.valueOf(position));
							
							dialogdiskon.setArguments(bundle);
							dialogdiskon.show(fragmentmanager, "dialogqty");*/
						}
					});
	    		}
	    		else{
	    			btn_cancel_order.setVisibility(View.VISIBLE);
	    			vi.setOnClickListener(new View.OnClickListener() {
						@Override
	 					
						public void onClick(View v) {
							// TODO Auto-generated method stub
							LayoutInflater li = LayoutInflater.from(Catalogue.getContext());
		     				View promptsView = li.inflate(R.layout.dialog_set_quantity, null);
		                 	final TextView txt_qty = (TextView)promptsView.findViewById(R.id.txt_qty);
		                 	Button btn_set_qty = (Button)promptsView.findViewById(R.id.btn_set_qty);
		                 	
		     				AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(Catalogue.getContext());
		                 	alertDialogBuilder.setTitle("Jumlah Pesanan Menu");
		     				alertDialogBuilder.setView(promptsView);
		     				final AlertDialog alertd = alertDialogBuilder.create();
		     				
		                 	btn_set_qty.setOnClickListener(new View.OnClickListener() {
								
								@Override
								public void onClick(View v) {
									// TODO Auto-generated method stub
									taskqtytemp = new setQtyTemp(item, txt_qty,alertd);
									taskqtytemp.execute();
								}
							});
		                 	alertd.show();
						}
					});
	    		}
	    		
	    		btn_cancel_order.setOnClickListener(new View.OnClickListener() {
    				@Override
    			  public void onClick(View v) {
					// TODO Auto-generated method stub
				  AlertDialog.Builder alert = new AlertDialog.Builder(Catalogue.getContext());
				  alert.setTitle("Batal Menu");
				  alert.setMessage("Apakah Anda Yakin Akan Membatalkan Menu "+item.get("name")+" ?");
				  alert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
				        public void onClick(DialogInterface dialog, int whichButton) {
				            //New Order Action
				        	task = new cancelOrderTemp(item);
				        	task.execute();
				        }
				   });
				    alert.setNegativeButton("Tidak",
				        new DialogInterface.OnClickListener() {
				            public void onClick(DialogInterface dialog, int whichButton) {
				            	//action booking
				            	dialog.cancel();
				            }
				        });
				    alert.show();
				}
	    		});
	    		
    		}
    		
		return vi;
    }
     
    private void init(Context context, int headerResId, int itemResId, ArrayList<HashMap<String, String>> data,ListView list_chart,android.support.v4.app.FragmentManager fragmentManager) {
    	this.mHeaderResId = headerResId;
        this.mItemResId = itemResId;
        this.DataArray = data;
        this.con = context;
        this.list_menu_chart = list_chart;
        this.fragmentmanager = fragmentManager;
        mInflater = LayoutInflater.from(context);
       
    }
      
   
  }
	