package com.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.ClipData.Item;
import android.content.Context;
import android.opengl.Visibility;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lib.Helper;
import com.pos.Api;
import com.pos.Main;
import com.pos.R;
import com.pos.TableFragment;

public class QueueAdapter extends BaseAdapter {  //implements StickyGridHeadersSimpleAdapter {
    private int mHeaderResId;
    private LayoutInflater mInflater;
    private int mItemResId;
    private ArrayList<HashMap<String, String>> DataArray;
    private int count = 0;
    private Context con;
    public QueueAdapter(Context context,int headerResId, int itemResId, ArrayList<HashMap<String, String>> data) {
    	init(context, headerResId, itemResId, data);
    }
    
    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public int getCount() {
    	return DataArray.size();
    }

    public CharSequence getHeaderId(int position) {
    	HashMap<String, String> item  = getItem(position);
        CharSequence value;
        if (item.get("name") instanceof CharSequence) {
            value = (CharSequence) item.get("name").toUpperCase();
        } else {
            value = item.get("name").toString().toUpperCase();
        }
        return value.subSequence(0, 1);
    }

    public View getHeaderView(int position, View convertView, ViewGroup parent) {
    	View vi=convertView;
		if(convertView==null)
			vi = mInflater.inflate(mHeaderResId, parent, false);
		TextView textView = (TextView) vi.findViewById(android.R.id.text1);
		
		HashMap<String, String> item = getItem(position);
		textView.setText(item.get("name").toUpperCase().subSequence(0, 1));
		
        return vi;
    }

    @Override
    public HashMap<String, String> getItem(int position) {
        return DataArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
    	View vi=convertView;
    	if(convertView == null)
    	{
    		vi = mInflater.inflate(mItemResId, parent, false);
    	}
		
    	TextView no_queue = (TextView)vi.findViewById(R.id.no_quee);
		TextView time = (TextView)vi.findViewById(R.id.time);
		TextView total = (TextView)vi.findViewById(R.id.total);
		TextView menu = (TextView)vi.findViewById(R.id.menu);
		
		TextView member = (TextView)vi.findViewById(R.id.member);
		
		HashMap<String, String> item = getItem(position);
		if(DataArray.size()>0){
			no_queue.setText("No Antrian : "+item.get("queue"));
		
			if(item.get("time_used")!="0"){
				time.setText(Helper.timeAgo(Long.valueOf(item.get("time_used"))));
			}
			
			if(item.get("total_menu").equals("null")){
				menu.setText("0");
			}
			else{
				menu.setText(item.get("total_menu"));
			}
			
			if(item.get("member_name").equals("")){
				member.setText("Umum");
			}
			else{
				member.setText(item.get("member_name"));
			}
			
			int txttotal= 0;
			if(!item.get("total_price").equals("null")){
				txttotal = Integer.valueOf(item.get("total_price"));
			}
			total.setText(Helper.toRupiahFormat(String.valueOf(txttotal)));	
		}
		return vi;		
    }
    
    private void init(Context context, int headerResId, int itemResId, ArrayList<HashMap<String, String>> data) {
    	this.mHeaderResId = headerResId;
        this.mItemResId = itemResId;
        this.DataArray = data;
        this.con = context;
        mInflater = LayoutInflater.from(context);
    }
    
}
