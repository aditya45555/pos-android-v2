package com.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.asyntask.addOrderTemp;
import com.json.MenuJson;
import com.lib.Helper;
import com.pos.Api;
import com.pos.Catalogue;
import com.pos.CatalogueFragment;
import com.pos.Chilkat;
import com.pos.ListMenuChartFragment;
import com.pos.Main;
import com.pos.ParentCatalogueFragment;
import com.pos.R;

public class MenuAdapterList extends BaseAdapter {  //implements StickyGridHeadersSimpleAdapter {
    private int mHeaderResId;
    private LayoutInflater mInflater;
    private int mItemResId;
    private ArrayList<HashMap<String, String>> DataArray;
    private ArrayList<HashMap<String, String>> DataArrayOrderTemp;
    private ArrayList<HashMap<String, String>> DataArrayOrderConfirm;
    private Context pcon;
    private GridView pgrid;
    private String psales_id;
    private String ptableId;
    private addOrderTemp task;
    private String catid,menu_type;
    public MenuAdapterList(Context context,int headerResId, int itemResId, ArrayList<HashMap<String, String>> data,GridView grid,String sales_id,String catid,String menu_type,ArrayList<HashMap<String, String>>dataarrayordertemp,ArrayList<HashMap<String, String>>dataarrayorderconfirm,String ptableid) {
        init(context, headerResId, itemResId, data,grid,sales_id,catid,menu_type,dataarrayordertemp,dataarrayorderconfirm,ptableid);
       
    }
    
    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public int getCount() {
        return DataArray.size();
    }

    public CharSequence getHeaderId(int position) {
    	HashMap<String, String> item  = getItem(position);
        CharSequence value;
        if (item.get("name") instanceof CharSequence) {
            value = (CharSequence) item.get("name").toUpperCase();
        } else {
            value = item.get("name").toString().toUpperCase();
        }
        return value.subSequence(0, 1);
    }

    public View getHeaderView(int position, View convertView, ViewGroup parent) {
    	View vi=convertView;
		if(convertView==null)
			vi = mInflater.inflate(mHeaderResId, parent, false);
		TextView textView = (TextView) vi.findViewById(android.R.id.text1);
		
		HashMap<String, String> item = getItem(position);
		textView.setText(item.get("name").toUpperCase().subSequence(0, 1));
        return vi;
    }

    @Override
    public HashMap<String, String> getItem(int position) {
        return DataArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @SuppressLint("NewApi")
	@Override
    public View getView(final int position, View convertView, ViewGroup parent) {
    	View vi=convertView;
		if(convertView==null)
		{
			vi = mInflater.inflate(mItemResId, parent, false);
		}
		
		int lenghtheight = 40;
		if(Helper.isTablet(pcon)){
			lenghtheight = 20;
		}
		if(ParentCatalogueFragment.grid != null){
			ParentCatalogueFragment.grid.getLayoutParams().height = (((((((Helper.getHeightScreen(pcon)-Catalogue.getInstance().layout_title.getHeight()) - Catalogue.getInstance().layoutslideup.getHeight()))-ParentCatalogueFragment.grid.getVerticalSpacing())-Catalogue.getInstance().tabs.getHeight())-Catalogue.getInstance().getSupportActionBar().getHeight()))-lenghtheight;
		}
		
		if(CatalogueFragment.grid !=null){
			CatalogueFragment.grid.getLayoutParams().height = (((((((Helper.getHeightScreen(pcon)-Catalogue.getInstance().layout_title.getHeight()) - Catalogue.getInstance().layoutslideup.getHeight()))-CatalogueFragment.grid.getVerticalSpacing())-Catalogue.getInstance().tabs.getHeight())-Catalogue.getInstance().getSupportActionBar().getHeight())-ParentCatalogueFragment.tab.getHeight())-lenghtheight;
		}
		
		TextView title = (TextView)vi.findViewById(R.id.title);
		TextView price = (TextView)vi.findViewById(R.id.price);
		TextView description_short = (TextView)vi.findViewById(R.id.description_short);
		TextView quantity = (TextView)vi.findViewById(R.id.quantity);
		//TextView txt_event = (TextView)vi.findViewById(R.id.txt_event);
		Button btnorder =  (Button)vi.findViewById(R.id.btn_order);
		LinearLayout layout_description = (LinearLayout)vi.findViewById(R.id.layoutdescription);
		ImageView gridimage = (ImageView)vi.findViewById(R.id.grid_image);
		
		final HashMap<String, String> map = getItem(position);
		//final HashMap<String, String> mapphoto = DataArrayPhoto.get(position);
		//final HashMap<String, String> photomenu = MenuJson.getById(map.get("menu_id"));
		if(DataArray.size()>0){
			
			if(map.get("status")!=null){
				if(!map.get("status").toUpperCase().equals("AVAILABLE")){
					vi.setBackgroundColor(Color.GRAY);
				}
			}
			
			title.setText(map.get("name"));
			quantity.setText(map.get("quantity"));
			
			
			if(map.get("price")!=null){
				price.setText(Helper.toRupiahFormat(map.get("price")));
			}
			else{
				price.setTag(Helper.toRupiahFormat("0"));
			}
			
			description_short.setText(map.get("short_description"));
			
			/*String urlphoto = "";
    		if(Helper.isTablet(pcon)){
    			urlphoto = map.get("photo")+"?w=200&h=200&t=w";
    		}
    		else{
    			urlphoto=map.get("photo")+"?w=100&h=100&t=w";
    		}*/
			//Helper.loadImage(urlphoto, pcon, gridimage);
			final Bitmap btimap = BitmapFactory.decodeFile(Helper.getDirDboxInFile()+"/imagemenu/"+map.get("photo"));
			gridimage.setImageBitmap(btimap);
			//txt_event.bringToFront();
			final String statusmenu = map.get("status");
			final String menu_id = map.get("menu_id");
			
			//set size components
			gridimage.getLayoutParams().width = pgrid.getWidth()/3;
			gridimage.getLayoutParams().height = pgrid.getWidth()/4;
			title.getLayoutParams().width = pgrid.getWidth()/4;
			description_short.getLayoutParams().width = pgrid.getWidth()/4;
			
			//grid action
			vi.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					//set dialog
					
					AlertDialog.Builder alert = new AlertDialog.Builder(Catalogue.getContext());
					LayoutInflater li = LayoutInflater.from(Catalogue.getContext());
					
					String statusmenu = map.get("status");
					
		 				View promptsView = li.inflate(R.layout.dialog_detail_menu, null);
						
						TextView title = (TextView)promptsView.findViewById(R.id.title);
			    		TextView price = (TextView)promptsView.findViewById(R.id.price);
			    		TextView description = (TextView)promptsView.findViewById(R.id.txt_description);
			    		Button btnorder = (Button)promptsView.findViewById(R.id.btn_order);
			    		Button btnclose = (Button)promptsView.findViewById(R.id.btn_close);
			    		
			    		ImageView gridimage = (ImageView)promptsView.findViewById(R.id.grid_image);
			    		ScrollView scroll_description = (ScrollView)promptsView.findViewById(R.id.scroll_description);
			    		
			    		gridimage.getLayoutParams().height = (Helper.getHeightScreen(pcon)/3);
			    		//scroll_description.getLayoutParams().height = (Helper.getHeightScreen(pcon)/3);
			    		
			    		title.setText(map.get("name"));
			    		description.setText(map.get("description"));
			    		price.setText(Helper.toRupiahFormat(map.get("price")));
			    		/*String urlphoto = "";
			    		if(Helper.isTablet(pcon)){
			    			urlphoto = map.get("photo")+"?w=300&h=300&t=w";
			    		}
			    		else{
			    			urlphoto=map.get("photo")+"?w=200&h=200&t=w";
			    		}
			    		Helper.loadImage(urlphoto, pcon, gridimage);*/
			    		
			    		gridimage.setImageBitmap(btimap);
			    		
		            	alert.setView(promptsView);
		            	
		            	final AlertDialog alertd = alert.create();
						
		            	//btnclose action
		            	btnclose.setOnClickListener(new View.OnClickListener() {
							
							@Override
							public void onClick(View v) {
								// TODO Auto-generated method stub
								alertd.cancel();
							}
						});
		            	
		            	//btn_order action
		        		btnorder.setOnClickListener(new View.OnClickListener() {
		    				@Override
		    				public void onClick(View v) {
		    				// TODO Auto-generated method stub
		    					task = new addOrderTemp(pcon,DataArrayOrderTemp,DataArrayOrderConfirm,map, psales_id,ptableId);
		    					task.execute();
		    				}
		    			});
		            	
		            	alertd.show();
					
				}
			});
			
			//btn_order action
			btnorder.setOnClickListener(new View.OnClickListener() {
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
						task = new addOrderTemp(pcon,DataArrayOrderTemp,DataArrayOrderConfirm,map, psales_id,ptableId);
						task.execute();
				}
			});
		
		}
		
    	return vi;
    }
    
    private void init(Context context, int headerResId, int itemResId, ArrayList<HashMap<String, String>> data,GridView grid,String sales_id,String catid,String menu_type, ArrayList<HashMap<String, String>> dataarrayordertemp, ArrayList<HashMap<String, String>> dataarrayorderconfirm,String ptableid) {
    	this.mHeaderResId = headerResId;
        this.mItemResId = itemResId;
        this.DataArray = data;
        this.menu_type = menu_type;
        this.catid = catid;
        this.pgrid = grid;
        this.psales_id = sales_id;
        this.pcon = context;
        this.DataArrayOrderTemp = dataarrayordertemp;
        this.DataArrayOrderConfirm = dataarrayorderconfirm;
        this.ptableId = ptableid;
        mInflater = LayoutInflater.from(context);
       // Toast.makeText(pcon, DataArray.toString(), Toast.LENGTH_LONG).show();
    }
    
public  void filter(String charText) {
    	//Log.i("TEST", charText);
    	DataArray.clear();
		if (charText.length()== 0) {
			DataArray = MenuJson.getByCategoryID(catid, menu_type);			
		} else {
			for(int i =0;i<DataArray.size();i++){
				if(DataArray.get(i).get("name").toLowerCase().contains(charText.toLowerCase())){
					HashMap<String, String>map = new HashMap<String, String>();
					map.put("menu_id", DataArray.get(i).get("menu_id"));
					map.put("category_id", DataArray.get(i).get("category_id"));
					map.put("description", DataArray.get(i).get("description"));
					map.put("name", DataArray.get(i).get("name"));
					map.put("price", DataArray.get(i).get("price"));
					map.put("status", DataArray.get(i).get("status"));
					map.put("short_description",DataArray.get(i).get("short_description"));
					map.put("event", DataArray.get(i).get("event"));
					map.put("color",DataArray.get(i).get("color"));
					map.put("bcolor", DataArray.get(i).get("bcolor"));
					map.put("photo", DataArray.get(i).get("photo"));
					
					DataArray.add(map);
				}
				
			}
			
		}
		Log.i("DATA-ARRAY", DataArray.toString());
		notifyDataSetChanged();
	}
    
}