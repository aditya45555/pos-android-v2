package com.adapter;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.lib.Helper;
import com.pos.Main;
import com.pos.R;

public class ListMenuCashierAdapter extends BaseAdapter {
	private Activity activity;
	private ArrayList<HashMap<String, String>> data;
	private LayoutInflater inflater=null;
	DecimalFormat format;
	public Double sbTotal;
	private ArrayList<HashMap<String, String>> orderDetilData = new ArrayList<HashMap<String, String>>();
	Double dDisc;
	Double Total;
	TextView txtsTotal;
	TextView txtDisc;
	TextView txtTotal;
	public ListMenuCashierAdapter(Activity a, ArrayList<HashMap<String, String>> d) {
		activity = a;
		data=d;
		format = new DecimalFormat("#,###,###.###");
		sbTotal = (double) 0;
		dDisc = (double) 0;
		Total = (double) 0;
		inflater = (LayoutInflater)activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	public int getCount() {
		return data.size();
	}
	
	public Object getItem(int position){
		return position;
	}
	
	public long getItemId(int position) {
		return position;
	}
	
	public View getView(final int position, View convertView, ViewGroup parent) {
		View vi=convertView;
		if(convertView==null)
			vi = inflater.inflate(R.layout.list_menu_cashier, null);
		
		LinearLayout lmenu = (LinearLayout)vi.findViewById(R.id.listOrderDetailItemLayout);
		LinearLayout litem = (LinearLayout)vi.findViewById(R.id.layoutDetilItem);
		LinearLayout lqty = (LinearLayout)vi.findViewById(R.id.layoutDetilQty);
		
		ImageView imgMenu = (ImageView)vi.findViewById(R.id.imgmenu);
		TextView item = (TextView)vi.findViewById(R.id.txtcitem);
		final TextView qty = (TextView)vi.findViewById(R.id.txtcqty);
		final TextView disc = (TextView)vi.findViewById(R.id.txtcdisc);
		TextView price = (TextView)vi.findViewById(R.id.txtcprice);
		TextView amount = (TextView)vi.findViewById(R.id.txtcamount);
		
		int w = activity.getResources().getDisplayMetrics().widthPixels;
		int wd = (int)Math.round(w*0.6);
		int whd = (int)Math.round(wd*0.5);
		int whd2 = (int)Math.round(wd*0.2);
		int whd3 = (int)Math.round(wd*0.1);
		
		LinearLayout.LayoutParams llh = new LinearLayout.LayoutParams(whd, LinearLayout.LayoutParams.WRAP_CONTENT);
		litem.setLayoutParams(llh);
		    		
		LinearLayout.LayoutParams llh2 = new LinearLayout.LayoutParams(whd2, LinearLayout.LayoutParams.WRAP_CONTENT);
		lqty.setLayoutParams(llh2);    		
		amount.setLayoutParams(llh2);
		
		LinearLayout.LayoutParams llh3 = new LinearLayout.LayoutParams(whd3, LinearLayout.LayoutParams.WRAP_CONTENT);
		disc.setLayoutParams(llh3);
		
		HashMap<String, String> map = new HashMap<String, String>();    		
		map = data.get(position);
		Helper.loadImage(map.get("photo"), activity.getApplicationContext(), imgMenu);
		item.setText(map.get("name"));
		qty.setText(map.get("quantity")+"x");
		
		Log.i("Discount", map.get("discount"));
		Double dDsc = Double.valueOf(map.get("discount"));
		
		//long lngPrice = (long) Math.round(dPrice);
		disc.setText(format.format(dDsc));
		
		Double dPrice = Double.valueOf(map.get("price"));
		
		price.setText(format.format(dPrice));
		
		Double sbDisc = (dDsc/100);
		final Double dAmount = ((dPrice-(dPrice*sbDisc))*Double.valueOf(map.get("quantity")));
		amount.setText(format.format(dAmount));
			
		/*sbTotal = sbTotal+(dAmount);
		txtsTotal.setText(format.format(sbTotal));
		
		Double iDisc = Double.valueOf(dDisc);
		txtDisc.setText(String.valueOf(iDisc));
		
		Total = (sbTotal-(sbTotal*(dDisc/100)));
		txtTotal.setText(format.format(Total));*/
		
		/*lmenu.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				HashMap<String, String> mapdet = new HashMap<String, String>();
				mapdet = data.get(position);
				
				DialogItems newFragment = new DialogItems(orderDetilData, adapterdetil);
				Bundle bundle = new Bundle();
				bundle.putInt("position", position);
				bundle.putString("ammount", String.valueOf(dAmount));
				newFragment.setArguments(bundle);
				newFragment.show(getSupportFragmentManager(), "dialogitem");										
			}
        });*/
		    		
		return vi;
	}    	    	
}