package com.adapter;

import java.util.ArrayList;
import java.util.HashMap;

import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.json.MenuJson;
import com.lib.Helper;
import com.pos.Api;
import com.pos.Catalogue;
import com.pos.R;

public class ListConfirmAdapter extends BaseAdapter  {  //implements StickyGridHeadersSimpleAdapter {
    private int mHeaderResId;
    private LayoutInflater mInflater;
    private int mItemResId;
    private ArrayList<HashMap<String, String>> DataArray;
    private Context pcon;
    private GridView pgrid;
    private String psales_id;
    public ListConfirmAdapter(Context context,int headerResId, int itemResId, ArrayList<HashMap<String, String>> data) {
        init(context, headerResId, itemResId, data);
       
    }
    
    @Override
    public boolean areAllItemsEnabled() {
        return false;
    }

    @Override
    public int getCount() {
        return DataArray.size();
    }

    public CharSequence getHeaderId(int position) {
    	HashMap<String, String> item  = getItem(position);
        CharSequence value;
        if (item.get("name") instanceof CharSequence) {
            value = (CharSequence) item.get("name").toUpperCase();
        } else {
            value = item.get("name").toString().toUpperCase();
        }
        return value.subSequence(0, 1);
    }

    public View getHeaderView(int position, View convertView, ViewGroup parent) {
    	View vi=convertView;
		if(convertView==null)
			vi = mInflater.inflate(mHeaderResId, parent, false);
		TextView textView = (TextView) vi.findViewById(android.R.id.text1);
		
		HashMap<String, String> item = getItem(position);
		textView.setText(item.get("name").toUpperCase().subSequence(0, 1));
        return vi;
    }

    @Override
    public HashMap<String, String> getItem(int position) {
        return DataArray.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
    	
		final HashMap<String, String>mapchart = new HashMap<String, String>();
		
    	View vi=convertView;
		if(convertView==null)
		vi = mInflater.inflate(mItemResId, parent, false);
		TextView txtname = (TextView)vi.findViewById(R.id.txt_name);
		TextView txtprice = (TextView)vi.findViewById(R.id.txt_price);
		TextView txtqty = (TextView)vi.findViewById(R.id.txt_qty);
		final HashMap<String, String> map = getItem(position);
		
		txtname.setText(map.get("name"));
		txtqty.setText("Total Menu : "+map.get("quantity"));
		txtprice.setText(Helper.toRupiahFormat(map.get("price")));
    	return vi;
    }
    
    private void init(Context context, int headerResId, int itemResId, ArrayList<HashMap<String, String>> data) {
    	this.mHeaderResId = headerResId;
        this.mItemResId = itemResId;
        this.DataArray = data;
        this.pcon = context;
        mInflater = LayoutInflater.from(context);
    }
    
}