package com.lib;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.Set;
import java.util.UUID;

import com.pos.Api;
import com.pos.Catalogue;

import android.R;
import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Handler;
import android.os.ParcelUuid;
import android.util.Log;

public class Print {
	BluetoothAdapter mBluetoothAdapter;
    BluetoothSocket mmSocket;
	BluetoothDevice mmDevice;
 
    OutputStream mmOutputStream;
    InputStream mmInputStream;
    Thread workerThread;
 
    byte[] readBuffer;
    int readBufferPosition;
    int counter;
    volatile boolean stopWorker;
    Boolean checkPrint = false;
    private Context mContext;
   
    //
    public static final byte Align_LEFT = (byte)0x30; //������
	public static final byte Align_CENTER = (byte)0x31;//����
	public static final byte Align_RIGHT = (byte)0x32;
	public final static byte B_ESC = (byte) 0x1B;
	
    public Print(Context c) {
		mContext = c;
		findBT();
		try {
			
			openBT();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
		}
	}
    
    
    
 // This will find a bluetooth printer device
    public void findBT() {
 
        try {
            mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
           
            if (mBluetoothAdapter == null) {
            	Log.i("","No bluetooth adapter available");
                //myLabel.setText("No bluetooth adapter available");
            }
 
            if (!mBluetoothAdapter.isEnabled()) {
                Intent enableBluetooth = new Intent(
                        BluetoothAdapter.ACTION_REQUEST_ENABLE);
                
                ((Activity) mContext).startActivityForResult(enableBluetooth, 0);
            }
 
            Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
            if (pairedDevices.size() > 0) {
                for (BluetoothDevice device : pairedDevices) { 
                    // MP300 is the name of the bluetooth printer device
                	if(Api.getActivePrinter() == true){
                		if(Api.getPrinterBluetooth() == null){
                    		Helper.dialogSettingPrinter(mContext);
                	}
            		else{
            			 if (device.getName().equals(Api.getPrinterBluetooth())) {    
                          	Log.i("",device.getName());
                              mmDevice = device;
                              break;
                          }
            		}
                	}
                	else{
                		 if (device.getName().equals(Api.getPrinterBluetooth())) {    
                         	Log.i("",device.getName());
                             mmDevice = device;
                             break;
                         }
                	}
                   
                }
            }
            //myLabel.setText("Bluetooth Device Found");
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
 
    // Tries to open a connection to the bluetooth printer device
    public void openBT() throws IOException {
    	try {
            // Standard SerialPortService ID
    	     UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
             mmSocket = mmDevice.createRfcommSocketToServiceRecord(uuid);
             mmSocket.connect();
			 mmOutputStream = mmSocket.getOutputStream();
		     mmInputStream = mmSocket.getInputStream();
		     beginListenForData();
		     checkPrint = true;
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
 
    public  boolean checkBluetoothOPen(){
    	if(mmSocket.isConnected()==true){
    		return true;
    	}
    	return false;
    }
    
    // After opening a connection to bluetooth printer device,
    // we have to listen and check if a data were sent to be printed.
    public void beginListenForData() {
        try {
            final Handler handler = new Handler();
 
            // This is the ASCII code for a newline character
            final byte delimiter = 10;
 
            stopWorker = false;
            readBufferPosition = 0;
            readBuffer = new byte[1024];
 
            workerThread = new Thread(new Runnable() {
                public void run() {
                    while (!Thread.currentThread().isInterrupted()
                            && !stopWorker) {
 
                        try {
 
                            int bytesAvailable = mmInputStream.available();
                            if (bytesAvailable > 0) {
                                byte[] packetBytes = new byte[bytesAvailable];
                                mmInputStream.read(packetBytes);
                                for (int i = 0; i < bytesAvailable; i++) {
                                    byte b = packetBytes[i];
                                    if (b == delimiter) {
                                        byte[] encodedBytes = new byte[readBufferPosition];
                                        System.arraycopy(readBuffer, 0,
                                                encodedBytes, 0,
                                                encodedBytes.length);
                                        final String data = new String(
                                                encodedBytes, "US-ASCII");
                                        readBufferPosition = 0;
 
                                        handler.post(new Runnable() {
                                            public void run() {
                                                //myLabel.setText(data);
                                            	
                                            }
                                        });
                                    } else {
                                        readBuffer[readBufferPosition++] = b;
                                    }
                                }
                            }
 
                        } catch (IOException ex) {
                            stopWorker = true;
                        }                    
                    }
                }
            });
 
            workerThread.start();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    /*
     * This will send data to be printed by the bluetooth printer
     */
    public void sendData(byte[] input) throws IOException {
        try {
 
            // the text typed by the user
        	mmOutputStream.write(input);
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
 
    // Close the connection to bluetooth printer.
    public void closeBT() throws IOException {
        try {
           
        	stopWorker = true;
            mmOutputStream.close();
            mmInputStream.close();
            mmSocket.close();
            checkPrint = false;
            
            //myLabel.setText("Bluetooth Closed");
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
    
   public void sendData2(byte[] byteimage) throws IOException {
        try {
 
            // the text typed by the use       	
            mmOutputStream.write(byteimage);
            // tell the user data were sent
            //myLabel.setText("Data Sent");
            closeBT();
        } catch (NullPointerException e) {
            e.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
