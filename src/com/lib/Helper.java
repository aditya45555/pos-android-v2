package com.lib;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.NetworkInterface;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.adapter.TableAdapter;
import com.asyntask.closeStore;
import com.asyntask.mergeAct;
import com.asyntask.saveImageFile;
import com.asyntask.setQtyTemp;
import com.json.CategoryJson;
import com.json.MenuJson;
import com.json.TableJson;
import com.pos.Api;
import com.pos.Catalogue;
import com.pos.Chilkat;
import com.pos.Db;
import com.pos.Main;
import com.pos.MyApp;
import com.pos.R;
import com.pos.SessionManager;
import com.pos.TableFragment;
import com.squareup.picasso.Picasso;

import android.R.color;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInstaller.Session;
import android.content.res.AssetFileDescriptor;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.ColorDrawable;
import android.media.MediaPlayer;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.text.TextUtils;
import android.text.format.DateFormat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Helper {
	private static Helper helper;
	static final String IMAGE_URL = "http://admin.kaskul.com/view/image/306a290572cdb20a9a11e454f2ead591/";
	private static SessionManager session;
	private static BluetoothAdapter mBluetoothAdapter;
	private static closeStore taskcloseStore;
	private static AlertDialog alertdclosetore = null;
	private static saveImageFile tasksaveimagefile;
	private static  ArrayList<HashMap<String, String>> categoryData = new ArrayList<HashMap<String, String>>();
	public static Helper getInstance() {
		if (helper == null)
			helper = new Helper();
		
		return helper;
	}
	
	private static String md5(String input) {
        if (input == null) {
            return null;
        }
        String passwordToHash = input;
        String generatedPassword = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(passwordToHash.getBytes());
            byte[] bytes = md.digest();
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.length; i++) {
                sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            generatedPassword = sb.toString();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return generatedPassword;
	 }
	 
	 public static boolean isJSONValid(String test) {
	    	if (test!=null)
	    	{
		        try {
		            new JSONObject(test);
		        } catch (JSONException ex) {
		            // edited, to include @Arthur's comment
		            // e.g. in case JSONArray is valid as well...
		            try {
		                new JSONArray(test);
		            } catch (JSONException ex1) {
		                return false;
		            }
		        }
		        return true;
	    	}
	    	return false;
	    }
	 
	 public static File getDirDbox() {
		File myDir = MyApp.getAppContext().getDir("pos.dboxid.com", Context.MODE_PRIVATE);
		return myDir;
	 }
	 
	 public static File getDirDboxInFile() {
		File myDir = MyApp.getAppContext().getDir("pos.dboxid.com.incoming", Context.MODE_PRIVATE);
		return myDir;
	 }
	 
	 public static File getDirDboxOutFile() {
			File myDir = MyApp.getAppContext().getDir("pos.dboxid.com.outgoing", Context.MODE_APPEND);
			return myDir;
	 }
	 
	 public static int getCountDirInFile(){
		 return getDirDboxInFile().listFiles().length;
	 }
	 
	 public static int getCountDirOutFile(){
		 return getDirDboxOutFile().listFiles().length;
	 }
	 
	 public static void deleteFile(String ffile)
	 {
		File mySubDir = new File(ffile);
		mySubDir.delete();
	 }
	 
	 public static void saveFileImage(File ffile,String fname,String url,String ext,Context context,String subdir)
	 {
		 tasksaveimagefile = new saveImageFile(ffile, fname, url, ext, context, subdir);
		 tasksaveimagefile.execute();
	 }
	 
	 public static void saveFile(File ffile, String fname, String fcontent)
	 {
		 if(isJSONValid(fcontent)){
			 Log.i("CONTENT", fcontent);
			 File mySubDir = new File(ffile, fname);
				if (mySubDir.isFile())
					mySubDir.delete();
				OutputStream myOutput;
				try {
					myOutput = new BufferedOutputStream(new FileOutputStream(mySubDir, false));
					myOutput.write(fcontent.getBytes());
					myOutput.flush();
					myOutput.close();
				} catch (FileNotFoundException e) {
				    e.printStackTrace();
				} catch (IOException e) {
				    e.printStackTrace();
				}
		 }
		
	 }
	
	 public static void saveFiletxt(File ffile, String fname, String fcontent)
	 {
			 Log.i("CONTENT", fcontent);
			 File mySubDir = new File(ffile, fname);
				if (mySubDir.isFile())
					mySubDir.delete();
				OutputStream myOutput;
				try {
					myOutput = new BufferedOutputStream(new FileOutputStream(mySubDir, false));
					myOutput.write(fcontent.getBytes());
					myOutput.flush();
					myOutput.close();
				} catch (FileNotFoundException e) {
				    e.printStackTrace();
				} catch (IOException e) {
				    e.printStackTrace();
				}
		
	 }
	 
	 public static void saveFileOutgoing(File ffile, String fname, String fcontent,String subdir,String childdir)
	 {
		 	if(Api.getNameClient()!=null){
		 		File parentPath = new File(ffile,Api.getNameClient().replace(" ",""));
			 	if(!parentPath.isDirectory())
			 		parentPath.mkdir();
				 File mySubDir = new File(parentPath, subdir);
					if (!mySubDir.isDirectory())
						mySubDir.mkdir();
					OutputStream myOutput;
				//	Log.i("JUMLAH-FILE",String.valueOf( mySubDir.listFiles().length));
					try {
						File mysubdir2 = new File(mySubDir.getAbsolutePath(),childdir);
						File myfile = new File(mysubdir2.getAbsolutePath(),fname);
						if(myfile.isFile())
							myfile.delete();
						myOutput = new BufferedOutputStream(new FileOutputStream(myfile, false));
						myOutput.write(fcontent.getBytes());
						myOutput.flush();
						myOutput.close();
						
					} catch (FileNotFoundException e) {
					    e.printStackTrace();
					} catch (IOException e) {
					    e.printStackTrace();
					}
			//}
		 	}
		 	
		
	 }
	
	 public static void saveImageFileOutgoing(File ffile, String fname, ImageView fcontent,String subdir,String childdir)
	 {
		if(Api.getNameClient()!=null){
			File parentPath = new File(ffile,Api.getNameClient().replace(" ",""));
		 	if(!parentPath.isDirectory())
		 		parentPath.mkdir();
			 File mySubDir = new File(parentPath, subdir);
				if (!mySubDir.isDirectory())
					mySubDir.mkdir();
				OutputStream myOutput;
			//	Log.i("JUMLAH-FILE",String.valueOf( mySubDir.listFiles().length));
				try {
					File mysubdir2 = new File(mySubDir.getAbsolutePath(),childdir);
					File myfile = new File(mysubdir2.getAbsolutePath(),fname);
					if(myfile.isFile())
						myfile.delete();
					fcontent.buildDrawingCache();
					Bitmap bm = fcontent.getDrawingCache();
					myOutput = new FileOutputStream(myfile);
					bm.compress(Bitmap.CompressFormat.JPEG, 100, myOutput);
					myOutput.flush();
					myOutput.close();
				} catch (FileNotFoundException e) {
				    e.printStackTrace();
				} catch (IOException e) {
				    e.printStackTrace();
				}
		//}
		}
	 }
	 
	 public static void createFolderOutgoing(File ffile,String subdir)
	 {
	 	File parentPath = new File(ffile,Api.getNameClient().replace(" ",""));
	 	if(!parentPath.isDirectory())
	 		parentPath.mkdir();
	 	File mySubDir = new File(parentPath, subdir);
			if (!mySubDir.isDirectory())
				mySubDir.mkdir();
	
	 	if(subdir.contains("order")){
			File mySubDir2 = new File(mySubDir, "new");
				if(!mySubDir2.isDirectory())
					mySubDir2.mkdir();
				
			File mySubDir3 = new File(mySubDir, "temp");
				if(!mySubDir3.isDirectory())
					mySubDir3.mkdir();
			File mySubDir4 = new File(mySubDir, "detail");
				if(!mySubDir4.isDirectory())
					mySubDir4.mkdir();
			File mySubDir5 = new File(mySubDir, "paid");
				if(!mySubDir5.isDirectory())
					mySubDir5.mkdir();
			File mySubDir6 = new File(mySubDir, "book");
				if(!mySubDir6.isDirectory())
					mySubDir6.mkdir();
			File mySubDir7 = new File(mySubDir, "newqueue");
				if(!mySubDir7.isDirectory())
					mySubDir7.mkdir();
			File mySubDir8 = new File(mySubDir, "status");
				if(!mySubDir8.isDirectory())
					mySubDir8.mkdir();
			File mySubDir9 = new File(mySubDir, "cancelnew");
				if(!mySubDir9.isDirectory())
					mySubDir9.mkdir();
			File mySubDir10 = new File(mySubDir, "changetable");
				if(!mySubDir10.isDirectory())
					mySubDir10.mkdir();
			File mySubDir11 = new File(mySubDir, "merge");
				if(!mySubDir11.isDirectory())
					mySubDir11.mkdir();
			File mySubDir12 = new File(mySubDir, "split");
				if(!mySubDir12.isDirectory())
					mySubDir12.mkdir();
			File mySubDir13 = new File(mySubDir, "tablestatus");
				if(!mySubDir13.isDirectory())
					mySubDir13.mkdir();
				
	 	}
	 	else if(subdir.contains("table")){
	 		File mySubDir2 = new File(mySubDir, "data");
	 			if(!mySubDir2.exists()){
	 				mySubDir2.mkdir();
	 			}
	 		File mySubDir3 = new File(mySubDir, "image");
	 		if(!mySubDir3.exists()){
 				mySubDir3.mkdir();
 			}
	 	}
	 	else{
	 		File mySubDir2 = new File(mySubDir, "register");
	 			if(!mySubDir2.isDirectory())
	 				mySubDir2.mkdir();
	 	}
		
	 }
	 
	 public static String getPathFileImageTable(){
		 File subdir = new File(Environment.getExternalStorageDirectory(), "table");
		 File subdir2 = new File(subdir.getAbsoluteFile(), "images");
		 return subdir2.getAbsolutePath();
	 }
	 
	 public static int getCountFileOutgoing(File ffile,String subdir,String childdir)
	 {	
		int countfile = 0;
	 	File parentPath = new File(ffile,Api.getNameClient().replace(" ",""));
		File mySubDir = new File(parentPath, subdir);
		File mySubDir2 = new File(mySubDir, childdir);
		
		if(mySubDir.exists()){
			countfile = mySubDir2.listFiles().length;
		}
		
		return countfile;
	 }
	 
	 public static void deleteFileOutgoing(File ffile, String fname,String subdir,String childDir)
	 {
		
		 	File parentPath = new File(ffile,Api.getNameClient().replace(" ",""));
			File mySubDir = new File(parentPath, subdir);
			File mySubDir2 = new File(mySubDir, childDir);
			File myfile= new File(mySubDir2.getAbsolutePath(),fname);
			Log.i("FILE-CHECK", String.valueOf(myfile.isFile()));
				if(myfile.isFile())
					myfile.delete();	
		
	 }
	 
	 public static String getFileContent(File ffile, String fname)
	 {
		File mySubDir = new File(ffile, fname);
		InputStream fos = null;
		try {
			fos = new FileInputStream(mySubDir);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return "";
		}
		
		InputStreamReader inputStreamReader = new InputStreamReader(fos);
	    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
	    StringBuilder sb = new StringBuilder();
	    String line;
	    try {
			while ((line = bufferedReader.readLine()) != null) {
			    sb.append(line);
			}
		    bufferedReader.close();
		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}
	    return sb.toString();
	 }
	 
	 public static String getFileContentDir(String path)
	 {
		File mySubDir = new File(path);
		InputStream fos = null;
		try {
			fos = new FileInputStream(mySubDir);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return "";
		}
		
		InputStreamReader inputStreamReader = new InputStreamReader(fos);
	    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
	    StringBuilder sb = new StringBuilder();
	    String line;
	    try {
			while ((line = bufferedReader.readLine()) != null) {
			    sb.append(line);
			}
		    bufferedReader.close();
		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}
	    return sb.toString();
	 }
	 
	 public static String getFileContentOutgoing(File ffile, String fname,String subdir,String childDir)
	 {
		File parentPath = new File(ffile,Api.getNameClient().replace(" ",""));
			
		File mySubDir = new File(parentPath, subdir);
		File mySubDir2 = new File(mySubDir, childDir);
		File filejson = new File(mySubDir2, fname);
		InputStream fos = null;
		try {
			fos = new FileInputStream(filejson);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return "";
		}
		
		InputStreamReader inputStreamReader = new InputStreamReader(fos);
	    BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
	    StringBuilder sb = new StringBuilder();
	    String line;
	    try {
			while ((line = bufferedReader.readLine()) != null) {
			    sb.append(line);
			}
		    bufferedReader.close();
		} catch (IOException e) {
			e.printStackTrace();
			return "";
		}
	    return sb.toString();
	 }
	 
	 
	 
	 public static String getDirContentOutgoing(File ffile,String subdir,String childDir)
	 {
		File parentPath = new File(ffile,Api.getNameClient().replace(" ",""));
			
		File mySubDir = new File(parentPath, subdir);
		File mySubDir2 = new File(mySubDir, childDir);
		
	    return mySubDir2.getAbsolutePath();
	 }
	 
	 public static String getImageUrl(String image) {
        return IMAGE_URL + image;
    }
	 
	 public static void loadImage(String imagename, Context context, ImageView view) {
    	 Picasso.with(context)
        .load(getImageUrl(imagename))
        .into(view);
    }
	
	 public static boolean isTablet(Context context) {
	        boolean xlarge = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == 4);
	        boolean large = ((context.getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) == Configuration.SCREENLAYOUT_SIZE_LARGE);
	        return (xlarge || large);
	 }
	 
	 public static String toRupiahFormat(String nominal) {
		 double inominal = Double.valueOf(nominal);
		 String rupiah = nominal; 
		 if(inominal>999){
			 rupiah = String.valueOf(inominal/1000);
			 if(inominal>0){
				 rupiah += " K";
			 }
		 }
		 return rupiah;  
	}
	
	 public static String toIntData(String nominal) {
		 String rnominal = nominal.replace(" K","");
		 double inominal = Double.valueOf(String.valueOf(rnominal));
		 String rupiah = rnominal; 
		 rupiah = String.valueOf(inominal*1000);
		 return rupiah;  
	}
	
	 public static Intent dialogCameraAddTable(Activity ac,int CAMERA_REQUEST) {
		 Intent cameraIntent = new Intent(
		            android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
		  ac.startActivityForResult(cameraIntent, CAMERA_REQUEST);
		  
		 return cameraIntent;  
	}
	 
	public static String timeAgo(Long time){
		int SECOND_MILLIS = 1000;
		int MINUTE_MILLIS = 60 * SECOND_MILLIS;
		int HOUR_MILLIS = 60 * MINUTE_MILLIS;
		int DAY_MILLIS = 24 * HOUR_MILLIS;
		 if (time < 1000000000000L) {
		        // if timestamp given in seconds, convert to millis
		        time *= 1000;
		    }

		    long now = System.currentTimeMillis();
		    if (time > now || time <= 0) {
		        return null;
		    }
		    
		    final long diff = now - time;
		    if (diff < MINUTE_MILLIS) {
		        return "baru saja";
		    } else if (diff < 2 * MINUTE_MILLIS) {
		        return "1 menit yang lalu";
		    } else if (diff < 50 * MINUTE_MILLIS) {
		        return diff / MINUTE_MILLIS + " menit yang lalu";
		    } else if (diff < 90 * MINUTE_MILLIS) {
		        return "1 jam yang lalu";
		    } else if (diff < 24 * HOUR_MILLIS) {
		        return diff / HOUR_MILLIS + " jam yang lalu";
		    } else if (diff < 48 * HOUR_MILLIS) {
		        return "kemarin";
		    } else {
		        return diff / DAY_MILLIS + " hari yang lalu";
		    }
	} 
	 
	public  static ArrayList<HashMap<String, String>> getTabMenuChart(String sales_id){
		ArrayList<HashMap<String, String>> arraydata=new ArrayList<HashMap<String,String>>();
		HashMap<String, String>mapdata = new HashMap<String, String>();
		mapdata.put("title_tab", "Belum Confirm");
		arraydata.add(mapdata);
		mapdata = new HashMap<String, String>();
		mapdata.put("title_tab", "Confirm");
		arraydata.add(mapdata);
		return arraydata;
	}
	
	public  static ArrayList<HashMap<String, String>> getTabQueeTable(){
		ArrayList<HashMap<String, String>> arraydata=new ArrayList<HashMap<String,String>>();
		
		for(int i=0;i<2;i++){
			HashMap<String, String>mapdata = new HashMap<String, String>();
			String data;
			if(i==0){
				data = "Meja";
			}
			else{
				data = "No Antrian";
			}
			mapdata.put("title_tab", data);
			arraydata.add(mapdata);
		}
		return arraydata;
	}

	public static String getAlertNoConnection(){
		String alert = "Periksa Kembali Koneksi Internet Anda";
		return alert;
	}
	
	public static String getMACAddress(String interfaceName) {
	    try {
	        List<NetworkInterface> interfaces = Collections.list(NetworkInterface.getNetworkInterfaces());
	        for (NetworkInterface intf : interfaces) {
	            if (interfaceName != null) {
	                if (!intf.getName().equalsIgnoreCase(interfaceName)) continue;
	            }
	            byte[] mac = intf.getHardwareAddress();
	            if (mac==null) return "";
	            StringBuilder buf = new StringBuilder();
	            for (int idx=0; idx<mac.length; idx++)
	                buf.append(String.format("%02X:", mac[idx]));
	            if (buf.length()>0) buf.deleteCharAt(buf.length()-1);
	            return buf.toString();
	        }
	    } catch (Exception ex) { }
	    return "";
	}
	
	public static String getDeviceName() {
	    String manufacturer = Build.MANUFACTURER;
	    String model = Build.MODEL;
	    if (model.startsWith(manufacturer)) {
	        return capitalize(model);
	    }
	    return capitalize(manufacturer) + " " + model;
	}

	private static String capitalize(String str) {
	    if (TextUtils.isEmpty(str)) {
	        return str;
	    }
	    char[] arr = str.toCharArray();
	    boolean capitalizeNext = true;
	    String phrase = "";
	    for (char c : arr) {
	        if (capitalizeNext && Character.isLetter(c)) {
	            phrase += Character.toUpperCase(c);
	            capitalizeNext = false;
	            continue;
	        } else if (Character.isWhitespace(c)) {
	            capitalizeNext = true;
	        }
	        phrase += c;
	    }
	    return phrase;
	}

	public static int getWidthScreen(Context context){
		  int w  = context.getResources().getDisplayMetrics().widthPixels;
		  return w;
	}
	
	public static int getHeightScreen(Context context){
		  int w  = context.getResources().getDisplayMetrics().heightPixels;
		  return w;
	}
	
	public static String getTabPrint(String type,String type2){
		String msg = "";
		int i = 0;
		int lenghttype = type.length();
		int lenghttype2 = type2.length();
		
		i = 32-(lenghttype+lenghttype2);
		
		for(int a=0;a<i;a++){
			msg += " "; 
		}
		
		Log.i("I", String.valueOf(lenghttype));
		return msg;
	}
	
	
	public static AlertDialog dialogSettingApp(final Context context){
		AlertDialog.Builder dialog = new AlertDialog.Builder(context);
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView = li.inflate(R.layout.dialog_setting_app, null);
	    dialog.setTitle("Setting App");
	    dialog.setCancelable(true);
	    final RadioButton rd_cashier = (RadioButton) promptsView.findViewById(R.id.rd_cashier);
	    final RadioButton rd_order = (RadioButton) promptsView.findViewById(R.id.rd_order);
	    final RadioButton rd_cashier_primary = (RadioButton) promptsView.findViewById(R.id.rd_cashier_primary);
	    
	    final RadioGroup rd_group_setting = (RadioGroup)promptsView.findViewById(R.id.group_setting);
	    Button btn_confirm = (Button)promptsView.findViewById(R.id.btn_confirm_setting_app);
	    dialog.setView(promptsView);
	    final AlertDialog alertd = dialog.create();
	    
	    btn_confirm.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				int choice = rd_group_setting.getCheckedRadioButtonId();;
				
				if(choice == -1){
					Toast.makeText(context, "Anda Belum Menentukan Pilihan", Toast.LENGTH_LONG).show();
				}
				else{
					TableFragment.mHandlerConnection.postDelayed(TableFragment.getInstance().mUpdateDownTaskConnection, 5000);
					final String active;
					if(choice == R.id.rd_order){
						active = rd_order.getText().toString();
					}
					else if (choice == R.id.rd_cashier){
						active = rd_cashier.getText().toString();
					}
					else{
						active = rd_cashier_primary.getText().toString();
					}
					AlertDialog.Builder alert = new AlertDialog.Builder(context);
					alert.setTitle("Konfirmasi Setting App");
					alert.setMessage("Apakah Anda Yakin App Akan di Seting Sebagai "+active+" ?");
					alert.setCancelable(false);
					alert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
				            //New Order Action
							session = new SessionManager(context);
							Api.setTypeApp(active);
							
							if(active.toLowerCase().equals("cashier")||active.toLowerCase().equals("cashier utama")){
								if(Api.getCheckConnection().toUpperCase().equals("SUCCESS")){
									if(Api.statusStore().toLowerCase().equals("close")||Api.statusStore().toLowerCase().equals("null")){
										AlertDialog alertDialog = new AlertDialog.Builder(context).create();
										alertDialog.setTitle("");
										alertDialog.setMessage("Buka Toko");
										alertDialog.setCancelable(false);
										alertDialog.setButton(AlertDialog.BUTTON_NEUTRAL, "OK",
										    new DialogInterface.OnClickListener() {
										        public void onClick(DialogInterface dialog, int which) {
									        		if(Api.openStore()){
										        		dialog.dismiss();
										        	}
										        }
										    });
										alertDialog.show();
									}
								}
								else{
									dialog.dismiss();
								}
							}
							
							session.createTypeAppSession(Api.getTypeApp());
						}
					   });
					   alert.setNegativeButton("Tidak",
					     new DialogInterface.OnClickListener() {
					       public void onClick(DialogInterface dialog, int whichButton) {
					            //action booking
					              dialog.cancel();
					            }
					        });
					 alert.show();
					alertd.cancel();
				}
				
			}
		});
	    alertd.setCancelable(false);
	    alertd.show();
	    return alertd;
	}
	
	public static AlertDialog dialogSettingPrinter(final Context context){
		AlertDialog.Builder dialog = new AlertDialog.Builder(context);
		LayoutInflater li = LayoutInflater.from(context);
		ArrayList<BluetoothDevice> mDeviceList = new ArrayList<BluetoothDevice>();
		View promptsView = li.inflate(R.layout.dialog_setting_printerbt, null);
	    dialog.setTitle("Setting Printer Bluetooth");
	    dialog.setCancelable(true);
	    final Spinner spinnernamebluetooth = (Spinner)promptsView.findViewById(R.id.spinnernamebluetooth);
	    Button btn_connect = (Button)promptsView.findViewById(R.id.btnconnect);
	    dialog.setView(promptsView);
	    final AlertDialog alertd = dialog.create();
	    mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
	    Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
	    mDeviceList.addAll(pairedDevices);
	    ArrayAdapter<String> adapter = new ArrayAdapter<String>(context, R.layout.spiner_table,R.id.txtnotable, getArray(mDeviceList));
		spinnernamebluetooth.setAdapter(adapter);
		spinnernamebluetooth.setSelection(0);
	    
	    btn_connect.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				final String active = spinnernamebluetooth.getSelectedItem().toString();
				AlertDialog.Builder alert = new AlertDialog.Builder(context);
				alert.setTitle("Konfirmasi Setting Printer Bluetooth");
				alert.setMessage("Apakah Anda Yakin Printer Bluetooth Akan di Setting "+active+" ?");
				alert.setCancelable(false);
				alert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int whichButton) {
			            //New Order Action
						if(active!=null){
							session = new SessionManager(context);
							Api.setPrinterBluetooth(active);
							session.createTypeAppSession(Api.getTypeApp());
							dialog.cancel();
						}
						else{
							Toast.makeText(context, "Bluetooth Harus Di Setting Terlebih Dahulu", Toast.LENGTH_LONG).show();
						}
						
					}
				   });
				   alert.setNegativeButton("Tidak",
				     new DialogInterface.OnClickListener() {
				       public void onClick(DialogInterface dialog, int whichButton) {
				            //action booking
				              dialog.cancel();
				            }
				        });
				 alert.show();
				 alertd.cancel();
			}
		});
	    alertd.setCancelable(false);
	    alertd.show();
	    return alertd;
	}
	
	public static AlertDialog dialogComplain(final Context context){
		AlertDialog.Builder dialog = new AlertDialog.Builder(context);
		LayoutInflater li = LayoutInflater.from(context);
		
		View promptsView = li.inflate(R.layout.dialog_complain, null);
	    dialog.setTitle("Input Complain");
	    dialog.setCancelable(true);
	    final Spinner spinnercategorycomplain = (Spinner)promptsView.findViewById(R.id.spinercategorycomplain);
	    Button btn_connect = (Button)promptsView.findViewById(R.id.btncomplain);
	    final EditText txt_complain = (EditText)promptsView.findViewById(R.id.txtcomplain);
	    dialog.setView(promptsView);
	    final AlertDialog alertd = dialog.create();
	    List<String>listdata = new ArrayList<String>();
	    categoryData = new ArrayList<HashMap<String,String>>();
	    categoryData = Api.getCategoryComplain();
	    
	    for(int i =0;i<categoryData.size();i++){
	    	listdata.add(categoryData.get(i).get("name"));
	    }
	    
	    ArrayAdapter<String>adaptecategory = new  ArrayAdapter<String>(context, R.layout.spiner_table,R.id.txtnotable,listdata);
		spinnercategorycomplain.setAdapter(adaptecategory);
		spinnercategorycomplain.setSelection(0);
	    
	    btn_connect.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				int position = spinnercategorycomplain.getSelectedItemPosition();
				String category_id = categoryData.get(position).get("category_id");
				String category_note = txt_complain.getText().toString();
				String resultSend = Api.complainSend(category_id, category_note);
				if(resultSend.toUpperCase().equals("SUCCESS")){
					alertd.dismiss();
				}
			}
		});
	    alertd.setCancelable(false);
	    alertd.show();
	    return alertd;
	}
	
	public static AlertDialog dialogSettingDisplayDisableMenu(final Context context){
		AlertDialog.Builder alert = new AlertDialog.Builder(context);
		alert.setTitle("Konfirmasi Display Disable Menu");
		alert.setMessage("Apakah Anda Yakin Akan Menampilkan Menu Yang di Disable ?");
		
		alert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
	            //New Order Action
				session = new SessionManager(context);
				Api.setDisplayDisableMenu(true);
				session.createDisplayDisableMenu(Api.getDisplayDisableMenu());
				dialog.cancel();
			}
		   });
		   alert.setNegativeButton("Tidak",
		     new DialogInterface.OnClickListener() {
		       public void onClick(DialogInterface dialog, int whichButton) {
		            //action booking
		    	   	session = new SessionManager(context);
					Api.setDisplayDisableMenu(false);
					session.createDisplayDisableMenu(Api.getDisplayDisableMenu());
					dialog.cancel();
		            }
		        });
		AlertDialog alertd = alert.create();
	    alertd.setCancelable(false);
	    alertd.show();
	    return alertd;
	}
	
	public static AlertDialog dialogConfirmCloseStore(final Context context){
		AlertDialog.Builder alert = new AlertDialog.Builder(context);
		alert.setTitle("Konfirmasi Tutup Toko");
		alert.setMessage("Apakah Anda Yakin Akan Menutup Toko ?");
		alert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
	            //New Order Action
				taskcloseStore = new closeStore(alertdclosetore, context);
				taskcloseStore.execute();
			}
		   });
		   alert.setNegativeButton("Tidak",
		     new DialogInterface.OnClickListener() {
		       public void onClick(DialogInterface dialog, int whichButton) {
		            //action booking
		              dialog.cancel();
		            }
		        });
		alertdclosetore = alert.create();
	    alertdclosetore.setCancelable(false);
	    alertdclosetore.show();	
	    return alertdclosetore;
	}
	
	public static AlertDialog.Builder dialogConfirmActivePrinter(final Context context){
		AlertDialog.Builder alert = new AlertDialog.Builder(context);
		alert.setTitle("Konfirmasi Aktifkan Fitur Printer");
		alert.setMessage("Apakah Anda Ingin Mengaktifkan Fitur Printer Bluetooth Pada APP ?");
		alert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
	            //New Order Action
				session = new SessionManager(context);
				Api.setActivePrinter(true);
				Api.setActivePrinterCheck("true");
				session.createActivePrinter(Api.getActivePrinter());
				session.createActivePrinterCheck(Api.getActivePrinterCheck());
				
				if(Api.getPrinterBluetooth() == null){
					dialogSettingPrinter(context);
				}
				dialog.cancel();
			}
		   });
		   alert.setNegativeButton("Tidak",
		     new DialogInterface.OnClickListener() {
		       public void onClick(DialogInterface dialog, int whichButton) {
		            //action booking
		    	   session = new SessionManager(context);
					Api.setActivePrinter(false);
					Api.setActivePrinterCheck("false");
					session.createActivePrinter(Api.getActivePrinter());
					session.createActivePrinterCheck(Api.getActivePrinterCheck());
				    session.createActivePrinter(Api.getActivePrinter());
		            dialog.cancel();
		            }
		        });
		alert.setCancelable(false);
		alert.show();
	    return alert;
	}
	
	public static AlertDialog.Builder dialogConfirmLogout(final Context context){
		AlertDialog.Builder alert = new AlertDialog.Builder(context);
		alert.setTitle("Konfirmasi Logout APP");
		alert.setMessage("Apakah Anda Yakin Ingin Logout ?");
		alert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
			public void onClick(final DialogInterface dialog, int whichButton) {
	            //New Order Action
				LayoutInflater li = LayoutInflater.from(context);
				View promptsView = li.inflate(R.layout.layoutnote, null);
		     	final TextView txtnote = (TextView)promptsView.findViewById(R.id.notes);
		     	Button btn_cancel = (Button)promptsView.findViewById(R.id.bCancel);
		     	
		     	AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
				alertDialogBuilder.setView(promptsView);
				final AlertDialog alertd = alertDialogBuilder.create();
		     	btn_cancel.setOnClickListener(new View.OnClickListener() {
					
					@Override
					public void onClick(View v) {
						// TODO Auto-generated method stub
						
						String logout = Api.logout(txtnote.getText().toString());
						if(logout.toUpperCase().equals("SUCCESS")){
							SessionManager session = new SessionManager(context);
							session.logoutUser();
							dialog.cancel();
						}
						
					}
				});
				alertd.show();
			}
		   });
		   alert.setNegativeButton("Tidak",
		     new DialogInterface.OnClickListener() {
		       public void onClick(DialogInterface dialog, int whichButton) {
		            //action booking
		            dialog.cancel();
		            }
		        });
		alert.setCancelable(false);
		alert.show();
	    return alert;
	}
	
	public static AlertDialog.Builder dialogConfirmExit(final Context context){
		AlertDialog.Builder alert = new AlertDialog.Builder(context);
		alert.setTitle("Konfirmasi Keluar APP");
		alert.setMessage("Apakah Anda Yakin Ingin Keluar dari APP ?");
		alert.setPositiveButton("Ya", new DialogInterface.OnClickListener() {
			public void onClick(final DialogInterface dialog, int whichButton) {
	            //New Order Action
				System.exit(0);
			}
		   });
		   alert.setNegativeButton("Tidak",
		     new DialogInterface.OnClickListener() {
		       public void onClick(DialogInterface dialog, int whichButton) {
		            //action booking
		            dialog.cancel();
		            }
		        });
		alert.setCancelable(false);
		alert.show();
	    return alert;
	}
	
	public static AlertDialog dialogSetNumCopyBill(final Context context){
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView = li.inflate(R.layout.dialog_set_copy_bill, null);
     	final TextView txt_qty = (TextView)promptsView.findViewById(R.id.txt_qty);
     	Button btn_set_qty = (Button)promptsView.findViewById(R.id.btn_set_qty);
     	
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		alertDialogBuilder.setTitle("Copy Bill Pembayaran");
		alertDialogBuilder.setView(promptsView);
		final AlertDialog alertd = alertDialogBuilder.create();
			
     	btn_set_qty.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				session = new SessionManager(context);
				Api.setNumCopyBill(txt_qty.getText().toString());
				session.createNumCopyBill(Api.getNumCopyBill());
				alertd.cancel();
			}
		});
     	alertd.show();
     	return alertd;
	}
	
	private static String[] getArray(ArrayList<BluetoothDevice> data) {
		String[] list = new String[0];
		if (data == null) return list;
		int size	= data.size();
		list		= new String[size];
		for (int i = 0; i < size; i++) {
			list[i] = data.get(i).getName();
		}
		return list;	
	}
	
	public static Boolean checkCashierApp(Context context){
		if(Api.getTypeApp() == null){
			return false;
		}
		else{
			if(Api.getTypeApp().toUpperCase().equals("CASHIER")||Api.getTypeApp().toUpperCase().equals("CASHIER UTAMA")){
				return true;
			}
		}
		
		return false;
	}
	
	public static Boolean checkCashierPrimary(Context context){
		
		if(Api.getTypeApp() == null){
			return false;
		}
		else{
			if(Api.getTypeApp().toUpperCase().equals("CASHIER UTAMA")){
				return true;
			}
		}
		
		return false;
	}
	
	public static String formatDecimal(double number) {
	  DecimalFormat dcf = new DecimalFormat("#,###,###,###.###");
	  return String.valueOf(dcf.format(number));
    }
	
	public static void notifSound(Context context){
		if(Api.getTypeApp()!=null){
			if(Api.getTypeApp().toLowerCase().equals("cashier")){
				AssetFileDescriptor afd;
				try {
					afd = context.getAssets().openFd("notif.mp3");
					MediaPlayer player = new MediaPlayer();
					player.setDataSource(afd.getFileDescriptor(),afd.getStartOffset(),afd.getLength());
					player.prepare();
					player.start();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	public static String generateId(String controller_name){
		long mstime = System.currentTimeMillis();
		String id = md5(controller_name+String.valueOf(mstime));
		return id;
	}
	
	public static boolean CheckJsonArray(JSONArray jsonArray, String id){
		return jsonArray.toString().contains("\"id\":\""+id+"\"");
	}
	
	public static String checkFileOrder(File ffile,String subdir,String[] filename,String childdir){
		File parentPath = new File(ffile,Api.getNameClient().replace(" ",""));
	 	if(!parentPath.isDirectory())
	 		parentPath.mkdir();
		File mySubDir = new File(parentPath, subdir);
		File mysubdir2 = new File(mySubDir.getAbsolutePath(),childdir);
		File [] lisFiles = mysubdir2.listFiles();
		String salesid = filename[0];
		String id = filename[1];
		
		if(lisFiles.length>0){
			for(int i = 0;i<lisFiles.length;i++){
				String file = lisFiles[i].getName().replace(".json", "");
				String strsplit = new String(file);
				String[] data = strsplit.split("_");
				String salesidfile = data[0];
				String idfile = data[2];
				if(salesidfile.equals(salesid)&&idfile.equals(id)){
					return lisFiles[i].getAbsoluteFile().toString();
				}
			}
		}
		
		return "failed";
	}
	
	public static Boolean checkFile(File ffile,String subdir,String filename,String childdir){
		File parentPath = new File(ffile,Api.getNameClient().replace(" ",""));
	 	if(!parentPath.isDirectory())
	 		parentPath.mkdir();
		File mySubDir = new File(parentPath, subdir);
		File mysubdir2 = new File(mySubDir.getAbsolutePath(),childdir);
		File myFile = new File(mysubdir2,filename);
		return myFile.exists();
	}
	
	public static ArrayList<HashMap<String, String>> checkListFileOrder(File ffile,String subdir,String childdir,String filename){
		File parentPath = new File(ffile,Api.getNameClient().replace(" ",""));
	 	ArrayList<HashMap<String, String>>tempData = new ArrayList<HashMap<String,String>>();
		if(!parentPath.isDirectory())
	 		parentPath.mkdir();
		File mySubDir = new File(parentPath, subdir);
		File mysubdir2 = new File(mySubDir.getAbsolutePath(),childdir);
		File [] lisFiles = mysubdir2.listFiles();
		if(lisFiles.length>0){
			for(int i = 0;i<lisFiles.length;i++){
				String file = lisFiles[i].getName().replace(".json", "");
				String strsplit = new String(file);
				String[] data = strsplit.split("_");
				String salesidfile = data[0];
				if(salesidfile.equals(filename)){
					HashMap<String, String>map= new HashMap<String, String>();
					map.put("pathfile", String.valueOf(lisFiles[i].getAbsoluteFile()));
					tempData.add(map);
				}
			}
		}
		
		return tempData;
	}
	
	public static AlertDialog dialogAddTable(final Context context,final Bitmap photo){
		LayoutInflater li = LayoutInflater.from(context);
		View promptsView = li.inflate(R.layout.layout_add_table, null);
		final ImageView imgview = (ImageView)promptsView.findViewById(R.id.image_table);
		final TextView txt_notable = (TextView)promptsView.findViewById(R.id.txtnotable);
     	final TextView txt_capacity = (TextView)promptsView.findViewById(R.id.txtcapacitytable);
     	Button btnaddtable = (Button)promptsView.findViewById(R.id.btnaddtable);
     	imgview.setImageBitmap(photo);
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
		alertDialogBuilder.setTitle("Tambah Table");
		alertDialogBuilder.setView(promptsView);
		final AlertDialog alertd = alertDialogBuilder.create();
		

     	btnaddtable.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				String table_id = Helper.generateId("table_controller");
				String imgname = table_id+".jpg";
				Db.saveTable(table_id, txt_notable.getText().toString(), txt_capacity.getText().toString(), imgname, imgview);
				alertd.dismiss();
			}
		});
     	
     	alertd.show();
     	return alertd;
	}
	
	public static byte[] getByteArrayFile(String path){
		File file = new File(path);
		int size = (int) file.length();
		byte[] bytes = new byte[size];
		try {
		    BufferedInputStream buf = new BufferedInputStream(new FileInputStream(file));
		    buf.read(bytes, 0, bytes.length);
		    buf.close();
		} catch (FileNotFoundException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		} catch (IOException e) {
		    // TODO Auto-generated catch block
		    e.printStackTrace();
		}
		return bytes;
	}
	
	
}
